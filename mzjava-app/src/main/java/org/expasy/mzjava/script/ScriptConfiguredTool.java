/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * https://sourceforge.net/projects/javaprotlib/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.script;

/**
 * A tool interface that allows the tool to be configured using a scripting
 * language such as javascript, groovy or python.
 * <p>Here is how a typical <code>Tool</code> is implemented:</p>
 * <p>First an java interface for the configuration needs to be defined. Here the configuration needs to
 * provide a tolerance and a peptide.</p>
 * <p><blockquote><pre>
 *     public interface SearchConfiguration {
 *
 *         Tolerance getTolerance();
 *
 *         Peptide getPeptide();
 *
 *     }
 * </pre></blockquote></p>
 * <p>The application is implemented as follows<p/>
 * <p><blockquote><pre>
 *
 *     public class MyApp extends ScriptConfiguredTool&lt;SearchConfiguration&gt; {
 *
 *         &#64;Override
 *         public Class&lt;SearchConfiguration&gt; getConfigurationClass() {
 *
 *             return SearchConfiguration.class;
 *         }
 *
 *         &#64;Override
 *         public int run(SearchConfiguration configuration) {
 *
 *             Tolerance tolerance = configuration.getTolerance();
 *             Peptide peptide = configuration.getPeptide();
 *
 *             //Run the search using the tolerance and peptide
 *             return 0;
 *         }
 *
 *         public static void main(String[] args) throws Exception {
 *
 *             File file = new File("/path/to/Config.js");
 *
 *             ScriptConfiguredToolRunner.run(file, new MyApp());
 *         }
 *     }
 * </pre></blockquote></p>
 * <p>The javascript script that provides configured objects is:</p>
 * <p><blockquote><pre>
 * function getTolerance(){
 *
 *     return new org.expasy.mzjava.core.ms.AbsoluteTolerance(0.2);
 * }
 *
 * function getPeptide(){
 *
 *     return org.expasy.mzjava.proteomics.mol.Peptide.parse('CERVILAS');
 * }
 * </pre></blockquote></p>
 *<p>and the same script in groovy:</p>
 * <p><blockquote><pre>
 * import org.expasy.mzjava.core.ms.AbsoluteTolerance
 * import org.expasy.mzjava.proteomics.mol.Peptide
 *
 * def getTolerance(){
 *
 *     new AbsoluteTolerance(0.2)
 * }
 *
 * def getPeptide(){
 *
 *     Peptide.parse("CERVILAS")
 * }
 * </pre></blockquote></p>
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public interface ScriptConfiguredTool<C> {

    /**
     * Execute the tool with the given configuration
     *
     * @param configuration the configuration
     * @return exit code.
     */
    int run(C configuration) throws Exception;

    /**
     * Returns the interface that the configuration implements
     *
     * @return the interface that the configuration implements
     */
    Class<C> getConfigurationClass();
}
