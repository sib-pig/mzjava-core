package org.expasy.mzjava.script;

import com.google.common.base.Preconditions;
import org.apache.commons.io.FilenameUtils;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A utility to help run {@link ScriptConfiguredTool}s.
 *
 * <p><code>ScriptRunner</code> can be used to run classes implementing
 * <code>ScriptConfiguredRunner</code> interface. It works in conjunction with
 * jsr-223 to allow a script to be used to configure the ScriptConfiguredTool.
 * </p>
 * <p>By default java has a scripting engine for javascript. Groovy script can be
 * added by using the following maven dependency.
 * <pre>
 * &lt;dependency&gt;
 *     &lt;groupId&gt;org.codehaus.groovy&lt;/groupId&gt;
 *     &lt;artifactId&gt;groovy-jsr223&lt;/artifactId&gt;
 *     &lt;version&gt;2.3.7&lt;/version&gt;
 * &lt;/dependency&gt;
 * </pre>
 * </p>
 * <p>And python support can be added using the following maven dependency.
 * <pre>
 * &lt;dependency&gt;
 *     &lt;groupId&gt;org.python&lt;/groupId&gt;
 *     &lt;artifactId&gt;jython&lt;/artifactId&gt;
 *     &lt;version&gt;2.5.3&lt;/version&gt;
 * &lt;/dependency&gt;
 * </pre>
 * </p>
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 * @see ScriptConfiguredTool
 */
public class ScriptConfiguredToolRunner {

    private ScriptConfiguredToolRunner() {}

    /**
     * Runs the given <code>ScriptedTool</code> by calling {@link ScriptConfiguredTool#run(C)}, after
     * creating the configuration from the script in the scriptFile. The <code>args</code> are set as a script variable called args.
     *
     * @param args the arguments. the first argument has to contains the path to the script file. The remaining arguments are
     *             passed to the scrip as the args scripting variable
     * @param tool <code>ScriptTool</code> to run
     * @return exit code of the {@link ScriptConfiguredTool#run(Object)} method.
     *
     * @throws FileNotFoundException if the script file cannot be found
     * @throws ScriptException if the script cannot be compiled
     */
    public static <C> int run(String[] args, ScriptConfiguredTool<C> tool) throws Exception {

        Preconditions.checkArgument(args.length > 1, "No script file was provided. The first element in the args array has to be the script file.");

        File file = new File(args[0]);
        Preconditions.checkState(file.exists(), "File " + file + " does not exist");

        String[] truncatedArgs = new String[args.length - 1];
        if (args.length > 1) {

            System.arraycopy(args, 1, truncatedArgs, 0, truncatedArgs.length);
        }

        return run(file, tool, truncatedArgs);
    }

    /**
     * Runs the given <code>ScriptedTool</code> by calling {@link ScriptConfiguredTool#run(C)}, after
     * creating the configuration from the script in the scriptFile.
     *
     * @param scriptFile the file containing the script
     * @param tool <code>ScriptTool</code> to run
     * @return exit code of the {@link ScriptConfiguredTool#run(Object)} method.
     *
     * @throws FileNotFoundException if the script file cannot be found
     * @throws ScriptException if the script cannot be compiled
     */
    public static <C> int run(File scriptFile, ScriptConfiguredTool<C> tool) throws Exception {

        return run(scriptFile, tool, new String[0]);
    }

    /**
     * Runs the given <code>ScriptedTool</code> by calling {@link ScriptConfiguredTool#run(C)}, after
     * creating the configuration from the script in the scriptFile. The <code>args</code> are set as a script variable called args.
     *
     * @param scriptFile the file containing the script
     * @param tool <code>ScriptTool</code> to run
     * @param args the arguments to set as the args script variable
     * @return exit code of the {@link ScriptConfiguredTool#run(Object)} method.
     *
     * @throws FileNotFoundException if the script file cannot be found
     * @throws ScriptException if the script cannot be compiled
     */
    public static <C> int run(File scriptFile, ScriptConfiguredTool<C> tool, String[] args) throws Exception {

        checkNotNull(scriptFile);

        ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByExtension(FilenameUtils.getExtension(scriptFile.getName()));
        engine.put("args", args);

        engine.eval(new FileReader(scriptFile));
        C configuration = ((Invocable) engine).getInterface(tool.getConfigurationClass());
        checkNotNull(configuration, "The scrip cannot be converted to interface " + tool.getConfigurationClass());

        return tool.run(configuration);
    }
}
