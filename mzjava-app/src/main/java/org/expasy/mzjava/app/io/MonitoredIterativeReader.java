package org.expasy.mzjava.app.io;

import org.expasy.mzjava.core.io.IterativeReader;
import org.expasy.mzjava.app.pb.ConsoleProgressBar;

import java.io.IOException;
import java.io.PrintStream;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * MonitoredIterativeReader monitors the progression of IterativeReader reads.
 *
 * @author fnikitin
 * Date: 4/19/13
 */
public class MonitoredIterativeReader<T> implements IterativeReader<T> {

    private final IterativeReader<T> reader;
    private final ConsoleProgressBar pb;

    public MonitoredIterativeReader(IterativeReader<T> reader) {

        this(reader, null);
    }

    public MonitoredIterativeReader(IterativeReader<T> reader, ConsoleProgressBar.View view) {

        checkNotNull(reader);

        this.reader = reader;

        this.pb = ConsoleProgressBar.indeterminate(view);

        pb.start();
    }

    public void setProgressBarPrintStream(PrintStream ps) {

        pb.setPrintStream(ps);
    }

    @Override
    public boolean hasNext() {

        return reader.hasNext();
    }

    @Override
    public T next() throws IOException {

        pb.incrementValue();

        return reader.next();
    }

    @Override
    public void close() throws IOException {

        pb.stop();

        reader.close();
    }

    public int getValue() {

        return pb.getValue();
    }
}
