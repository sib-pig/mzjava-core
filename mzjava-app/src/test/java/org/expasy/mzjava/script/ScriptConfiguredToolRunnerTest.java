package org.expasy.mzjava.script;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class ScriptConfiguredToolRunnerTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void testRun() throws Exception {

        File scriptFile = writeScript("function run() { println('run called'); }");

        //noinspection unchecked
        ScriptConfiguredTool<Runnable> tool = mock(ScriptConfiguredTool.class);
        when(tool.getConfigurationClass()).thenReturn(Runnable.class);
        ScriptConfiguredToolRunner.run(scriptFile, tool);

        verify(tool).run(Mockito.notNull(Runnable.class));
    }

    @Test
    public void testRun1() throws Exception {

        File scriptFile = writeScript("function getArgs() { return args; }");
        MockScriptConfiguredTool tool = new MockScriptConfiguredTool(new String[]{"first", "second"});

        ScriptConfiguredToolRunner.run(scriptFile, tool, new String[]{"first", "second"});

        Assert.assertEquals(1, tool.runCalled);
    }

    @Test
    public void testRun2() throws Exception {

        File scriptFile = writeScript("function getArgs() { return args; }");

        MockScriptConfiguredTool tool = new MockScriptConfiguredTool(new String[]{"first", "second"});

        ScriptConfiguredToolRunner.run(new String[]{scriptFile.getAbsolutePath(), "first", "second"}, tool);

        Assert.assertEquals(1, tool.runCalled);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRunArgsNoArgs() throws Exception {

        File scriptFile = writeScript("function getArgs() { return args; }");
        MockScriptConfiguredTool tool = new MockScriptConfiguredTool(new String[0]);

        ScriptConfiguredToolRunner.run(new String[]{scriptFile.getAbsolutePath()}, tool);

        Assert.assertEquals(1, tool.runCalled);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRunNoScriptFile() throws Exception {

        //noinspection unchecked
        ScriptConfiguredTool<Runnable> tool = mock(ScriptConfiguredTool.class);
        ScriptConfiguredToolRunner.run(new String[0], tool);
    }


    private File writeScript(String script) throws IOException {

        File scriptFile = temporaryFolder.newFile("script.js");
        BufferedWriter writer = new BufferedWriter(new FileWriter(scriptFile));
        writer.write(script);
        writer.close();
        return scriptFile;
    }

    public static interface Config {


        public String[] getArgs();
    }

    private static class MockScriptConfiguredTool implements ScriptConfiguredTool<Config> {

        private final String[] expected;

        private int runCalled = 0;

        public MockScriptConfiguredTool(String[] expected) {

            this.expected = expected;
        }

        @Override
        public int run(Config configuration) {

            runCalled += 1;
            Assert.assertArrayEquals(expected, configuration.getArgs());
            return 0;
        }

        @Override
        public Class<Config> getConfigurationClass() {

            return Config.class;
        }
    }
}