package org.expasy.mzjava.app.pb;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


public class ConsoleProgressBarTest {

	ConsoleProgressBar progressBar;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

	@Before
	public void setUp() throws Exception {

		progressBar = ConsoleProgressBar.newInstance(0, 10);
        progressBar.setPrintStream(new PrintStream(outContent));
    }

	@Test
	public void testDeterminateBar() {

		progressBar.setLeftMarginLength(5);
		progressBar.setBarLength(10);

		for (int i = 0; i <= 10; i++) {

		    progressBar.setValue(i);
		}

        Assert.assertEquals(
                "\r  0/10 [          ]" +
                "\r  1/10 [=         ]" +
                "\r  2/10 [==        ]" +
                "\r  3/10 [===       ]" +
                "\r  4/10 [====      ]" +
                "\r  5/10 [=====     ]" +
                "\r  6/10 [======    ]" +
                "\r  7/10 [=======   ]" +
                "\r  8/10 [========  ]" +
                "\r  9/10 [========= ]" +
                "\r 10/10 [==========] Done"+
                System.getProperty("line.separator"), outContent.toString());
	}
	
	@Test
	public void testIndeterminateBar() {

		progressBar.setIndeterminate(true);
		progressBar.setLeftMarginLength(2);
		progressBar.setBarLength(10);
		
		for (int i = 0; i <= 10; i++) {
			progressBar.setValue(i);
		}
		progressBar.stop();

        Assert.assertEquals(
                "\r  0 [=====     ]" +
                "\r  1 [=====     ]" +
                "\r  2 [ =====    ]" +
                "\r  3 [ =====    ]" +
                "\r  4 [  =====   ]" +
                "\r  5 [  =====   ]" +
                "\r  6 [   =====  ]" +
                "\r  7 [   =====  ]" +
                "\r  8 [    ===== ]" +
                "\r  9 [    ===== ]" +
                "\r 10 [     =====]" +
                "\r 10 [==========] Done"+
                System.getProperty("line.separator"), outContent.toString());
	}
	
	@Test
	public void testDeterminateBarWithTaskInfo() {

        progressBar.setTaskName("counting");
		progressBar.setLeftMarginLength(5);
		progressBar.setBarLength(10);
		for (int i = 0; i <= 10; i++) {
			progressBar.setValue(i);
		}

        Assert.assertEquals(
                "\rcounting:  0/10 [          ]" +
                "\rcounting:  1/10 [=         ]" +
                "\rcounting:  2/10 [==        ]" +
                "\rcounting:  3/10 [===       ]" +
                "\rcounting:  4/10 [====      ]" +
                "\rcounting:  5/10 [=====     ]" +
                "\rcounting:  6/10 [======    ]" +
                "\rcounting:  7/10 [=======   ]" +
                "\rcounting:  8/10 [========  ]" +
                "\rcounting:  9/10 [========= ]" +
                "\rcounting: 10/10 [==========] Done"+
                System.getProperty("line.separator"), outContent.toString());
	}
	
	@Test
	public void testDeterminateBarWithTaskInfoIncomplete() {

		progressBar.setTaskName("counting");
		progressBar.setLeftMarginLength(5);
		progressBar.setBarLength(10);
		for (int i = 0; i < 6; i++) {
			progressBar.setValue(i);
		}
		progressBar.stop();

        Assert.assertEquals(
                "\rcounting:  0/10 [          ]" +
                "\rcounting:  1/10 [=         ]" +
                "\rcounting:  2/10 [==        ]" +
                "\rcounting:  3/10 [===       ]" +
                "\rcounting:  4/10 [====      ]" +
                "\rcounting:  5/10 [=====     ] Incomplete"+
                System.getProperty("line.separator"), outContent.toString());
	}
	
	@Test
	public void test2SuccessiveDeterminateBars() {

		progressBar.setRefreshBarPeriod(1);
		progressBar.setBarLength(10);
		progressBar.setLeftMarginLength(5);
		for (int i = 0; i <= 10; i++) {
			progressBar.setValue(i);
		}
		
		for (int i = 0; i <= 10; i++) {
			progressBar.setValue(i);
		}

        Assert.assertEquals(
                "\r  0/10 [          ]" +
                "\r  1/10 [=         ]" +
                "\r  2/10 [==        ]" +
                "\r  3/10 [===       ]" +
                "\r  4/10 [====      ]" +
                "\r  5/10 [=====     ]" +
                "\r  6/10 [======    ]" +
                "\r  7/10 [=======   ]" +
                "\r  8/10 [========  ]" +
                "\r  9/10 [========= ]" +
                "\r 10/10 [==========] Done"+
                System.getProperty("line.separator") +
                "\r  0/10 [          ]" +
                "\r  1/10 [=         ]" +
                "\r  2/10 [==        ]" +
                "\r  3/10 [===       ]" +
                "\r  4/10 [====      ]" +
                "\r  5/10 [=====     ]" +
                "\r  6/10 [======    ]" +
                "\r  7/10 [=======   ]" +
                "\r  8/10 [========  ]" +
                "\r  9/10 [========= ]" +
                "\r 10/10 [==========] Done"+
                System.getProperty("line.separator"), outContent.toString());
	}
	
	@Test
	public void test2SuccessiveIndeterminateBars() {

        progressBar.setIndeterminate(true);
        progressBar.setLeftMarginLength(2);
        progressBar.setBarLength(10);

        for (int i = 0; i <= 10; i++) {
            progressBar.setValue(i);
        }
        progressBar.stop();

        // you have to reinit completed progressbar in indeterminate mode
		progressBar.start();
		for (int i = 0; i <= 10; i++) {
			progressBar.setValue(i);
		}

		progressBar.stop();

        Assert.assertEquals(
                "\r  0 [=====     ]" +
                "\r  1 [=====     ]" +
                "\r  2 [ =====    ]" +
                "\r  3 [ =====    ]" +
                "\r  4 [  =====   ]" +
                "\r  5 [  =====   ]" +
                "\r  6 [   =====  ]" +
                "\r  7 [   =====  ]" +
                "\r  8 [    ===== ]" +
                "\r  9 [    ===== ]" +
                "\r 10 [     =====]" +
                "\r 10 [==========] Done"+
                System.getProperty("line.separator")+
                "\r  0 [=====     ]" +
                "\r  1 [=====     ]" +
                "\r  2 [ =====    ]" +
                "\r  3 [ =====    ]" +
                "\r  4 [  =====   ]" +
                "\r  5 [  =====   ]" +
                "\r  6 [   =====  ]" +
                "\r  7 [   =====  ]" +
                "\r  8 [    ===== ]" +
                "\r  9 [    ===== ]" +
                "\r 10 [     =====]" +
                "\r 10 [==========] Done"+
                System.getProperty("line.separator"), outContent.toString());
    }
}
