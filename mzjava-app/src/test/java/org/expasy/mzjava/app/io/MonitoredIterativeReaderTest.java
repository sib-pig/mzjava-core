package org.expasy.mzjava.app.io;

import org.expasy.mzjava.core.io.IterativeReader;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 * @author fnikitin
 * Date: 3/3/14
 */
public class MonitoredIterativeReaderTest {

    PrintStream ps = new PrintStream(new ByteArrayOutputStream());

    @Test
    public void test() throws IOException {

        int maxIteration = 10;

        MonitoredIterativeReader reader = new MonitoredIterativeReader<Integer>(new MockIterativeReader(maxIteration));
        reader.setProgressBarPrintStream(ps);

        int i=1;
        while (reader.hasNext()) {

            reader.next();

            Assert.assertEquals(i, reader.getValue());

            i++;
        }

        Assert.assertEquals(maxIteration, reader.getValue());
        reader.close();

        Assert.assertEquals(0, reader.getValue());
    }

    private static class MockIterativeReader implements IterativeReader<Integer> {

        private int count;
        protected int maxIteration;

        MockIterativeReader(int maxIteration) {

            this.maxIteration = maxIteration;
        }

        @Override
        public boolean hasNext() {

            return count < maxIteration;
        }

        @Override
        public Integer next() throws IOException {

            return count++;
        }

        @Override
        public void close() throws IOException {

        }

    }
}
