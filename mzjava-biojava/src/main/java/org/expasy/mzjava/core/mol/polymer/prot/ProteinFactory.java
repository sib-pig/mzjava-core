package org.expasy.mzjava.core.mol.polymer.prot;

import org.biojava.nbio.core.sequence.ProteinSequence;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompoundSet;
import org.biojava.nbio.core.sequence.loader.UniprotProxySequenceReader;
import org.biojava.nbio.core.sequence.template.Sequence;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.Protein;

/**
 * This factory creates new instance of {@code Protein} by wrapping biojava {@code Sequence} or delegating to biojava objects.
 *
 * @author fnikitin
 *         Date: 9/24/12
 */
public class ProteinFactory {

    private ProteinFactory() {

    }

    public static Protein newUniprotProtein(String accessionId) {

        // now load the sequence from the UniProt website
        UniprotProxySequenceReader<AminoAcidCompound> uniprotSequence = null;
        Exception exception = null;

        //Retry if there is an exception with the fetch
        for (int i = 0; i < 10; i++) {

            try {

                uniprotSequence = new UniprotProxySequenceReader<>(accessionId, AminoAcidCompoundSet.getAminoAcidCompoundSet());
            } catch (Exception e) {

                exception = e;
                uniprotSequence = null;
            }

            if(uniprotSequence != null)
                break;
        }

        if(uniprotSequence == null)
            throw new IllegalStateException(exception);

        // and make a protein sequence out of it
        return newProtein(accessionId, new ProteinSequence(uniprotSequence));
    }

    public static Protein newProtein(ProteinSequence proteinSequence) {

        String id;

        if (proteinSequence.getAccession() == null)
            id = "null";
        else
            id = proteinSequence.getAccession().getID();

        return newProtein(id, proteinSequence);
    }

    public static Protein newProtein(String accessionId, Sequence<AminoAcidCompound> aaCompoundSequence) {

        final int length = aaCompoundSequence.getLength();
        AminoAcid[] aminoAcids = new AminoAcid[length];

        for (int i = 0; i < length; i++) {

            aminoAcids[i] = AminoAcid.valueOf(aaCompoundSequence.getCompoundAt(i + 1).getShortName());
        }

        return new Protein(accessionId, aminoAcids);
    }
}
