package org.expasy.mzjava.core.mol.polymer.prot;

import org.biojava.nbio.core.sequence.AccessionID;
import org.biojava.nbio.core.sequence.ProteinSequence;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompoundSet;
import org.biojava.nbio.core.sequence.io.FastaReader;
import org.biojava.nbio.core.sequence.io.GenericFastaHeaderParser;
import org.biojava.nbio.core.sequence.io.ProteinSequenceCreator;
import org.expasy.mzjava.core.io.IterativeReader;
import org.expasy.mzjava.proteomics.mol.Protein;

import java.io.*;
import java.util.Iterator;
import java.util.Map;

/**
 * A Protein reader based on biojava3 FastaReader object.
 *
 * @author fnikitin
 *         Date: 1/13/14
 */
public class BioJavaFastaProteinReader implements IterativeReader<Protein> {

    private final Iterator<ProteinSequence> proteinSequenceIterator;

    public BioJavaFastaProteinReader(File file) {

        InputStream is;
        try {

            is = new FileInputStream(file);
        } catch (FileNotFoundException e) {

            throw new IllegalStateException(e);
        }

        proteinSequenceIterator = readProteinSequenceMap(is);
    }

    public BioJavaFastaProteinReader(InputStream is) {

        proteinSequenceIterator = readProteinSequenceMap(is);
    }

    private Iterator<ProteinSequence> readProteinSequenceMap(InputStream is) {

        FastaReader<ProteinSequence, AminoAcidCompound> reader = new FastaReader<>(is,
                new GenericFastaHeaderParser<>(),
                new ProteinSequenceCreator(AminoAcidCompoundSet.getAminoAcidCompoundSet()));

        Iterator<ProteinSequence> it;
        try {

            Map<String, ProteinSequence> map = reader.process();
            it = map.values().iterator();
        } catch (Exception e) {

            throw new IllegalStateException(e);
        }

        return it;
    }

    @Override
    public boolean hasNext() {

        return proteinSequenceIterator.hasNext();
    }

    @Override
    public Protein next() {

        ProteinSequence proteinSequence = proteinSequenceIterator.next();

        AccessionID accession = proteinSequence.getAccession();

        String proteinId = "";
        if (accession != null && accession.getID() != null && accession.getID().length() > 0)
            proteinId = accession.getID();

        return ProteinFactory.newProtein(proteinId, proteinSequence);
    }

    @Override
    public void close() {

        // Do nothing because this reader reads all the data when created
    }
}
