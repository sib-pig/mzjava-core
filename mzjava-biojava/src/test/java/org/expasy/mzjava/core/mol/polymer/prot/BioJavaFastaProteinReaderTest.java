package org.expasy.mzjava.core.mol.polymer.prot;

import org.expasy.mzjava.core.io.IterativeReader;
import org.expasy.mzjava.proteomics.mol.Protein;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * @author fnikitin
 * Date: 1/13/14
 */
public class BioJavaFastaProteinReaderTest {

    @Test
    public void test() throws IOException {

        String fastaContent = ">sp|P61981|1433G_HUMAN 14-3-3 protein gamma OS=Homo sapiens GN=YWHAG PE=1 SV=2\n" +
                "MVDREQLVQKARLAEQAERYDDMAAAMKSVTELNEALSNEERNLLSVAYKNVVGARRSSW\n" +
                "RVISSIEQKTSADGNEKKMEMVRAYREKIEKELETVCRDVLNLLDNFLIKNCNETQHESK\n" +
                "VFYLKMKGDYYRYLAEVATGEKRVGVVESSEKSYSEAHEISKEHMQPTHPIRLGLALNYS\n" +
                "VFYYEIQNAPEQACHLAKTAFDDAIAELDTLNEDSYKDSTLIMQLLRDNLTLWTSDQZDD\n" +
                "EGGETNN\n";

        IterativeReader<Protein> proteinReader = new BioJavaFastaProteinReader(new ByteArrayInputStream(fastaContent.getBytes()));

        Assert.assertEquals(true, proteinReader.hasNext());
        Protein protein = proteinReader.next();
        Assert.assertEquals("MVDREQLVQKARLAEQAERYDDMAAAMKSVTELNEALSNEERNLLSVAYKNVVGARRSSW" +
                "RVISSIEQKTSADGNEKKMEMVRAYREKIEKELETVCRDVLNLLDNFLIKNCNETQHESK" +
                "VFYLKMKGDYYRYLAEVATGEKRVGVVESSEKSYSEAHEISKEHMQPTHPIRLGLALNYS" +
                "VFYYEIQNAPEQACHLAKTAFDDAIAELDTLNEDSYKDSTLIMQLLRDNLTLWTSDQZDD" +
                "EGGETNN", protein.toString());
        Assert.assertEquals("P61981", protein.getAccessionId());
    }
}
