package org.expasy.mzjava.core.mol.polymer.prot;

import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.ModifiedPeptideFactory;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.Protein;
import org.expasy.mzjava.core.mol.polymer.prot.ProteinFactory;
import org.expasy.mzjava.proteomics.mol.digest.CleavageSiteMatcher;
import org.expasy.mzjava.proteomics.mol.digest.Protease;
import org.expasy.mzjava.proteomics.mol.digest.ProteinDigester;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

// # $RECIPE$ $NUMBER$ - Digesting a Protein #
//
// ## $PROBLEM$ ##
// You want to digest a *Protein* in-silico.
//
// ## $SOLUTION$ ##
//
// The digestion is done by the object *ProteinDigester*.
//
public class DigestingProteinRecipe {

// Use *ProteinDigester.Builder* to build an immutable instance of *ProteinDigester*.

// ### Building ProteinDigester ###
// The Builder needs only on mandatory parameter: a *Protease* or a *CleavageSiteMatcher*.
//
// *Protease* enum provides a selection of 27 proteases based on descriptions of their cleavage sites [here](http://web.expasy.org/peptide_cutter/peptidecutter_enzymes.html).
    @Test
    public void createProtease() throws Exception {

        //<SNIP>
        ProteinDigester digester = new ProteinDigester.Builder(Protease.TRYPSIN).build();
        //</SNIP>

        Assert.assertNotNull(digester);
    }

// It's also possible to create custom *ProteinDigester* by defining a custom *CleavageSiteMatcher*
    @Test
    public void customProtease() throws Exception {

        //<SNIP>
        // the cleavage site expects the following pattern: Pn ... P4 P3 P2 P1 | P1' P2' P3' ... Pm'
        // where '|' represent the cleavage between amino-acid at position 1 and 1'
        CleavageSiteMatcher csm = new CleavageSiteMatcher("N|G");

        ProteinDigester hydroxylamine = new ProteinDigester.Builder(csm).build();
        //</SNIP>

        Assert.assertNotNull(hydroxylamine);
    }

// Building with more optional parameters: semi digestion, with/without missed-cleavages...
    @Test
    public void createCustomDigester() throws Exception {

        //<SNIP>
        ProteinDigester digester = new ProteinDigester.Builder(Protease.TRYPSIN)
            .missedCleavageMax(2).semi()
            .addFixedMod(AminoAcid.T, Modification.parseModification("H3PO4"))
            .build();
        //</SNIP>
    }

// ### Digestion ###
// Once instanciated, method digest(protein) is applied on a *Protein* object
// and returns products of digestion
    @Test
    public void digest() throws Exception {

        ProteinDigester digester = new ProteinDigester.Builder(Protease.TRYPSIN).build();

        //<SNIP>
        Protein prot = ProteinFactory.newUniprotProtein("Q13231");

        // standard mode (no missed-cleavage)
        List<Peptide> digests = digester.digest(prot);
        //</SNIP>

        Assert.assertEquals(44, digests.size());
    }

// or alternatively method digest(protein, container) is applied on a *Protein* object
// and digests will be stored into the given container
    @Test
    public void digest2() throws Exception {

        ProteinDigester digester = new ProteinDigester.Builder(Protease.TRYPSIN).build();
        Protein prot = ProteinFactory.newUniprotProtein("Q13231");

        //<SNIP>
        // provide a container to store digests
        List<Peptide> digests = new ArrayList<Peptide>();

        digester.digest(prot, digests);
        //</SNIP>

        Assert.assertEquals(44, digests.size());
    }

// With Variable modifications
    @Test
    public void digest3() throws Exception {

        ProteinDigester digester = new ProteinDigester.Builder(Protease.TRYPSIN).build();
        Protein prot = ProteinFactory.newUniprotProtein("Q13231");

        //<SNIP>
        // standard mode (no missed-cleavage)
        List<Peptide> digests = digester.digest(prot);

        Assert.assertEquals(44, digests.size());

        // this factory generate set of ProteinDigestProduct given variable modifications
        ModifiedPeptideFactory factory = new ModifiedPeptideFactory.Builder()
            .addTarget(EnumSet.of(AminoAcid.T, AminoAcid.Y, AminoAcid.S), ModAttachment.SIDE_CHAIN, Modification.parseModification("PO3H-1"))
            .build();

        List<Peptide> varModProteinDigests = factory.createVarModPeptides(digests.get(1));
        //</SNIP>

        Assert.assertEquals(3, varModProteinDigests.size());
    }

// ### Counting cleavage sites ###

// Counting the number of cleavage sites of any *SymbolSequence<AminoAcid>*
    @Test
    public void countCleavageSites() throws Exception {

        ProteinDigester digester = new ProteinDigester.Builder(Protease.TRYPSIN).build();

        //<SNIP>
        // count cleavage sites in a Protein
        int count = digester.countCleavageSites(ProteinFactory.newUniprotProtein("Q13231"));

        //<SKIP>
        Assert.assertEquals(43, count);
        //</SKIP>

        // count cleavage sites in a Peptide with Protease
        count = Protease.TRYPSIN.countCleavageSites(Peptide.parse("RESALYTNIKALASKR"));
        Assert.assertEquals(3, count);
        //</SNIP>
    }

// ## $DISCUSSION$ ##

// ## $RELATED$ ##
// See also $CreatingVarModPeptideRecipe$ to generate *Peptide* with variable modifications.
}