package org.expasy.mzjava.core.mol.polymer.prot;

import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.ProteinSequence;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.Protein;
import org.expasy.mzjava.core.mol.polymer.prot.ProteinFactory;
import org.junit.Assert;
import org.junit.Test;

// # $RECIPE$ $NUMBER$ - Creating Protein #
//
// ## $PROBLEM$ ##
// You want to make an instance of *Protein*.
//
// ## $SOLUTION$ ##
//
// There are mainly 2 ways to build a protein:
// Either build it from our *Protein* class or from *Sequence* [biojava](http://biojava.org/wiki/Main_Page)
// object thanks to our *ProteinFactory* class.
//
// The first solution is simpler but if you need to create a protein from a Uniprot accession number
// or simply if you already use BioJava objects, the second one has the advantage of passing the torch with ease.
public class CreatingProteinRecipe {

// Creating a *Protein* from an identifier and an amino-acid sequence string
    @Test
    public void createProteinFromSequenceString() {

        //<SNIP>
        Protein protein = new Protein("Prot1", "PROTEIN");

        AminoAcid aa = protein.getSymbol(1);
        int length = protein.size();

        Assert.assertEquals(AminoAcid.R, aa);
        Assert.assertEquals(7, length);
        //</SNIP>
    }

// Creating a *Protein* from an identifier and an array of *AminoAcid*s
    @Test
    public void createProteinFromAAs() {

        //<SNIP>
        Protein protein = new Protein("Prot1", AminoAcid.P, AminoAcid.R, AminoAcid.O,AminoAcid.T, AminoAcid.E,AminoAcid.I, AminoAcid.N);
        //</SNIP>

        Assert.assertEquals(AminoAcid.R, protein.getSymbol(1));
        Assert.assertEquals(7, protein.size());
    }

// Creating a *Protein* from a biojava *Sequence*
    @Test
    public void createProteinFromBJ1() throws CompoundNotFoundException {

        //<SNIP>
        // an instance of ProteinSequence coming from biojava
        ProteinSequence sequence = new ProteinSequence("PROTEIN");

        // ProteinFactory provides a adapter static factory method to build a Protein from a Sequence<AminoAcidCompound>
        Protein protein = ProteinFactory.newProtein(sequence);
        //</SNIP>

        Assert.assertEquals(7, protein.size());
    }

// Creating a *Protein* from a UniProt accession number
    @Test
    public void createProteinFromBJ2() {

        //<SNIP>
        try {
            // this factory method delegates to BioJava objects
            Protein protein = ProteinFactory.newUniprotProtein("P26663");

            Assert.assertEquals(3010, protein.size());
        } catch (Exception e) {

            throw new IllegalStateException("cannot access this number!");
        }
        //</SNIP>
    }

// ## $DISCUSSION$ ##

// ## $RELATED$ ##

}