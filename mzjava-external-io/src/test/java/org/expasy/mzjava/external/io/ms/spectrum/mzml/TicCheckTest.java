package org.expasy.mzjava.external.io.ms.spectrum.mzml;

import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.junit.Assert;
import org.junit.Test;
import uk.ac.ebi.jmzml.model.mzml.Spectrum;

import java.util.List;

public class TicCheckTest {

    @Test
    public void testGetValues() {

        TicCheck check = new TicCheck();

        List<Spectrum> mzmlSpectra = EbiMzmlReaderTest.newSpectrumList();
        List<MsnSpectrum> msnSpectra = EbiMzmlReaderTest.newMsnSpectrumList();

        Assert.assertEquals(1.66755E7, check.getExpectedValue(mzmlSpectra.get(0)).get(), 0.001);
        Assert.assertEquals(120.0, check.getObservedValue(msnSpectra.get(0)).get(), 0.001);

        Assert.assertEquals(1.66755E7, check.getExpectedValue(mzmlSpectra.get(1)).get(), 0.001);
        Assert.assertEquals(110.0, check.getObservedValue(msnSpectra.get(1)).get(), 0.001);

        Assert.assertTrue(!check.getExpectedValue(mzmlSpectra.get(2)).isPresent());
        Assert.assertEquals(0.0, check.getObservedValue(msnSpectra.get(2)).get(), 0.001);

        Assert.assertEquals(4200.0, check.getExpectedValue(mzmlSpectra.get(3)).get(), 0.001);
        Assert.assertEquals(120.0, check.getObservedValue(msnSpectra.get(3)).get(), 0.001);
    }

    @Test
    public void testCheckConsistencies() {

        TicCheck check = new TicCheck();

        List<Spectrum> mzmlSpectra = EbiMzmlReaderTest.newSpectrumList();
        List<MsnSpectrum> msnSpectra = EbiMzmlReaderTest.newMsnSpectrumList();

        Assert.assertTrue(!check.checkConsistency(msnSpectra.get(0), mzmlSpectra.get(0), 0.001f));
        Assert.assertTrue(!check.checkConsistency(msnSpectra.get(1), mzmlSpectra.get(1), 0.001f));
        Assert.assertTrue(check.checkConsistency(msnSpectra.get(2), mzmlSpectra.get(2), 0.001f));
        Assert.assertTrue(!check.checkConsistency(msnSpectra.get(3), mzmlSpectra.get(3), 0.001f));
    }
}