package org.expasy.mzjava.external.io.ms.spectrum.mzml;

import org.expasy.mzjava.core.ms.spectrum.ScanNumber;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class ScanNumberParserTest {

    @Test
    public void testParse() {
        ScanNumberParser parser = new ScanNumberParser();

        List<ScanNumber> scans = parser.parseScanNumbers("controllerType=0 controllerNumber=1 scan=5134");

        Assert.assertEquals(1, scans.size());
        Assert.assertEquals(5134,scans.get(0).getValue());

        scans = parser.parseScanNumbers("controllerType=0 controllerNumber=1 scan=5134 blabla=xy");

        Assert.assertEquals(1, scans.size());
        Assert.assertEquals(5134,scans.get(0).getValue());
    }
}
