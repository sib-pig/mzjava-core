package org.expasy.mzjava.external.io.ms.spectrum.mzml;

import com.google.common.collect.Lists;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.ThresholdFilter;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.IdentityPeakProcessor;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.spectrum.ScanNumber;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberDiscrete;
import org.expasy.mzjava.external.io.ms.spectrum.ConsistencyCheckException;
import org.expasy.mzjava.external.io.ms.spectrum.ConsistencyChecker;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import uk.ac.ebi.jmzml.model.mzml.MzMLObject;
import uk.ac.ebi.jmzml.model.mzml.Spectrum;
import uk.ac.ebi.jmzml.xml.io.MzMLObjectIterator;
import uk.ac.ebi.jmzml.xml.io.MzMLUnmarshaller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

public class EbiMzmlReaderTest {

    private static final Logger[] LOGS = new Logger[] {
            Logger.getLogger(ConsistencyChecker.class.getName()),
            Logger.getLogger(EbiMzmlReader.class.getName())
    };
    private static OutputStream logCapturingStream;
    private static StreamHandler customLogHandler;

    @Before
    public void attachLogCapturer() {

        logCapturingStream = new ByteArrayOutputStream();
        Handler[] handlers = LOGS[0].getParent().getHandlers();
        customLogHandler = new StreamHandler(logCapturingStream, handlers[0].getFormatter());

        for (Logger log : LOGS) {

            log.addHandler(customLogHandler);
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDelta() throws IOException {

        EbiMzmlReader reader = new EbiMzmlReader(new MzMLUnmarshaller(new File(getClass().getResource("tiny.pwiz.mzML").getFile())),
                PeakList.Precision.DOUBLE);

        reader.setDelta(-1);
    }

    @Test (expected = ConsistencyCheckException.class)
    public void testStrictReader() throws IOException {

        File xmlFile = new File(getClass().getResource("tiny.pwiz.mzML").getFile());

        EbiMzmlReader reader = EbiMzmlReader.newStrictReader(new MzMLUnmarshaller(xmlFile), PeakList.Precision.DOUBLE);

        Assert.assertEquals(4, reader.size());

        while (reader.hasNext()) {

            reader.next();
        }
    }

    @Test
    public void testTolerantReader() throws IOException {

        File xmlFile = new File(getClass().getResource("tiny.pwiz.mzML").getFile());

        EbiMzmlReader reader = EbiMzmlReader.newTolerantReader(new MzMLUnmarshaller(xmlFile), PeakList.Precision.DOUBLE);

        assertTolerantReader(reader);
    }

    @Test
    public void testStrictTurnTolerant() throws IOException {

        File xmlFile = new File(getClass().getResource("tiny.pwiz.mzML").getFile());

        EbiMzmlReader reader = EbiMzmlReader.newStrictReader(new MzMLUnmarshaller(xmlFile), PeakList.Precision.DOUBLE);

        reader.turnTolerant(EnumSet.of(MzmlConsistencyCheck.LOWEST_MZ, MzmlConsistencyCheck.HIGHEST_MZ, MzmlConsistencyCheck.TOTAL_ION_CURRENT));

        assertTolerantReader(reader);
    }

    @Test (expected = ConsistencyCheckException.class)
    public void testTolerantTurnStrict() throws IOException {

        File xmlFile = new File(getClass().getResource("tiny.pwiz.mzML").getFile());

        EbiMzmlReader reader = EbiMzmlReader.newTolerantReader(new MzMLUnmarshaller(xmlFile), PeakList.Precision.DOUBLE);

        reader.turnStrict(EnumSet.allOf(MzmlConsistencyCheck.class));

        while (reader.hasNext()) {

            reader.next();
        }
    }

    @Test
    public void testReaderNoCheck() throws IOException {

        File xmlFile = new File(getClass().getResource("tiny.pwiz.mzML").getFile());

        EbiMzmlReader reader = new EbiMzmlReader(new MzMLUnmarshaller(xmlFile), PeakList.Precision.DOUBLE) {

            @Override
            protected void checkConsistencies(MsnSpectrum spectrum, Spectrum mzmlSpectrum) {  }
        };

        Assert.assertEquals(4, reader.size());

        double[][] expectedMzs = createExpectedMzs();
        double[][] expectedIntensities = createExpectedIntensities();
        int[] expectedMsLevel = createExpectedMsLevel();

        int i=0;
        while (reader.hasNext()) {

            assertSpectrum(reader.next(), i, expectedMzs, expectedIntensities, expectedMsLevel);

            i++;
        }
    }

    @Test
    public void testTolerantReaderWithIdProcessor() throws IOException {

        File xmlFile = new File(getClass().getResource("tiny.pwiz.mzML").getFile());

        PeakProcessorChain<PeakAnnotation> processor = new PeakProcessorChain<PeakAnnotation>(new IdentityPeakProcessor<PeakAnnotation>());

        EbiMzmlReader reader = EbiMzmlReader.newTolerantReader(new MzMLUnmarshaller(xmlFile), PeakList.Precision.DOUBLE, processor);

        double[][] expectedMzs = createExpectedMzs();
        double[][] expectedIntensities = createExpectedIntensities();
        int[] expectedMsLevel = createExpectedMsLevel();

        int i=0;
        while (reader.hasNext()) {

            assertSpectrum(reader.next(), i, expectedMzs, expectedIntensities, expectedMsLevel);

            i++;
        }
    }

    @Test
    public void testTolerantReaderWithFilter() throws IOException {

        File xmlFile = new File(getClass().getResource("tiny.pwiz.mzML").getFile());

        PeakProcessorChain<PeakAnnotation> processor = new PeakProcessorChain<PeakAnnotation>(new ThresholdFilter<PeakAnnotation>(10, ThresholdFilter.Inequality.GREATER));

        EbiMzmlReader reader = EbiMzmlReader.newTolerantReader(new MzMLUnmarshaller(xmlFile), PeakList.Precision.DOUBLE, processor);

        double[][] expectedMzs = createExpectedFilteredMzs();
        double[][] expectedIntensities = createExpectedFilteredIntensities();
        int[] expectedMsLevel = createExpectedMsLevel();

        int i=0;
        while (reader.hasNext()) {

            assertSpectrum(reader.next(), i, expectedMzs, expectedIntensities, expectedMsLevel);

            i++;
        }
    }

    @Test
    public void testScanNumberReader() throws IOException {

        File xmlFile = new File(getClass().getResource("tiny.pwiz.mzML").getFile());

        EbiMzmlReader reader = EbiMzmlReader.newTolerantReader(new MzMLUnmarshaller(xmlFile), PeakList.Precision.DOUBLE);
        reader.setSpectrumIdParser(new SpectrumIdParser() {

            @Override
            public List<ScanNumber> parseScanNumbers(String id) {

                String scan = id.split("=")[1];

                return Arrays.<ScanNumber>asList(new ScanNumberDiscrete(Integer.valueOf(scan)));
            }
        });

        int[] expectedScanNumber = new int[] {19, 20, 21, 22};

        int i=0;
        while (reader.hasNext()) {

            MsnSpectrum spectrum = reader.next();

            Assert.assertEquals(expectedScanNumber[i++], spectrum.getScanNumbers().getFirst().getValue());
        }
    }

    @Test (expected = IllegalArgumentException.class)
    public void testScanNumberReaderEmpty() throws IOException {

        File xmlFile = new File(getClass().getResource("tiny.pwiz.mzML").getFile());

        EbiMzmlReader reader = EbiMzmlReader.newTolerantReader(new MzMLUnmarshaller(xmlFile), PeakList.Precision.DOUBLE);
        reader.setSpectrumIdParser(new SpectrumIdParser() {

            @Override
            public List<ScanNumber> parseScanNumbers(String id) {

                return Lists.newArrayList();
            }
        });

        reader.next();
    }

    @Test
    public void testReadExample() throws IOException {

        File xmlFile = new File(getClass().getResource("neutral_loss_example_1.1.0.mzML").getFile());

        EbiMzmlReader reader = EbiMzmlReader.newTolerantReader(new MzMLUnmarshaller(xmlFile), PeakList.Precision.DOUBLE);

        while (reader.hasNext()) {

            reader.next();
        }

        assertCapturedLogCorrectness("INFO: spectrum 0 lacks MS level information");
    }

    static double[][] createExpectedMzs() {

        double[][] expectedMzs = new double[4][];
        expectedMzs[0] = new double[] {0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0};
        expectedMzs[1] = new double[] {0.0, 2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0};
        expectedMzs[2] = new double[] {};
        expectedMzs[3] = new double[] {0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0};

        return expectedMzs;
    }

    static double[][] createExpectedIntensities() {

        double[][] expectedIntensities = new double[4][];
        expectedIntensities[0] = new double[] {15.0, 14.0, 13.0, 12.0, 11.0, 10.0, 9.0, 8.0, 7.0, 6.0, 5.0, 4.0, 3.0, 2.0, 1.0};
        expectedIntensities[1] = new double[] {20.0, 18.0, 16.0, 14.0, 12.0, 10.0, 8.0, 6.0, 4.0, 2.0};
        expectedIntensities[2] = new double[] {};
        expectedIntensities[3] = new double[] {15.0, 14.0, 13.0, 12.0, 11.0, 10.0, 9.0, 8.0, 7.0, 6.0, 5.0, 4.0, 3.0, 2.0, 1.0};

        return expectedIntensities;
    }

    private static double[][] createExpectedFilteredMzs() {

        double[][] expectedMzs = new double[4][];
        expectedMzs[0] = new double[] {0.0, 1.0, 2.0, 3.0, 4.0};
        expectedMzs[1] = new double[] {0.0, 2.0, 4.0, 6.0, 8.0};
        expectedMzs[2] = new double[] {};
        expectedMzs[3] = new double[] {0.0, 1.0, 2.0, 3.0, 4.0};

        return expectedMzs;
    }

    private static double[][] createExpectedFilteredIntensities() {

        double[][] expectedIntensities = new double[4][];
        expectedIntensities[0] = new double[] {15.0, 14.0, 13.0, 12.0, 11.0};
        expectedIntensities[1] = new double[] {20.0, 18.0, 16.0, 14.0, 12.0};
        expectedIntensities[2] = new double[] {};
        expectedIntensities[3] = new double[] {15.0, 14.0, 13.0, 12.0, 11.0};

        return expectedIntensities;
    }

    private static int[] createExpectedMsLevel() {

        return new int[] {1, 2, 1, 1};
    }

    private static <A extends PeakAnnotation> void assertSpectrum(MsnSpectrum spectrum, int index, double[][] expectedMzs, double[][] expectedIntensities, int[] expectedMsLevel) {

        Assert.assertEquals(expectedMsLevel[index], spectrum.getMsLevel());
        Assert.assertArrayEquals(expectedMzs[index], spectrum.getMzs(null), 0.01);
        Assert.assertArrayEquals(expectedIntensities[index], spectrum.getIntensities(null), 0.01);
        Assert.assertEquals(index, spectrum.getScanNumbers().getFirst().getValue());
    }

    static List<MsnSpectrum> newMsnSpectrumList() {

        List<MsnSpectrum> spectra = Lists.newArrayList();

        double[][] expectedMzs = EbiMzmlReaderTest.createExpectedMzs();
        double[][] expectedIntensities = EbiMzmlReaderTest.createExpectedIntensities();

        for (int i=0 ; i<expectedMzs.length ; i++) {

            MsnSpectrum spectrum = new MsnSpectrum();

            spectrum.addSorted(expectedMzs[i], expectedIntensities[i]);

            spectra.add(spectrum);
        }

        return spectra;
    }

    static List<Spectrum> newSpectrumList() {

        List<Spectrum> spectra = Lists.newArrayList();

        File xmlFile = new File(FirstMzCheck.class.getResource("tiny.pwiz.mzML").getFile());
        MzMLUnmarshaller um = new MzMLUnmarshaller(xmlFile);

        MzMLObjectIterator<MzMLObject> iterator = um.unmarshalCollectionFromXpath("/run/spectrumList/spectrum", Spectrum.class);

        while(iterator.hasNext())
            spectra.add((Spectrum)iterator.next());

        return spectra;
    }

    private String getTestCapturedLog() throws IOException {

        customLogHandler.flush();
        return logCapturingStream.toString();
    }

    private void assertTolerantReader(EbiMzmlReader reader) throws IOException {

        Assert.assertEquals(4, reader.size());

        double[][] expectedMzs = createExpectedMzs();
        double[][] expectedIntensities = createExpectedIntensities();
        int[] expectedMsLevel = createExpectedMsLevel();

        int i=0;
        while (reader.hasNext()) {

            assertSpectrum(reader.next(), i, expectedMzs, expectedIntensities, expectedMsLevel);

            i++;
        }

        assertCapturedLogCorrectness("INFO: spectrum0.LOWEST_MZ: expected=400.39, observed=0.0",
                "INFO: spectrum0.TOTAL_ION_CURRENT: expected=1.66755E7, observed=120.0",
                "INFO: spectrum0.LOWEST_MZ: expected=400.39, observed=0.0",
                "INFO: spectrum1.HIGHEST_MZ: expected=1003.56, observed=18.0",
                "INFO: spectrum1.TOTAL_ION_CURRENT: expected=1.66755E7, observed=110.0",
                "INFO: spectrum1.LOWEST_MZ: expected=320.39, observed=0.0",
                "INFO: spectrum3.HIGHEST_MZ: expected=942.56, observed=14.0",
                "INFO: spectrum3.TOTAL_ION_CURRENT: expected=4200.0, observed=120.0",
                "INFO: spectrum3.LOWEST_MZ: expected=142.39, observed=0.0");
    }

    private void assertCapturedLogCorrectness(String... expectedInfos) throws IOException {

        String capturedLog = getTestCapturedLog();

        Assert.assertTrue(!capturedLog.isEmpty());

        for (String expectedInfo : expectedInfos) {

            Assert.assertTrue(capturedLog.contains(expectedInfo));
        }

    }
}