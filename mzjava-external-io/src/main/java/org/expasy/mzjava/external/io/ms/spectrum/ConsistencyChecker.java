package org.expasy.mzjava.external.io.ms.spectrum;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import uk.ac.ebi.jmzml.model.mzml.Spectrum;

import java.util.Set;
import java.util.logging.Logger;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * ConsistencyChecker checks for consistencies on MsnSpectrum.
 *
 * This object handles a set of ConsistencyChecks in 2 sets: one strict and another tolerant.
 * A ConsistencyCheck is strict if its failure cause a ConsistencyCheckException to be thrown.
 * A ConsistencyCheck is tolerant if its failure cause a warning to be optionally logged.
 *
 * @param <C> the type of ConsistencyCheck handled by this object.
 */
public class ConsistencyChecker<C extends ConsistencyCheck> implements ConsistencyCheckHandler<C> {

    private static final Logger LOGGER = Logger.getLogger(ConsistencyChecker.class.getName());

    private static final float DEFAULT_DELTA = 0.001f;

    private float delta = DEFAULT_DELTA;
    private final Set<C> strictChecks;
    private final Set<C> tolerantChecks;

    /**
     * Create an instance of ConsistencyChecker composed of a given strict consistencies
     *
     * @param consistencies strict consistency checks
     */
    public ConsistencyChecker(Set<C> consistencies) {

        strictChecks = Sets.newHashSet(consistencies);
        tolerantChecks = Sets.newHashSet();
    }

    @Override
    public void setDelta(float delta) {

        Preconditions.checkArgument(delta>=0);

        this.delta = delta;
    }

    /** All strict checks become tolerant */
    public void turnTolerant() {

        tolerantChecks.addAll(strictChecks);
        strictChecks.clear();
    }

    /**
     * Check strict and tolerant consistencies on spectrum
     * @param spectrum the spectrum to check for consistencies
     * @param expectedValueContainer contains expected values
     */
    public void checkConsistencies(MsnSpectrum spectrum, Spectrum expectedValueContainer) {

        checkStrictChecks(spectrum, expectedValueContainer);
        checkTolerantChecks(spectrum, expectedValueContainer);
    }

    /**
     * Set the given checks as tolerant
     * @param ccs consistency checks
     */
    @Override
    public void turnTolerant(Set<C> ccs) {

        checkNotNull(ccs);
        strictChecks.removeAll(ccs);
        tolerantChecks.addAll(ccs);
    }

    /**
     * Set the given checks as strict
     * @param ccs consistency checks
     */
    @Override
    public void turnStrict(Set<C> ccs) {

        checkNotNull(ccs);

        strictChecks.addAll(ccs);
        tolerantChecks.removeAll(ccs);
    }

    private void checkStrictChecks(MsnSpectrum spectrum, Spectrum t) {

        for (ConsistencyCheck strict : strictChecks) {

            if (!strict.checkConsistency(spectrum, t, delta)) {

                throw strict.onFailThrowException(spectrum, t);
            }
        }
    }

    private void checkTolerantChecks(MsnSpectrum spectrum, Spectrum t) {

        for (ConsistencyCheck tolerant : tolerantChecks) {

            if (!tolerant.checkConsistency(spectrum, t, delta))
                LOGGER.info(tolerant.onFailSendWarning(spectrum, t));
        }
    }
}
