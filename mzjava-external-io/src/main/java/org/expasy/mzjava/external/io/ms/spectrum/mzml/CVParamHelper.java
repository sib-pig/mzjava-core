package org.expasy.mzjava.external.io.ms.spectrum.mzml;

import com.google.common.collect.Sets;
import org.expasy.mzjava.core.ms.spectrum.RetentionTime;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeDiscrete;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import uk.ac.ebi.jmzml.model.mzml.CVParam;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 * A bunch of static utility methods for CVParam
 *
 * @author fnikitin
 */
public class CVParamHelper {

    private static final Logger LOGGER = Logger.getLogger(CVParamHelper.class.getName());

    private CVParamHelper() {

        throw new AssertionError("non instanciable");
    }

    /**
     * Search a list of CVParams from given identifier(s)
     *
     * @param cvParams the list to look into
     * @param ids possible CVParam accession numbers or names
     * @return found CVParam or null if not found
     */
    public static CVParam searchCVParam(final List<CVParam> cvParams, String... ids) {

        for (CVParam cvParam : cvParams) {

            for (String id : ids) {
                if (cvParam.getAccession().equals(id) || cvParam.getName().equals(id))
                    return cvParam;
            }
        }

        return null;
    }

    /**
     * Search a list of CVParams from given identifier(s)
     *
     * @param cvParams the list to look into
     * @param ids possible CVParam accession numbers or names
     * @return the found CVParams or empty set
     */
    public static Set<CVParam> searchCVParams(final List<CVParam> cvParams, String... ids) {

        Set<CVParam> params = Sets.newHashSet();

        for (CVParam cvParam : cvParams) {

            for (String id : ids) {
                if (cvParam.getAccession().equals(id) || cvParam.getName().equals(id))
                    params.add(cvParam);
            }
        }

        return params;
    }

    public static RetentionTime searchRetentionTime(final List<CVParam> cvParams, String id) {

        final CVParam cvp = searchCVParam(cvParams, id);

        if (cvp == null) {
            return null;
        }

        String unitName = cvp.getUnitName();

        if ("minute".equals(unitName)) {
            return new RetentionTimeDiscrete(Double.parseDouble(cvp.getValue()),
                    TimeUnit.MINUTE);
        } else if ("second".equals(unitName)) {
            return new RetentionTimeDiscrete(Double.parseDouble(cvp.getValue()),
                    TimeUnit.SECOND);
        } else {
            LOGGER.info("unknown scan unit [" + cvp.getUnitName() + "]");
            return null;
        }
    }

    public static double getCVParamDoubleValue(List<CVParam> params, String... ids) {

        CVParam cvParam = CVParamHelper.searchCVParam(params, ids);
        if (cvParam == null)
            throw new IllegalArgumentException("missing id from "+ Arrays.toString(ids));
        return Double.parseDouble(cvParam.getValue());
    }
}
