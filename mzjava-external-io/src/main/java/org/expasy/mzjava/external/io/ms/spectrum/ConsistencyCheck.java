package org.expasy.mzjava.external.io.ms.spectrum;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import uk.ac.ebi.jmzml.model.mzml.Spectrum;

/**
 * A consistency check interface
 *
 * @author fnikitin
 */
public interface ConsistencyCheck {

    /**
     * Check consistency
     * @param observedValueContainer spectrum mscheck value from
     * @param expectedValueContainer container to mscheck value with
     * @param delta the maximum delta between expected and actual for which both numbers are still considered equal.
     * @return false if failed
     */
    boolean checkConsistency(MsnSpectrum observedValueContainer, Spectrum expectedValueContainer, float delta);

    /**
     * Get expected value from a container
     *
     * @param expectedValueContainer potentially contains expected value
     * @return an optional value
     */
    Optional<Double> getExpectedValue(Spectrum expectedValueContainer);

    /**
     * Get observed value from an MsnSpectrum
     *
     * @param spectrum the spectrum container
     * @return the optional observed value to check for consistency
     */
    Optional<Double> getObservedValue(MsnSpectrum spectrum);

    /**
     * Send warning when check fails
     * @return a warning message
     */
    String onFailSendWarning(MsnSpectrum observedValueContainer, Spectrum expectedValueContainer);

    /**
     * Throw a ConsistencyCheckException when check fails
     * @return a ConsistencyCheckException
     */
    ConsistencyCheckException onFailThrowException(MsnSpectrum observedValueContainer, Spectrum expectedValueContainer);
}
