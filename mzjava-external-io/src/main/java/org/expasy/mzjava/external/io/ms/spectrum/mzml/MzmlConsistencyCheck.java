package org.expasy.mzjava.external.io.ms.spectrum.mzml;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.external.io.ms.spectrum.ConsistencyCheck;
import org.expasy.mzjava.external.io.ms.spectrum.ConsistencyCheckException;
import uk.ac.ebi.jmzml.model.mzml.Spectrum;

/**
 * An enumeration of all consistency to check in mzml.
 *
 * @author fnikitin
 */
public enum MzmlConsistencyCheck implements ConsistencyCheck {

    LOWEST_MZ(new FirstMzCheck()),
    HIGHEST_MZ(new LastMzCheck()),
    TOTAL_ION_CURRENT(new TicCheck());

    private MzmlSpectrumCheck cc;

    MzmlConsistencyCheck(MzmlSpectrumCheck cc) {

        this.cc = cc;
        cc.setName(name());
    }

    @Override
    public boolean checkConsistency(MsnSpectrum observedValueContainer, Spectrum expectedValueContainer, float delta) {
        return cc.checkConsistency(observedValueContainer, expectedValueContainer, delta);
    }

    @Override
    public Optional<Double> getExpectedValue(Spectrum expectedValueContainer) {
        return cc.getExpectedValue(expectedValueContainer);
    }

    @Override
    public Optional<Double> getObservedValue(MsnSpectrum observedValueContainer) {
        return cc.getObservedValue(observedValueContainer);
    }

    @Override
    public String onFailSendWarning(MsnSpectrum observedValueContainer, Spectrum expectedValueContainer) {
        return cc.onFailSendWarning(observedValueContainer, expectedValueContainer);
    }

    @Override
    public ConsistencyCheckException onFailThrowException(MsnSpectrum observedValueContainer, Spectrum expectedValueContainer) {
        return cc.onFailThrowException(observedValueContainer, expectedValueContainer);
    }
}
