package org.expasy.mzjava.external.io.ms.spectrum.mzml;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import uk.ac.ebi.jmzml.model.mzml.Spectrum;

public class TicCheck extends MzmlSpectrumCheck {

    @Override
    public Optional<Double> getExpectedValue(Spectrum mzmlSpectrum) {

        return getExpectedValue(mzmlSpectrum, "total ion current");
    }

    @Override
    public Optional<Double> getObservedValue(MsnSpectrum observedValueContainer) {

        return Optional.of(observedValueContainer.getTotalIonCurrent());
    }
}
