package org.expasy.mzjava.external.io.ms.spectrum.mzml;

import org.expasy.mzjava.core.ms.spectrum.ScanNumber;

import java.util.List;

/**
 * This interface has to be implemented by parsers that extract scan number(s)
 * from a mzml spectrum {@code id} attribute.
 *
 * @author fnikitin
 * @version 1.0
 */
public interface SpectrumIdParser {

    List<ScanNumber> parseScanNumbers(String id);
}
