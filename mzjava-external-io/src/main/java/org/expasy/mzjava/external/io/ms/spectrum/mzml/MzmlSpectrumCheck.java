package org.expasy.mzjava.external.io.ms.spectrum.mzml;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.external.io.ms.spectrum.ConsistencyCheck;
import org.expasy.mzjava.external.io.ms.spectrum.ConsistencyCheckException;
import uk.ac.ebi.jmzml.model.mzml.CVParam;
import uk.ac.ebi.jmzml.model.mzml.Spectrum;

/**
 * The base object to define a consistency check between a MsnSpectrum
 * and a ebi mzml Spectrum for a specific target value.
 */
public abstract class MzmlSpectrumCheck implements ConsistencyCheck {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean checkConsistency(MsnSpectrum msnSpectrum, Spectrum mzXmlSpectrum, float delta) {

        Optional<Double> expectedValue = getExpectedValue(mzXmlSpectrum);
        Optional<Double> observedValue = getObservedValue(msnSpectrum);

        return !(expectedValue.isPresent() && observedValue.isPresent()) || assertConsistent(expectedValue.get(), observedValue.get(), delta);

    }

    public boolean assertConsistent(double expected, double observed, float delta) {

        return Math.abs(expected - observed) <= delta;
    }

    @Override
    public String onFailSendWarning(MsnSpectrum observedValueContainer, Spectrum expectedValueContainer) {

        return "spectrum"+observedValueContainer.getSpectrumIndex()+"."+name+": expected="+getExpectedValue(expectedValueContainer).get()+", observed="+getObservedValue(observedValueContainer).get();
    }

    @Override
    public ConsistencyCheckException onFailThrowException(MsnSpectrum observedValueContainer, Spectrum expectedValueContainer) {

        return new ConsistencyCheckException(String.valueOf(getExpectedValue(expectedValueContainer).get()),
                String.valueOf(getObservedValue(observedValueContainer)), "turnTolerant(" + MzmlConsistencyCheck.class.getSimpleName() + "." +
                    name + ")");
    }

    protected Optional<Double> getExpectedValue(Spectrum mzmlSpectrum, String param) {

        Preconditions.checkNotNull(mzmlSpectrum);
        Preconditions.checkNotNull(param);

        CVParam mzCVParam = CVParamHelper.searchCVParam(mzmlSpectrum.getCvParam(), param);

        if (mzCVParam == null)
            return Optional.absent();

        return Optional.of(Double.parseDouble(mzCVParam.getValue()));
    }
}
