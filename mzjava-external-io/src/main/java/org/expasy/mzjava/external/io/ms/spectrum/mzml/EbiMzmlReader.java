package org.expasy.mzjava.external.io.ms.spectrum.mzml;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import gnu.trove.map.hash.TIntObjectHashMap;
import org.expasy.mzjava.core.io.IterativeReader;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakCollectorSink;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.spectrum.*;
import org.expasy.mzjava.external.io.ms.spectrum.ConsistencyChecker;
import org.expasy.mzjava.external.io.ms.spectrum.ConsistencyCheckHandler;
import uk.ac.ebi.jmzml.model.mzml.*;
import uk.ac.ebi.jmzml.model.mzml.Spectrum;
import uk.ac.ebi.jmzml.xml.io.MzMLObjectIterator;
import uk.ac.ebi.jmzml.xml.io.MzMLUnmarshaller;

import java.io.IOException;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

/**
 * EbiMzmlReader is a wrapper over jmzml reader provided by EBI (https://code.google.com/p/jmzml/)
 * that fully support mzML 1.1.0 specifications (http://www.psidev.info/mzml_1_0_0%20).
 *
 * Scan number are extracted from spectrum id with SpectrumIdParser (default set with ScanNumberIncrementer).
 */
public class EbiMzmlReader<S extends MsnSpectrum> implements IterativeReader<S>, ConsistencyCheckHandler<MzmlConsistencyCheck> {

    private static final Logger LOGGER = Logger.getLogger(EbiMzmlReader.class.getName());

    private final MzMLObjectIterator<MzMLObject> iterator;
    private final int size;
    private final PeakList.Precision precision;
    private SpectrumIdParser spectrumIdParser;
    private final Map<String, List<? extends ScanNumber>> spectrumIds;
    private final PeakProcessorChain<PeakAnnotation> processorChain;
    private final PeakCollectorSink<PeakAnnotation> collectorSink = new PeakCollectorSink<>();

    private final ConsistencyChecker<MzmlConsistencyCheck> checker;

    public EbiMzmlReader(MzMLUnmarshaller unmarshaller, PeakList.Precision precision) {

        this(unmarshaller, precision, new PeakProcessorChain<>());
    }

    public EbiMzmlReader(MzMLUnmarshaller unmarshaller, PeakList.Precision precision, PeakProcessorChain<PeakAnnotation> processorChain) {

        Preconditions.checkNotNull(unmarshaller);
        Preconditions.checkNotNull(precision);
        Preconditions.checkNotNull(processorChain);

        String xpath = "/run/spectrumList/spectrum";

        iterator = unmarshaller.unmarshalCollectionFromXpath(xpath, Spectrum.class);
        size = unmarshaller.getObjectCountForXpath(xpath);
        this.precision = precision;
        this.processorChain = processorChain;

        spectrumIdParser = new ScanNumberIncrementer(0);
        spectrumIds = Maps.newHashMap();

        checker = new ConsistencyChecker(EnumSet.allOf(MzmlConsistencyCheck.class));
    }

    public static EbiMzmlReader newStrictReader(MzMLUnmarshaller unmarshaller, PeakList.Precision precision) {

        return new EbiMzmlReader(unmarshaller, precision);
    }

    public static <S extends MsnSpectrum> EbiMzmlReader newStrictReader(MzMLUnmarshaller unmarshaller, PeakList.Precision precision, PeakProcessorChain<PeakAnnotation> processorChain) {

        return new EbiMzmlReader<>(unmarshaller, precision, processorChain);
    }

    public static <A extends PeakAnnotation> EbiMzmlReader newTolerantReader(MzMLUnmarshaller unmarshaller, PeakList.Precision precision) {

        return newTolerantReader(unmarshaller, precision, new PeakProcessorChain<PeakAnnotation>());
    }

    public static <S extends MsnSpectrum> EbiMzmlReader newTolerantReader(MzMLUnmarshaller unmarshaller, PeakList.Precision precision, PeakProcessorChain<PeakAnnotation> processorChain) {

        EbiMzmlReader<S> reader = new EbiMzmlReader<>(unmarshaller, precision, processorChain);

        reader.checker.turnTolerant();

        return reader;
    }

    public void setDelta(float delta) {

        checker.setDelta(delta);
    }

    public void turnTolerant(Set<MzmlConsistencyCheck> ccs) {

        checker.turnTolerant(ccs);
    }

    public void turnStrict(Set<MzmlConsistencyCheck> ccs) {

        checker.turnStrict(ccs);
    }

    /**
     * Set parser that extract scan number from spectrum id attribute.
     *
     * @param spectrumIdParser the parser
     */
    public void setSpectrumIdParser(SpectrumIdParser spectrumIdParser) {

        Preconditions.checkNotNull(spectrumIdParser);

        this.spectrumIdParser = spectrumIdParser;
    }

    /** @return the number of spectra to read */
    public int size() {

        return size;
    }

    @Override
    public boolean hasNext() {

        return iterator.hasNext();
    }

    /**
     * Override this method to make an instance of S (MsnSpectrum<A> by default)
     * @param precision peaks precision
     * @return an S instance
     */
    protected S newSpectrum(PeakList.Precision precision) {

        return (S) new MsnSpectrum(precision);
    }

    @Override
    public S next() throws IOException {

        Spectrum mzmlSpectrum = (Spectrum) iterator.next();

        S spectrum = newSpectrum(precision);

        if (mzmlSpectrum != null) {

            spectrum.setSpectrumIndex(mzmlSpectrum.getIndex());

            handleSpectrumId(mzmlSpectrum.getId(), spectrum);

            handleMsLevelAtLeast(mzmlSpectrum, spectrum);

            if (spectrum.getMsLevel() == 0)
                LOGGER.info("spectrum "+mzmlSpectrum.getIndex()+" lacks MS level information");
            else if (spectrum.getMsLevel() > 1)
                handlePrecursorList(mzmlSpectrum.getPrecursorList().getPrecursor(), spectrum);

            if (mzmlSpectrum.getScanList() != null)
                handleScanList(mzmlSpectrum.getScanList().getScan(), spectrum);

            handlePeaks(mzmlSpectrum.getBinaryDataArrayList().getBinaryDataArray(), spectrum);
        }

        checkConsistencies(spectrum, mzmlSpectrum);

        return spectrum;
    }

    @Override
    public void close() throws IOException {

        // Nothing to close in MzMLUnmarshaller instance
    }

    protected void checkConsistencies(S spectrum, Spectrum mzmlSpectrum) {

        checker.checkConsistencies(spectrum, mzmlSpectrum);
    }

    /**
     * Extract information from mzml spectrum to msn spectrum.
     * This default implementation only extract ms level.
     * [This method is meant to be overriden if more data have to be extracted from the source spectrum]
     *
     * @param src the mzxml spectrum source
     * @param dest the msn spectrum destination
     */
    protected void handleMsLevelAtLeast(Spectrum src, S dest) {

        handleMsLevel(src, dest);
    }

    /**
     * Handle isolation window parameters.
     *
     * @param targetMz center of window m/z
     * @param lowerOffset lower bound offset
     * @param upperOffset upper bound offset
     * @param spectrum the destination spectrum
     */
    protected void handleIsolationWindow(double targetMz, double lowerOffset, double upperOffset, S spectrum) {

        // This method is empty and is meant to be overriden by any subclass
        // that needs to store information into S spectrum
    }

    /**
     * Provide ms Level from mzml spectrum to msn spectrum
     * @param src the ms level source
     * @param dest the ms level destination
     */
    private void handleMsLevel(Spectrum src, S dest) {

        CVParam msLevelCVParam = CVParamHelper.searchCVParam(src.getCvParam(), "ms level");

        if (msLevelCVParam != null)
            dest.setMsLevel(Integer.parseInt(msLevelCVParam.getValue()));
    }

    private void handleSpectrumId(String id, S spectrum) {

        if (spectrumIds.containsKey(id))
            throw new IllegalArgumentException("duplicated scan number for spectrum id "+id);

        List<? extends ScanNumber> list = spectrumIdParser.parseScanNumbers(id);

        if (list.isEmpty())
            throw new IllegalArgumentException("missing scan number for spectrum id "+id);

        spectrum.addScanNumbers(list);

        spectrumIds.put(id, list);
    }

    private void handleScanList(List<Scan> scans, S spectrum) {

        for (Scan scan : scans) {

            RetentionTime rt = CVParamHelper.searchRetentionTime(scan.getCvParam(), "scan time");

            if (rt != null)
                spectrum.addRetentionTime(rt);
        }
    }

    /**
     * Set spectrum precursor
     *
     * @param precursorList the list of precursors coming from mzml spectrum
     * @param spectrum the spectrum to set precursors in
     */
    private void handlePrecursorList(List<Precursor> precursorList, S spectrum) {

        if (precursorList.isEmpty()) {
            LOGGER.info("empty precursor list");

            return;
        }
        if (precursorList.size()>1)
            throw new IllegalStateException("multiple precursors not supported");

        Precursor precursor = precursorList.get(0);

        handlePrecursor(precursor, spectrum);

        ParamGroup window = precursor.getIsolationWindow();
        if (window != null) {

            List<CVParam> isoWindowParams = precursor.getIsolationWindow().getCvParam();

            handleIsolationWindow(CVParamHelper.getCVParamDoubleValue(isoWindowParams, "isolation window target m/z"),
                    CVParamHelper.getCVParamDoubleValue(isoWindowParams, "isolation window lower offset"),
                    CVParamHelper.getCVParamDoubleValue(isoWindowParams, "isolation window upper offset"),
                    spectrum);
        }

        String precId = precursor.getSpectrumRef();

        if (spectrumIds != null && spectrumIds.containsKey(precId))
            spectrum.setParentScanNumber(spectrumIds.get(precId).get(0));
    }

    private void handlePrecursor(Precursor precursor, S spectrum) {

        List<ParamGroup> ions = Lists.newArrayList();
        SelectedIonList selectedIonList = precursor.getSelectedIonList();

        if (selectedIonList != null && selectedIonList.getCount()>0)
            ions = selectedIonList.getSelectedIon();

        if (ions.isEmpty())
            return;

        List<CVParam> precursorParams = ions.get(0).getCvParam();

        CVParam cvp = CVParamHelper.searchCVParam(precursorParams, "m/z", "selected ion m/z");

        if (cvp == null)
           throw new IllegalArgumentException("missing precursor m/z");

        double mz = Double.parseDouble(cvp.getValue());

        cvp = CVParamHelper.searchCVParam(precursorParams, "intensity", "peak intensity");
        double intensity = 0;

        if (cvp != null)
            intensity = Double.parseDouble(cvp.getValue());

        Peak precursorPeak = new Peak(mz, intensity, handlePrecursorPeakCharge(precursorParams));

        spectrum.setPrecursor(precursorPeak);
    }

    private int[] handlePrecursorPeakCharge(List<CVParam> precursorParams) {

        Set<CVParam> cvps = CVParamHelper.searchCVParams(precursorParams, "possible charge state");

        if (cvps.isEmpty()) {
            CVParam cvp = CVParamHelper.searchCVParam(precursorParams, "charge state");
            int charge = 0;

            if (cvp != null)
                charge = Integer.parseInt(cvp.getValue());

            return new int[] {charge};
        }

        else {

            int[] charges = new int[cvps.size()];

            int i=0;
            for (CVParam param : cvps) {

                charges[i++] = Integer.parseInt(param.getValue());
            }

            return charges;
        }
    }

    /**
     * Store binary encoded peaks in mzjava Spectrum
     *
     * @param data peaks encoded in BinaryDataArrays
     * @param spectrum spectrum to add peaks to
     */
    private void handlePeaks(List<BinaryDataArray> data, S spectrum) {

        Preconditions.checkArgument(data.size() >= 2, data.size());

        Number[] mzs = data.get(0).getBinaryDataAsNumberArray();
        Number[] intensities = data.get(1).getBinaryDataAsNumberArray();

        Preconditions.checkArgument(mzs.length == intensities.length);

        collectorSink.setPeakList(spectrum);

        processorChain.process(mzs, intensities, new TIntObjectHashMap<List<PeakAnnotation>>(), collectorSink);
    }
}
