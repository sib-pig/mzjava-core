package org.expasy.mzjava.external.io.ms.spectrum;

public class ConsistencyCheckException extends RuntimeException {

    public ConsistencyCheckException(String expected, String observed, String disablingMethodMessage) {

        super("expected=" + expected + ", observed=" + observed + ".\n<Call method '"+disablingMethodMessage
              +"' to disable this consistency mscheck exception (logger will warn for potential inconsistencies)>");
    }
}