package org.expasy.mzjava.external.io.ms.spectrum.mzml;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import uk.ac.ebi.jmzml.model.mzml.Spectrum;

public class LastMzCheck extends MzmlSpectrumCheck {

    @Override
    public Optional<Double> getExpectedValue(Spectrum mzmlSpectrum) {

        return getExpectedValue(mzmlSpectrum, "highest m/z value");
    }

    @Override
    public Optional<Double> getObservedValue(MsnSpectrum spectrum) {

        if (!spectrum.isEmpty())
            return Optional.of(spectrum.getMz(spectrum.size()-1));

        return Optional.absent();
    }
}
