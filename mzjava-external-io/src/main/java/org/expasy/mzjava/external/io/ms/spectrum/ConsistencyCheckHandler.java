package org.expasy.mzjava.external.io.ms.spectrum;


import java.util.Set;

/**
 * Class implementing this interface are able to control the ConsistencyCheck behavior
 *
 * @see ConsistencyChecker
 */
public interface ConsistencyCheckHandler<C extends ConsistencyCheck> {

    /**
     * Set delta value
     * @param delta the maximum delta between expected and actual for which both numbers are still considered equal.
     */
    void setDelta(float delta);

    /**
     * Make these checks tolerant (log a message if a check fails)
     * @param ccs consistency checks
     */
    void turnTolerant(Set<C> ccs);

    /**
     * Make these checks strict (throw a ConsistencyCheckException if a check fails)
     * @param ccs consistency checks
     */
    void turnStrict(Set<C> ccs);
}
