package org.expasy.mzjava.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.SuffixFileFilter;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PathUtils {

    private PathUtils() {
    }

    public static List<String> listFilePaths(String inputPath, String suffix) {

        File parentFile = getFile(inputPath);

        if(!parentFile.isDirectory()){

            return Collections.singletonList(parentFile.getAbsolutePath());
        }

        File[] files = parentFile.listFiles((FileFilter) new SuffixFileFilter(suffix));
        if (files == null) {

            throw new IllegalStateException("There are no input files for " + inputPath);
        }

        List<String> pathList = new ArrayList<>(files.length);
        for (File file : files) {

            pathList.add(file.getAbsolutePath());
        }
        return pathList;
    }

    public static boolean pathExists(String path) {

        return getFile(path).exists();
    }

    public static void deletePath(String path) throws IOException {

        FileUtils.deleteDirectory(getFile(path));
    }

    public static File getFile(String path) {

        if(path.charAt(1) == ':')
            return new File(path);

        URI uri;
        try {

            uri = new URI(path);
        } catch (URISyntaxException e) {

            throw new IllegalStateException(e);
        }

        File file;
        if(uri.isAbsolute())
            file = new File(uri);
        else
            file = new File(path);
        return file;
    }
}
