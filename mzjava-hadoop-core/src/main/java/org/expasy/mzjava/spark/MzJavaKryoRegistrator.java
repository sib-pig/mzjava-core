package org.expasy.mzjava.spark;

import com.esotericsoftware.kryo.Kryo;
import org.apache.spark.serializer.KryoRegistrator;
import org.expasy.mzjava.avro.io.AvroReader;
import org.expasy.mzjava.avro.io.AvroWriter;
import org.openide.util.Lookup;

import java.util.*;
import java.util.logging.Logger;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MzJavaKryoRegistrator implements KryoRegistrator {

    private static final Logger LOGGER = Logger.getLogger(MzJavaKryoRegistrator.class.getName());

    @Override
    public void registerClasses(Kryo kryo) {

        Lookup lookup = Lookup.getDefault();

        Map<Class, AvroReader> readerMap = new HashMap<>();
        for (AvroReader objectReader : lookup.lookupAll(AvroReader.class)) {

            readerMap.put(objectReader.getObjectClass(), objectReader);
        }

        Map<Class, AvroWriter> writerMap = new HashMap<>();
        for (AvroWriter objectWriter : lookup.lookupAll(AvroWriter.class)) {

            writerMap.put(objectWriter.getObjectClass(), objectWriter);
        }

        Set<Class> avroSerializable = new HashSet<>(readerMap.keySet());
        avroSerializable.retainAll(writerMap.keySet());

        List<Class> sortedAvroSerializable = new ArrayList<>(avroSerializable);
        Collections.sort(sortedAvroSerializable, new Comparator<Class>() {
            @Override
            public int compare(Class c1, Class c2) {

                return c1.getName().compareTo(c2.getName());
            }
        });

        for(Class clazz : sortedAvroSerializable){

            //Because spark can run more than one thread per VM the reader and writer have to be copied
            //to make sure that each thread has it's own class
            //noinspection unchecked
            kryo.register(clazz, new KryoAvroSerializer(copy(writerMap.get(clazz)), copy(readerMap.get(clazz))));

            LOGGER.fine("Registering " + clazz + " with kryo serialization");
        }
    }

    private <T> T copy(T object) {

        try {
            //noinspection unchecked
            return (T)object.getClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
    }
}
