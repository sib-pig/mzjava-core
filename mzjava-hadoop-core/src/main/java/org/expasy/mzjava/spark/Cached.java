package org.expasy.mzjava.spark;

import java.io.Serializable;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public abstract class Cached<T> implements Serializable {

    private transient T value;

    protected Cached() {
    }

    public T get(){

        if(value == null)
            value = build();

        return value;
    }

    protected abstract T build();
}
