package org.expasy.mzjava.spark;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.hadoop.io.MsnSpectrumValue;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import scala.Tuple2;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MzJavaSparkUtils {

    private MzJavaSparkUtils() {}

    public static JavaRDD<MsnSpectrum> msnSpectra(final JavaSparkContext sc, final String queryPath) {

        return sc.sequenceFile(queryPath, SpectrumKey.class, MsnSpectrumValue.class)
                .map(new Function<Tuple2<SpectrumKey, MsnSpectrumValue>, MsnSpectrum>() {
                    @Override
                    public MsnSpectrum call(Tuple2<SpectrumKey, MsnSpectrumValue> tuple) throws Exception {

                        return tuple._2().get();
                    }
                });
    }
}
