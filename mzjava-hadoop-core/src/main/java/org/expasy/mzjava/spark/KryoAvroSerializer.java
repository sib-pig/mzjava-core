package org.expasy.mzjava.spark;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.EncoderFactory;
import org.expasy.mzjava.avro.io.AvroReader;
import org.expasy.mzjava.avro.io.AvroWriter;

import java.io.IOException;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class KryoAvroSerializer<O> extends Serializer<O> {

    private final AvroWriter<O> writer;
    private final AvroReader<O> reader;

    private BinaryEncoder encoder;
    private BinaryDecoder decoder;

    /**
     * Build a serializer that fowards the serialization to the <code>writer</code>
     * and <code>read</code>
     *
     * @param writer the Avro writer
     * @param reader the Avro reader
     */
    public KryoAvroSerializer(AvroWriter<O> writer, AvroReader<O> reader) {

        this.writer = writer;
        this.reader = reader;
    }

    @Override
    public void write(Kryo kryo, Output output, O object) {

        encoder = EncoderFactory.get().directBinaryEncoder(output, encoder);

        try {

            writer.write(object, encoder);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public O read(Kryo kryo, Input input, Class<O> type) {

        decoder = DecoderFactory.get().directBinaryDecoder(input, decoder);

        try {

            return reader.read(decoder);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}