/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.hadoop.io;

import org.apache.avro.io.*;
import org.apache.hadoop.io.Writable;

import java.io.*;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public abstract class AvroWritable implements Writable {

    private BinaryEncoder encoder;
    private BinaryDecoder decoder;

    @Override
    public void write(DataOutput dataOutput) throws IOException {

        if (dataOutput instanceof OutputStream) {

            encoder = EncoderFactory.get().directBinaryEncoder((OutputStream) dataOutput, encoder);
        } else {

            throw new IllegalStateException("Data output needs to be instance of OutputStream to work with AvroWritable");
        }

        avroWrite(encoder);
        encoder.flush();
    }

    protected abstract void avroWrite(Encoder encoder) throws IOException;

    @Override
    public void readFields(DataInput dataInput) throws IOException {

        if (dataInput instanceof InputStream) {

            decoder = DecoderFactory.get().directBinaryDecoder((InputStream)dataInput, decoder);
        } else {

            throw new IllegalStateException("Data input needs to be instance of InputStream to work with AvroWritable");
        }

        avroRead(decoder);
    }

    protected abstract void avroRead(Decoder decoder) throws IOException;
}
