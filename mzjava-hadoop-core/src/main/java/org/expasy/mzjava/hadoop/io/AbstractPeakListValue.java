/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.hadoop.io;

import org.apache.avro.io.Decoder;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.avro.io.AbstractPeakListWriter;
import org.expasy.mzjava.avro.io.AvroReader;
import org.expasy.mzjava.avro.io.AvroWriter;
import org.expasy.mzjava.core.ms.peaklist.PeakList;

import java.io.IOException;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public abstract class AbstractPeakListValue<PL extends PeakList> extends AvroWritable {

    private final AvroWriter<PL> writer;
    private final AvroReader<PL> reader;

    private PL peakList;

    protected AbstractPeakListValue(AbstractPeakListWriter<PL> writer, AbstractAvroReader<PL> reader) {

        this.writer = writer;
        this.reader = reader;
    }

    @Override
    protected void avroWrite(Encoder encoder) throws IOException {

        writer.write(peakList, encoder);
    }

    @Override
    protected void avroRead(Decoder decoder) throws IOException {

        peakList = reader.read(decoder);
    }

    /**
     * Returns the read peak list
     *
     * @return the read peak list
     */
    public PL get() {

        return peakList;
    }

    public void set(PL peakList) {

        this.peakList = peakList;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractPeakListValue that = (AbstractPeakListValue) o;

        return !(peakList != null ? !peakList.equals(that.peakList) : that.peakList != null);
    }

    @Override
    public int hashCode() {

        return peakList != null ? peakList.hashCode() : 0;
    }
}
