/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.hadoop.io;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.expasy.mzjava.core.io.IterativeReader;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.spectrum.Spectrum;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class HadoopSpectraReader<A extends PeakAnnotation, S extends Spectrum<A>> implements IterativeReader<S>, Closeable {

    private S current;
    private final SequenceFile.Reader reader;

    private final SpectrumKey key;
    private final AbstractPeakListValue<S> value;

    public HadoopSpectraReader(Configuration conf, Path path, AbstractPeakListValue<S> value) throws IOException {

        this(getLocal(conf), path, value);
    }

    public HadoopSpectraReader(String path, String name, AbstractPeakListValue<S> value) throws IOException {

        this(new Configuration(), new Path(path, name), value);
    }

    public HadoopSpectraReader(String path, String name, SpectrumKey key, AbstractPeakListValue<S> value) throws IOException {

        this(new Configuration(), new Path(path, name), key, value);
    }

    public HadoopSpectraReader(FileSystem fs, Path path, AbstractPeakListValue<S> value) throws IOException {

        this(fs.getConf(), path, new SpectrumKey(), value);
    }

    public HadoopSpectraReader(Configuration conf, Path path, SpectrumKey key, AbstractPeakListValue<S> value) throws IOException {

        this.key = key;
        this.value = value;

        reader = new SequenceFile.Reader(conf, SequenceFile.Reader.file(path));
    }

    public static LocalFileSystem getLocal(Configuration conf) throws IOException {

        return FileSystem.getLocal(conf);
    }

    @Override
    public void close() throws IOException {

        reader.close();
    }

    public boolean hasNext() {

        if (current != null) return true;

        boolean next;
        try {
            next = reader.next(key, value);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        if (!next) return false;

        current = value.get();
        current.trimToSize();

        return true;
    }

    public S next() throws IOException {

        if (current == null) {

            boolean hasNext = hasNext();
            if (!hasNext) throw new NoSuchElementException();
        }

        S next = current;

        current = null;

        return next;
    }

    public static HadoopSpectraReader<PeakAnnotation, MsnSpectrum> msnReader(File file) throws IOException {

        return new HadoopSpectraReader<>(file.getParent(), file.getName(), new MsnSpectrumValue());
    }
}
