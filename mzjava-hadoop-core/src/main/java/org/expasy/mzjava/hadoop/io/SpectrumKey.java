/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.hadoop.io;

import org.apache.hadoop.io.WritableComparable;
import org.expasy.mzjava.core.ms.peaklist.Peak;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
* @author Oliver Horlacher
* @version sqrt -1
*/
public class SpectrumKey implements WritableComparable<SpectrumKey> {

    private int charge;
    private double mz;

    public SpectrumKey() {
    }

    public SpectrumKey(double mz, int charge) {

        this.mz = mz;
        this.charge = charge;
    }

    public double getMz() {

        return mz;
    }

    public int getCharge() {

        return charge;
    }

    public void set(Peak precursor) {

        set(precursor.getCharge(), precursor.getMz());
    }

    public void set(int charge, double mz) {

        this.mz = mz;
        this.charge = charge;
    }

    @Override
    public int compareTo(SpectrumKey o) {

        int compare = Double.compare(charge, o.charge);
        if (compare != 0) {
            return compare;
        }

        return Double.compare(mz, o.mz);
    }

    @Override
    public void write(DataOutput out) throws IOException {

        //Using int instead of a vInt so that DefaultSpectrumRawComparator can read an int and does not need to worry about zigzag encoding
        out.writeInt(charge);
        out.writeDouble(mz);
    }

    @Override
    public void readFields(DataInput in) throws IOException {

        charge = in.readInt();
        mz = in.readDouble();
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!(o instanceof SpectrumKey)) return false;

        SpectrumKey key = (SpectrumKey) o;

        return charge == key.charge &&
                Double.compare(key.mz, mz) == 0;

    }

    @Override
    public int hashCode() {

        int result;
        long temp;
        temp = mz != +0.0d ? Double.doubleToLongBits(mz) : 0L;
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + charge;
        return result;
    }

    @Override
    public String toString() {

        return "SpectrumKey{" +
                "charge=" + charge +
                ", mz=" + mz +
                '}';
    }
}
