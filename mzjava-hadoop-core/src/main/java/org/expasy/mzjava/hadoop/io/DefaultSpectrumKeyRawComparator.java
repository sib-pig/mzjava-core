/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.hadoop.io;

import org.apache.hadoop.io.RawComparator;

/**
* @author Oliver Horlacher
* @version sqrt -1
*/
public class DefaultSpectrumKeyRawComparator implements RawComparator<SpectrumKey> {

    @Override
    public int compare(SpectrumKey o1, SpectrumKey o2) {

        int compare = compare(o1.getCharge(), o2.getCharge());
        if (compare != 0) {
            return compare;
        }

        return Double.compare(o1.getMz(), o2.getMz());
    }

    private int compare(int thisValue, int thatValue) {

        return thisValue < thatValue ? -1 : thisValue == thatValue ? 0 : 1;
    }

    @Override
    public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2) {

        int charge1 = readInt(b1, s1);
        int charge2 = readInt(b2, s2);

        int compare = compare(charge1, charge2);

        if (compare != 0) {
            return compare;
        }

        return Double.compare(readDouble(b1, s1 + 4), readDouble(b2, s2 + 4));
    }

    /**
     * Parse an integer from a byte array.
     */
    public static int readInt(byte[] bytes, int start) {

        return ((bytes[start] & 0xff) << 24) +
                ((bytes[start + 1] & 0xff) << 16) +
                ((bytes[start + 2] & 0xff) << 8) +
                (bytes[start + 3] & 0xff);

    }

    /**
     * Parse a double from a byte array.
     */
    public static double readDouble(byte[] bytes, int start) {

        return Double.longBitsToDouble(readLong(bytes, start));
    }

    /**
     * Parse a long from a byte array.
     */
    public static long readLong(byte[] bytes, int start) {

        return ((long) (readInt(bytes, start)) << 32) +
                (readInt(bytes, start + 4) & 0xFFFFFFFFL);
    }
}
