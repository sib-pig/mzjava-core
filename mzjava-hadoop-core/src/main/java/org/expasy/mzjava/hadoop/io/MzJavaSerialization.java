package org.expasy.mzjava.hadoop.io;

import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.EncoderFactory;
import org.apache.hadoop.io.serializer.Deserializer;
import org.apache.hadoop.io.serializer.Serialization;
import org.apache.hadoop.io.serializer.Serializer;
import org.expasy.mzjava.avro.io.AvroReader;
import org.expasy.mzjava.avro.io.AvroWriter;
import org.openide.util.Lookup;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MzJavaSerialization implements Serialization<Object> {

    private final Map<Class, AvroReader> readerMap = new HashMap<>();
    private final Map<Class, AvroWriter> writerMap = new HashMap<>();

    public MzJavaSerialization() {

        Lookup lookup = Lookup.getDefault();

        for (AvroReader objectReader : lookup.lookupAll(AvroReader.class)) {

            readerMap.put(objectReader.getObjectClass(), objectReader);
        }

        for (AvroWriter objectWriter : lookup.lookupAll(AvroWriter.class)) {

            writerMap.put(objectWriter.getObjectClass(), objectWriter);
        }
    }

    @Override
    public boolean accept(Class<?> c) {

        return readerMap.containsKey(c) && writerMap.containsKey(c);
    }

    @Override
    public Serializer<Object> getSerializer(Class<Object> c) {

        return new MzJavaSerializer(c);
    }

    @Override
    public Deserializer<Object> getDeserializer(Class<Object> c) {

        return new MzJavaDeserializer(c);
    }

    private AvroWriter getWriter(Class c) {

        return writerMap.get(c);
    }

    private AvroReader getReader(Class c) {

        return readerMap.get(c);
    }

    class MzJavaSerializer implements Serializer<Object> {

        private final AvroWriter writer;
        private BinaryEncoder encoder;
        private OutputStream outStream;

        MzJavaSerializer(Class<Object> clazz) {

            this.writer = getWriter(clazz);
        }

        @Override
        public void open(OutputStream out) throws IOException {

            outStream = out;
            encoder = EncoderFactory.get().directBinaryEncoder(out, encoder);
        }

        @Override
        public void serialize(Object t) throws IOException {

            writer.write(t, encoder);
        }

        @Override
        public void close() throws IOException {

            encoder.flush();
            outStream.close();
        }
    }

    class MzJavaDeserializer implements Deserializer<Object> {

        private final AvroReader reader;
        private BinaryDecoder decoder;
        private InputStream inStream;

        MzJavaDeserializer(Class<Object> clazz) {

            this.reader = getReader(clazz);
        }

        @Override
        public void open(InputStream in) throws IOException {

            inStream = in;
            decoder = DecoderFactory.get().directBinaryDecoder(in, decoder);
        }

        @Override
        public Object deserialize(Object t) throws IOException {

            return reader.read(decoder);
        }

        @Override
        public void close() throws IOException {

            inStream.close();
        }
    }
}
