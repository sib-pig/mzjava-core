/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.hadoop.io;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.RawComparator;
import org.apache.hadoop.io.SequenceFile;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.spectrum.Spectrum;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class HadoopSpectraWriter<S extends Spectrum> implements Closeable {

    private final SequenceFile.CompressionType compressionType;
    private final FileSystem fs;
    private final Path path;
    private final Path tmpPath;

    private SequenceFile.Writer writer;

    private final SpectrumKey key;
    private final AbstractPeakListValue<S> value;
    private final RawComparator<SpectrumKey> rawKeyComparator = new DefaultSpectrumKeyRawComparator();

    private HadoopSpectraWriter(FileSystem fs, Path path, AbstractPeakListValue<S> value, SequenceFile.CompressionType compressionType) {

        this.fs = fs;
        key = new SpectrumKey();
        this.value = value;

        this.path = path;
        this.tmpPath = new Path(path.getParent(), path.getName() + ".tmp");
        this.compressionType = compressionType;
    }

    public HadoopSpectraWriter(String path, String name, AbstractPeakListValue<S> value) throws IOException {

        this(FileSystem.getLocal(new Configuration()), new Path(path, name), value, SequenceFile.CompressionType.NONE);
    }

    public HadoopSpectraWriter(String path, String name, AbstractPeakListValue<S> value, Configuration conf, SequenceFile.CompressionType compressionType) throws IOException {

        this(FileSystem.getLocal(conf), new Path(path, name), value, compressionType);
    }

    public void write(S spectrum) throws IOException {

        if (writer == null) createWriter(tmpPath);

        key.set(spectrum.getPrecursor());
        value.set(spectrum);

        writer.append(key, value);
    }

    public void close() throws IOException {

        if (writer != null) {

            writer.close();
            SequenceFile.Sorter sorter = new SequenceFile.Sorter(fs, rawKeyComparator, key.getClass(), value.getClass(), fs.getConf());
            sorter.sort(new Path[]{tmpPath}, path, true);
        } else {

            createWriter(path);
            writer.close();
        }
    }

    private void createWriter(Path writerPath) throws IOException {

        writer = SequenceFile.createWriter(fs.getConf(),
                SequenceFile.Writer.file(writerPath),
                SequenceFile.Writer.keyClass(key.getClass()),
                SequenceFile.Writer.valueClass(value.getClass()),
                SequenceFile.Writer.compression(compressionType));
    }

    public static HadoopSpectraWriter<MsnSpectrum> msnWriter(File file) throws IOException {

        return new HadoopSpectraWriter<>(file.getParent(), file.getName(), new MsnSpectrumValue());
    }
}
