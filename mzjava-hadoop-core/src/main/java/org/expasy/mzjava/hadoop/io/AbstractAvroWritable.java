package org.expasy.mzjava.hadoop.io;

import org.apache.avro.io.Decoder;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AvroReader;
import org.expasy.mzjava.avro.io.AvroWriter;

import java.io.IOException;

/**
 * Serves as an adapter between Avro and Hadoop serialization.
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public abstract class AbstractAvroWritable<T> extends AvroWritable {

    private final AvroWriter<T> writer;
    private final AvroReader<T> reader;

    private T value;

    protected AbstractAvroWritable(AvroWriter<T> writer, AvroReader<T> reader) {

        this.writer = writer;
        this.reader = reader;
    }

    protected AbstractAvroWritable(T value, AvroWriter<T> writer, AvroReader<T> reader) {

        this.value = value;
        this.writer = writer;
        this.reader = reader;
    }

    @Override
    protected void avroWrite(Encoder encoder) throws IOException {

        writer.write(value, encoder);
    }

    @Override
    protected void avroRead(Decoder decoder) throws IOException {

        value = reader.read(decoder);
    }

    public T get() {

        return value;
    }

    public void set(T value) {

        this.value = value;
    }
}