/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.avro.io;

import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeList;
import org.expasy.mzjava.core.ms.spectrum.ScanNumber;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberList;

import java.io.IOException;
import java.net.URI;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class MsnSpectrumMetaDataReader {

    private final ScanNumberListReader scanNumberListReader = new ScanNumberListReader();
    private final RetentionTimeListReader retentionTimeListReader = new RetentionTimeListReader();

    private final AbstractAvroReader<? extends MsnSpectrum> reader;

    private int spectrumIndex;
    private ScanNumber parentScanNumber;
    private String fragMethod;
    private RetentionTimeList retentionTimes;
    private ScanNumberList scanNumbers;
    private URI spectrumSource;
    private String comment;

    public MsnSpectrumMetaDataReader(AbstractAvroReader<? extends MsnSpectrum> reader) {

        this.reader = reader;
        reader.registerUnion(ScanNumber.class, new ScanNumberDiscreteReader(), new ScanNumberIntervalReader());
    }

    public void read(Decoder in) throws IOException {

        spectrumIndex = in.readInt();
        parentScanNumber = reader.readUnion(ScanNumber.class, in);

        fragMethod = in.readString();
        retentionTimes = retentionTimeListReader.read(in);
        scanNumbers = scanNumberListReader.read(in);

        spectrumSource = URI.create(in.readString());
        comment = in.readString();
    }

    public void setValues(MsnSpectrum spectrum) throws IOException {

        spectrum.setSpectrumIndex(spectrumIndex);
        spectrum.setParentScanNumber(parentScanNumber);

        spectrum.setFragMethod(fragMethod);
        spectrum.addRetentionTimes(retentionTimes);
        spectrum.addScanNumbers(scanNumbers);

        spectrum.setSpectrumSource(spectrumSource);
        spectrum.setComment(comment);
    }

    protected void addFields(List<Schema.Field> fields) {

        fields.add(reader.createSchemaField("spectrumIndex", Schema.create(Schema.Type.INT)));
        fields.add(reader.createSchemaField("parentScanNumber", reader.createUnionSchema(ScanNumber.class)));

        fields.add(reader.createSchemaField("fragMethod", Schema.create(Schema.Type.STRING)));
        fields.add(reader.createSchemaField("retentionTimes", retentionTimeListReader.createSchema()));
        fields.add(reader.createSchemaField("scanNumbers", scanNumberListReader.createSchema()));

        fields.add(reader.createSchemaField("source", Schema.create(Schema.Type.STRING)));
        fields.add(reader.createSchemaField("comment", Schema.create(Schema.Type.STRING)));
    }
}
