package org.expasy.mzjava.avro.io;

import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.core.ms.spectrum.LibPeakAnnotation;

import java.io.IOException;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public abstract class AbstractLibPeakAnnotationWriter<A extends LibPeakAnnotation> extends AbstractAvroWriter<A> {

    public void writeLibAnnotation(LibPeakAnnotation annotation, Encoder out) throws IOException {

        out.writeInt(annotation.getMergedPeakCount());
        out.writeDouble(annotation.getMzStd());
        out.writeDouble(annotation.getIntensityStd());
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("mergedPeakCount", Schema.create(Schema.Type.INT)));
        fields.add(createSchemaField("mzStd", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("intensityStd", Schema.create(Schema.Type.DOUBLE)));
    }
}
