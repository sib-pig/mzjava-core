/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.avro.io;

import org.apache.avro.Schema;

import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * Base class for MzJava Hadoop Avro IO.
 *
 * Helps with creating Avro schemas. Works with both AvroWriter and and AvroReader implementations
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public abstract class AvroIO<U extends AvroExternalizable> {

    private final Map<Class, List<U>> unionMap = new HashMap<>();

    /**
     * Creates a schema for this reader or writer
     *
     * @return the schema
     */
    public Schema createSchema(){

        Schema schema = Schema.createRecord(getRecordName(), getRecordDoc(), getRecordNameSpace(), false);

        List<Schema.Field> recordFields = new ArrayList<>(10);
        createRecordFields(recordFields);
        schema.setFields(recordFields);

        return schema;
    }

    /**
     * Create a list containing the Schema.Fields for the fields that this class writes
     *
     * @param fields the list to add the fields to
     */
    protected abstract void createRecordFields(List<Schema.Field> fields);

    /**
     * Returns the documentation for this record
     *
     * Default is to have no documentation, subclasses can override this to provide doc
     *
     * @return the documentation for this record
     */
    protected String getRecordDoc() {

        return null;
    }

    /**
     * Return the name of this record
     *
     * @return the name of this record
     */
    protected abstract String getRecordName();

    /**
     * Return the namespace of this record
     *
     * @return the namespace of this record
     */
    protected abstract String getRecordNameSpace();

    /**
     * Rewrite the class namespace to the serialization namespace
     *
     * @param clazz the class
     * @return the rewritten namespace
     */
    protected String rewriteNameSpace(Class clazz) {

        return clazz.getPackage().getName().replace(".mzjava.", ".mzjava_avro.");
    }

    /**
     * Utility method for creating a schema for the union for the recordSuperclass for unions that were registered with
     * registerUnion
     *
     * @param recordSuperClass the base class
     * @return the schema for the union for the recordSuperclass
     */
    public Schema createUnionSchema(Class recordSuperClass) {

        checkNotNull(recordSuperClass);
        if(!unionMap.containsKey(recordSuperClass)) throw new IllegalStateException("No union is registered for " + recordSuperClass.getSimpleName() + " to register one call the registerUnion() method");

        List<Schema> types = new ArrayList<>();
        for(U writer : unionMap.get(recordSuperClass))
            types.add(writer.createSchema());

        return Schema.createUnion(types);
    }

    /**
     * Utility method for creating a schema for the union for the recordSuperclass for unions that were registered with
     * registerUnion
     *
     * @param recordSuperClass the base class
     * @return the schema for the union for the recordSuperclass
     */
    protected Schema createUnionSchema(Class recordSuperClass, final AvroExternalizable... members) {

        checkNotNull(recordSuperClass);

        List<Schema> types = new ArrayList<>();
        for(AvroExternalizable writer : members)
            types.add(writer.createSchema());

        return Schema.createUnion(types);
    }

    /**
     * Utility method for creating a Field
     *
     * @param name the field name
     * @param schema the schema for the field
     * @return new Schema.Field
     */
    public Schema.Field createSchemaField(String name, Schema schema) {

        return new Schema.Field(name, schema, null, null);
    }

    protected Schema createEnumSchema(Class<? extends Enum> clazz, Enum... enums) {

        List<String> values = new ArrayList<>(enums.length);
        for (Enum aEnum : enums) {

            values.add(aEnum.name());
        }
        return Schema.createEnum(clazz.getSimpleName(), null, clazz.getPackage().getName(), values);
    }

    /**
     * Register a union
     *
     * @param recordSuperclass the superclass of the classes in the union
     * @param externalizables the readers or externalizables for the classes in the union
     */
    public void registerUnion(Class recordSuperclass, U... externalizables) {

        checkNotNull(recordSuperclass);
        checkNotNull(externalizables);

        for (U externalizable : externalizables) {

            Class recordClass = externalizable.getObjectClass();
            checkState(recordSuperclass.isAssignableFrom(recordClass));
        }

        unionMap.put(recordSuperclass, Arrays.asList(externalizables));
    }

    /**
     * Return the readers or writers for the union of the recordSuperclass
     *
     * @param recordSuperclass the superclass
     * @return the readers or writers for the union of the recordSuperclass
     */
    protected List<U> getUnion(Class recordSuperclass){

        List<U> objectWriters = unionMap.get(recordSuperclass);

        if(objectWriters == null) throw new IllegalStateException("No classes are registered for a union of " + recordSuperclass.getSimpleName() + " unions can be registered using registerUnion()");

        return objectWriters;
    }
}
