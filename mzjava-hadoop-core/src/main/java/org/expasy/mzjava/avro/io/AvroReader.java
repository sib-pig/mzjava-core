package org.expasy.mzjava.avro.io;

import org.apache.avro.io.Decoder;

import java.io.IOException;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public interface AvroReader<O> extends AvroExternalizable {

    /**
     * Read the object from the decoder
     *
     * @param in the decoder
     * @return the read object
     * @throws java.io.IOException
     */
    O read(Decoder in) throws IOException;
}

