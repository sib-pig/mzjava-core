/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.avro.io;

import com.google.common.base.Optional;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.core.ms.peaklist.*;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroReader.class)
public class PeakListReader extends AbstractPeakListReader<PeakAnnotation, PeakList<PeakAnnotation>> {

    private final PeakReader precursorReader = new PeakReader();

    public PeakListReader() {

        this(Optional.<PeakList.Precision>absent(), Collections.<PeakProcessor<PeakAnnotation, PeakAnnotation>>emptyList());
    }

    public PeakListReader(Optional<PeakList.Precision> precisionOverride, List<PeakProcessor<PeakAnnotation, PeakAnnotation>> peakProcessorList) {

        super(new AbstractAvroReader[0], precisionOverride, peakProcessorList);
    }

    @Override
    public Class getObjectClass() {

        return PeakList.class;
    }

    @Override
    protected PeakList<PeakAnnotation> newPeakList(PeakList.Precision precision, double constantIntensity) {

        PeakList<PeakAnnotation> peakList;

        switch (precision) {

            case DOUBLE:

                peakList = new DoublePeakList<>();
                break;
            case FLOAT:

                peakList = new FloatPeakList<>();
                break;
            case DOUBLE_FLOAT:

                peakList = new DoubleFloatPeakList<>();
                break;
            case DOUBLE_CONSTANT:

                peakList = new DoubleConstantPeakList<>(constantIntensity);
                break;
            case FLOAT_CONSTANT:

                peakList = new FloatConstantPeakList<>(constantIntensity);
                break;
            default:

                throw new IllegalStateException("Cannot create peak list with precision " + precision);
        }

        return peakList;
    }

    @Override
    public PeakList<PeakAnnotation> read(Decoder in) throws IOException {

        Peak precursor = precursorReader.read(in);
        PeakList<PeakAnnotation> peakList = super.read(in);
        peakList.setPrecursor(precursor);
        return peakList;
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("precursor", precursorReader.createSchema()));
        super.createRecordFields(fields);
    }
}
