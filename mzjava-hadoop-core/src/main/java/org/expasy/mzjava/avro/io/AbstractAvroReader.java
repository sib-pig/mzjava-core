/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.avro.io;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import gnu.trove.map.TObjectDoubleMap;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import org.apache.avro.io.Decoder;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Base class for reading objects using the Avro Decoder
 *
 * @author Oliver Horlacher
 * @version 1.0
 */
public abstract class AbstractAvroReader<O> extends AvroIO<AbstractAvroReader> implements AvroReader<O> {

    @Override
    protected String getRecordName() {

        return getObjectClass().getSimpleName();
    }

    @Override
    protected String getRecordNameSpace() {

        return rewriteNameSpace(getObjectClass());
    }

    /**
     * Read a union
     *
     * @param recordSuperclass the super class of the union
     * @param in the decoder
     * @param <C> the class of the object to be read
     * @return the the record superclass
     * @throws IOException
     */
    public <C> C readUnion(Class<C> recordSuperclass, Decoder in) throws IOException {

        int index = in.readIndex();

        AbstractAvroReader reader = getUnion(recordSuperclass).get(index);

        //noinspection unchecked
        return (C)reader.read(in);
    }

    /**
     * Read an array that contains unions of classes that have recordSuperclass as a superclass
     *
     * @param recordSuperclass the superclass of the union
     * @param collection the collection to read the array into
     * @param in the decoder
     * @param <C> the record superclass
     * @throws IOException
     */
    protected <C> void readUnionArray(Class<C> recordSuperclass, Collection<C> collection, Decoder in) throws IOException {

        for(long i = in.readArrayStart(); i != 0; i = in.arrayNext()) {
            for (long j = 0; j < i; j++) {

                collection.add(readUnion(recordSuperclass, in));
            }
        }
    }

    /**
     * Read an array that contains records into collection
     *
     * @param reader the reader
     * @param collection the collection
     * @param in the decoder
     * @param <E> the record class
     * @throws IOException
     */
    protected <C extends Collection<E>, E> C readArray(AbstractAvroReader<E> reader, C collection, Decoder in) throws IOException {

        for(long i = in.readArrayStart(); i != 0; i = in.arrayNext()) {
            for (long j = 0; j < i; j++) {

                collection.add(reader.read(in));
            }
        }

        return collection;
    }

    /**
     * Read an the int's from an array into the provided collection.
     *
     * @param in the decoder
     * @param collection the collection
     * @return the collection
     * @throws IOException
     */
    protected <C extends Collection<Integer>> C readIntArray(C collection, Decoder in) throws IOException {

        for(long i = in.readArrayStart(); i != 0; i = in.arrayNext()) {
            for (long j = 0; j < i; j++) {

                collection.add(in.readInt());
            }
        }

        return collection;
    }

    /**
     * Read an the String's from an array into the provided collection.
     *
     * @param in the decoder
     * @param collection the collection
     * @return the collection
     * @throws IOException
     */
    protected <C extends Collection<String>> C readStringArray(C collection, Decoder in) throws IOException {

        for(long i = in.readArrayStart(); i != 0; i = in.arrayNext()) {
            for (long j = 0; j < i; j++) {

                collection.add(in.readString());
            }
        }

        return collection;
    }

    /**
     * Read an union of C and null and return a Optional<C>.of() or Optional<C>.absent()
     *
     * @param reader the reader if the value is not null
     * @param in the decoder
     * @param <C> the record class
     * @return Optional containing the read record if the record is not null, Optional.absent otherwise
     * @throws IOException
     */
    protected <C> Optional<C> readOptional(AbstractAvroReader<C> reader, Decoder in) throws IOException {

        int index = in.readIndex();

        if(index == 0) return Optional.of(reader.read(in));
        else {
            in.readNull();
            return Optional.absent();
        }
    }

    protected <K, V> Map<K, V> readMap(Function<String, K> keyFunction, Function<Decoder, V> valueFunction, Decoder in) throws IOException {

        Map<K, V> map = new HashMap<>();
        for(long i = in.readMapStart(); i != 0; i = in.mapNext()) {
            for (long j = 0; j < i; j++) {

                map.put(keyFunction.apply(in.readString()), valueFunction.apply(in));
            }
        }

        return map;
    }

    protected <K> TObjectDoubleMap<K> readMap(Function<String, K> keyFunction, Decoder in) throws IOException {

        TObjectDoubleMap<K> map = new TObjectDoubleHashMap<>();
        for(long i = in.readMapStart(); i != 0; i = in.mapNext()) {
            for (long j = 0; j < i; j++) {

                map.put(keyFunction.apply(in.readString()), in.readDouble());
            }
        }

        return map;
    }

    protected <K, V> Map<K, V> readMap(Function<String, K> keyFunction, AbstractAvroReader<V> reader, Decoder in) throws IOException {

        Map<K, V> map = new HashMap<>();
        for(long i = in.readMapStart(); i != 0; i = in.mapNext()) {
            for (long j = 0; j < i; j++) {

                map.put(keyFunction.apply(in.readString()), reader.read(in));
            }
        }

        return map;
    }
}
