package org.expasy.mzjava.avro.io;

import com.google.common.base.Preconditions;
import org.apache.avro.Schema;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.Decoder;

import java.io.IOException;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class AvroDatumReader <D> implements DatumReader<D> {

    private final AbstractAvroReader<D> reader;
    private Schema schema;

    public AvroDatumReader(final AbstractAvroReader<D> reader) {

        this.reader = reader;
    }

    @Override
    public void setSchema(final Schema schema) {

        if(this.schema == null){
            this.schema = reader.createSchema();
        }
        Preconditions.checkState(schema.equals(this.schema));
    }

    @Override
    public D read(final D reuse, final Decoder in) throws IOException {

        return reader.read(in);
    }
}
