/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.avro.io;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import gnu.trove.iterator.TObjectDoubleIterator;
import gnu.trove.map.TObjectDoubleMap;
import org.apache.avro.io.Encoder;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Base class for writing objects using Avro Encoder
 *
 * @author Oliver Horlacher
 * @version 1.0
 */
public abstract class AbstractAvroWriter<O> extends AvroIO<AbstractAvroWriter> implements AvroWriter<O> {

    @Override
    protected String getRecordName() {

        return getObjectClass().getSimpleName();
    }

    @Override
    protected String getRecordNameSpace() {

        return rewriteNameSpace(getObjectClass());
    }

    /**
     * Write a union
     *
     * @param recordSuperclass the superclass of the union
     * @param o the object
     * @param out the encoder
     * @param <C> the class of the object to be written
     * @throws IOException
     */
    public <C> void writeUnion(Class<C> recordSuperclass, C o, Encoder out) throws IOException {

        Class objectClass = o.getClass();
        AbstractAvroWriter writer = null;

        int unionIndex = 0;
        for(AbstractAvroWriter candidate : getUnion(recordSuperclass)) {

            if(objectClass.equals(candidate.getObjectClass())) {

                writer = candidate;
                break;
            }

            unionIndex++;
        }

        if(writer == null) throw new IllegalStateException("Cannot find a writer for the union " + recordSuperclass + " and class " + objectClass);

        out.writeIndex(unionIndex);
        //noinspection unchecked
        writer.write(o, out);
    }

    /**
     * Write an Optional, this writes a union with null
     *
     * @param writer the writer for the object in the Optional
     * @param optional the Optional
     * @param out the encoder
     * @param <C> the class of the object to be written
     * @throws IOException
     */
    protected <C> void writeOptional(AbstractAvroWriter<C> writer, Optional<C> optional, Encoder out) throws IOException {

        if (optional.isPresent()) {

            out.writeIndex(0);
            writer.write(optional.get(), out);
        } else {

            out.writeIndex(1);
            out.writeNull();
        }
    }

    /**
     * Write an array containing a union
     *
     * @param recordSuperclass the superclass of the union
     * @param list the collection that contains the values that are to be written
     * @param out the encoder
     * @param <C> The class of the superclass
     * @throws IOException
     */
    protected <C> void writeUnionArray(Class<C> recordSuperclass, Collection<C> list, Encoder out) throws IOException {

        out.writeArrayStart();
        out.setItemCount(list.size());

        for(C item : list) {

            out.startItem();
            writeUnion(recordSuperclass, item, out);
        }
        out.writeArrayEnd();
    }

    /**
     * Write an array of the objects in list
     *
     * @param writer the writer for the objects on the collection
     * @param list the collection to be written
     * @param out the encoder
     * @param <C> the class of the objects in the collection
     * @throws IOException
     */
    protected <C> void writeArray(AbstractAvroWriter<C> writer, Collection<C> list, Encoder out) throws IOException {

        out.writeArrayStart();
        out.setItemCount(list.size());

        for(C item : list) {

            out.startItem();
            writer.write(item, out);
        }
        out.writeArrayEnd();
    }

    /**
     * Write an array of the objects in list
     *
     * @param integers the array to be written
     * @param out the encoder
     * @throws IOException
     */
    protected void writeIntArray(List<Integer> integers, Encoder out) throws IOException {

        out.writeArrayStart();
        out.setItemCount(integers.size());

        for(int item : integers) {

            out.startItem();
            out.writeInt(item);
        }
        out.writeArrayEnd();
    }

    /**
     * Writes a collection containing strings as an array
     *
     * @param strings the collection of strings
     * @param out the encoder
     * @throws IOException
     */
    protected void writeStringArray(Collection<String> strings, Encoder out) throws IOException {

        out.writeArrayStart();
        out.setItemCount(strings.size());

        for(String item : strings) {

            out.startItem();
            out.writeString(item);
        }
        out.writeArrayEnd();
    }

    protected <K, V> void writeMap(AbstractAvroWriter<V> writer, Map<K, V> map, Function<K, String> keyExtractor, Encoder out) throws IOException {

        out.writeMapStart();
        out.setItemCount(map.size());
        for (Map.Entry<K, V> entry : map.entrySet()) {
            out.startItem();
            out.writeString(extractKey(keyExtractor, entry.getKey()));
            writer.write(entry.getValue(), out);
        }
        out.writeMapEnd();
    }

    protected <K> void writeMap(TObjectDoubleMap<K> map, final Function<K, String> keyExtractor, final Encoder out) throws IOException {

        out.writeMapStart();
        out.setItemCount(map.size());
        for (TObjectDoubleIterator<K> it = map.iterator(); it.hasNext(); ){

            it.advance();
            out.startItem();
            out.writeString(extractKey(keyExtractor, it.key()));
            out.writeDouble(it.value());
        }
        out.writeMapEnd();
    }

    protected <K> void writeMap(Map<K, ? extends Number> map, Function<K, String> keyExtractor, Encoder out) throws IOException {

        out.writeMapStart();
        out.setItemCount(map.size());
        for (Map.Entry<K, ? extends Number> entry : map.entrySet()) {
            out.startItem();
            out.writeString(extractKey(keyExtractor, entry.getKey()));
            Number number = entry.getValue();
            if(number instanceof Byte)
                out.writeInt(number.intValue());
            else if(number instanceof Integer)
                out.writeInt(number.intValue());
            else if(number instanceof Long)
                out.writeDouble(number.longValue());
            else if(number instanceof Short)
                out.writeInt(number.shortValue());
            else if(number instanceof Float)
                out.writeFloat(number.floatValue());
            else if(number instanceof Double)
                out.writeDouble(number.doubleValue());
        }
        out.writeMapEnd();
    }

    private <K> String extractKey(Function<K, String> keyExtractor, K key) {

        final String newKey = keyExtractor.apply(key);
        Preconditions.checkNotNull(newKey);
        return newKey;
    }
}
