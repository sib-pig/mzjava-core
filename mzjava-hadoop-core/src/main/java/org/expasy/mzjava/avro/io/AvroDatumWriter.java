package org.expasy.mzjava.avro.io;

import com.google.common.base.Preconditions;
import org.apache.avro.Schema;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.Encoder;

import java.io.IOException;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class AvroDatumWriter <D> implements DatumWriter<D> {

    private final AbstractAvroWriter<D> writer;
    private Schema schema;

    public AvroDatumWriter(final AbstractAvroWriter<D> writer) {

        this.writer = writer;
    }

    @Override
    public void setSchema(final Schema schema) {

        if(this.schema == null){
            this.schema = writer.createSchema();
        }
        Preconditions.checkState(schema.equals(this.schema));
    }

    @Override
    public void write(final D datum, final Encoder out) throws IOException {

        writer.write(datum, out);
    }
}
