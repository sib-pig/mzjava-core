/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.avro.io;

import com.google.common.base.Optional;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessor;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.spectrum.ScanNumber;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroReader.class)
public class MsnSpectrumReader extends AbstractPeakListReader<PeakAnnotation, MsnSpectrum> {

    private final PeakReader precursorReader = new PeakReader();
    private final MsnSpectrumMetaDataReader metaDataReader;

    public MsnSpectrumReader() {

        this(new AbstractAvroReader[0], Optional.<PeakList.Precision>absent(), Collections.<PeakProcessor<PeakAnnotation, PeakAnnotation>>emptyList());
    }

    public MsnSpectrumReader(AbstractAvroReader<PeakAnnotation> annotationReader, Optional<PeakList.Precision> precisionOverride, List<PeakProcessor<PeakAnnotation, PeakAnnotation>> peakProcessors) {

        this(new AbstractAvroReader[]{annotationReader}, precisionOverride, peakProcessors);
    }

    public MsnSpectrumReader(AbstractAvroReader<? extends PeakAnnotation>[] annotationReaders) {

        this(annotationReaders, Optional.<PeakList.Precision>absent(), Collections.<PeakProcessor<PeakAnnotation, PeakAnnotation>>emptyList());
    }

    public MsnSpectrumReader(AbstractAvroReader<? extends PeakAnnotation>[] annotationReaders, Optional<PeakList.Precision> precisionOverride, List<PeakProcessor<PeakAnnotation, PeakAnnotation>> peakProcessors) {

        super(annotationReaders, precisionOverride, peakProcessors);

        registerUnion(ScanNumber.class, new ScanNumberDiscreteReader(), new ScanNumberIntervalReader());
        metaDataReader = new MsnSpectrumMetaDataReader(this);
    }

    @Override
    public Class getObjectClass() {

        return MsnSpectrum.class;
    }

    @Override
    protected MsnSpectrum newPeakList(PeakList.Precision precision, double constantIntensity) {

        if (precision == PeakList.Precision.FLOAT_CONSTANT || precision == PeakList.Precision.DOUBLE_CONSTANT) {

            return new MsnSpectrum(100, constantIntensity, precision);
        } else {

            return new MsnSpectrum(precision);
        }
    }

    @Override
    public MsnSpectrum read(Decoder in) throws IOException {

        Peak precursor = precursorReader.read(in);
        int msLevel = in.readInt();

        metaDataReader.read(in);

        MsnSpectrum spectrum = super.read(in);
        spectrum.setPrecursor(precursor);
        spectrum.setMsLevel(msLevel);

        metaDataReader.setValues(spectrum);

        return spectrum;
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("precursor", precursorReader.createSchema()));
        fields.add(createSchemaField("msLevel", Schema.create(Schema.Type.INT)));

        metaDataReader.addFields(fields);

        super.createRecordFields(fields);
    }
}
