/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.avro.io;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public abstract class AbstractPeakListWriter<PL extends PeakList> extends AbstractAvroWriter<PL> {

    private final UUIDWriter uuidWriter = new UUIDWriter();

    private final Optional<PeakList.Precision> precisionOverride;

    private final boolean handleAnnotations;

    protected AbstractPeakListWriter(Optional<PeakList.Precision> precisionOverride, AbstractAvroWriter<? extends PeakAnnotation>... annotationWriters) {

        handleAnnotations = annotationWriters.length > 0;
        registerUnion(PeakAnnotation.class, annotationWriters);
        this.precisionOverride = precisionOverride;
    }

    protected void writePeakList(PeakList peakList, Encoder out) throws IOException {

        int size = peakList.size();
        PeakList.Precision precision;
        if (precisionOverride.isPresent()) {
            precision = precisionOverride.get();
        } else {
            precision = peakList.getPrecision();
        }

        NUMERIC_PRECISION mzPrecision;
        switch (precision) {

            case DOUBLE:
            case DOUBLE_FLOAT:
            case DOUBLE_CONSTANT:
                mzPrecision = NUMERIC_PRECISION.DOUBLE;
                break;
            case FLOAT:
            case FLOAT_CONSTANT:
                mzPrecision = NUMERIC_PRECISION.FLOAT;
                break;
            default:
                throw new IllegalStateException("Cannot encode " + precision);
        }

        NUMERIC_PRECISION intensityPrecision;
        switch (precision) {

            case DOUBLE:
                intensityPrecision = NUMERIC_PRECISION.DOUBLE;
                break;
            case FLOAT:
            case DOUBLE_FLOAT:
                intensityPrecision = NUMERIC_PRECISION.FLOAT;
                break;
            case DOUBLE_CONSTANT:
            case FLOAT_CONSTANT:
                intensityPrecision = NUMERIC_PRECISION.CONSTANT;
                break;
            default:
                throw new IllegalStateException("Cannot encode " + precision);
        }
        boolean writeAnnotations = handleAnnotations && peakList.hasAnnotations();

        uuidWriter.write(peakList.getId(), out);
        out.writeEnum(precision.ordinal());
        //Indicate the record based on precision and if we have annotations
        out.writeIndex(precision.ordinal() + (writeAnnotations ? 5 : 0));
        if(precision == PeakList.Precision.DOUBLE_CONSTANT || precision == PeakList.Precision.FLOAT_CONSTANT){

            out.writeDouble(peakList.isEmpty() ? 0 : peakList.getIntensity(0));
        }

        out.writeArrayStart();
        out.setItemCount(size);
        for(int i = 0; i < size; i++) {

            out.startItem();

            if (mzPrecision == NUMERIC_PRECISION.DOUBLE) {

                out.writeDouble(peakList.getMz(i));
            } else {

                out.writeFloat((float) peakList.getMz(i));
            }

            if (intensityPrecision == NUMERIC_PRECISION.DOUBLE) {

                out.writeDouble(peakList.getIntensity(i));
            } else if(intensityPrecision == NUMERIC_PRECISION.FLOAT) {

                out.writeFloat((float) peakList.getIntensity(i));
            }

            if (writeAnnotations) {
                //noinspection unchecked
                writeUnionArray(PeakAnnotation.class, peakList.getAnnotations(i), out);
            }
        }
        out.writeArrayEnd();
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("id", uuidWriter.createSchema()));
        fields.add(createSchemaField("precision", createEnumSchema(PeakList.Precision.class, PeakList.Precision.values())));

        final List<Schema> types = Lists.newArrayList(
                createPeakSchema("Double", Schema.Type.DOUBLE, Schema.Type.DOUBLE, false),
                createPeakSchema("Float", Schema.Type.FLOAT, Schema.Type.FLOAT, false),
                createPeakSchema("DoubleFloat", Schema.Type.DOUBLE, Schema.Type.FLOAT, false),
                createPeakSchema("DoubleConstant", Schema.Type.DOUBLE, Schema.Type.NULL, false),
                createPeakSchema("FloatConstant", Schema.Type.FLOAT, Schema.Type.NULL, false)
        );
        if (handleAnnotations) {

            types.add(createPeakSchema("AnnotatedDouble", Schema.Type.DOUBLE, Schema.Type.DOUBLE, true));
            types.add(createPeakSchema("AnnotatedFloat", Schema.Type.FLOAT, Schema.Type.FLOAT, true));
            types.add(createPeakSchema("AnnotatedDoubleFloat", Schema.Type.DOUBLE, Schema.Type.FLOAT, true));
            types.add(createPeakSchema("AnnotatedDoubleConstant", Schema.Type.DOUBLE, Schema.Type.NULL, true));
            types.add(createPeakSchema("AnnotatedFloatConstant", Schema.Type.FLOAT, Schema.Type.NULL, true));
        }

        fields.add(createSchemaField("peaks", Schema.createUnion(types)));
    }

    private Schema createPeakSchema(String name, Schema.Type mzType, Schema.Type intensityType, boolean hasAnnotations) {

        final String namespace = rewriteNameSpace(PeakList.class);
        Schema peakSchema = Schema.createRecord(name + "Peak", null, namespace, false);

        List<Schema.Field> fields = new ArrayList<>(2);
        fields.add(createSchemaField("mz", Schema.create(mzType)));
        if (intensityType != Schema.Type.NULL) {

            fields.add(createSchemaField("i", Schema.create(intensityType)));
        }
        if(hasAnnotations) {

            fields.add(createSchemaField("annotations", Schema.createArray(createUnionSchema(PeakAnnotation.class))));
        }
        peakSchema.setFields(fields);

        Schema schema = Schema.createRecord(name + "PeakList", null, namespace, false);
        List<Schema.Field> peakListFields = new ArrayList<>();
        if (intensityType == Schema.Type.NULL) {

            peakListFields.add(createSchemaField("intensity", Schema.create(Schema.Type.DOUBLE)));
        }
        peakListFields.add(createSchemaField("peaks", Schema.createArray(peakSchema)));
        schema.setFields(peakListFields);

        return schema;
    }

    private enum NUMERIC_PRECISION {DOUBLE, FLOAT, CONSTANT}
}
