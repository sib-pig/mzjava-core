/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.avro.io;

import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.spectrum.ScanNumber;

import java.io.IOException;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class MsnSpectrumMetaDataWriter {

    private final ScanNumberListWriter scanNumberListWriter = new ScanNumberListWriter();
    private final RetentionTimeListWriter retentionTimeListWriter = new RetentionTimeListWriter();

    private final MsnSpectrumWriter writer;

    public MsnSpectrumMetaDataWriter(MsnSpectrumWriter writer) {

        this.writer = writer;
        writer.registerUnion(ScanNumber.class, new ScanNumberDiscreteWriter(), new ScanNumberIntervalWriter());
    }

    public void writeMetaData(MsnSpectrum spectrum, Encoder out) throws IOException {

        out.writeInt(spectrum.getSpectrumIndex());
        writer.writeUnion(ScanNumber.class, spectrum.getParentScanNumber(), out);

        out.writeString(spectrum.getFragMethod());
        retentionTimeListWriter.write(spectrum.getRetentionTimes(), out);
        scanNumberListWriter.write(spectrum.getScanNumbers(), out);

        out.writeString(spectrum.getSpectrumSource().toString());
        out.writeString(spectrum.getComment());
    }

    protected void addFields(List<Schema.Field> fields) {

        fields.add(writer.createSchemaField("spectrumIndex", Schema.create(Schema.Type.INT)));
        fields.add(writer.createSchemaField("parentScanNumber", writer.createUnionSchema(ScanNumber.class)));

        fields.add(writer.createSchemaField("fragMethod", Schema.create(Schema.Type.STRING)));
        fields.add(writer.createSchemaField("retentionTimes", retentionTimeListWriter.createSchema()));
        fields.add(writer.createSchemaField("scanNumbers", scanNumberListWriter.createSchema()));

        fields.add(writer.createSchemaField("source", Schema.create(Schema.Type.STRING)));
        fields.add(writer.createSchemaField("comment", Schema.create(Schema.Type.STRING)));
    }
}
