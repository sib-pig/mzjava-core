package org.expasy.mzjava.avro.io;

import org.apache.avro.io.Encoder;

import java.io.IOException;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public interface AvroWriter<O> extends AvroExternalizable {

    /**
     * Write the value to the encoder
     *
     * @param value the value to write
     * @param out the encoder
     * @throws java.io.IOException
     */
    void write(O value, Encoder out) throws IOException;
}

