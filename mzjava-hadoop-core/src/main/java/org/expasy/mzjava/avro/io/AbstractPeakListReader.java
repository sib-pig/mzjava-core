/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.avro.io;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessor;
import org.expasy.mzjava.core.ms.peaklist.PeakSink;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.function.BiFunction;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public abstract class AbstractPeakListReader<A extends PeakAnnotation, PL extends PeakList<A>> extends AbstractAvroReader<PL> {

    private final UUIDReader uuidReader = new UUIDReader();

    protected final Optional<PeakList.Precision> precisionOverride;
    protected final List<PeakProcessor<A, A>> peakProcessorList;
    protected final boolean handleAnnotations;

    public AbstractPeakListReader(AbstractAvroReader<? extends A>[] annotationReaders, Optional<PeakList.Precision> precisionOverride, List<PeakProcessor<A, A>> peakProcessorList) {

        this.handleAnnotations = annotationReaders.length > 0;
        this.peakProcessorList = peakProcessorList;
        this.precisionOverride = precisionOverride;

        registerUnion(PeakAnnotation.class, annotationReaders);
    }

    @Override
    public PL read(Decoder in) throws IOException {

        return read(in, this::newPeakList);
    }

    public PL read(Decoder in, BiFunction<PeakList.Precision, Double, PL> peakListSupplier) throws IOException {

        UUID id = uuidReader.read(in);
        PeakList.Precision precision = PeakList.Precision.values()[in.readEnum()];

        AbstractPeakReader peakReader;
        int peakListUnionIndex = in.readIndex();
        boolean annotated = peakListUnionIndex >= 5;

        double constantIntensity = 1;
        if (precision == PeakList.Precision.DOUBLE_CONSTANT || precision == PeakList.Precision.FLOAT_CONSTANT) {

            constantIntensity = in.readDouble();
        }
        PL peakList = peakListSupplier.apply(precisionOverride.isPresent() ? precisionOverride.get() : precision, constantIntensity);

        peakList.setId(id);
        switch (precision) {

            case DOUBLE:
                peakReader = new DoublePeakReader(peakList, peakProcessorList, annotated);
                break;
            case FLOAT:
                peakReader = new FloatPeakReader(peakList, peakProcessorList, annotated);
                break;
            case DOUBLE_FLOAT:
                peakReader = new DoubleFloatPeakReader(peakList, peakProcessorList, annotated);
                break;
            case DOUBLE_CONSTANT:
                peakReader = new DoubleConstantPeakReader(peakList, peakProcessorList, annotated);
                break;
            case FLOAT_CONSTANT:
                peakReader = new FloatConstantPeakReader(peakList, peakProcessorList, annotated);
                break;
            default:
                throw new IllegalStateException("Cannot read peak list union " + in.readIndex());
        }

        boolean started = false;
        for (long i = in.readArrayStart(); i != 0; i = in.arrayNext()) {
            if (!started) {
                peakReader.sourceStart((int) i);
                started = true;
            }
            for (long j = 0; j < i; j++) {

                peakReader.read(in);
            }
        }
        peakReader.sourceEnd();

        return peakList;
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("id", uuidReader.createSchema()));
        fields.add(createSchemaField("precision", createEnumSchema(PeakList.Precision.class, PeakList.Precision.values())));

        final List<Schema> types = Lists.newArrayList(
                createPeakSchema("Double", Schema.Type.DOUBLE, Schema.Type.DOUBLE, false),
                createPeakSchema("Float", Schema.Type.FLOAT, Schema.Type.FLOAT, false),
                createPeakSchema("DoubleFloat", Schema.Type.DOUBLE, Schema.Type.FLOAT, false),
                createPeakSchema("DoubleConstant", Schema.Type.DOUBLE, Schema.Type.NULL, false),
                createPeakSchema("FloatConstant", Schema.Type.FLOAT, Schema.Type.NULL, false)
        );
        if (handleAnnotations) {

            types.add(createPeakSchema("AnnotatedDouble", Schema.Type.DOUBLE, Schema.Type.DOUBLE, true));
            types.add(createPeakSchema("AnnotatedFloat", Schema.Type.FLOAT, Schema.Type.FLOAT, true));
            types.add(createPeakSchema("AnnotatedDoubleFloat", Schema.Type.DOUBLE, Schema.Type.FLOAT, true));
            types.add(createPeakSchema("AnnotatedDoubleConstant", Schema.Type.DOUBLE, Schema.Type.NULL, true));
            types.add(createPeakSchema("AnnotatedFloatConstant", Schema.Type.FLOAT, Schema.Type.NULL, true));
        }

        fields.add(createSchemaField("peaks", Schema.createUnion(types)));
    }

    private Schema createPeakSchema(String name, Schema.Type mzType, Schema.Type intensityType, boolean hasAnnotations) {

        final String namespace = rewriteNameSpace(PeakList.class);
        Schema peakSchema = Schema.createRecord(name + "Peak", null, namespace, false);

        List<Schema.Field> fields = new ArrayList<>(2);
        fields.add(createSchemaField("mz", Schema.create(mzType)));
        if (intensityType != Schema.Type.NULL) {

            fields.add(createSchemaField("i", Schema.create(intensityType)));
        }
        if (hasAnnotations) {

            fields.add(createSchemaField("annotations", Schema.createArray(createUnionSchema(PeakAnnotation.class))));
        }
        peakSchema.setFields(fields);

        Schema schema = Schema.createRecord(name + "PeakList", null, namespace, false);
        List<Schema.Field> peakListFields = new ArrayList<>();
        if (intensityType == Schema.Type.NULL) {

            peakListFields.add(createSchemaField("intensity", Schema.create(Schema.Type.DOUBLE)));
        }
        peakListFields.add(createSchemaField("peaks", Schema.createArray(peakSchema)));
        schema.setFields(peakListFields);

        return schema;
    }

    protected abstract PL newPeakList(PeakList.Precision precision, double constantIntensity);

    private abstract class AbstractPeakReader implements PeakSink<A> {

        private final PL peakList;
        private final PeakSink<A> first;
        private final boolean annotated;
        private final double[] peakData = new double[]{0, 0};

        protected AbstractPeakReader(PL peakList, List<PeakProcessor<A, A>> peakProcessorList, boolean annotated) {

            this.peakList = peakList;
            this.annotated = annotated;
            if (!peakProcessorList.isEmpty()) {
                PeakProcessor<A, A> current = peakProcessorList.get(0);
                first = current;

                for (int i = 1, peakProcessorListSize = peakProcessorList.size(); i < peakProcessorListSize; i++) {

                    PeakProcessor<A, A> processor = peakProcessorList.get(i);
                    current.setSink(processor);
                    current = processor;
                }

                current.setSink(this);
            } else
                first = this;
        }

        void read(Decoder in) throws IOException {

            readPeak(in, peakData);
            List<A> annotations;
            if (annotated)
                annotations = readAnnotationArray(in);
            else
                annotations = Collections.emptyList();

            first.processPeak(peakData[0], peakData[1], annotations);
        }

        abstract void readPeak(Decoder in, double[] peakData) throws IOException;

        protected List<A> readAnnotationArray(Decoder in) throws IOException {

            long start = in.readArrayStart();

            List<A> annotations = start == 0 ? Collections.<A>emptyList() : new ArrayList<A>();

            for (long i = start; i != 0; i = in.arrayNext()) {
                for (long j = 0; j < i; j++) {

                    //noinspection unchecked
                    annotations.add((A) readUnion(PeakAnnotation.class, in));
                }
            }

            return annotations;
        }

        public void sourceStart(int size) {

            first.start(size);
        }

        @Override
        public void start(int size) {

            peakList.ensureCapacity(size);
        }

        @Override
        public void processPeak(double mz, double intensity, List<A> annotations) {

            peakList.add(mz, intensity, annotations);
        }

        public void sourceEnd() {

            first.end();
        }

        @Override
        public void end() {

            peakList.trimToSize();
        }
    }

    private class DoublePeakReader extends AbstractPeakReader {

        private DoublePeakReader(PL peakList, List<PeakProcessor<A, A>> peakProcessorList, boolean readAnnotations) {

            super(peakList, peakProcessorList, readAnnotations);
        }

        @Override
        public void readPeak(Decoder in, double[] peakData) throws IOException {

            peakData[0] = in.readDouble();
            peakData[1] = in.readDouble();
        }
    }

    private class DoubleFloatPeakReader extends AbstractPeakReader {

        private DoubleFloatPeakReader(PL peakList, List<PeakProcessor<A, A>> peakProcessorList, boolean annotated) {

            super(peakList, peakProcessorList, annotated);
        }

        @Override
        public void readPeak(Decoder in, double[] peakData) throws IOException {

            peakData[0] = in.readDouble();
            peakData[1] = in.readFloat();
        }
    }

    private class DoubleConstantPeakReader extends AbstractPeakReader {

        private DoubleConstantPeakReader(PL peakList, List<PeakProcessor<A, A>> peakProcessorList, boolean annotated) {

            super(peakList, peakProcessorList, annotated);
        }

        @Override
        public void readPeak(Decoder in, double[] peakData) throws IOException {

            peakData[0] = in.readDouble();
            peakData[1] = 1;
        }
    }

    private class FloatPeakReader extends AbstractPeakReader {

        private FloatPeakReader(PL peakList, List<PeakProcessor<A, A>> peakProcessorList, boolean annotated) {

            super(peakList, peakProcessorList, annotated);
        }

        @Override
        public void readPeak(Decoder in, double[] peakData) throws IOException {

            peakData[0] = in.readFloat();
            peakData[1] = in.readFloat();
        }
    }

    private class FloatConstantPeakReader extends AbstractPeakReader {

        private FloatConstantPeakReader(PL peakList, List<PeakProcessor<A, A>> peakProcessorList, boolean annotated) {

            super(peakList, peakProcessorList, annotated);
        }

        @Override
        public void readPeak(Decoder in, double[] peakData) throws IOException {

            peakData[0] = in.readFloat();
            peakData[1] = 1;
        }
    }
}
