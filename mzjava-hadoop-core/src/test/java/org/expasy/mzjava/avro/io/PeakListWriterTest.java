/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.avro.io;

import com.google.common.base.Optional;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.util.DefaultPrettyPrinter;
import org.expasy.mzjava.avro.io.PeakListWriter;
import org.expasy.mzjava.core.ms.peaklist.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.util.UUID;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeakListWriterTest {

    @Test
    public void testWriteDoubleFloat() throws Exception {

        PeakList peakList = new DoubleFloatPeakList(3);
        initPeakList(peakList);

        PeakListWriter writer = new PeakListWriter(Optional.<PeakList.Precision>absent());

        StringWriter out = new StringWriter();
        Encoder encoder = createEncoder(writer, out);
        writer.write(peakList, encoder);
        encoder.flush();

        Assert.assertEquals("{\n" +
                "  \"precursor\" : {\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\n" +
                "    \"mz\" : 1789.98765,\n" +
                "    \"intensity\" : 875.68\n" +
                "  },\n" +
                "  \"id\" : {\n" +
                "    \"mostSignificantBits\" : -2693133135828597770,\n" +
                "    \"leastSignificantBits\" : -4936060129817699777\n" +
                "  },\n" +
                "  \"precision\" : \"DOUBLE_FLOAT\",\n" +
                "  \"peaks\" : {\n" +
                "    \"org.expasy.mzjava_avro.core.ms.peaklist.DoubleFloatPeakList\" : {\n" +
                "      \"peaks\" : [ {\n" +
                "        \"mz\" : 12.93234579876,\n" +
                "        \"i\" : 145.25874\n" +
                "      }, {\n" +
                "        \"mz\" : 58.92698543776,\n" +
                "        \"i\" : 145.25874\n" +
                "      }, {\n" +
                "        \"mz\" : 152.956783276,\n" +
                "        \"i\" : 145.25874\n" +
                "      } ]\n" +
                "    }\n" +
                "  }\n" +
                "}", out.toString().replace("\r", ""));
    }

    private void initPeakList(PeakList peakList) {

        peakList.getPrecursor().setValues(1789.98765, 875.68, 2);
        peakList.setId(UUID.fromString("daa011ae-8a1a-43f6-bb7f-9776dc3f5a3f"));
        peakList.add(12.93234579876, 145.258741256354125);
        peakList.add(58.92698543776, 145.258741256354125);
        peakList.add(152.956783276, 145.258741256354125);
    }

    @Test
    public void testWriteDouble() throws Exception {

        PeakList peakList = new DoublePeakList(3);
        initPeakList(peakList);

        PeakListWriter writer = new PeakListWriter(Optional.<PeakList.Precision>absent());

        StringWriter out = new StringWriter();
        Encoder encoder = createEncoder(writer, out);
        writer.write(peakList, encoder);
        encoder.flush();

        Assert.assertEquals("{\n" +
                "  \"precursor\" : {\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\n" +
                "    \"mz\" : 1789.98765,\n" +
                "    \"intensity\" : 875.68\n" +
                "  },\n" +
                "  \"id\" : {\n" +
                "    \"mostSignificantBits\" : -2693133135828597770,\n" +
                "    \"leastSignificantBits\" : -4936060129817699777\n" +
                "  },\n" +
                "  \"precision\" : \"DOUBLE\",\n" +
                "  \"peaks\" : {\n" +
                "    \"org.expasy.mzjava_avro.core.ms.peaklist.DoublePeakList\" : {\n" +
                "      \"peaks\" : [ {\n" +
                "        \"mz\" : 12.93234579876,\n" +
                "        \"i\" : 145.2587412563541\n" +
                "      }, {\n" +
                "        \"mz\" : 58.92698543776,\n" +
                "        \"i\" : 145.2587412563541\n" +
                "      }, {\n" +
                "        \"mz\" : 152.956783276,\n" +
                "        \"i\" : 145.2587412563541\n" +
                "      } ]\n" +
                "    }\n" +
                "  }\n" +
                "}", out.toString().replace("\r", ""));
    }

    @Test
    public void testWriteFloat() throws Exception {

        PeakList peakList = new FloatPeakList(3);
        initPeakList(peakList);

        PeakListWriter writer = new PeakListWriter(Optional.<PeakList.Precision>absent());

        StringWriter out = new StringWriter();
        Encoder encoder = createEncoder(writer, out);
        writer.write(peakList, encoder);
        encoder.flush();

        Assert.assertEquals("{\n" +
                "  \"precursor\" : {\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\n" +
                "    \"mz\" : 1789.98765,\n" +
                "    \"intensity\" : 875.68\n" +
                "  },\n" +
                "  \"id\" : {\n" +
                "    \"mostSignificantBits\" : -2693133135828597770,\n" +
                "    \"leastSignificantBits\" : -4936060129817699777\n" +
                "  },\n" +
                "  \"precision\" : \"FLOAT\",\n" +
                "  \"peaks\" : {\n" +
                "    \"org.expasy.mzjava_avro.core.ms.peaklist.FloatPeakList\" : {\n" +
                "      \"peaks\" : [ {\n" +
                "        \"mz\" : 12.932345,\n" +
                "        \"i\" : 145.25874\n" +
                "      }, {\n" +
                "        \"mz\" : 58.926987,\n" +
                "        \"i\" : 145.25874\n" +
                "      }, {\n" +
                "        \"mz\" : 152.95679,\n" +
                "        \"i\" : 145.25874\n" +
                "      } ]\n" +
                "    }\n" +
                "  }\n" +
                "}", out.toString().replace("\r", ""));
    }

    private Encoder createEncoder(PeakListWriter writer, StringWriter out) throws IOException {

        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        gen.setPrettyPrinter(new DefaultPrettyPrinter());
        return EncoderFactory.get().jsonEncoder(writer.createSchema(), gen);
    }

    @Test
    public void testWriteDoubleConstant() throws Exception {

        PeakList peakList = new DoubleConstantPeakList(3);
        initPeakList(peakList);

        PeakListWriter writer = new PeakListWriter(Optional.<PeakList.Precision>absent());

        StringWriter out = new StringWriter();
        Encoder encoder = createEncoder(writer, out);
        writer.write(peakList, encoder);
        encoder.flush();

        Assert.assertEquals("{\n" +
                "  \"precursor\" : {\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\n" +
                "    \"mz\" : 1789.98765,\n" +
                "    \"intensity\" : 875.68\n" +
                "  },\n" +
                "  \"id\" : {\n" +
                "    \"mostSignificantBits\" : -2693133135828597770,\n" +
                "    \"leastSignificantBits\" : -4936060129817699777\n" +
                "  },\n" +
                "  \"precision\" : \"DOUBLE_CONSTANT\",\n" +
                "  \"peaks\" : {\n" +
                "    \"org.expasy.mzjava_avro.core.ms.peaklist.DoubleConstantPeakList\" : {\n" +
                "      \"intensity\" : 3.0,\n" +
                "      \"peaks\" : [ {\n" +
                "        \"mz\" : 12.93234579876\n" +
                "      }, {\n" +
                "        \"mz\" : 58.92698543776\n" +
                "      }, {\n" +
                "        \"mz\" : 152.956783276\n" +
                "      } ]\n" +
                "    }\n" +
                "  }\n" +
                "}", out.toString().replace("\r", ""));
    }

    @Test
    public void testCreateSchema() throws Exception {

        Schema expected = new Schema.Parser().parse("{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"PeakList\",\n" +
                "  \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"precursor\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Peak\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"polarity\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"enum\",\n" +
                "          \"name\" : \"Polarity\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "          \"symbols\" : [ \"POSITIVE\", \"NEGATIVE\", \"UNKNOWN\" ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"charge\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : \"int\"\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"mz\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"id\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"UUID\",\n" +
                "      \"namespace\" : \"java.util\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"mostSignificantBits\",\n" +
                "        \"type\" : \"long\"\n" +
                "      }, {\n" +
                "        \"name\" : \"leastSignificantBits\",\n" +
                "        \"type\" : \"long\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"precision\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"enum\",\n" +
                "      \"name\" : \"Precision\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "      \"symbols\" : [ \"DOUBLE\", \"FLOAT\", \"DOUBLE_FLOAT\", \"DOUBLE_CONSTANT\", \"FLOAT_CONSTANT\" ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"peaks\",\n" +
                "    \"type\" : [ {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoublePeakList\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoublePeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"double\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"FloatPeakList\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"FloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoubleFloatPeakList\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoubleFloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoubleConstantPeakList\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoubleConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"FloatConstantPeakList\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"FloatConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    } ]\n" +
                "  } ]\n" +
                "}");

        Assert.assertEquals(expected.toString(true), new PeakListWriter(Optional.<PeakList.Precision>absent()).createSchema().toString(true));
    }
}
