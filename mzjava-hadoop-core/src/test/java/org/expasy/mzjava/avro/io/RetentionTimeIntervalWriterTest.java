/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.avro.io;

import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.expasy.mzjava.avro.io.RetentionTimeIntervalWriter;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeInterval;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.junit.Assert;
import org.junit.Test;

import java.io.StringWriter;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class RetentionTimeIntervalWriterTest {

    @Test
    public void testWrite() throws Exception {

        RetentionTimeIntervalWriter writer = new RetentionTimeIntervalWriter();

        RetentionTimeInterval retentionTime = new RetentionTimeInterval(157, 8765, TimeUnit.SECOND);

        StringWriter out = new StringWriter();
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        Encoder encoder = EncoderFactory.get().jsonEncoder(writer.createSchema(), gen);
        writer.write(retentionTime, encoder);
        encoder.flush();

        Assert.assertEquals("{\"minRetentionTime\":157.0,\"maxRetentionTime\":8765.0}", out.toString());
    }

    @Test
    public void testCreateSchema() throws Exception {

        Schema schema = new Schema.Parser().parse("{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"RetentionTimeInterval\",\n" +
                "  \"namespace\" : \"org.expasy.mzjava_avro.core.ms.spectrum\",\n" +
                "  \"doc\" : \"Retention time in seconds\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"minRetentionTime\",\n" +
                "    \"type\" : \"double\"\n" +
                "  }, {\n" +
                "    \"name\" : \"maxRetentionTime\",\n" +
                "    \"type\" : \"double\"\n" +
                "  } ]\n" +
                "}");

        Assert.assertEquals(schema.toString(true), new RetentionTimeIntervalWriter().createSchema().toString(true));
    }
}
