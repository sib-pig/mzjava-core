/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.avro.io;

import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.junit.Assert;
import org.junit.Test;

import java.io.StringWriter;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class CompositionWriterTest {

    @Test
    public void test() throws Exception {

        CompositionWriter writer = new CompositionWriter();

        StringWriter out = new StringWriter();
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        Schema schema = writer.createSchema();
        Encoder encoder = EncoderFactory.get().jsonEncoder(schema, gen);

        Composition composition = Composition.parseComposition("CH3");

        writer.write(composition, encoder);
        encoder.flush();

        Assert.assertEquals("{\"composition\":{\"H\":3,\"C\":1}}", out.toString());
    }

    @Test
    public void testIsotope() throws Exception {

        CompositionWriter writer = new CompositionWriter();

        StringWriter out = new StringWriter();
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        Schema schema = writer.createSchema();
        Encoder encoder = EncoderFactory.get().jsonEncoder(schema, gen);

        Composition composition = new Composition.Builder().addIsotope(AtomicSymbol.C, 13).add(AtomicSymbol.H, 3).build();

        writer.write(composition, encoder);
        encoder.flush();

        Assert.assertEquals("{\"composition\":{\"H\":3,\"C[13]\":1}}", out.toString());
    }

    @Test
    public void testCreateSchema() throws Exception {

        Assert.assertEquals(new Schema.Parser().parse("{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"Composition\",\n" +
                "  \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"composition\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"map\",\n" +
                "      \"values\" : \"int\"\n" +
                "    }\n" +
                "  } ]\n" +
                "}").toString(true),
                new CompositionWriter().createSchema().toString(true));
    }
}
