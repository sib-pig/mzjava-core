/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.avro.io;

import com.google.common.base.Optional;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.util.DefaultPrettyPrinter;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeDiscrete;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberDiscrete;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.junit.Assert;
import org.junit.Test;

import java.io.StringWriter;
import java.net.URI;
import java.util.UUID;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MsnSpectrumWriterTest {

    @Test
    public void testWrite() throws Exception {

        MsnSpectrumWriter writer = new MsnSpectrumWriter(Optional.<PeakList.Precision>absent());

        MsnSpectrum spectrum = new MsnSpectrum(PeakList.Precision.DOUBLE);
        initPeakList(spectrum);
        spectrum.setMsLevel(2);
        spectrum.setParentScanNumber(new ScanNumberDiscrete(12));
        spectrum.addScanNumber(87);
        spectrum.setFragMethod("Magic");
        spectrum.setComment("No comment");
        spectrum.addRetentionTime(new RetentionTimeDiscrete(8796, TimeUnit.SECOND));
        spectrum.setSpectrumIndex(876);
        spectrum.setSpectrumSource(new URI("file:///home/person/msdata/data.mgf"));

        StringWriter out = new StringWriter();
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        gen.setPrettyPrinter(new DefaultPrettyPrinter());
        Encoder encoder = EncoderFactory.get().jsonEncoder(writer.createSchema(), gen);
        writer.write(spectrum, encoder);
        encoder.flush();

        Assert.assertEquals("{\n" +
                        "  \"precursor\" : {\n" +
                        "    \"polarity\" : \"POSITIVE\",\n" +
                        "    \"charge\" : [ 2 ],\n" +
                        "    \"mz\" : 1789.98765,\n" +
                        "    \"intensity\" : 875.68\n" +
                        "  },\n" +
                        "  \"msLevel\" : 2,\n" +
                        "  \"spectrumIndex\" : 876,\n" +
                        "  \"parentScanNumber\" : {\n" +
                        "    \"org.expasy.mzjava_avro.core.ms.spectrum.ScanNumberDiscrete\" : {\n" +
                        "      \"value\" : 12\n" +
                        "    }\n" +
                        "  },\n" +
                        "  \"fragMethod\" : \"Magic\",\n" +
                        "  \"retentionTimes\" : [ {\n" +
                        "    \"org.expasy.mzjava_avro.core.ms.spectrum.RetentionTimeDiscrete\" : {\n" +
                        "      \"time\" : 8796.0\n" +
                        "    }\n" +
                        "  } ],\n" +
                        "  \"scanNumbers\" : [ {\n" +
                        "    \"org.expasy.mzjava_avro.core.ms.spectrum.ScanNumberDiscrete\" : {\n" +
                        "      \"value\" : 87\n" +
                        "    }\n" +
                        "  } ],\n" +
                        "  \"source\" : \"file:///home/person/msdata/data.mgf\",\n" +
                        "  \"comment\" : \"No comment\",\n" +
                        "  \"id\" : {\n" +
                        "    \"mostSignificantBits\" : -2693133135828597770,\n" +
                        "    \"leastSignificantBits\" : -4936060129817699777\n" +
                        "  },\n" +
                        "  \"precision\" : \"DOUBLE\",\n" +
                        "  \"peaks\" : {\n" +
                        "    \"org.expasy.mzjava_avro.core.ms.peaklist.DoublePeakList\" : {\n" +
                        "      \"peaks\" : [ {\n" +
                        "        \"mz\" : 12.93234579876,\n" +
                        "        \"i\" : 145.2587412563541\n" +
                        "      }, {\n" +
                        "        \"mz\" : 58.92698543776,\n" +
                        "        \"i\" : 145.2587412563541\n" +
                        "      }, {\n" +
                        "        \"mz\" : 152.956783276,\n" +
                        "        \"i\" : 145.2587412563541\n" +
                        "      } ]\n" +
                        "    }\n" +
                        "  }\n" +
                        "}",
                out.toString().replace("\r", "")
        );
    }

    private void initPeakList(PeakList peakList) {

        peakList.getPrecursor().setValues(1789.98765, 875.68, 2);
        peakList.setId(UUID.fromString("daa011ae-8a1a-43f6-bb7f-9776dc3f5a3f"));
        peakList.add(12.93234579876, 145.258741256354125);
        peakList.add(58.92698543776, 145.258741256354125);
        peakList.add(152.956783276, 145.258741256354125);
    }

    @Test
    public void testCreateSchema() throws Exception {

        Schema expected = new Schema.Parser().parse("{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"MsnSpectrum\",\n" +
                "  \"namespace\" : \"org.expasy.mzjava_avro.core.ms.spectrum\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"precursor\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Peak\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"polarity\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"enum\",\n" +
                "          \"name\" : \"Polarity\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "          \"symbols\" : [ \"POSITIVE\", \"NEGATIVE\", \"UNKNOWN\" ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"charge\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : \"int\"\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"mz\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"msLevel\",\n" +
                "    \"type\" : \"int\"\n" +
                "  }, {\n" +
                "    \"name\" : \"spectrumIndex\",\n" +
                "    \"type\" : \"int\"\n" +
                "  }, {\n" +
                "    \"name\" : \"parentScanNumber\",\n" +
                "    \"type\" : [ {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"ScanNumberDiscrete\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"value\",\n" +
                "        \"type\" : \"int\"\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"ScanNumberInterval\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"minScanNumber\",\n" +
                "        \"type\" : \"int\"\n" +
                "      }, {\n" +
                "        \"name\" : \"maxScanNumber\",\n" +
                "        \"type\" : \"int\"\n" +
                "      } ]\n" +
                "    } ]\n" +
                "  }, {\n" +
                "    \"name\" : \"fragMethod\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"retentionTimes\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : [ {\n" +
                "        \"type\" : \"record\",\n" +
                "        \"name\" : \"RetentionTimeDiscrete\",\n" +
                "        \"doc\" : \"Retention time in seconds\",\n" +
                "        \"fields\" : [ {\n" +
                "          \"name\" : \"time\",\n" +
                "          \"type\" : \"double\"\n" +
                "        } ]\n" +
                "      }, {\n" +
                "        \"type\" : \"record\",\n" +
                "        \"name\" : \"RetentionTimeInterval\",\n" +
                "        \"doc\" : \"Retention time in seconds\",\n" +
                "        \"fields\" : [ {\n" +
                "          \"name\" : \"minRetentionTime\",\n" +
                "          \"type\" : \"double\"\n" +
                "        }, {\n" +
                "          \"name\" : \"maxRetentionTime\",\n" +
                "          \"type\" : \"double\"\n" +
                "        } ]\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"scanNumbers\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : [ \"ScanNumberDiscrete\", \"ScanNumberInterval\" ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"source\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"comment\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"id\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"UUID\",\n" +
                "      \"namespace\" : \"java.util\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"mostSignificantBits\",\n" +
                "        \"type\" : \"long\"\n" +
                "      }, {\n" +
                "        \"name\" : \"leastSignificantBits\",\n" +
                "        \"type\" : \"long\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"precision\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"enum\",\n" +
                "      \"name\" : \"Precision\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "      \"symbols\" : [ \"DOUBLE\", \"FLOAT\", \"DOUBLE_FLOAT\", \"DOUBLE_CONSTANT\", \"FLOAT_CONSTANT\" ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"peaks\",\n" +
                "    \"type\" : [ {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoublePeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoublePeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"double\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"FloatPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"FloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoubleFloatPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoubleFloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoubleConstantPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoubleConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"FloatConstantPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"FloatConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    } ]\n" +
                "  } ]\n" +
                "}");

        String actual = new MsnSpectrumWriter(Optional.<PeakList.Precision>absent()).createSchema().toString(true);
        Assert.assertEquals(expected.toString(true), new Schema.Parser().parse(actual).toString(true));
    }
}
