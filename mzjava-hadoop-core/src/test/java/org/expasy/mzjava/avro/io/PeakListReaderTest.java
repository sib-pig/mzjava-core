/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.avro.io;

import com.google.common.base.Optional;
import org.apache.avro.AvroTypeException;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.expasy.mzjava.core.ms.peaklist.*;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.IdentityPeakProcessor;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.SqrtTransformer;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeakListReaderTest {

    @Test
    public void testReadDoubleFloat() throws Exception {

        PeakListReader reader = new PeakListReader();

        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(), "{\r\n" +
                "  \"precursor\" : {\r\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\r\n" +
                "    \"mz\" : 1789.98765,\r\n" +
                "    \"intensity\" : 875.68\r\n" +
                "  },\r\n" +
                "  \"id\" : {\r\n" +
                "    \"mostSignificantBits\" : -2693133135828597770,\r\n" +
                "    \"leastSignificantBits\" : -4936060129817699777\r\n" +
                "  },\r\n" +
                "  \"precision\" : \"DOUBLE_FLOAT\",\r\n" +
                "  \"peaks\" : {\r\n" +
                "    \"org.expasy.mzjava_avro.core.ms.peaklist.DoubleFloatPeakList\" : {\r\n" +
                "      \"peaks\" : [ {\r\n" +
                "        \"mz\" : 12.93234579876,\r\n" +
                "        \"i\" : 145.25874\r\n" +
                "      }, {\r\n" +
                "        \"mz\" : 58.92698543776,\r\n" +
                "        \"i\" : 145.25874\r\n" +
                "      }, {\r\n" +
                "        \"mz\" : 152.956783276,\r\n" +
                "        \"i\" : 145.25874\r\n" +
                "      } ]\r\n" +
                "    }\r\n" +
                "  }\r\n" +
                "}");

        PeakList expected = new DoubleFloatPeakList(3);
        initPeakList(expected);
        Assert.assertEquals(expected, reader.read(in));
    }

    @Test
    public void testReadDouble() throws Exception {

        PeakListReader reader = new PeakListReader();

        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(), "{\r\n" +
                "  \"precursor\" : {\r\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\r\n" +
                "    \"mz\" : 1789.98765,\r\n" +
                "    \"intensity\" : 875.68\r\n" +
                "  },\r\n" +
                "  \"id\" : {\r\n" +
                "    \"mostSignificantBits\" : -2693133135828597770,\r\n" +
                "    \"leastSignificantBits\" : -4936060129817699777\r\n" +
                "  },\r\n" +
                "  \"precision\" : \"DOUBLE\",\r\n" +
                "  \"peaks\" : {\r\n" +
                "    \"org.expasy.mzjava_avro.core.ms.peaklist.DoublePeakList\" : {\r\n" +
                "      \"peaks\" : [ {\r\n" +
                "        \"mz\" : 12.93234579876,\r\n" +
                "        \"i\" : 145.2587412563541\r\n" +
                "      }, {\r\n" +
                "        \"mz\" : 58.92698543776,\r\n" +
                "        \"i\" : 145.2587412563541\r\n" +
                "      }, {\r\n" +
                "        \"mz\" : 152.956783276,\r\n" +
                "        \"i\" : 145.2587412563541\r\n" +
                "      } ]\r\n" +
                "    }\r\n" +
                "  }\r\n" +
                "}");

        PeakList expected = new DoublePeakList(3);
        initPeakList(expected);
        Assert.assertEquals(expected, reader.read(in));
    }

    @Test
    public void testReadOverridePrecision() throws Exception {

        PeakListReader reader
                = new PeakListReader(Optional.of(PeakList.Precision.FLOAT_CONSTANT), Collections.<PeakProcessor<PeakAnnotation, PeakAnnotation>>emptyList());

        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(), "{\r\n" +
                "  \"precursor\" : {\r\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\r\n" +
                "    \"mz\" : 1789.98765,\r\n" +
                "    \"intensity\" : 875.68\r\n" +
                "  },\r\n" +
                "  \"id\" : {\r\n" +
                "    \"mostSignificantBits\" : -2693133135828597770,\r\n" +
                "    \"leastSignificantBits\" : -4936060129817699777\r\n" +
                "  },\r\n" +
                "  \"precision\" : \"DOUBLE\",\r\n" +
                "  \"peaks\" : {\r\n" +
                "    \"org.expasy.mzjava_avro.core.ms.peaklist.DoublePeakList\" : {\r\n" +
                "      \"peaks\" : [ {\r\n" +
                "        \"mz\" : 12.93234579876,\r\n" +
                "        \"i\" : 145.2587412563541\r\n" +
                "      }, {\r\n" +
                "        \"mz\" : 58.92698543776,\r\n" +
                "        \"i\" : 145.2587412563541\r\n" +
                "      }, {\r\n" +
                "        \"mz\" : 152.956783276,\r\n" +
                "        \"i\" : 145.2587412563541\r\n" +
                "      } ]\r\n" +
                "    }\r\n" +
                "  }\r\n" +
                "}");

        PeakList expected = new FloatConstantPeakList(1);
        initPeakList(expected);
        PeakList read = reader.read(in);
        Assert.assertEquals(expected, read);
    }

    @Test
    public void testReadProcessedDouble() throws Exception {

        List<PeakProcessor<PeakAnnotation, PeakAnnotation>> processorList = new ArrayList<>();
        processorList.add(new IdentityPeakProcessor<>());
        processorList.add(new SqrtTransformer<>());

        PeakListReader reader = new PeakListReader(
                Optional.<PeakList.Precision>absent(),
                processorList);

        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(), "{\r\n" +
                "  \"precursor\" : {\r\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\r\n" +
                "    \"mz\" : 1789.98765,\r\n" +
                "    \"intensity\" : 875.68\r\n" +
                "  },\r\n" +
                "  \"id\" : {\r\n" +
                "    \"mostSignificantBits\" : -2693133135828597770,\r\n" +
                "    \"leastSignificantBits\" : -4936060129817699777\r\n" +
                "  },\r\n" +
                "  \"precision\" : \"DOUBLE\",\r\n" +
                "  \"peaks\" : {\r\n" +
                "    \"org.expasy.mzjava_avro.core.ms.peaklist.DoublePeakList\" : {\r\n" +
                "      \"peaks\" : [ {\r\n" +
                "        \"mz\" : 12.93234579876,\r\n" +
                "        \"i\" : 145.2587412563541\r\n" +
                "      }, {\r\n" +
                "        \"mz\" : 58.92698543776,\r\n" +
                "        \"i\" : 145.2587412563541\r\n" +
                "      }, {\r\n" +
                "        \"mz\" : 152.956783276,\r\n" +
                "        \"i\" : 145.2587412563541\r\n" +
                "      } ]\r\n" +
                "    }\r\n" +
                "  }\r\n" +
                "}");

        PeakList<PeakAnnotation> expected = new DoublePeakList<>(3);
        initPeakList(expected);
        PeakProcessorChain<PeakAnnotation> processorChain = new PeakProcessorChain<>(
                new SqrtTransformer<>()
        );
        expected.apply(processorChain);
        Assert.assertEquals(expected, reader.read(in));
    }

    @Test
    public void testReadFloat() throws Exception {

        PeakListReader reader = new PeakListReader();

        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(), "{\r\n" +
                "  \"precursor\" : {\r\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\r\n" +
                "    \"mz\" : 1789.98765,\r\n" +
                "    \"intensity\" : 875.68\r\n" +
                "  },\r\n" +
                "  \"id\" : {\r\n" +
                "    \"mostSignificantBits\" : -2693133135828597770,\r\n" +
                "    \"leastSignificantBits\" : -4936060129817699777\r\n" +
                "  },\r\n" +
                "  \"precision\" : \"FLOAT\",\r\n" +
                "  \"peaks\" : {\r\n" +
                "    \"org.expasy.mzjava_avro.core.ms.peaklist.FloatPeakList\" : {\r\n" +
                "      \"peaks\" : [ {\r\n" +
                "        \"mz\" : 12.932345,\r\n" +
                "        \"i\" : 145.25874\r\n" +
                "      }, {\r\n" +
                "        \"mz\" : 58.926987,\r\n" +
                "        \"i\" : 145.25874\r\n" +
                "      }, {\r\n" +
                "        \"mz\" : 152.95679,\r\n" +
                "        \"i\" : 145.25874\r\n" +
                "      } ]\r\n" +
                "    }\r\n" +
                "  }\r\n" +
                "}");

        PeakList expected = new FloatPeakList(3);
        initPeakList(expected);
        Assert.assertEquals(expected, reader.read(in));
    }

    @Test
    public void testReadDoubleConstant() throws Exception {

        PeakListReader reader = new PeakListReader();

        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(), "{\r\n" +
                "  \"precursor\" : {\r\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\r\n" +
                "    \"mz\" : 1789.98765,\r\n" +
                "    \"intensity\" : 875.68\r\n" +
                "  },\r\n" +
                "  \"id\" : {\r\n" +
                "    \"mostSignificantBits\" : -2693133135828597770,\r\n" +
                "    \"leastSignificantBits\" : -4936060129817699777\r\n" +
                "  },\r\n" +
                "  \"precision\" : \"DOUBLE_CONSTANT\",\r\n" +
                "  \"peaks\" : {\r\n" +
                "    \"org.expasy.mzjava_avro.core.ms.peaklist.DoubleConstantPeakList\" : {\r\n" +
                "      \"intensity\" : 3.0,\r\n" +
                "      \"peaks\" : [ {\r\n" +
                "        \"mz\" : 12.93234579876\r\n" +
                "      }, {\r\n" +
                "        \"mz\" : 58.92698543776\r\n" +
                "      }, {\r\n" +
                "        \"mz\" : 152.956783276\r\n" +
                "      } ]\r\n" +
                "    }\r\n" +
                "  }\r\n" +
                "}");

        PeakList expected = new DoubleConstantPeakList(3);
        initPeakList(expected);
        Assert.assertEquals(expected, reader.read(in));
    }

    @Test
    public void testReadFloatConstant() throws Exception {

        PeakListReader reader = new PeakListReader();

        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(), "{\r\n" +
                "  \"precursor\" : {\r\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\r\n" +
                "    \"mz\" : 1789.98765,\r\n" +
                "    \"intensity\" : 875.68\r\n" +
                "  },\r\n" +
                "  \"id\" : {\r\n" +
                "    \"mostSignificantBits\" : -2693133135828597770,\r\n" +
                "    \"leastSignificantBits\" : -4936060129817699777\r\n" +
                "  },\r\n" +
                "  \"precision\" : \"FLOAT_CONSTANT\",\r\n" +
                "  \"peaks\" : {\r\n" +
                "    \"org.expasy.mzjava_avro.core.ms.peaklist.FloatConstantPeakList\" : {\r\n" +
                "      \"intensity\" : 3.0,\r\n" +
                "      \"peaks\" : [ {\r\n" +
                "        \"mz\" : 12.932345\r\n" +
                "      }, {\r\n" +
                "        \"mz\" : 58.926987\r\n" +
                "      }, {\r\n" +
                "        \"mz\" : 152.95679\r\n" +
                "      } ]\r\n" +
                "    }\r\n" +
                "  }\r\n" +
                "}");

        PeakList expected = new FloatConstantPeakList(3);
        initPeakList(expected);
        Assert.assertEquals(expected, reader.read(in));
    }

    @Test(expected = AvroTypeException.class)
    public void testIgnoreAnnotations() throws Exception {

        PeakListReader reader = new PeakListReader();

        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(), "{\n" +
                "  \"precursor\" : {\n" +
                "    \"charge\" : [ ],\n" +
                "    \"mz\" : 0.0,\n" +
                "    \"intensity\" : 0.0\n" +
                "  },\n" +
                "  \"id\" : {\n" +
                "    \"mostSignificantBits\" : -2693133135828597770,\n" +
                "    \"leastSignificantBits\" : -4936060129817699777\n" +
                "  },\n" +
                "  \"precision\" : \"DOUBLE_FLOAT\",\n" +
                "  \"peaks\" : {\n" +
                "    \"AnnotatedDoubleFloatPeakList\" : {\n" +
                "      \"peaks\" : [ {\n" +
                "        \"mz\" : 12.93234579876,\n" +
                "        \"i\" : 145.25874,\n" +
                "        \"annotations\" : [ {\n" +
                "          \"org.expasy.mzjava_avro.proteomics.ms.spectrum.PepFragAnnotation\" : {\n" +
                "            \"ionType\" : \"b\",\n" +
                "            \"isotopeComposition\" : {\n" +
                "              \"composition\" : {\n" +
                "              }\n" +
                "            },\n" +
                "            \"charge\" : 1,\n" +
                "            \"peptideFragment\" : {\n" +
                "              \"fragmentType\" : \"FORWARD\",\n" +
                "              \"seq\" : [ \"C\", \"E\", \"R\" ],\n" +
                "              \"sideChainModMap\" : [ ],\n" +
                "              \"termModMap\" : [ ]\n" +
                "            },\n" +
                "            \"neutralLoss\" : {\n" +
                "              \"org.expasy.mzjava_avro.core.mol.NumericMass\" : {\n" +
                "                \"mass\" : 0.0\n" +
                "              }\n" +
                "            }\n" +
                "          }\n" +
                "        } ]\n" +
                "      }, {\n" +
                "        \"mz\" : 58.92698543776,\n" +
                "        \"i\" : 145.25874,\n" +
                "        \"annotations\" : [ ]\n" +
                "      }, {\n" +
                "        \"mz\" : 152.956783276,\n" +
                "        \"i\" : 145.25874,\n" +
                "        \"annotations\" : [ {\n" +
                "          \"org.expasy.mzjava_avro.proteomics.ms.spectrum.PepFragAnnotation\" : {\n" +
                "            \"ionType\" : \"b\",\n" +
                "            \"isotopeComposition\" : {\n" +
                "              \"composition\" : {\n" +
                "              }\n" +
                "            },\n" +
                "            \"charge\" : 1,\n" +
                "            \"peptideFragment\" : {\n" +
                "              \"fragmentType\" : \"FORWARD\",\n" +
                "              \"seq\" : [ \"C\", \"E\", \"R\", \"V\" ],\n" +
                "              \"sideChainModMap\" : [ ],\n" +
                "              \"termModMap\" : [ ]\n" +
                "            },\n" +
                "            \"neutralLoss\" : {\n" +
                "              \"org.expasy.mzjava_avro.core.mol.NumericMass\" : {\n" +
                "                \"mass\" : 0.0\n" +
                "              }\n" +
                "            }\n" +
                "          }\n" +
                "        }, {\n" +
                "          \"org.expasy.mzjava_avro.proteomics.ms.spectrum.PepFragAnnotation\" : {\n" +
                "            \"ionType\" : \"y\",\n" +
                "            \"isotopeComposition\" : {\n" +
                "              \"composition\" : {\n" +
                "              }\n" +
                "            },\n" +
                "            \"charge\" : 2,\n" +
                "            \"peptideFragment\" : {\n" +
                "              \"fragmentType\" : \"REVERSE\",\n" +
                "              \"seq\" : [ \"I\", \"L\", \"A\", \"K\" ],\n" +
                "              \"sideChainModMap\" : [ ],\n" +
                "              \"termModMap\" : [ ]\n" +
                "            },\n" +
                "            \"neutralLoss\" : {\n" +
                "              \"org.expasy.mzjava_avro.core.mol.NumericMass\" : {\n" +
                "                \"mass\" : 0.0\n" +
                "              }\n" +
                "            }\n" +
                "          }\n" +
                "        } ]\n" +
                "      } ]\n" +
                "    }\n" +
                "  }\n" +
                "}");

        reader.read(in);
    }

    private void initPeakList(PeakList peakList) {

        peakList.getPrecursor().setValues(1789.98765, 875.68, 2);
        peakList.setId(UUID.fromString("daa011ae-8a1a-43f6-bb7f-9776dc3f5a3f"));
        peakList.add(12.93234579876, 145.258741256354125);
        peakList.add(58.92698543776, 145.258741256354125);
        peakList.add(152.956783276, 145.258741256354125);
        peakList.trimToSize();
    }

    @Test
    public void testCreateSchema() throws Exception {

        Schema expected = new Schema.Parser().parse("{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"PeakList\",\n" +
                "  \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"precursor\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Peak\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"polarity\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"enum\",\n" +
                "          \"name\" : \"Polarity\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "          \"symbols\" : [ \"POSITIVE\", \"NEGATIVE\", \"UNKNOWN\" ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"charge\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : \"int\"\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"mz\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"id\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"UUID\",\n" +
                "      \"namespace\" : \"java.util\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"mostSignificantBits\",\n" +
                "        \"type\" : \"long\"\n" +
                "      }, {\n" +
                "        \"name\" : \"leastSignificantBits\",\n" +
                "        \"type\" : \"long\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"precision\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"enum\",\n" +
                "      \"name\" : \"Precision\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "      \"symbols\" : [ \"DOUBLE\", \"FLOAT\", \"DOUBLE_FLOAT\", \"DOUBLE_CONSTANT\", \"FLOAT_CONSTANT\" ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"peaks\",\n" +
                "    \"type\" : [ {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoublePeakList\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoublePeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"double\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"FloatPeakList\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"FloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoubleFloatPeakList\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoubleFloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoubleConstantPeakList\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoubleConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"FloatConstantPeakList\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"FloatConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    } ]\n" +
                "  } ]\n" +
                "}");

        Assert.assertEquals(expected.toString(true), new PeakListReader().createSchema().toString(true));
    }
}
