/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.avro.io;

import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.expasy.mzjava.avro.io.ScanNumberListWriter;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberList;
import org.junit.Assert;
import org.junit.Test;

import java.io.StringWriter;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class ScanNumberListWriterTest {

    @Test
    public void test() throws Exception {

        ScanNumberList scanNumbers = new ScanNumberList();
        scanNumbers.add(12);
        scanNumbers.add(46);
        scanNumbers.add(789);

        ScanNumberListWriter writer = new ScanNumberListWriter();

        StringWriter out = new StringWriter();
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        Encoder encoder = EncoderFactory.get().jsonEncoder(writer.createSchema(), gen);
        writer.write(scanNumbers, encoder);
        encoder.flush();

        Assert.assertEquals("[" +
                "{\"org.expasy.mzjava_avro.core.ms.spectrum.ScanNumberDiscrete\":{\"value\":12}}," +
                "{\"org.expasy.mzjava_avro.core.ms.spectrum.ScanNumberDiscrete\":{\"value\":46}}," +
                "{\"org.expasy.mzjava_avro.core.ms.spectrum.ScanNumberDiscrete\":{\"value\":789}}" +
                "]", out.toString());
    }

    @Test
    public void testCreateSchema() throws Exception {

        Schema schema = new Schema.Parser().parse("{\n" +
                "  \"type\" : \"array\",\n" +
                "  \"items\" : [ {\n" +
                "    \"type\" : \"record\",\n" +
                "    \"name\" : \"ScanNumberDiscrete\",\n" +
                "    \"namespace\" : \"org.expasy.mzjava_avro.core.ms.spectrum\",\n" +
                "    \"fields\" : [ {\n" +
                "      \"name\" : \"value\",\n" +
                "      \"type\" : \"int\"\n" +
                "    } ]\n" +
                "  }, {\n" +
                "    \"type\" : \"record\",\n" +
                "    \"name\" : \"ScanNumberInterval\",\n" +
                "    \"namespace\" : \"org.expasy.mzjava_avro.core.ms.spectrum\",\n" +
                "    \"fields\" : [ {\n" +
                "      \"name\" : \"minScanNumber\",\n" +
                "      \"type\" : \"int\"\n" +
                "    }, {\n" +
                "      \"name\" : \"maxScanNumber\",\n" +
                "      \"type\" : \"int\"\n" +
                "    } ]\n" +
                "  } ]\n" +
                "}");

        Assert.assertEquals(schema.toString(true), new ScanNumberListWriter().createSchema().toString(true));
    }
}
