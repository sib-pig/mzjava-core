/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.avro.io;

import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.expasy.mzjava.avro.io.NumericMassWriter;
import org.expasy.mzjava.core.mol.NumericMass;
import org.expasy.mzjava.hadoop.io.MockDataOutput;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import java.io.StringWriter;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class NumericMassWriterTest {

    @Test
    public void test() throws Exception {

        NumericMassWriter writer = new NumericMassWriter();

        Encoder encoder = Mockito.mock(Encoder.class);

        writer.write(new NumericMass(80.01), encoder);

        InOrder inOrder = inOrder(encoder);
        inOrder.verify(encoder).writeDouble(80.01);
        verifyNoMoreInteractions(encoder);
    }

    @Test
    public void testJson() throws Exception {

        NumericMassWriter writer = new NumericMassWriter();

        StringWriter out = new StringWriter();
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        Schema schema = writer.createSchema();
        Encoder encoder = EncoderFactory.get().jsonEncoder(schema, gen);

        writer.write(new NumericMass(80.01), encoder);
        encoder.flush();

        Assert.assertEquals("{\"mass\":80.01}", out.toString());
    }

    @Test
    public void testBinary() throws Exception {

        NumericMassWriter writer = new NumericMassWriter();

        MockDataOutput out = new MockDataOutput(9);
        Encoder encoder = EncoderFactory.get().binaryEncoder(out, null);

        writer.write(new NumericMass(80.01), encoder);
        encoder.flush();

        Assert.assertEquals("cT0K16MAVEA=", out.getBase64());
    }

    @Test
    public void testCreateSchema() throws Exception {

        Assert.assertEquals(new Schema.Parser().parse(
                "{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"NumericMass\",\n" +
                "  \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"mass\",\n" +
                "    \"type\" : \"double\"\n" +
                "  } ]\n" +
                "}").toString(true),
                new NumericMassWriter().createSchema().toString(true));
    }
}
