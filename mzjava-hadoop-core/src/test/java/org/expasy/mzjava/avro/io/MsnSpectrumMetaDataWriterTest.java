/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.avro.io;

import com.google.common.base.Optional;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.util.DefaultPrettyPrinter;
import org.expasy.mzjava.avro.io.MsnSpectrumMetaDataWriter;
import org.expasy.mzjava.avro.io.MsnSpectrumWriter;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeDiscrete;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberDiscrete;
import org.junit.Assert;
import org.junit.Test;

import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class MsnSpectrumMetaDataWriterTest {

    @Test
    public void test() throws Exception {

        MsnSpectrum spectrum = new MsnSpectrum();
        spectrum.setParentScanNumber(new ScanNumberDiscrete(12));
        spectrum.addScanNumber(87);
        spectrum.setFragMethod("Magic");
        spectrum.setComment("No comment");
        spectrum.addRetentionTime(new RetentionTimeDiscrete(8796, TimeUnit.SECOND));
        spectrum.setSpectrumIndex(876);
        spectrum.setSpectrumSource(new URI("file:///home/person/msdata/data.mgf"));

        MsnSpectrumMetaDataWriter writer = new MsnSpectrumMetaDataWriter(new MsnSpectrumWriter(Optional.<PeakList.Precision>absent()));

        StringWriter out = new StringWriter();
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        gen.setPrettyPrinter(new DefaultPrettyPrinter());
        Encoder encoder = EncoderFactory.get().jsonEncoder(createSchema(writer), gen);
        writer.writeMetaData(spectrum, encoder);
        encoder.flush();

        Assert.assertEquals("{\n" +
                "  \"spectrumIndex\" : 876,\n" +
                "  \"parentScanNumber\" : {\n" +
                "    \"org.expasy.mzjava_avro.core.ms.spectrum.ScanNumberDiscrete\" : {\n" +
                "      \"value\" : 12\n" +
                "    }\n" +
                "  },\n" +
                "  \"fragMethod\" : \"Magic\",\n" +
                "  \"retentionTimes\" : [ {\n" +
                "    \"org.expasy.mzjava_avro.core.ms.spectrum.RetentionTimeDiscrete\" : {\n" +
                "      \"time\" : 8796.0\n" +
                "    }\n" +
                "  } ],\n" +
                "  \"scanNumbers\" : [ {\n" +
                "    \"org.expasy.mzjava_avro.core.ms.spectrum.ScanNumberDiscrete\" : {\n" +
                "      \"value\" : 87\n" +
                "    }\n" +
                "  } ],\n" +
                "  \"source\" : \"file:///home/person/msdata/data.mgf\",\n" +
                "  \"comment\" : \"No comment\"\n" +
                "}", out.toString().replace("\r", ""));
    }

    @Test
    public void testCreateSchema() throws Exception {

        Schema expected = new Schema.Parser().parse("{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"test\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"spectrumIndex\",\n" +
                "    \"type\" : \"int\"\n" +
                "  }, {\n" +
                "    \"name\" : \"parentScanNumber\",\n" +
                "    \"type\" : [ {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"ScanNumberDiscrete\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.spectrum\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"value\",\n" +
                "        \"type\" : \"int\"\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"ScanNumberInterval\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.spectrum\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"minScanNumber\",\n" +
                "        \"type\" : \"int\"\n" +
                "      }, {\n" +
                "        \"name\" : \"maxScanNumber\",\n" +
                "        \"type\" : \"int\"\n" +
                "      } ]\n" +
                "    } ]\n" +
                "  }, {\n" +
                "    \"name\" : \"fragMethod\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"retentionTimes\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : [ {\n" +
                "        \"type\" : \"record\",\n" +
                "        \"name\" : \"RetentionTimeDiscrete\",\n" +
                "        \"namespace\" : \"org.expasy.mzjava_avro.core.ms.spectrum\",\n" +
                "        \"doc\" : \"Retention time in seconds\",\n" +
                "        \"fields\" : [ {\n" +
                "          \"name\" : \"time\",\n" +
                "          \"type\" : \"double\"\n" +
                "        } ]\n" +
                "      }, {\n" +
                "        \"type\" : \"record\",\n" +
                "        \"name\" : \"RetentionTimeInterval\",\n" +
                "        \"namespace\" : \"org.expasy.mzjava_avro.core.ms.spectrum\",\n" +
                "        \"doc\" : \"Retention time in seconds\",\n" +
                "        \"fields\" : [ {\n" +
                "          \"name\" : \"minRetentionTime\",\n" +
                "          \"type\" : \"double\"\n" +
                "        }, {\n" +
                "          \"name\" : \"maxRetentionTime\",\n" +
                "          \"type\" : \"double\"\n" +
                "        } ]\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"scanNumbers\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : [ \"org.expasy.mzjava_avro.core.ms.spectrum.ScanNumberDiscrete\", \"org.expasy.mzjava_avro.core.ms.spectrum.ScanNumberInterval\" ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"source\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"comment\",\n" +
                "    \"type\" : \"string\"\n" +
                "  } ]\n" +
                "}");


        MsnSpectrumMetaDataWriter writer = new MsnSpectrumMetaDataWriter(new MsnSpectrumWriter(Optional.<PeakList.Precision>absent()));

        Schema schema = createSchema(writer);

        Assert.assertEquals(expected.toString(true), schema.toString(true));
    }

    private Schema createSchema(MsnSpectrumMetaDataWriter writer) {

        Schema schema = Schema.createRecord("test", null, null, false);

        List<Schema.Field> recordFields = new ArrayList<>(10);
        writer.addFields(recordFields);
        schema.setFields(recordFields);
        return schema;
    }
}
