/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.hadoop.io;

import org.apache.commons.codec.binary.Base64;

import java.io.DataOutput;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class MockDataOutput extends OutputStream implements DataOutput {

    private final ByteBuffer byteBuffer;
    private final Base64 base64 = new Base64(76, new byte[]{'\n'});

    public MockDataOutput(int capacity) {

        byteBuffer = ByteBuffer.allocate(capacity);
    }

    @Override
    public void write(int b) throws IOException {

        byteBuffer.put((byte)b);
    }

    @Override
    public void write(byte[] b) throws IOException {

        byteBuffer.put(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {

        byteBuffer.put(b, off, len);
    }

    @Override
    public void writeBoolean(boolean v) throws IOException {

        byteBuffer.put(v ? (byte)1 : (byte)0);
    }

    @Override
    public void writeByte(int v) throws IOException {

        byteBuffer.put((byte)v);
    }

    @Override
    public void writeShort(int v) throws IOException {

        byteBuffer.putShort((short) v);
    }

    @Override
    public void writeChar(int v) throws IOException {

        byteBuffer.putChar((char)v);
    }

    @Override
    public void writeInt(int v) throws IOException {

        byteBuffer.putInt(v);
    }

    @Override
    public void writeLong(long v) throws IOException {

        byteBuffer.putLong(v);
    }

    @Override
    public void writeFloat(float v) throws IOException {

        byteBuffer.putFloat(v);
    }

    @Override
    public void writeDouble(double v) throws IOException {

        byteBuffer.putDouble(v);
    }

    @Override
    public void writeBytes(String s) throws IOException {

        throw new UnsupportedOperationException();
    }

    @Override
    public void writeChars(String s) throws IOException {

        throw new UnsupportedOperationException();
    }

    @Override
    public void writeUTF(String s) throws IOException {

        byteBuffer.putInt(s.length());
        byteBuffer.put(s.getBytes(Charset.forName("UTF8")));
    }

    public String getBase64() {

        return base64.encodeToString(getBytes()).trim();
    }

    @Override
    public String toString() {

        return getBase64();
    }

    public byte[] getBytes() {

        int length = byteBuffer.position();
        byte[] pArray = new byte[length];
        System.arraycopy(byteBuffer.array(), 0, pArray, 0, length);
        return pArray;
    }
}
