/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.hadoop.io;

import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class SpectrumKeyRawComparatorTest {
    @Test
    public void testCompareKeys() throws Exception {

        SpectrumKey key = newKey(new Peak(547, 100, 2));

        SpectrumKey same = newKey(new Peak(547, 100, 2));
        SpectrumKey smallerCharge = newKey(new Peak(547, 100, 1));
        SpectrumKey largerCharge = newKey(new Peak(547, 100, 3));
        SpectrumKey smallerMz = newKey(new Peak(347, 100, 2));
        SpectrumKey largerMz = newKey(new Peak(647, 100, 2));

        DefaultSpectrumKeyRawComparator keyRawComparator = new DefaultSpectrumKeyRawComparator();

        Assert.assertEquals(0, keyRawComparator.compare(key, same));
        Assert.assertEquals(1, keyRawComparator.compare(key, smallerCharge));
        Assert.assertEquals(1, keyRawComparator.compare(key, smallerMz));
        Assert.assertEquals(-1, keyRawComparator.compare(key, largerCharge));
        Assert.assertEquals(-1, keyRawComparator.compare(key, largerMz));
    }

    @Test
    public void testRaw() throws Exception {

        byte[] key = bytes(547, 2);

        byte[]  same = bytes(547, 2);
        byte[]  smallerCharge = bytes(547, 1);
        byte[]  largerCharge = bytes(547, 3);
        byte[]  smallerMz = bytes(347, 2);
        byte[]  largerMz = bytes(647, 2);

        DefaultSpectrumKeyRawComparator rawComparator = new DefaultSpectrumKeyRawComparator();

        Assert.assertEquals(0, rawComparator.compare(key, 0, key.length, same, 0, same.length));
        Assert.assertEquals(1, rawComparator.compare(key, 0, key.length, smallerCharge, 0, smallerCharge.length));
        Assert.assertEquals(1, rawComparator.compare(key, 0, key.length, smallerMz, 0, smallerMz.length));
        Assert.assertEquals(-1, rawComparator.compare(key, 0, key.length, largerCharge, 0, largerCharge.length));
        Assert.assertEquals(-1, rawComparator.compare(key, 0, key.length, largerMz, 0, largerMz.length));
    }

    @Test
    public void testCompare2() throws Exception {

        byte[] key1 = bytes(350.243284, 2);
        byte[] key2 = bytes(350.242826, 2);

        DefaultSpectrumKeyRawComparator rawComparator = new DefaultSpectrumKeyRawComparator();

        Assert.assertEquals(1, rawComparator.compare(key1, 0, key1.length, key2, 0, key2.length));
        Assert.assertEquals(-1, rawComparator.compare(key2, 0, key2.length, key1, 0, key1.length));
    }

    private byte[] bytes(double mz, int charge) throws IOException {

        SpectrumKey key = new SpectrumKey();
        key.set(charge, mz);

        MockDataOutput out = new MockDataOutput(12);
        key.write(out);

        return out.getBytes();
    }

    private SpectrumKey newKey(Peak precursor) {

        SpectrumKey key = new SpectrumKey();
        key.set(precursor.getCharge(), precursor.getMz());
        return key;
    }
}
