/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.hadoop.io;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.peaklist.*;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.IdentityPeakProcessor;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberDiscrete;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.*;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class MsnSpectrumValueTest {

    private final String base64 = "AAIGAOxRuB6FXZpAAAAAAACAX0AEAQCcAQpTaG9jawAAEi9kZXYvbnVsbACEt9KtxM/MnrkB/dqv\n" +
            "vO6zjfPJAQAACFK4HoXrX5pAAAAAAABwfkApXI/C9f+kQAAAAAAAcIhAKVyPwvXPrEAAAAAAAAAo\n" +
            "QBSuR+H6T7JAAAAAAABAj0AA";

    @Test
    public void testWrite() throws Exception {

        MsnSpectrum spectrum = new MsnSpectrum(PeakList.Precision.DOUBLE);
        spectrum.setId(UUID.fromString("5c9e993e-22da-4dc2-9b0c-e5308c3a0941"));
        spectrum.setMsLevel(2);
        spectrum.setFragMethod("Shock");
        spectrum.setParentScanNumber(new ScanNumberDiscrete(78));

        spectrum.getPrecursor().setValues(1687.38, 126, 3);
        spectrum.setMsLevel(2);

        spectrum.add(1687.98, 487);
        spectrum.add(2687.98, 782);
        spectrum.add(3687.98, 12);
        spectrum.add(4687.98, 1000);

        MsnSpectrumValue value = new MsnSpectrumValue(Optional.<PeakList.Precision>absent(), Collections.<PeakProcessor<PeakAnnotation, PeakAnnotation>>emptyList());

        MockDataOutput out = new MockDataOutput(128 * 2);

        value.set(spectrum);
        value.write(out);

        Assert.assertEquals(base64, out.getBase64());
    }

    @Test
    public void testReadFields() throws Exception {

        MsnSpectrumValue value = new MsnSpectrumValue(Optional.<PeakList.Precision>absent(), Collections.<PeakProcessor<PeakAnnotation, PeakAnnotation>>emptyList());

        MockDataInput dataInput = new MockDataInput(base64);
        value.readFields(dataInput);

        MsnSpectrum spectrum = value.get();

        Assert.assertEquals(2, spectrum.getMsLevel());

        Assert.assertEquals(PeakList.Precision.DOUBLE, spectrum.getPrecision());
        Assert.assertEquals(4, spectrum.size());
        Assert.assertEquals(UUID.fromString("5c9e993e-22da-4dc2-9b0c-e5308c3a0941"), spectrum.getId());
        Assert.assertEquals(new Peak(1687.38, 126, 3), spectrum.getPrecursor());
        Assert.assertEquals(2, spectrum.getMsLevel());
        Assert.assertEquals("Shock", spectrum.getFragMethod());
        Assert.assertEquals(new ScanNumberDiscrete(78), spectrum.getParentScanNumber());

        Assert.assertEquals(1687.98, spectrum.getMz(0), 0.000001);
        Assert.assertEquals(487, spectrum.getIntensity(0), 0.000001);
        Assert.assertEquals(2687.98, spectrum.getMz(1), 0.000001);
        Assert.assertEquals(782, spectrum.getIntensity(1), 0.000001);
        Assert.assertEquals(3687.98, spectrum.getMz(2), 0.000001);
        Assert.assertEquals(12, spectrum.getIntensity(2), 0.000001);
        Assert.assertEquals(4687.98, spectrum.getMz(3), 0.000001);
        Assert.assertEquals(1000, spectrum.getIntensity(3), 0.000001);

        Assert.assertEquals(0, spectrum.getAnnotationIndexes().length);
    }

    @Test
    public void testRoundTrip() throws Exception {

        MsnSpectrum spectrum = new MsnSpectrum(PeakList.Precision.DOUBLE);
        spectrum.setFragMethod("Shock");
        spectrum.setParentScanNumber(new ScanNumberDiscrete(78));

        spectrum.setMsLevel(2);

        spectrum.add(1687.98, 487);
        spectrum.add(2687.98, 782);
        spectrum.add(3687.98, 12);
        spectrum.add(4687.98, 1000);

        MsnSpectrumValue value = new MsnSpectrumValue();

        MockDataOutput out = new MockDataOutput(128 * 2);

        value.set(spectrum);
        value.write(out);

        String base64 = out.getBase64();

        value.readFields(new MockDataInput(base64));
        MsnSpectrum readSpectrum = value.get();

        Assert.assertEquals(spectrum, readSpectrum);
    }

    @Test
    public void testProcessed() throws Exception {

        IdentityPeakProcessor<PeakAnnotation> peakProcessor = Mockito.spy(new IdentityPeakProcessor<>());
        MsnSpectrumValue value = new MsnSpectrumValue(Optional.<PeakList.Precision>absent(), Collections.<PeakProcessor<PeakAnnotation, PeakAnnotation>>singletonList(peakProcessor));

        value.readFields(new MockDataInput(base64));

        MsnSpectrum spectrum = value.get();

        Assert.assertEquals(2, spectrum.getMsLevel());

        //Precursor info is in the key so this will not be set

        Assert.assertEquals(PeakList.Precision.DOUBLE, spectrum.getPrecision());
        Assert.assertEquals(4, spectrum.size());

        double delta = 0.01;
        Assert.assertEquals(1687.98, spectrum.getMz(0), delta);
        Assert.assertEquals(487, spectrum.getIntensity(0), delta);
        Assert.assertEquals(2687.98, spectrum.getMz(1), delta);
        Assert.assertEquals(782, spectrum.getIntensity(1), delta);
        Assert.assertEquals(3687.98, spectrum.getMz(2), delta);
        Assert.assertEquals(12, spectrum.getIntensity(2), delta);
        Assert.assertEquals(4687.98, spectrum.getMz(3), delta);
        Assert.assertEquals(1000, spectrum.getIntensity(3), delta);

        Assert.assertEquals(0, spectrum.getAnnotationIndexes().length);

        verify(peakProcessor).setSink(Mockito.<PeakSink<PeakAnnotation>>any());
        verify(peakProcessor).start(4);
        verify(peakProcessor, times(4)).processPeak(anyDouble(), anyDouble(), Mockito.<List<PeakAnnotation>>any());
        verify(peakProcessor).end();
        verifyNoMoreInteractions(peakProcessor);
    }

    @Test
    public void testProcessedDifferentPeakPrecision() throws Exception {

        IdentityPeakProcessor<PeakAnnotation> peakProcessor = Mockito.spy(new IdentityPeakProcessor<>());
        MsnSpectrumValue value = new MsnSpectrumValue(Optional.of(PeakList.Precision.FLOAT), Collections.<PeakProcessor<PeakAnnotation, PeakAnnotation>>singletonList(peakProcessor));


        value.readFields(new MockDataInput(base64));

        MsnSpectrum spectrum = value.get();

        Assert.assertEquals(2, spectrum.getMsLevel());

        //Precursor info is in the key so this will not be set

        Assert.assertEquals(PeakList.Precision.FLOAT, spectrum.getPrecision());
        Assert.assertEquals(4, spectrum.size());

        double delta = 0.01;
        Assert.assertEquals(1687.98, spectrum.getMz(0), delta);
        Assert.assertEquals(487, spectrum.getIntensity(0), delta);
        Assert.assertEquals(2687.98, spectrum.getMz(1), delta);
        Assert.assertEquals(782, spectrum.getIntensity(1), delta);
        Assert.assertEquals(3687.98, spectrum.getMz(2), delta);
        Assert.assertEquals(12, spectrum.getIntensity(2), delta);
        Assert.assertEquals(4687.98, spectrum.getMz(3), delta);
        Assert.assertEquals(1000, spectrum.getIntensity(3), delta);

        Assert.assertEquals(0, spectrum.getAnnotationIndexes().length);

        verify(peakProcessor).setSink(Mockito.<PeakSink<PeakAnnotation>>any());
        verify(peakProcessor).start(4);
        verify(peakProcessor, times(4)).processPeak(anyDouble(), anyDouble(), Mockito.<List<PeakAnnotation>>any());
        verify(peakProcessor).end();
        verifyNoMoreInteractions(peakProcessor);
    }
}
