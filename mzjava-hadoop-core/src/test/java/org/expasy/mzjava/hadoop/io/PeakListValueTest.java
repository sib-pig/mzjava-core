/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.hadoop.io;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.peaklist.*;
import org.expasy.mzjava.hadoop.io.MockDataInput;
import org.expasy.mzjava.hadoop.io.MockDataOutput;
import org.expasy.mzjava.hadoop.io.PeakListValue;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.UUID;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeakListValueTest {

    private final String base64 = "AAQEBgBcj8L1KJR8QAAAAAAAUHVAyIDSptPag5A056jq6YjqwsLzAQAAFAAAAAAAAFlAAAAAAAAA\n" +
            "8D8AAAAAAABpQAAAAAAAAABAAAAAAADAckAAAAAAAAAIQAAAAAAAAHlAAAAAAAAAEEAAAAAAAEB/\n" +
            "QAAAAAAAABRAAAAAAADAgkAAAAAAAAAYQAAAAAAA4IVAAAAAAAAAHEAAAAAAAACJQAAAAAAAACBA\n" +
            "AAAAAAAgjEAAAAAAAAAiQAAAAAAAQI9AAAAAAAAAJEAA";

    @Test
    public void testWrite() throws Exception {

        PeakListValue value = new PeakListValue(
                Optional.<PeakList.Precision>absent(),
                Collections.<PeakProcessor<PeakAnnotation, PeakAnnotation>>emptyList());

        MockDataOutput out = new MockDataOutput(128*2);

        PeakList<PeakAnnotation> peakList = new DoublePeakList<>(10);
        peakList.setId(UUID.fromString("1a10076a-9a6a-4024-863d-7a57b962b5cc"));
        peakList.getPrecursor().setValues(457.26, 341, 2, 3);
        peakList.addSorted(
                new double[]{100, 200, 300, 400, 500, 600, 700, 800, 900, 1000},
                new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
        );

        value.set(peakList);
        value.write(out);
        out.flush();

        Assert.assertEquals(base64, out.getBase64());
    }

    @Test
    public void testReadFields() throws Exception {

        PeakListValue value = new PeakListValue();

        value.readFields(new MockDataInput(base64));

        PeakList peakList = value.get();

        Peak precursor = peakList.getPrecursor();
        Assert.assertEquals(457.26, precursor.getMz(), 0.000001);
        Assert.assertEquals(341.0, precursor.getIntensity(), 0.000001);
        Assert.assertArrayEquals(new int[]{2, 3}, precursor.getChargeList());

        Assert.assertEquals(10, peakList.size());
        Assert.assertEquals(UUID.fromString("1a10076a-9a6a-4024-863d-7a57b962b5cc"), peakList.getId());
        Assert.assertEquals(PeakList.Precision.DOUBLE, peakList.getPrecision());
        Assert.assertArrayEquals(
                new double[]{100, 200, 300, 400, 500, 600, 700, 800, 900, 1000},
                peakList.getMzs(new double[10]), 0.0001);
        Assert.assertArrayEquals(
                new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
                peakList.getIntensities(new double[10]), 0.0001);
    }
}
