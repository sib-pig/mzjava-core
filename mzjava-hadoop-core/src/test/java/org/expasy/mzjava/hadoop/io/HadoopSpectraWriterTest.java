/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.hadoop.io;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeDiscrete;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberDiscrete;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class HadoopSpectraWriterTest {

    private String path;
    private final String name = "test.hdio";

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Before
    public void setup() throws Exception {

        path = folder.getRoot().getPath();
    }

    @Test
    public void testDouble() throws Exception {

        runTest(PeakList.Precision.DOUBLE);
    }

    @Test
    public void testDoubleFloat() throws Exception {

        runTest(PeakList.Precision.DOUBLE_FLOAT);
    }

    @Test
    public void testDoubleConstant() throws Exception {

        runTest(PeakList.Precision.DOUBLE_CONSTANT);
    }

    @Test
    public void testFloat() throws Exception {

        runTest(PeakList.Precision.FLOAT);
    }

    @Test
    public void testFloatConstant() throws Exception {

        runTest(PeakList.Precision.FLOAT_CONSTANT);
    }

    @Test
    public void testCloseNoWrite() throws Exception {

        HadoopSpectraWriter<MsnSpectrum> writer = new HadoopSpectraWriter<>(path, name, new MsnSpectrumValue());
        writer.close();

        Assert.assertEquals(true, new File(path, name).exists());
    }

    private void runTest(PeakList.Precision precision) throws IOException {

        MsnSpectrumValue value = new MsnSpectrumValue();

        MsnSpectrum spectrum1 = newSpectrum(235.975, 12, 2, 4456, 231, 154, precision, 2);
        MsnSpectrum spectrum2 = newSpectrum(1537.648, 1087, 2, 5641, 587, 15, precision, 2);
        MsnSpectrum spectrum3 = newSpectrum(171.542, 100, 2, 4501, 318, 1043, precision, 3);

        HadoopSpectraWriter<MsnSpectrum> writer = new HadoopSpectraWriter<>(path, name, value);
        writer.write(spectrum1);
        writer.write(spectrum3);
        writer.write(spectrum2);
        writer.close();

        //Test reading
        List<MsnSpectrum> readSpectra = new ArrayList<>();

        HadoopSpectraReader<PeakAnnotation, MsnSpectrum> reader = new HadoopSpectraReader<>(new Configuration(), new Path(path, name), value);
        while (reader.hasNext()) {

            readSpectra.add(reader.next());
        }
        reader.close();

        Assert.assertEquals(3, readSpectra.size());
        checkSpectrum(spectrum1, readSpectra.get(0));
        checkSpectrum(spectrum2, readSpectra.get(1));
        checkSpectrum(spectrum3, readSpectra.get(2));
    }

    /**
     * The problem was that when the file got large enough to require more than one segment for sorting
     * windows had issues when cygwin is not installed
     *
     * @throws Exception because it is a test
     */
    @Test
    public void runNoExceptionOnSort() throws Exception {

        MsnSpectrumValue value = new MsnSpectrumValue();

        HadoopSpectraWriter<MsnSpectrum> writer = new HadoopSpectraWriter<>(path, name, value);

        Random random = new Random();
        for (int i = 0; i < 11111; i++) {

            writer.write(newSpectrum(random.nextDouble() * 5000, 12, 2, 4456, 231, 154, PeakList.Precision.DOUBLE, 2));
        }

        writer.close(); //On win an exception was thrown here
    }

    @Test
    public void testChargeList() throws Exception {

        MsnSpectrumValue value = new MsnSpectrumValue();

        PeakList.Precision precision = PeakList.Precision.DOUBLE;

        MsnSpectrum spectrum1 = newSpectrum(235.975, 12, 2, 4456, 231, 154, precision, 2, 3);
        MsnSpectrum spectrum2 = newSpectrum(1537.648, 1087, 2, 5641, 587, 15, precision, 2, 3);
        MsnSpectrum spectrum3 = newSpectrum(171.542, 100, 2, 4501, 318, 1043, precision, 3, 4);

        HadoopSpectraWriter<MsnSpectrum> writer = new HadoopSpectraWriter<>(path, name, value);
        writer.write(spectrum1);
        writer.write(spectrum3);
        writer.write(spectrum2);
        writer.close();

        //Test reading
        List<MsnSpectrum> readSpectra = new ArrayList<>();

        HadoopSpectraReader<PeakAnnotation, MsnSpectrum> reader = new HadoopSpectraReader<>(new Configuration(), new Path(path, name), value);
        while (reader.hasNext()) {

            readSpectra.add(reader.next());
        }
        reader.close();

        Assert.assertEquals(3, readSpectra.size());
        checkSpectrum(spectrum1, readSpectra.get(0));
        checkSpectrum(spectrum2, readSpectra.get(1));
        checkSpectrum(spectrum3, readSpectra.get(2));
    }

    private void checkSpectrum(MsnSpectrum expected, MsnSpectrum actual) {

        Assert.assertEquals(expected.getPrecision(), actual.getPrecision());
        Assert.assertEquals(expected.getMsLevel(), actual.getMsLevel());

        Assert.assertEquals(expected.getScanNumbers(), actual.getScanNumbers());
        Assert.assertEquals(expected.getRetentionTimes(), actual.getRetentionTimes());

        Assert.assertEquals(expected.getPrecursor(), actual.getPrecursor());

        Assert.assertEquals(expected.size(), actual.size());
        for (int i = 0; i < expected.size(); i++) {

            Assert.assertEquals(expected.getMz(i), actual.getMz(i), 0.0000001);
            Assert.assertEquals(expected.getIntensity(i), expected.getIntensity(i), 0.0000001);
        }

        for (int i = 0; i < expected.size(); i++) {

            Assert.assertEquals(expected.hasAnnotationsAt(i), actual.hasAnnotationsAt(i));
            if (expected.hasAnnotationsAt(i)) {

                Assert.assertEquals(expected.getAnnotations(i), actual.getAnnotations(i));
            }
        }
    }

    private double[] randomArray(int size, double min, double max) {

        Random random = new Random();
        double range = max - min;

        double[] arr = new double[size];

        for (int i = 0; i < arr.length; i++) {

            arr[i] = min + random.nextDouble() * range;
        }

        return arr;
    }

    private MsnSpectrum newSpectrum(double precursorMz, double precursorIntensity, int msLevel, int scanNum, double retention, int size, PeakList.Precision precision, int... precursorCharges) {


        double[] mzs = randomArray(size, 200, 3000);
        Arrays.sort(mzs);
        double[] intensities = randomArray(size, 1, 1000);

        MsnSpectrum spectrum = new MsnSpectrum(mzs.length, precision);
        spectrum.addRetentionTime(new RetentionTimeDiscrete(retention, TimeUnit.SECOND));
        spectrum.addScanNumber(new ScanNumberDiscrete(scanNum));
        spectrum.setMsLevel(msLevel);
        Peak precursor = spectrum.getPrecursor();
        precursor.setValues(precursorMz, precursorIntensity, precursorCharges);

        for (int i = 0; i < mzs.length; i++) {

            spectrum.add(mzs[i], intensities[i]);
        }

        return spectrum;
    }
}
