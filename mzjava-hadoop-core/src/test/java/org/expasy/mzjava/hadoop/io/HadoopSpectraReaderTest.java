/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.hadoop.io;

import com.google.common.base.Optional;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessor;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.SqrtTransformer;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeDiscrete;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberDiscrete;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class HadoopSpectraReaderTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void test() throws Exception {

        MsnSpectrumValue value = newPeptideMsnSpectrumValue(Optional.<PeakList.Precision>absent(), Collections.<PeakProcessor<PeakAnnotation, PeakAnnotation>>emptyList());

        String path = folder.getRoot().getPath();
        String name = "test.hdio";

        File file = new File(path, name);

        if (file.exists()) Assert.assertTrue(file.delete());

        PeakList.Precision precision = PeakList.Precision.FLOAT;
        MsnSpectrum spectrum3 = newSpectrum(171.542, 100, 3, 2, 4501, 318, 1043, precision);
        MsnSpectrum spectrum1 = newSpectrum(235.975, 12, 2, 2, 4456, 231, 154, precision);
        MsnSpectrum spectrum2 = newSpectrum(1537.648, 1087, 2, 2, 5641, 587, 15, precision);

        HadoopSpectraWriter<MsnSpectrum> writer = new HadoopSpectraWriter<>(path, name, value);
        writer.write(spectrum1);
        writer.write(spectrum2);
        writer.write(spectrum3);
        writer.close();

        HadoopSpectraReader<PeakAnnotation, MsnSpectrum> reader = new HadoopSpectraReader<>(new Configuration(), new Path(path, name), value);

//        Assert.assertEquals(true, reader.hasNext());
        assertSpectrumEquals(spectrum1, reader.next());
        Assert.assertEquals(true, reader.hasNext());
        assertSpectrumEquals(spectrum2, reader.next());
        Assert.assertEquals(true, reader.hasNext());
        assertSpectrumEquals(spectrum3, reader.next());
        Assert.assertEquals(false, reader.hasNext());
        Assert.assertEquals(false, reader.hasNext());

        reader.close();

        if (file.exists()) Assert.assertTrue(file.delete());
    }

    private void assertSpectrumEquals(MsnSpectrum expected, MsnSpectrum actual) {

        Assert.assertEquals(expected.getComment(), actual.getComment());
        Assert.assertEquals(expected.getFragMethod(), actual.getFragMethod());
        Assert.assertEquals(expected.getParentScanNumber(), actual.getParentScanNumber());
        Assert.assertEquals(expected.getRetentionTimes(), actual.getRetentionTimes());
        Assert.assertEquals(expected.getSpectrumIndex(), actual.getSpectrumIndex());
        Assert.assertEquals(expected.getScanNumbers(), actual.getScanNumbers());
        Assert.assertEquals(expected.getSpectrumSource(), actual.getSpectrumSource());
        Assert.assertEquals(expected.getMsLevel(), actual.getMsLevel());
        Assert.assertEquals(expected.getId(), actual.getId());

        Assert.assertEquals(expected.size(), actual.size());
        for(int i = 0; i < expected.size(); i++) {

            Assert.assertEquals(expected.getMz(i), actual.getMz(i), 0.000001);
            Assert.assertEquals(expected.getIntensity(i), actual.getIntensity(i), 0.000001);
            Assert.assertEquals(expected.getAnnotations(i), actual.getAnnotations(i));
        }
    }

    @Test
    public void testWithProcessor() throws Exception {

        MsnSpectrumValue writeValue = newPeptideMsnSpectrumValue(Optional.<PeakList.Precision>absent(), Collections.<PeakProcessor<PeakAnnotation, PeakAnnotation>>emptyList());

        String path = folder.getRoot().getPath();
        String name = "test.hdio";

        File file = new File(path, name);

        if (file.exists()) Assert.assertTrue(file.delete());

        PeakList.Precision precision = PeakList.Precision.FLOAT;
        MsnSpectrum spectrum3 = newSpectrum(171.542, 100, 3, 2, 4501, 318, 1043, precision);
        MsnSpectrum spectrum1 = newSpectrum(235.975, 12, 2, 2, 4456, 231, 154, precision);
        MsnSpectrum spectrum2 = newSpectrum(1537.648, 1087, 2, 2, 5641, 587, 15, precision);

        HadoopSpectraWriter<MsnSpectrum> writer = new HadoopSpectraWriter<>(path, name, writeValue);
        writer.write(spectrum1);
        writer.write(spectrum2);
        writer.write(spectrum3);
        writer.close();

        List<PeakProcessor<PeakAnnotation, PeakAnnotation>> peakProcessors = Collections.<PeakProcessor<PeakAnnotation, PeakAnnotation>>singletonList(new SqrtTransformer<PeakAnnotation>());
        MsnSpectrumValue readerValue = newPeptideMsnSpectrumValue(Optional.<PeakList.Precision>absent(), peakProcessors);
        HadoopSpectraReader<PeakAnnotation, MsnSpectrum> reader = new HadoopSpectraReader<>(new Configuration(), new Path(path, name), readerValue);

        Assert.assertEquals(true, reader.hasNext());
        checkSpectrum(spectrum1, reader.next());
        Assert.assertEquals(true, reader.hasNext());
        checkSpectrum(spectrum2, reader.next());
        Assert.assertEquals(true, reader.hasNext());
        checkSpectrum(spectrum3, reader.next());
        Assert.assertEquals(false, reader.hasNext());

        reader.close();

        if (file.exists()) Assert.assertTrue(file.delete());
    }

    private void checkSpectrum(MsnSpectrum expected, MsnSpectrum actual) {

        Assert.assertEquals(expected.size(), actual.size());
        Assert.assertEquals(expected.getId(), actual.getId());

        for(int i = 0; i < expected.size(); i++) {

            Assert.assertEquals(expected.getMz(i), actual.getMz(i), 0.0000001);
            Assert.assertEquals(Math.sqrt(expected.getIntensity(i)), actual.getIntensity(i), 0.00001);
        }
    }

    private MsnSpectrum newSpectrum(double precursorMz, double precursorIntensity, int precursorCharge, int msLevel, int scanNum, double retention, int size, PeakList.Precision precision) {


        double[] mzs = randomArray(size, 200, 3000);
        Arrays.sort(mzs);
        double[] intensities = randomArray(size, 1, 1000);

        MsnSpectrum spectrum = new MsnSpectrum(mzs.length, precision);
        spectrum.addRetentionTime(new RetentionTimeDiscrete(retention, TimeUnit.SECOND));
        spectrum.addScanNumber(new ScanNumberDiscrete(scanNum));
        spectrum.setMsLevel(msLevel);
        Peak precursor = spectrum.getPrecursor();
        precursor.setMzAndCharge(precursorMz, precursorCharge);
        precursor.setIntensity(precursorIntensity);

        for (int i = 0; i < mzs.length; i++) {

            spectrum.add(mzs[i], intensities[i]);
        }

        return spectrum;
    }

    private double[] randomArray(int size, double min, double max) {

        Random random = new Random();
        double range = max - min;

        double[] arr = new double[size];

        for (int i = 0; i < arr.length; i++) {

            arr[i] = min + random.nextDouble() * range;
        }

        return arr;
    }

    public static MsnSpectrumValue newPeptideMsnSpectrumValue(Optional<PeakList.Precision> precisionOverride, List<PeakProcessor<PeakAnnotation, PeakAnnotation>> peakProcessors) {

        return new MsnSpectrumValue(precisionOverride, peakProcessors);
    }
}
