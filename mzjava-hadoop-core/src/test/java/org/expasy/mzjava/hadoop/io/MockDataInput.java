/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.hadoop.io;

import org.apache.commons.codec.binary.Base64;

import java.io.DataInput;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class MockDataInput extends InputStream implements DataInput {

    private final ByteBuffer buffer;

    public MockDataInput(String base64) {

        buffer = ByteBuffer.wrap(new Base64().decode(base64));
    }

    @Override
    public void readFully(byte[] b) throws IOException {

        for(int i = 0; i < b.length; i++) {

            b[i] = buffer.get();
        }
    }

    @Override
    public void readFully(byte[] b, int off, int len) throws IOException {

        throw new UnsupportedOperationException();
    }

    @Override
    public int skipBytes(int n) throws IOException {

        buffer.position(buffer.position() + n);

        return n;
    }

    @Override
    public boolean readBoolean() throws IOException {

        return buffer.get() == 1;
    }

    @Override
    public byte readByte() throws IOException {

        return buffer.get();
    }

    @Override
    public int readUnsignedByte() throws IOException {

        throw new UnsupportedOperationException();
    }

    @Override
    public short readShort() throws IOException {

        return buffer.getShort();
    }

    @Override
    public int readUnsignedShort() throws IOException {

        throw new UnsupportedOperationException();
    }

    @Override
    public char readChar() throws IOException {

        return buffer.getChar();
    }

    @Override
    public int readInt() throws IOException {

        return buffer.getInt();
    }

    @Override
    public long readLong() throws IOException {

        return buffer.getLong();
    }

    @Override
    public float readFloat() throws IOException {

        return buffer.getFloat();
    }

    @Override
    public double readDouble() throws IOException {

        return buffer.getDouble();
    }

    @Override
    public String readLine() throws IOException {

        throw new UnsupportedOperationException();
    }

    @Override
    public String readUTF() throws IOException {

        int length = buffer.getInt();

        byte[] bytes = new byte[length];

        buffer.get(bytes);

        return new String(bytes, 0, length, "UTF8");
    }

    @Override
    public int read() throws IOException {

        if(buffer.remaining() <= 0)
            return -1;

        return buffer.get() & 0xff;
    }
}
