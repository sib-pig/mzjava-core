package org.expasy.mzjava.hadoop.io;

import org.apache.hadoop.io.serializer.Deserializer;
import org.apache.hadoop.io.serializer.Serializer;
import org.expasy.mzjava.avro.io.UUIDReader;
import org.expasy.mzjava.avro.io.UUIDWriter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.netbeans.junit.MockServices;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.UUID;

import static org.mockito.Mockito.*;

public class MzJavaSerializationTest {

    @Before
    public void setUp() throws Exception {

        MockServices.setServices(UUIDReader.class, UUIDWriter.class);
    }

    @Test
    public void testAccept() throws Exception {

        MzJavaSerialization serialization = new MzJavaSerialization();
        Assert.assertEquals(true, serialization.accept(UUID.class));
    }

    @Test
    public void testGetSerializer() throws Exception {

        MzJavaSerialization serialization = new MzJavaSerialization();

        final Serializer<Object> serializer = serialization.getSerializer((Class)UUID.class);
        Assert.assertNotNull(serializer);

        ByteArrayOutputStream outStream = spy(new ByteArrayOutputStream(19));
        serializer.open(outStream);
        serializer.serialize(UUID.fromString("e858c6e7-ae4b-45b3-9680-139e3e60b5e6"));
        serializer.close();

        verify(outStream, times(2)).write(Mockito.any(byte[].class), Mockito.anyInt(), Mockito.anyInt());
        verify(outStream).flush();
        verify(outStream).close();
        verifyNoMoreInteractions(outStream);

        Assert.assertArrayEquals(new byte[]{-103, -23, -91, -101, -118, -58, -100, -89, 47, -77, -88, -6, -103, -72, -104, -10, -1, -46, 1}, outStream.toByteArray());
    }

    @Test
    public void testGetDeserializer() throws Exception {

        MzJavaSerialization serialization = new MzJavaSerialization();

        final Deserializer<Object> deserializer = serialization.getDeserializer((Class) UUID.class);
        Assert.assertNotNull(deserializer);

        final InputStream inputStream = spy(new ByteArrayInputStream(
                new byte[]{-103, -23, -91, -101, -118, -58, -100, -89, 47, -77, -88, -6, -103, -72, -104, -10, -1, -46, 1}
        ));
        deserializer.open(inputStream);
        UUID uuid = (UUID)deserializer.deserialize(null);
        deserializer.close();

        Assert.assertEquals(UUID.fromString("e858c6e7-ae4b-45b3-9680-139e3e60b5e6"), uuid);
        //noinspection ResultOfMethodCallIgnored
        verify(inputStream, times(19)).read();
        verify(inputStream).close();
        verifyNoMoreInteractions(inputStream);
    }
}