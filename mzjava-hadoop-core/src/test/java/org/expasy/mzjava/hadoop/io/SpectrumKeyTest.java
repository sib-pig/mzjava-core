/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.hadoop.io;

import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.spectrum.Spectrum;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.UUID;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class SpectrumKeyTest {

    @Test
    public void testWrite() throws Exception {

        Peak precursor = new Peak(785.987, 674, 2);
        UUID id = new UUID(864657, 893568);

        Spectrum spectrum = mockSpectrum(precursor, id);

        SpectrumKey key = new SpectrumKey();
        key.set(spectrum.getPrecursor());

        Assert.assertEquals(785.987, key.getMz(), 0.0001);
        Assert.assertEquals(2, key.getCharge());

        MockDataOutput out = new MockDataOutput(256);

        key.write(out);

        Assert.assertEquals("AAAAAkCIj+VgQYk3", out.toString());
    }

    @Test
    public void testReadFields() throws Exception {

        MockDataInput in = new MockDataInput("AAAAAkCIj+VgQYk3");

        SpectrumKey key = new SpectrumKey();
        key.readFields(in);

        Assert.assertEquals(785.987, key.getMz(), 0.0001);
        Assert.assertEquals(2, key.getCharge());
    }

    @Test
    public void testSort() throws Exception {

        Spectrum spectrum = mockSpectrum(new Peak(547, 100, 2), UUID.randomUUID());

        Spectrum spectrumSame = mockSpectrum(new Peak(547, 100, 2), UUID.randomUUID());
        Spectrum spectrumSmallerCharge = mockSpectrum(new Peak(547, 100, 1), UUID.randomUUID());
        Spectrum spectrumLargerCharge = mockSpectrum(new Peak(547, 100, 3), UUID.randomUUID());
        Spectrum spectrumSmallerMz = mockSpectrum(new Peak(347, 100, 2), UUID.randomUUID());
        Spectrum spectrumLargerMz = mockSpectrum(new Peak(647, 100, 2), UUID.randomUUID());

        SpectrumKey key = new SpectrumKey();
        key.set(spectrum.getPrecursor().getCharge(), spectrum.getPrecursor().getMz());

        Assert.assertEquals(0, key.compareTo(makeKey(spectrumSame)));
        Assert.assertEquals(1, key.compareTo(makeKey(spectrumSmallerCharge)));
        Assert.assertEquals(1, key.compareTo(makeKey(spectrumSmallerMz)));
        Assert.assertEquals(-1, key.compareTo(makeKey(spectrumLargerCharge)));
        Assert.assertEquals(-1, key.compareTo(makeKey(spectrumLargerMz)));
    }

    private Spectrum mockSpectrum(Peak precursor, UUID id) {

        Spectrum spectrum = Mockito.mock(Spectrum.class);
        Mockito.when(spectrum.getPrecursor()).thenReturn(precursor);
        Mockito.when(spectrum.getId()).thenReturn(id);
        return spectrum;
    }

    private SpectrumKey makeKey(Spectrum spectrum) {

        SpectrumKey key = new SpectrumKey();
        key.set(spectrum.getPrecursor().getCharge(), spectrum.getPrecursor().getMz());

        return key;
    }
}
