package org.expasy.mzjava.utils;

import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.util.HashSet;

public class PathUtilsTest {

    @Rule
    public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void testListFilePaths() throws Exception {

        File mgf1 = temporaryFolder.newFile("file1.mgf");
        File pepXml1 = temporaryFolder.newFile("file1.pep.xml");
        File mgf2 = temporaryFolder.newFile("file2.mgf");
        File pepXml2 = temporaryFolder.newFile("file2.pep.xml");

        final String absolutePath = temporaryFolder.getRoot().getAbsolutePath();
        Assert.assertEquals(
                Sets.newHashSet(pepXml1.getAbsolutePath(), pepXml2.getAbsolutePath()),
                new HashSet<>(PathUtils.listFilePaths("file://" + absolutePath.replace('\\', '/').replace("C:", ""), "pep.xml")));
        Assert.assertEquals(
                Sets.newHashSet(mgf1.getAbsolutePath(), mgf2.getAbsolutePath()),
                new HashSet<>(PathUtils.listFilePaths(absolutePath, "mgf")));

        Assert.assertEquals(
                Sets.newHashSet(mgf1.getAbsolutePath()),
                new HashSet<>(PathUtils.listFilePaths(absolutePath + "/file1.mgf", "mgf")));
    }

    @Test
    public void testPathExists() throws Exception {

        temporaryFolder.newFile("file1.mgf");
        File root = temporaryFolder.getRoot();

        Assert.assertEquals(true, PathUtils.pathExists(new File(root, "file1.mgf").getPath()));
        Assert.assertEquals(false, PathUtils.pathExists(new File(root, "file2.mgf").getPath()));
    }

    @Test
    public void testDeletePath() throws Exception {

        File mgf1 = temporaryFolder.newFile("file1.mgf");
        Assert.assertEquals(true, mgf1.exists());
        File root = temporaryFolder.getRoot();
        Assert.assertEquals(true, root.exists());

        PathUtils.deletePath(root.getPath());

        Assert.assertEquals(false, mgf1.exists());
        Assert.assertEquals(false, root.exists());
    }
}