package org.expasy.mzjava.spark;

import org.expasy.mzjava.core.ms.Tolerance;
import org.junit.Assert;
import org.junit.Test;

public class CachedPpmToleranceTest {

    @Test
    public void testGet() throws Exception {

        Cached<Tolerance> cached = new CachedPpmTolerance(10);

        Tolerance tolerance = cached.get();
        Assert.assertNotNull(tolerance);
        Assert.assertSame(tolerance, cached.get());
    }
}