package org.expasy.mzjava.spark;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AvroReader;
import org.expasy.mzjava.avro.io.AvroWriter;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class KryoAvroSerializerTest {

    @SuppressWarnings("unchecked")
    @Test
    public void testWrite() throws Exception {

        AvroWriter<Object> writer = mock(AvroWriter.class);
        AvroReader<Object> reader = mock(AvroReader.class);

        KryoAvroSerializer<Object> serializer = new KryoAvroSerializer<>(writer, reader);

        serializer.write(mock(Kryo.class), mock(Output.class), "To write");

        verify(writer, Mockito.times(1)).write(Mockito.anyString(), any(Encoder.class));
    }

    @SuppressWarnings("unchecked")
    @Test(expected = IllegalStateException.class)
    public void testWriteException() throws Exception {

        AvroWriter<Object> writer = mock(AvroWriter.class);
        AvroReader<Object> reader = mock(AvroReader.class);

        KryoAvroSerializer<Object> serializer = new KryoAvroSerializer<>(writer, reader);
        Mockito.doThrow(new IOException()).when(writer).write(any(Object.class), any(Encoder.class));

        serializer.write(mock(Kryo.class), mock(Output.class), "To write");

        verify(writer, Mockito.times(1)).write(Mockito.anyString(), any(Encoder.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testRead() throws Exception {

        AvroWriter<Object> writer = mock(AvroWriter.class);
        AvroReader<Object> reader = mock(AvroReader.class);

        when(reader.read(any(Decoder.class))).thenReturn("A value");

        KryoAvroSerializer<Object> serializer = new KryoAvroSerializer<>(writer, reader);

        Object read = serializer.read(mock(Kryo.class), mock(Input.class), Object.class);

        Assert.assertEquals(read.toString(), "A value");
    }

    @SuppressWarnings("unchecked")
    @Test(expected = IllegalStateException.class)
    public void testReadWithException() throws Exception {

        AvroWriter<Object> writer = mock(AvroWriter.class);
        AvroReader<Object> reader = mock(AvroReader.class);

        when(reader.read(any(Decoder.class))).thenThrow(new IOException());

        KryoAvroSerializer<Object> serializer = new KryoAvroSerializer<>(writer, reader);

        serializer.read(mock(Kryo.class), mock(Input.class), Object.class);
    }
}