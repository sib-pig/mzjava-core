package org.expasy.mzjava.spark;

import org.expasy.mzjava.core.ms.Tolerance;
import org.junit.Assert;
import org.junit.Test;

public class CachedAbsoluteToleranceTest {

    @Test
    public void testGet() throws Exception {

        Cached<Tolerance> cached = new CachedAbsoluteTolerance(0.5);

        Tolerance tolerance = cached.get();
        Assert.assertNotNull(tolerance);
        Assert.assertSame(tolerance, cached.get());
    }
}