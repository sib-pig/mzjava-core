package org.expasy.mzjava.spark;

import com.esotericsoftware.kryo.Kryo;
import org.expasy.mzjava.avro.io.UUIDReader;
import org.expasy.mzjava.avro.io.UUIDWriter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.netbeans.junit.MockServices;

import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class MzJavaKryoRegistratorTest {

    @Before
    public void setUp() throws Exception {

        MockServices.setServices(UUIDReader.class, UUIDWriter.class);
    }

    @Test
    public void testRegisterClasses() throws Exception {

        MzJavaKryoRegistrator kryoRegistrator = new MzJavaKryoRegistrator();

        Kryo kryo = mock(Kryo.class);
        kryoRegistrator.registerClasses(kryo);

        verify(kryo).register(Mockito.eq(UUID.class), Mockito.any(KryoAvroSerializer.class));
    }
}