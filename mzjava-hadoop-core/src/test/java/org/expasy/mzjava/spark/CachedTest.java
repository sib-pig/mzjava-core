package org.expasy.mzjava.spark;

import org.junit.Assert;
import org.junit.Test;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class CachedTest {

    @Test
    public void testGet() throws Exception {

        Cached cached = mock(Cached.class);
        when(cached.get()).thenCallRealMethod();
        when(cached.build()).thenReturn("Hello");

        Assert.assertEquals("Hello", cached.get());
        Assert.assertEquals("Hello", cached.get());
        verify(cached, times(2)).get();
        verify(cached).build();
        verifyNoMoreInteractions(cached);
    }
}