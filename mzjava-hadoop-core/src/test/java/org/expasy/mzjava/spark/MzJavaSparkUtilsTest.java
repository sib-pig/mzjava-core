package org.expasy.mzjava.spark;

import com.google.common.collect.Lists;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.*;
import org.expasy.mzjava.hadoop.io.MsnSpectrumValue;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MzJavaSparkUtilsTest {

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();

    private static JavaSparkContext sc;

    @BeforeClass
    public static void initSpark() {

        System.clearProperty("spark.driver.port");
        System.clearProperty("spark.hostPort");

        SparkConf conf = new SparkConf()
                .setAppName(MzJavaSparkUtilsTest.class.getName())
                .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
                .set("spark.kryo.registrator", "org.expasy.mzjava.spark.MzJavaKryoRegistrator")
                .setMaster("local");
        sc = new JavaSparkContext(conf);
    }

    @AfterClass
    public static void stopSpark() {

        sc.stop();

        System.clearProperty("spark.driver.port");
        System.clearProperty("spark.hostPort");
    }

    @Test
    public void testMsnSpectra() throws Exception {

        Spectrum.Precision precision = PeakList.Precision.DOUBLE;
        List<MsnSpectrum> expectedSpectra = Lists.newArrayList(
                newSpectrum(235.975, 12, 2, 4456, 231, 154, precision, 2),
                newSpectrum(1537.648, 1087, 2, 5641, 587, 15, precision, 2),
                newSpectrum(171.542, 100, 2, 4501, 318, 1043, precision, 3)
        );

        final String pathString = testFolder.newFile("hadoop").getAbsolutePath();

        writeSpectra(expectedSpectra, pathString);

        List<MsnSpectrum> actualSpectra = MzJavaSparkUtils.msnSpectra(sc, pathString).collect();

        Assert.assertEquals(expectedSpectra, actualSpectra);
    }

    private void writeSpectra(List<MsnSpectrum> expectedSpectra, String pathString) throws IOException {

        final SequenceFile.Writer writer = SequenceFile.createWriter(new Configuration(),
                SequenceFile.Writer.file(new Path(pathString)),
                SequenceFile.Writer.keyClass(SpectrumKey.class),
                SequenceFile.Writer.valueClass(MsnSpectrumValue.class),
                SequenceFile.Writer.compression(SequenceFile.CompressionType.NONE));

        SpectrumKey key = new SpectrumKey();
        MsnSpectrumValue value = new MsnSpectrumValue();

        for(MsnSpectrum spectrum : expectedSpectra) {

            key.set(spectrum.getPrecursor());
            value.set(spectrum);

            writer.append(key, value);
        }
        writer.close();
    }

    private MsnSpectrum newSpectrum(double precursorMz, double precursorIntensity, int msLevel, int scanNum, double retention, int size, PeakList.Precision precision, int... precursorCharges) {


        double[] mzs = randomArray(size, 200, 3000);
        Arrays.sort(mzs);
        double[] intensities = randomArray(size, 1, 1000);

        MsnSpectrum spectrum = new MsnSpectrum(mzs.length, precision);
        spectrum.addRetentionTime(new RetentionTimeDiscrete(retention, TimeUnit.SECOND));
        spectrum.addScanNumber(new ScanNumberDiscrete(scanNum));
        spectrum.setMsLevel(msLevel);
        Peak precursor = spectrum.getPrecursor();
        precursor.setValues(precursorMz, precursorIntensity, precursorCharges);

        for (int i = 0; i < mzs.length; i++) {

            spectrum.add(mzs[i], intensities[i]);
        }

        return spectrum;
    }

    private double[] randomArray(int size, double min, double max) {

        Random random = new Random();
        double range = max - min;

        double[] arr = new double[size];

        for (int i = 0; i < arr.length; i++) {

            arr[i] = min + random.nextDouble() * range;
        }

        return arr;
    }
}