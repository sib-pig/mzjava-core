package org.expasy.mzjava.core.ms.peaklist;

import org.expasy.mzjava.core.ms.Tolerance;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeakLists {

    public static double getIntensitySum(double mz, PeakList<?> peakList, Tolerance tolerance) {

        final double minMz = tolerance.getMin(mz);
        final double maxMz = tolerance.getMax(mz);
        double intensitySum = 0;
        for (int index = getClosestSmallerIndex(minMz, peakList); index < peakList.size(); index++) {

            final double currentMz = peakList.getMz(index);
            if (currentMz > maxMz)
                break;

            if (currentMz >= minMz) {
                intensitySum += peakList.getIntensity(index);
            }
        }
        return intensitySum;
    }

    public static int getClosestSmallerIndex(double mz, PeakList<?> peakList) {

        if(peakList.isEmpty())
            return  0;

        int index = peakList.getClosestIndex(mz);
        if (peakList.getMz(index) > mz)
            index = Math.max(0, index - 1);
        return index;
    }
}
