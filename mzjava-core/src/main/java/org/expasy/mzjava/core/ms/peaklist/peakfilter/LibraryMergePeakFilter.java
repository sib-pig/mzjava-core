package org.expasy.mzjava.core.ms.peaklist.peakfilter;

import org.expasy.mzjava.core.ms.spectrum.LibPeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class LibraryMergePeakFilter<A extends PeakAnnotation> extends AbstractMergePeakFilter<A, LibPeakAnnotation> {

    public LibraryMergePeakFilter(double mzMaxDiff, double maxMzClusterWidth, IntensityMode intCombMeth, int totSpectrumCount) {

        super(mzMaxDiff, maxMzClusterWidth, intCombMeth, totSpectrumCount);
    }

    @Override
    protected LibPeakAnnotation createPeakAnnotation(int peakCount, double mzStd, double intensityStd) {

        return new LibPeakAnnotation(peakCount, mzStd, intensityStd);
    }
}
