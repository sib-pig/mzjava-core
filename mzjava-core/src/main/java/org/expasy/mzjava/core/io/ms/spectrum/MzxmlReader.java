package org.expasy.mzjava.core.io.ms.spectrum;

import com.google.common.base.Optional;
import org.apache.commons.codec.binary.Base64;
import org.expasy.mzjava.core.ms.peaklist.*;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.spectrum.RetentionTime;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeDiscrete;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.expasy.mzjava.utils.PrimitiveArrayUtils;
import org.expasy.mzjava.utils.RegexConstants;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.DataFormatException;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.StrictMath.abs;

/**
 * @author fnikitin
 *         Date: 6/24/13
 * @author Oliver Horlacher
 */
public class MzxmlReader extends AbstractMsReader<PeakAnnotation, MsnSpectrum> {

    private final static Logger LOGGER = Logger.getLogger(MzxmlReader.class.getName());

    private static final Pattern DURATION_PATTERN = Pattern.compile("PT(" + RegexConstants.REAL + ")([SM])");

    private static final String PEAKS_COUNT = "peaksCount";

    private static final float DEFAULT_DELTA = 0.001f;

    private static final String PREFIX_HINT_MESSAGE = "To disable this consistency check call";

    public enum ConsistencyCheck {

        PEAKS_COUNT(new ExpectedPeakCountsChecker()),
        LOWEST_MZ(new ExpectedLowestMzChecker()),
        HIGHEST_MZ(new ExpectedHighestMzChecker()),
        MOST_INTENSE_PEAK(new ExpectedMostIntensePeakChecker()),
        TOTAL_ION_CURRENT(new ExpectedTicChecker());

        private final ConsistencyChecker checker;

        ConsistencyCheck(ConsistencyChecker checker) {

            this.checker = checker;
        }

        public ConsistencyChecker getChecker() {

            return checker;
        }
    }

    private XMLEventReader xmlEventReader;
    private int expectedScanNumber;
    private float delta;
    private double[] mzs;
    private double[] intensities;
    private final Base64 base64;
    private Optional<MsnSpectrum> lastPrecursorSpectrum;
    private Optional<MsnSpectrum> previousSpectrum;
    private final Set<ConsistencyCheck> consistencyChecks;

    // global for consistency checks
    private StartElement startScan;

    public MzxmlReader(File file, PeakList.Precision precision) throws IOException {

        this(new FileReader(file), file.toURI(), precision);
    }

    public MzxmlReader(File file, PeakList.Precision precision, PeakProcessorChain<PeakAnnotation> processorChain) throws IOException {

        this(new FileReader(file), file.toURI(), precision, processorChain);
    }

    public MzxmlReader(Reader reader, URI source, PeakList.Precision precision) throws IOException {

        this(reader, source, precision, new PeakProcessorChain<>());
    }

    private MzxmlReader(Reader reader, URI source, PeakList.Precision precision, PeakProcessorChain<PeakAnnotation> processorChain) throws IOException {

        super(reader, source, precision, processorChain);

        mzs = new double[100];
        intensities = new double[100];

        base64 = new Base64();

        lastPrecursorSpectrum = previousSpectrum = Optional.absent();
        delta = DEFAULT_DELTA;

        consistencyChecks = EnumSet.allOf(ConsistencyCheck.class);
    }

    /**
     * Create an instance of MzxmlReader that strictly checks that the decoded spectrum is consistent with xml <scan> attributes
     * (see also addConsistencyChecks()/removeConsistencyChecks() method)
     *
     * @param file      the mzxml file to read
     * @param precision the peak precision
     * @return MzxmlReader object
     * @throws IOException
     */
    public static MzxmlReader newStrictReader(File file, PeakList.Precision precision) throws IOException {

        return new MzxmlReader(file, precision);
    }

    /**
     * Create an instance of MzxmlReader that strictly checks that the decoded spectrum is consistent with xml <scan> attributes
     * (see also addConsistencyChecks()/removeConsistencyChecks() method)
     *
     * @param file           the mzxml file to read
     * @param precision      the peak precision
     * @param processorChain PeakProcessorChain to process the peaks while reading
     * @return MzxmlReader object
     * @throws IOException
     */
    public static MzxmlReader newStrictReader(File file, PeakList.Precision precision, PeakProcessorChain<PeakAnnotation> processorChain) throws IOException {

        return new MzxmlReader(file, precision, processorChain);
    }

    /**
     * Create an instance of MzxmlReader that does not check for inconsistencies
     *
     * @param file      the mzxml file to read
     * @param precision the peak precision
     * @return MzxmlReader object
     * @throws IOException
     */
    public static MzxmlReader newTolerantReader(File file, PeakList.Precision precision) throws IOException {

        MzxmlReader reader = new MzxmlReader(file, precision);

        reader.removeConsistencyChecks(EnumSet.allOf(ConsistencyCheck.class));
        reader.acceptUnsortedSpectra();

        return reader;
    }

    public void removeConsistencyChecks(Set<ConsistencyCheck> ccs) {

        checkNotNull(ccs);
        consistencyChecks.removeAll(ccs);
    }

    public void addConsistencyChecks(Set<ConsistencyCheck> ccs) {

        checkNotNull(ccs);
        consistencyChecks.addAll(ccs);
    }

    @Override
    protected void init(ParseContext context) throws IOException {

        try {

            LineNumberReader lnr = new LineNumberReader(context.getCurrentReader());

            this.xmlEventReader = newXmlInputFactory().createXMLEventReader(lnr);
        } catch (XMLStreamException e) {

            throw new IOException(e);
        }
    }

    protected XMLInputFactory newXmlInputFactory() {

        XMLInputFactory xmlif = XMLInputFactory.newInstance();
        xmlif.setProperty("javax.xml.stream.isCoalescing", Boolean.TRUE);
        return xmlif;
    }

    public void setDelta(float delta) {

        this.delta = delta;
    }

    @Override
    public void close() throws IOException {

        super.close();
        try {

            xmlEventReader.close();
        } catch (XMLStreamException e) {

            throw new IOException(e);
        }
    }

    @Override
    protected String parseHeader(ParseContext context) throws IOException {

        Writer writer = new StringWriter();

        try {

            Optional<StartElement> optStartRun = XMLEventUtils.seekStartElement(xmlEventReader, "msRun");

            if (optStartRun.isPresent()) {

                StartElement startRun = optStartRun.get();
                context.setColumnNumber(startRun.getLocation().getColumnNumber());

                expectedScanNumber = Integer.parseInt(XMLEventUtils.getMandatoryAttribute(startRun, "scanCount"));

                startRun.writeAsEncodedUnicode(writer);

                XMLEventUtils.writeAllEventsUntilStartElement(xmlEventReader, "scan", writer);

                writer.close();

                return writer.toString();
            }

            throw new XMLStreamException("missing msRun start element!");

        } catch (XMLStreamException e) {

            throw new IOException("error while seeking msRun from parseHeader()!", e);
        }
    }

    public int getExpectedScanCount() {

        return expectedScanNumber;
    }

    @Override
    public MsnSpectrum next() throws IOException {

        MsnSpectrum spectrum = super.next();

        consistencyChecks(spectrum);

        return spectrum;
    }

    @Override
    protected MsnSpectrum parseNextEntry(ParseContext context) throws IOException {

        try {

            // parse <scan>
            Optional<StartElement> optionalStartScan = XMLEventUtils.seekStartElement(xmlEventReader, "scan");

            if (!optionalStartScan.isPresent()) {

                context.setEndOfParsing(true);

                // no more spectrum to read
                return null;
            }

            startScan = optionalStartScan.get();

            // create new spectrum + set infos from scan attributes
            MsnSpectrum spectrum = newSpectrum(startScan, context);

            // parse <precursorMz>
            Optional<StartElement> optPrecMzStartElement = XMLEventUtils.nextStartElementIf(xmlEventReader, "precursorMz");

            if (optPrecMzStartElement.isPresent()) {

                // create new Precursor and set spectrum
                setNewSpectrumPrecursor(spectrum, optPrecMzStartElement.get());
            }

            // parse <peaks>
            StartElement startPeaks =
                    XMLEventUtils.getMandatoryXMLEvent(XMLEventUtils.nextStartElementIf(xmlEventReader, "peaks"), "missing <peaks> xml element!");

            // decode, [decompress] and add peaks to spectrum
            int peaksCount = Integer.parseInt(XMLEventUtils.getMandatoryAttribute(startScan, PEAKS_COUNT));
            if (peaksCount > 0) {
                handlePeaks(spectrum, peaksCount, startPeaks, context);
            }

            // if previous spectrum is the precursor of the current one
            if (previousSpectrum.isPresent() && spectrum.getMsLevel() > previousSpectrum.get().getMsLevel()) {

                lastPrecursorSpectrum = Optional.of(previousSpectrum.get());
            }

            if (lastPrecursorSpectrum.isPresent()) {

                spectrum.setParentScanNumber(lastPrecursorSpectrum.get().getScanNumbers().getFirst());
            }

            previousSpectrum = Optional.of(spectrum);

            return spectrum;

        } catch (XMLStreamException | DataFormatException e) {

            throw new IOException(e);
        }
    }

    /**
     * Check for all spectrum consistencies
     *
     * @param spectrum the spectrum on which checks are done
     * @throws IllegalStateException     if xml stream error
     * @throws ConsistencyCheckException if a check reveals inconsistencies
     */
    protected void consistencyChecks(MsnSpectrum spectrum) {

        for (ConsistencyCheck check : ConsistencyCheck.values()) {

            Optional<ConsistencyCheckException> optException;
            try {
                optException = check.getChecker().check(startScan, spectrum, delta);
            } catch (XMLStreamException e) {

                throw new IllegalStateException(e);
            }

            if (optException.isPresent()) {

                ConsistencyCheckException e = optException.get();
                if (consistencyChecks.contains(e.getConsistencyCheck())) {
                    throw e;
                }
            }
        }
    }

    /**
     * Create a spectrum
     *
     * @param scanElement the container providing data to spectrum
     * @return a new instance of MsnSpectrum<M>
     */
    protected MsnSpectrum newSpectrum(StartElement scanElement, ParseContext context) throws XMLStreamException {

        MsnSpectrum spectrum = new MsnSpectrum(precision);
        spectrum.setSpectrumIndex(context.getNumberOfParsedEntry());
        spectrum.setSpectrumSource(context.getSource());
        context.setColumnNumber(scanElement.getLocation().getColumnNumber());

        spectrum.addScanNumber(Integer.parseInt(XMLEventUtils.getMandatoryAttribute(scanElement, "num")));
        spectrum.setMsLevel(Integer.parseInt(XMLEventUtils.getMandatoryAttribute(scanElement, "msLevel")));

        Optional<String> optRetentionTime = XMLEventUtils.getOptionalAttribute(scanElement, "retentionTime");

        if (optRetentionTime.isPresent()) {

            RetentionTime rt = mzXMLDuration2retentionTime(optRetentionTime.get());
            spectrum.addRetentionTime(rt);
        }

        return spectrum;
    }

    private void setNewSpectrumPrecursor(MsnSpectrum spectrum, StartElement precMzStartElement) throws XMLStreamException {

        // get mandatory precursor intensity
        double precursorIntensity = Double.parseDouble(XMLEventUtils.getMandatoryAttribute(precMzStartElement, "precursorIntensity"));

        // get mandatory precursor mz
        double precursorMz = Double.parseDouble(XMLEventUtils.getMandatoryXMLEvent(XMLEventUtils.nextCharacters(xmlEventReader),
                "missing precursor mz value!").getData());

        // get optional charge
        Optional<String> optPrecursorCharge = XMLEventUtils.getOptionalAttribute(precMzStartElement, "precursorCharge");
        Optional<String> optPolarity = XMLEventUtils.getOptionalAttribute(precMzStartElement, "polarity");

        Polarity polarity;
        final String value = optPolarity.or("?");
        if ("+".equals(value))
            polarity = Polarity.POSITIVE;
        else if ("-".equals(value))
            polarity = Polarity.NEGATIVE;
        else
            polarity = Polarity.UNKNOWN;

        Peak precursor;
        if (optPrecursorCharge.isPresent()) {

            int precursorCharge = Polarity.toInteger(polarity, Integer.parseInt(optPrecursorCharge.get()));

            precursor = new Peak(precursorMz, precursorIntensity, precursorCharge);
        } else {

            precursor = new Peak(precursorMz, precursorIntensity);
        }

        // get optional activation method
        Optional<String> optActivationMethod = XMLEventUtils.getOptionalAttribute(precMzStartElement, "activationMethod");

        if (optActivationMethod.isPresent()) {

            spectrum.setFragMethod(optActivationMethod.get());
        }

        spectrum.setPrecursor(precursor);
    }

    private void handlePeaks(MsnSpectrum spectrum, int expectedPeakCount, StartElement startPeaks, ParseContext context) throws XMLStreamException, DataFormatException {

        // parse precision for mzs and intensities primitives
        int bitPrecision = Integer.parseInt(XMLEventUtils.getMandatoryAttribute(startPeaks, "precision"));

        if (bitPrecision != 32 && bitPrecision != 64)
            throw newXMLStreamException(startPeaks, bitPrecision + ": illegal peaks precision!", context);

        // parse metadata about peak pair order
        String pairOrder = XMLEventUtils.getMandatoryAttribute(startPeaks, "pairOrder", "contentType");

        // decode only m/z-int peak pair order by now
        if ("m/z-int".equals(pairOrder)) {

            // parse base64 peak content
            Characters peaksCharacters =
                    XMLEventUtils.getMandatoryXMLEvent(XMLEventUtils.seekNextCharacters(xmlEventReader), "missing <peaks> base64 data!");

            int bytePrecision = bitPrecision / 8;

            // decode [and uncompress] Base64 string peaks to bytes
            byte[] peaksBytes = decodePeaksCharacters(startPeaks, peaksCharacters, bytePrecision, expectedPeakCount, context);

            // 1st grow if needed (instanciate a bigger array)
            reinitMzsAndIntensities(expectedPeakCount);

            // convert bytes to doubles and load mzs and intensities arrays
            if (bitPrecision == 32)
                convert32bitsAndPopulate(peaksBytes, mzs, intensities, expectedPeakCount);
            else
                convert64bitsAndPopulate(peaksBytes, mzs, intensities, expectedPeakCount);

            // add peaks to spectrum
            addPeaksToSpectrum(spectrum, mzs, intensities, expectedPeakCount);
        } else {

            throw newXMLStreamException(startPeaks, pairOrder + ": cannot decode peaks pair order (only m/z-int)!", context);
        }
    }

    private void reinitMzsAndIntensities(int expectedPeakCount) {

        if (mzs.length < expectedPeakCount) {

            int factor = expectedPeakCount - mzs.length;

            mzs = PrimitiveArrayUtils.grow(mzs, factor);
            intensities = PrimitiveArrayUtils.grow(intensities, factor);
        }

        Arrays.fill(mzs, 0);
        Arrays.fill(intensities, 0);
    }

    private static void convert32bitsAndPopulate(byte[] bytes, double[] mzs, double[] intensities, int expectedScanNumber) {

        checkNotNull(bytes);
        checkArgument(bytes.length % 4 == 0);
        checkArgument(mzs.length == intensities.length);

        int len = bytes.length / 8;
        checkArgument(len == expectedScanNumber);

        ByteBuffer buff = ByteBuffer.wrap(bytes);

        buff.clear();

        int i = 0;
        while (buff.hasRemaining()) {

            mzs[i] = buff.getFloat();
            intensities[i++] = buff.getFloat();
        }
    }

    public static void convert64bitsAndPopulate(byte[] bytes, double[] mzs, double[] intensities, int expectedScanNumber) {

        checkNotNull(bytes);
        checkArgument(bytes.length % 8 == 0);
        checkArgument(mzs.length == intensities.length);

        int len = bytes.length / 16;
        checkArgument(len == expectedScanNumber);

        ByteBuffer buff = ByteBuffer.wrap(bytes);

        buff.clear();

        int i = 0;
        while (buff.hasRemaining()) {

            mzs[i] = buff.getDouble();
            intensities[i++] = buff.getDouble();
        }
    }

    private byte[] decodePeaksCharacters(StartElement startPeaks, Characters peaksCharacters,
                                         int bytePrecision, int expectedPeakCount, ParseContext context) throws XMLStreamException, DataFormatException {

        Optional<String> optCompressedLen = XMLEventUtils.getOptionalAttribute(startPeaks, "compressedLen");

        int compressedLen = (optCompressedLen.isPresent()) ? Integer.parseInt(optCompressedLen.get()) : 0;

        String peaksBase64Data = peaksCharacters.getData();

        // Decode peaks base64 char data to bytes
        byte[] peaksBytes = base64.decode(peaksBase64Data);

        // Uncompression (optional)
        if (compressedLen > 0) {

            // 2 values per peak, precision/8 bytes per value
            int uncompressedLen = expectedPeakCount * bytePrecision * 2;

            peaksBytes = BytesUtils.uncompress(peaksBytes, compressedLen, uncompressedLen);
        }

        // m/z + int
        int expectedByteCount = expectedPeakCount * bytePrecision * 2;

        if (peaksBytes.length != expectedByteCount) {

            throw newXMLStreamException(peaksCharacters, "peak bytes expected=" + expectedByteCount + ", actual=" + peaksBytes.length
                    + ", actual base64 peaks=" + peaksBase64Data + ": error while parsing <peaks> characters!", context);
        }

        return peaksBytes;
    }

    private static RetentionTime mzXMLDuration2retentionTime(
            final String duration) {

        Matcher matcher = DURATION_PATTERN.matcher(duration);
        double retentionTime = 0;
        char retentionTimeUnit = 'S';

        if (matcher.matches()) {
            retentionTime = Double.parseDouble(matcher.group(1));
            retentionTimeUnit = matcher.group(2).charAt(0);
        }

        return new RetentionTimeDiscrete(retentionTime, (retentionTimeUnit == 'S') ? TimeUnit.SECOND : TimeUnit.MINUTE);
    }

    private XMLStreamException newXMLStreamException(XMLEvent event, String message, ParseContext context) {

        context.setColumnNumber(event.getLocation().getColumnNumber());

        return new XMLStreamException(message);
    }

    public static class ConsistencyCheckException extends RuntimeException {

        private final ConsistencyCheck consistencyCheck;
        private final String shortMessage;

        ConsistencyCheckException(ConsistencyCheck consistencyCheck, String message) {

            super(message + "\n<Call method removeConsistencyChecks(EnumSet.of(ConsistencyCheck."
                    + consistencyCheck + ")) to disable this consistency check exception (logger will warn for potential inconsistencies)>");

            this.consistencyCheck = consistencyCheck;
            shortMessage = message;
        }

        private ConsistencyCheck getConsistencyCheck() {

            return consistencyCheck;
        }

        private String getShortMessage() {

            return shortMessage;
        }

    }

    /**
     * @author fnikitin
     *         Date: 6/27/13
     */
    public static class XMLEventUtils {

        /**
         * Seek to the next start tag
         *
         * @param reader  the xml reader
         * @param tagName the start element name to return
         * @return optional scan start element
         */
        public static Optional<StartElement> seekStartElement(XMLEventReader reader, String tagName) throws XMLStreamException {

            while (reader.hasNext()) {

                XMLEvent event = reader.nextEvent();

                if (event.isStartElement()) {

                    StartElement se = event.asStartElement();

                    if (se.getName().getLocalPart().equals(tagName)) {

                        return Optional.of(se);
                    }
                }
            }

            return Optional.absent();
        }

        /**
         * Return the next start element only if match tagName.
         *
         * @param reader  the xml reader
         * @param tagName the start element name to return
         * @return optional StartElement
         * @throws javax.xml.stream.XMLStreamException
         */
        public static Optional<StartElement> nextStartElementIf(XMLEventReader reader, String tagName) throws XMLStreamException {

            while (reader.hasNext()) {

                XMLEvent event = reader.peek();

                // test the next start element for tagName and return anyway
                if (event.isStartElement()) {

                    StartElement se = event.asStartElement();

                    if (se.getName().getLocalPart().equals(tagName)) {

                        // consume this start element
                        reader.nextEvent();

                        return Optional.of(se);
                    } else {

                        return Optional.absent();
                    }
                }
                // consume all non-start elements
                else {

                    reader.nextEvent();
                }
            }

            return Optional.absent();
        }

        public static Optional<Characters> seekNextCharacters(XMLEventReader reader) throws XMLStreamException {

            while (reader.hasNext()) {

                XMLEvent event = reader.nextEvent();

                if (event.isCharacters()) {

                    Characters chars = event.asCharacters();

                    if (reader.peek().isCharacters()) {

                        throw new XMLStreamException("Please set property 'javax.xml.stream.isCoalescingproperty' of XMLInputFactory to true !");
                    }

                    if (!chars.isWhiteSpace()) {

                        return Optional.of(chars);
                    }
                }
            }

            return Optional.absent();
        }

        public static Optional<Characters> nextCharacters(XMLEventReader reader) throws XMLStreamException {

            if (reader.hasNext()) {

                XMLEvent event = reader.nextEvent();

                if (event.isCharacters()) {

                    Characters chars = event.asCharacters();

                    if (reader.peek().isCharacters()) {

                        throw new XMLStreamException("Please set property 'javax.xml.stream.isCoalescingproperty' of XMLInputFactory to true !");
                    }

                    if (!chars.isWhiteSpace()) {

                        return Optional.of(chars);
                    }
                } else {

                    Optional.absent();
                }
            }

            return Optional.absent();
        }

        public static Optional<String> getOptionalAttribute(StartElement startElement, String attributeName) {

            Attribute attribute = startElement.getAttributeByName(QName.valueOf(attributeName));

            if (attribute != null) {

                return Optional.of(attribute.getValue());
            }

            return Optional.absent();
        }

        public static String getMandatoryAttribute(StartElement startElement, String attributeName, String... alternateNames) throws XMLStreamException {

            Optional<String> optAttribute = XMLEventUtils.getOptionalAttribute(startElement, attributeName);

            if (optAttribute.isPresent()) {

                return optAttribute.get();
            } else {

                for (String name : alternateNames) {

                    optAttribute = XMLEventUtils.getOptionalAttribute(startElement, name);

                    if (optAttribute.isPresent()) return optAttribute.get();
                }
            }

            throw new XMLStreamException("mandatory attribute " + attributeName + " is missing from start element " + startElement.getName().getLocalPart() + "!");
        }

        public static <E extends XMLEvent> E getMandatoryXMLEvent(Optional<E> mandatoryEvent, String message) throws XMLStreamException {

            if (mandatoryEvent.isPresent()) {

                return mandatoryEvent.get();
            }

            throw new XMLStreamException(message + ": XMLEvent is missing!");
        }

        /**
         * Return every events until reaching start element named <code>tagName</code>
         *
         * @param reader  the xml event reader
         * @param tagName the tag name
         * @return absent if start element named <code>tagName</code> or end of file
         * @throws javax.xml.stream.XMLStreamException
         */
        private static Optional<XMLEvent> nextEventUntilStartElement(XMLEventReader reader, String tagName) throws XMLStreamException {

            if (reader.hasNext()) {

                XMLEvent event = reader.peek();

                if (event.isStartElement()) {

                    StartElement se = event.asStartElement();

                    if (se.getName().getLocalPart().equals(tagName)) {

                        return Optional.absent();
                    }
                }

                return Optional.of(reader.nextEvent());
            }

            // no more events
            return Optional.absent();
        }

        public static void writeAllEventsUntilStartElement(XMLEventReader reader, String tagName, Writer writer) throws XMLStreamException {

            while (reader.hasNext()) {

                Optional<XMLEvent> optEvent = XMLEventUtils.nextEventUntilStartElement(reader, tagName);

                if (optEvent.isPresent()) {

                    optEvent.get().writeAsEncodedUnicode(writer);
                } else {

                    break;
                }
            }
        }
    }

    private static interface ConsistencyChecker {

        Optional<ConsistencyCheckException> check(StartElement startScan, MsnSpectrum spectrum, double delta) throws XMLStreamException;
    }

    private static class ExpectedTicChecker implements ConsistencyChecker {

        @Override
        public Optional<ConsistencyCheckException> check(StartElement startScan, MsnSpectrum spectrum, double delta) throws XMLStreamException {

            checkNotNull(startScan);

            double obs = spectrum.getTotalIonCurrent();

            Optional<String> opt = XMLEventUtils.getOptionalAttribute(startScan, "totIonCurrent");

            if (opt.isPresent()) {

                double exp = Double.parseDouble(opt.get());

                if (abs(obs - exp) > delta)
                    return Optional.of(new ConsistencyCheckException(ConsistencyCheck.TOTAL_ION_CURRENT, "The total ion current of scan " +
                            XMLEventUtils.getMandatoryAttribute(startScan, "num") + " (" + obs + ") is not equals to attribute 'totIonCurrent' (" + exp + "). " +
                            PREFIX_HINT_MESSAGE + " reader.removeConsistencyChecks(EnumSet.of(ConsistencyCheck.TOTAL_ION_CURRENT))"));
            }

            return Optional.absent();
        }
    }

    private static class ExpectedPeakCountsChecker implements ConsistencyChecker {

        @Override
        public Optional<ConsistencyCheckException> check(StartElement startScan, MsnSpectrum spectrum, double delta) throws XMLStreamException {

            checkNotNull(startScan);

            int obs = spectrum.size();

            Optional<String> opt = XMLEventUtils.getOptionalAttribute(startScan, PEAKS_COUNT);

            if (opt.isPresent()) {

                int exp = Integer.parseInt(opt.get());

                if (obs != exp)
                    return Optional.of(new ConsistencyCheckException(ConsistencyCheck.PEAKS_COUNT, "The peaks count of scan " +
                            XMLEventUtils.getMandatoryAttribute(startScan, "num") + " (" + obs + ") is not equals to attribute 'peaksCount' (" + exp + "). " +
                            PREFIX_HINT_MESSAGE + " reader.removeConsistencyChecks(EnumSet.of(ConsistencyCheck.PEAKS_COUNT))"));
            }

            return Optional.absent();
        }
    }

    private static class ExpectedLowestMzChecker implements ConsistencyChecker {

        @Override
        public Optional<ConsistencyCheckException> check(StartElement startScan, MsnSpectrum spectrum, double delta) throws XMLStreamException {

            checkNotNull(startScan);

            double obs = spectrum.isEmpty() ? 0 : spectrum.getMz(0);
            Optional<String> opt = XMLEventUtils.getOptionalAttribute(startScan, "lowMz");

            if (opt.isPresent()) {

                double exp = Double.parseDouble(opt.get());

                if (abs(obs - exp) > delta)

                    return Optional.of(new ConsistencyCheckException(ConsistencyCheck.LOWEST_MZ, "The first m/z peak of scan " +
                            XMLEventUtils.getMandatoryAttribute(startScan, "num") + " (" + obs + ") is not equals to attribute 'lowMz' (" + exp + "). " +
                            PREFIX_HINT_MESSAGE + " reader.removeConsistencyChecks(EnumSet.of(ConsistencyCheck.LOWEST_MZ))"));
            }

            return Optional.absent();
        }
    }

    private static class ExpectedHighestMzChecker implements ConsistencyChecker {

        @Override
        public Optional<ConsistencyCheckException> check(StartElement startScan, MsnSpectrum spectrum, double delta) throws XMLStreamException {

            checkNotNull(startScan);

            double obs = spectrum.isEmpty() ? 0 : spectrum.getMz(spectrum.size() - 1);
            Optional<String> opt = XMLEventUtils.getOptionalAttribute(startScan, "highMz");

            if (opt.isPresent()) {

                double exp = Double.parseDouble(opt.get());

                if (abs(obs - exp) > delta)
                    return Optional.of(new ConsistencyCheckException(ConsistencyCheck.HIGHEST_MZ, "The last m/z peak of scan " +
                            XMLEventUtils.getMandatoryAttribute(startScan, "num") + " (" + obs + ") is not equals to attribute 'highMz' (" + exp + "). " +
                            PREFIX_HINT_MESSAGE + " reader.removeConsistencyChecks(EnumSet.of(ConsistencyCheck.HIGHEST_MZ))"));
            }

            return Optional.absent();
        }
    }

    private static class ExpectedMostIntensePeakChecker implements ConsistencyChecker {

        @Override
        public Optional<ConsistencyCheckException> check(StartElement startScan, MsnSpectrum spectrum, double delta) throws XMLStreamException {

            checkNotNull(startScan);

            int mii = spectrum.getMostIntenseIndex();

            double obsMz;
            double obsIntensity;
            if (mii >= 0) {

                obsMz = spectrum.getMz(mii);
                obsIntensity = spectrum.getIntensity(mii);
            } else {

                obsMz = 0;
                obsIntensity = 0;
            }

            Optional<String> optMz = XMLEventUtils.getOptionalAttribute(startScan, "basePeakMz");
            if (optMz.isPresent()) {

                double expMz = Double.parseDouble(optMz.get());
                if (abs(obsMz - expMz) > DEFAULT_DELTA)
                    return Optional.of(new ConsistencyCheckException(ConsistencyCheck.MOST_INTENSE_PEAK, "The most intense peak m/z of scan " +
                            XMLEventUtils.getMandatoryAttribute(startScan, "num") + " (" + obsMz + ") is not equals to attribute 'basePeakMz' (" + expMz + "). " +
                            PREFIX_HINT_MESSAGE + " reader.removeConsistencyChecks(EnumSet.of(ConsistencyCheck.MOST_INTENSE_PEAK))"));

                if (abs(obsMz - expMz) > delta)
                    return Optional.of(new ConsistencyCheckException(ConsistencyCheck.MOST_INTENSE_PEAK, "The most intense peak of scan " +
                            XMLEventUtils.getMandatoryAttribute(startScan, "num") + " (m/z=" + obsMz + ") != 'basePeakMz' (m/z=" + expMz + " +/-" + delta + "). " +
                            PREFIX_HINT_MESSAGE + " reader.removeConsistencyChecks(EnumSet.of(ConsistencyCheck.MOST_INTENSE_PEAK))"));
            }

            Optional<String> optIntensity = XMLEventUtils.getOptionalAttribute(startScan, "basePeakIntensity");
            if (optIntensity.isPresent()) {
                double expIntensity = Double.parseDouble(optIntensity.get());

                if (abs(obsIntensity - expIntensity) > DEFAULT_DELTA)
                    return Optional.of(new ConsistencyCheckException(ConsistencyCheck.MOST_INTENSE_PEAK, "The most intense peak intensity of scan " +
                            XMLEventUtils.getMandatoryAttribute(startScan, "num") + " (" + obsIntensity + ") is not equals to attribute 'basePeakIntensity' (" + expIntensity + "). " +
                            PREFIX_HINT_MESSAGE + " reader.removeConsistencyChecks(EnumSet.of(ConsistencyCheck.CONS))"));

                if (abs(obsIntensity - expIntensity) > delta)
                    return Optional.of(new ConsistencyCheckException(ConsistencyCheck.MOST_INTENSE_PEAK, "The most intense peak of scan " +
                            XMLEventUtils.getMandatoryAttribute(startScan, "num") + " (intensity=" + obsIntensity + ") != 'basePeakIntensity' (intensity=" + expIntensity + " +/-" + delta + "). " +
                            PREFIX_HINT_MESSAGE + " reader.removeConsistencyChecks(EnumSet.of(ConsistencyCheck.CONS))"));
            }

            return Optional.absent();
        }
    }
}
