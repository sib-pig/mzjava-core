/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.transformer;

import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.map.hash.TIntObjectHashMap;
import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.DelayedPeakPairProcessor;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;

import java.util.List;

/**
 * Rescales the intensities so that the length of the each intensity vector is equal to 1.
 *
 * @author Oliver Horlacher
 * @version 1.0
 */
public class UnitVectorPeakPairTransformer<X extends PeakAnnotation, Y extends PeakAnnotation> extends DelayedPeakPairProcessor<X, Y> {

    @Override
    protected void process(TDoubleArrayList centroids, TDoubleArrayList vectorX, TDoubleArrayList vectorY, TIntObjectHashMap<List<X>> xAnnotationMap, TIntObjectHashMap<List<Y>> yAnnotationMap) {

        double lengthX = calcLength(vectorX);
        double lengthY = calcLength(vectorY);

        for(int i = 0; i < centroids.size(); i++) {

            sink.processPeakPair(centroids.get(i), vectorX.get(i) / lengthX, vectorY.get(i) / lengthY, getAnnotations(i, xAnnotationMap), getAnnotations(i, yAnnotationMap));
        }
    }

    private double calcLength(TDoubleArrayList vector) {

        double dSquared = 0;

        for(int i = 0; i < vector.size(); i++)
            dSquared += Math.pow(vector.get(i), 2);

        return Math.sqrt(dSquared);
    }
}
