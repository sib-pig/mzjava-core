package org.expasy.mzjava.core.io.ms.spectrum;

import org.expasy.mzjava.core.io.ms.spectrum.mgf.TitleParser;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeList;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberList;
import org.openide.util.Lookup;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author fnikitin
 * @author Oliver Horlacher
 */
public class MgfReader extends AbstractMgfReader<PeakAnnotation, MsnSpectrum> {

    private final List<TitleParser> titleParsers;
    private TitleParser currentTitleParser;

    public MgfReader(File file, PeakList.Precision precision) throws IOException {

        this(new FileReader(file), file.toURI(), precision);
    }

    public MgfReader(File file, PeakList.Precision precision, PeakProcessorChain<PeakAnnotation> processorChain) throws IOException {

        super(new FileReader(file), file.toURI(), precision, processorChain);
        titleParsers = initTitleParsers();
    }

    public MgfReader(Reader reader, URI spectraSource, PeakList.Precision precision) throws IOException {

        super(reader, spectraSource, precision, new PeakProcessorChain<>());
        titleParsers = initTitleParsers();
    }

    private List<TitleParser> initTitleParsers() {

        List<TitleParser> parsers = new ArrayList<>();

        Lookup lookup = Lookup.getDefault();

        for (TitleParser titleParser : lookup.lookupAll(TitleParser.class)) {

            parsers.add(titleParser);
        }

        if (parsers.isEmpty()) {

            throw new IllegalStateException("No TitleParser implementation class found: org.openide.util.Lookup has not" +
                    " properly worked (hint: check that Annotation Compiler Processing is enable)");

        } else {

            currentTitleParser = parsers.get(0);
        }

        return parsers;
    }

    public void addTitleParser(TitleParser parser) {

        checkNotNull(parser);
        titleParsers.add(parser);
        currentTitleParser = parser;
    }

    @Override
    protected MsnSpectrum newSpectrum(ParseContext context, PeakList.Precision precision) {

        MsnSpectrum spectrum = new MsnSpectrum(precision);
        spectrum.setSpectrumIndex(context.getNumberOfParsedEntry());
        spectrum.setSpectrumSource(context.getSource());

        return spectrum;
    }

    @Override
    protected void setRetentionTimes(MsnSpectrum spectrum, RetentionTimeList retentionTimeList) {

        spectrum.addRetentionTimes(retentionTimeList);
    }

    @Override
    protected void setScanNumbers(MsnSpectrum spectrum, ScanNumberList scanNumbers) {

        spectrum.addScanNumbers(scanNumbers);
    }

    protected boolean parseTitleTag(String value, MsnSpectrum spectrum) {

        boolean parsed = currentTitleParser.parseTitle(value, spectrum);

        if (!parsed) {

            for (TitleParser titleParser : titleParsers) {

                if (titleParser.parseTitle(value, spectrum)) {

                    currentTitleParser = titleParser;
                    break;
                }
            }
        }

        spectrum.setComment(value);

        return parsed;
    }

    /**
     * Overridden so that the compiler knows that MsnSpectra are returned by next().
     * If this method is missing the compiler thinks Spectrum are returned
     *
     * @inheritDoc
     */
    @Override
    public MsnSpectrum next() throws IOException {

        return super.next();
    }
}
