package org.expasy.mzjava.core.io.ms.spectrum;


import java.nio.ByteBuffer;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;


/**
 * @author nikitin
 */
public class BytesUtils {

    private BytesUtils() {

    }

    /**
     * Convert four bytes into one float
     *
     * @param bytes the quad to convert
     * @return one float
     * @throws IllegalArgumentException if not four bytes
     */
    public static float quadToFloat(byte... bytes) {

        checkArgument(bytes.length == 4, "needs exactly 4 bytes to make a float!");

        return ByteBuffer.wrap(bytes).asFloatBuffer().get();
    }

    /**
     * Convert eight bytes into one double
     *
     * @param bytes the octet to convert
     * @return one double
     * @throws IllegalArgumentException if not eight bytes
     */
    public static double octetToDouble(byte... bytes) {

        checkArgument(bytes.length == 8, "needs exactly 8 bytes to make a double!");

        return ByteBuffer.wrap(bytes).asDoubleBuffer().get();
    }

    /**
     * Convert one float into four bytes
     *
     * @param real the float to convert
     * @return an array of four bytes
     */
    public static byte[] floatToQuad(float real) {

        int intBits = Float.floatToIntBits(real);

        byte[] ba = new byte[4];

        for (int i = ba.length - 1; i >= 0; i--) {
            ba[i] = (byte) (intBits & 0xff);
            intBits >>= 8;
        }

        return ba;
    }

    /**
     * Convert one double into eight bytes
     *
     * @param real the double to convert
     * @return an array of eight bytes
     */
    public static byte[] doubleToOctet(double real) {

        long longBits = Double.doubleToLongBits(real);

        byte[] ba = new byte[8];

        for (int i = ba.length - 1; i >= 0; i--) {
            ba[i] = (byte) (longBits & 0xff);
            longBits >>= 8;
        }

        return ba;
    }

    public static float[] quadsToFloats(byte[] bytes, float[] floats) {

        checkArgument(bytes.length % 4 == 0);

        int len = bytes.length / 4;

        if (floats == null || floats.length < len) floats = new float[len];

        ByteBuffer buff = ByteBuffer.wrap(bytes);

        buff.clear();

        int i = 0;
        while (buff.hasRemaining()) {

            floats[i++] = buff.getFloat();
        }

        return floats;
    }

    public static double[] quadsToDoubles(byte[] bytes, double[] doubles) {

        checkArgument(bytes.length % 4 == 0);

        int len = bytes.length / 4;

        if (doubles == null || doubles.length < len) doubles = new double[len];

        ByteBuffer buff = ByteBuffer.wrap(bytes);

        buff.clear();

        int i = 0;
        while (buff.hasRemaining()) {

            doubles[i++] = buff.getFloat();
        }

        return doubles;
    }

    public static double[] octetsToDoubles(byte[] bytes, double[] doubles) {

        checkArgument(bytes.length % 8 == 0);

        int len = bytes.length / 8;

        if (doubles == null || doubles.length < len) doubles = new double[len];

        ByteBuffer buff = ByteBuffer.wrap(bytes);

        buff.clear();

        int i = 0;
        while (buff.hasRemaining()) {

            doubles[i++] = buff.getDouble();
        }

        return doubles;
    }

    public static byte[] floatsToQuads(float[] floats) {

        checkNotNull(floats);
        checkArgument(floats.length > 0);

        ByteBuffer buffer = ByteBuffer.allocate(floats.length * 4);

        for (int i = 0; i < floats.length; i++) {

            buffer.putInt(Float.floatToRawIntBits(floats[i]));
        }

        return buffer.array();
    }

    public static byte[] doublesToOctets(double[] doubles) {

        checkNotNull(doubles);
        checkArgument(doubles.length > 0);

        ByteBuffer buffer = ByteBuffer.allocate(doubles.length * 8);

        for (int i = 0; i < doubles.length; i++) {

            buffer.putLong(Double.doubleToRawLongBits(doubles[i]));
        }

        return buffer.array();
    }

    // zlib uncompression from an example at
    // http://java.sun.com/developer/technicalArticles/Programming/compression/
    // see ramp.c
    public static byte[] uncompress(byte[] bytes, int compressedLen,
                                    int uncompressedLen) throws DataFormatException {

        checkNotNull(bytes);
        checkArgument(bytes.length > 0);
        checkArgument(compressedLen > 0);
        checkArgument(uncompressedLen > 0);

        // Decompress the bytes
        Inflater decompresser = new Inflater();
        decompresser.setInput(bytes, 0, compressedLen);
        byte[] result = new byte[uncompressedLen];
        decompresser.inflate(result);
        decompresser.end();

        return result;
    }

    public static byte[] compress(byte[] bytes) throws DataFormatException {

        checkNotNull(bytes);
        checkArgument(bytes.length > 0);

        byte[] buf = new byte[bytes.length * 4];

        // Compress the bytes
        Deflater compresser = new Deflater();
        compresser.setInput(bytes);
        compresser.finish();
        int compressedDataLength = compresser.deflate(buf);

        byte[] output = new byte[compressedDataLength];

        System.arraycopy(buf, 0, output, 0, compressedDataLength);

        return output;
    }
}
