/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.transformer;

import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.AbstractPeakPairProcessor;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Processor that equalizes the number of peaks such that peak lists x and y have the same number of peaks
 *
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PeakCountEqualizer<X extends PeakAnnotation, Y extends PeakAnnotation> extends AbstractPeakPairProcessor<X, Y> {

    private final List<Entry<X, Y>> entries = new ArrayList<Entry<X, Y>>();

    private int xCount, yCount;

    private final Comparator<Entry<X, Y>> xComparator = new Comparator<Entry<X, Y>>() {
        public int compare(Entry<X, Y> e1, Entry<X, Y> e2) {

            return Double.compare(e2.getX(), e1.getX());
        }
    };

    private final Comparator<Entry<X, Y>> yComparator = new Comparator<Entry<X, Y>>() {
        public int compare(Entry<X, Y> e2, Entry<X, Y> e1) {

            return Double.compare(e1.getY(), e2.getY());
        }
    };

    private final Comparator<Entry<X, Y>> centroidComparator = new Comparator<Entry<X, Y>>() {
        public int compare(Entry<X, Y> e1, Entry<X, Y> e2) {

            return Double.compare(e1.getCentroid(), e2.getCentroid());
        }
    };

    @Override
    public void begin(PeakList<X> xPeakList, PeakList<Y> yPeakList) {

        entries.clear();

        xCount = 0;
        yCount = 0;

        sink.begin(xPeakList, yPeakList);
    }

    @Override
    public void processPeakPair(double centroid, double xIntensity, double yIntensity, List<X> xAnnotations, List<Y> yAnnotations) {

        if(xIntensity > 0) xCount++;
        if(yIntensity > 0) yCount++;

        entries.add(new Entry<X, Y>(centroid, xIntensity, yIntensity, xAnnotations, yAnnotations));
    }

    @Override
    public void end() {

        int retainCount = Math.min(xCount, yCount);

        Collections.sort(entries, xComparator);
        for(int i = 0; i < retainCount; i++) {

            entries.get(i).setRetainX(true);
        }

        Collections.sort(entries, yComparator);
        for(int i = 0; i < retainCount; i++) {

            entries.get(i).setRetainY(true);
        }

        Collections.sort(entries, centroidComparator);

        for(Entry<X, Y> entry : entries) {

            if (!entry.isEmpty()) {
                sink.processPeakPair(entry.getCentroid(), entry.extractX(), entry.extractY(), entry.extractAnnotationX(), entry.extractAnnotationY());
            }
        }

        sink.end();
    }

    private static class Entry<X, Y> {

        private final double centroid, x, y;
        private final List<X> annotationsX;
        private final List<Y> annotationsY;

        private final List<X> emptyAnnotationsX = Collections.emptyList();
        private final List<Y> emptyAnnotationsY = Collections.emptyList();

        private boolean retainX = false, retainY = false;

        private Entry(double centroid, double x, double y, List<X> annotationsX, List<Y> annotationsY) {

            this.centroid = centroid;
            this.x = x;
            this.y = y;
            this.annotationsX = annotationsX;
            this.annotationsY = annotationsY;
        }

        private double getCentroid() {

            return centroid;
        }

        private double getX() {

            return x;
        }

        private double getY() {

            return y;
        }

        private void setRetainX(boolean retainX) {

            this.retainX = retainX;
        }

        private void setRetainY(boolean retainY) {

            this.retainY = retainY;
        }

        private boolean isEmpty() {

            return !retainX && !retainY;
        }

        private double extractX() {

            return retainX ? x : 0;
        }

        private double extractY() {

            return retainY ? y : 0;
        }

        private List<X> extractAnnotationX() {

            return retainX ? annotationsX : emptyAnnotationsX;
        }

        private List<Y> extractAnnotationY() {

            return retainY ? annotationsY : emptyAnnotationsY;
        }
    }
}
