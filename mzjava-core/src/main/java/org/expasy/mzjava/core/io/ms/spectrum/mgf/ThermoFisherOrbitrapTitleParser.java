/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.io.ms.spectrum.mgf;

import org.expasy.mzjava.core.ms.spectrum.RetentionTimeDiscrete;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeInterval;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeList;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberDiscrete;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberList;
import org.openide.util.lookup.ServiceProvider;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Title parse for data obtained from Savitski et. al. (2011). Confident phosphorylation site localization using the Mascot Delta Score. Molecular & cellular proteomics : MCP
 *
 * This may be a generic Mascot export
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=TitleParser.class, position = 10000)
public class ThermoFisherOrbitrapTitleParser implements TitleParser {

    private final Pattern pattern = Pattern.compile("msmsid:F(\\d+),quan:(\\d+\\.?\\d*),start:(\\d+\\.?\\d*),end:(\\d+\\.?\\d*),survey:S(\\d+),parent:(\\d+\\.?\\d*),parent_area:(\\d+\\.?\\d*),Qstart:(\\d+\\.?\\d*),Qend:(\\d+\\.?\\d*),AnalTime:(\\d+),Activation:(\\w*)");

    @Override
    public boolean parseTitle(String title, MsnSpectrum spectrum) {

        Matcher matcher = pattern.matcher(title);
        if(matcher.matches()) {

            spectrum.addScanNumbers(new ScanNumberList(new ScanNumberDiscrete(Integer.parseInt(matcher.group(5)))));
            spectrum.setComment(title);
            spectrum.setFragMethod(matcher.group(11));

            double timeFrom = Double.parseDouble(matcher.group(3));
            double timeTo = Double.parseDouble(matcher.group(4));

            if (timeFrom == timeTo) {

                spectrum.addRetentionTimes(new RetentionTimeList(new RetentionTimeDiscrete(timeFrom, TimeUnit.HOUR)));
            } else {

                spectrum.addRetentionTimes(new RetentionTimeList(new RetentionTimeInterval(timeFrom, timeTo, TimeUnit.HOUR)));
            }
            return true;
        } else {

            return false;
        }
    }

}
