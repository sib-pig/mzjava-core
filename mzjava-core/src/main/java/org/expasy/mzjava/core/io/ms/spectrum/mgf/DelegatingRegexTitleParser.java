/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.io.ms.spectrum.mgf;

import org.expasy.mzjava.core.ms.spectrum.RetentionTimeDiscrete;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeInterval;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.openide.util.lookup.ServiceProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A title parser that delegates
 *
 * @author Oliver Horlacher
 * @version 1.0
 */
@ServiceProvider(service = TitleParser.class)
public class DelegatingRegexTitleParser implements TitleParser {

    private final List<TitleParser> titleParsers = new ArrayList<TitleParser>();
    private TitleParser defaultTitleParser;

    public DelegatingRegexTitleParser() {

        titleParsers.add(new RegexScanNumTitleParser(Pattern.compile("scan\\s+(\\d+).*")));
        titleParsers.add(new TitleParser1());
        titleParsers.add(new TitleParser2());
        titleParsers.add(new TitleParser3());
        titleParsers.add(new TitleParser4());

        defaultTitleParser = titleParsers.get(0);
    }

    @Override
    public boolean parseTitle(String title, MsnSpectrum spectrum) {

        if (defaultTitleParser.parseTitle(title, spectrum)) return true;

        for (TitleParser titleParser : titleParsers) {

            if (titleParser.parseTitle(title, spectrum)) {

                defaultTitleParser = titleParser;
                return true;
            }
        }

        return false;
    }

    private static class TitleParser1 implements TitleParser {

        private final Pattern pattern = Pattern.compile("^([^\\.]+)\\.(\\d+)\\.(\\d+)\\.(\\d+)");

        @Override
        public boolean parseTitle(String title, MsnSpectrum spectrum) {

            Matcher matcher = pattern.matcher(title);

            if (matcher.find()) {

                spectrum.addScanNumber(Integer.parseInt(matcher.group(2)));
                return true;
            } else {

                return false;
            }
        }

    }

    private static class TitleParser2 implements TitleParser {

        private final Pattern pattern = Pattern.compile("[Ss]can\\s[Nn]umber\\s?(\\d+)");

        @Override
        public boolean parseTitle(String title, MsnSpectrum spectrum) {

            Matcher matcher = pattern.matcher(title);

            if (matcher.find()) {

                spectrum.addScanNumber(Integer.parseInt(matcher.group(1)));
                return true;
            } else {

                return false;
            }
        }

    }

    private static class TitleParser3 implements TitleParser {

        private final Pattern pattern = Pattern.compile("[Ss]can\\s*(\\d+)");

        @Override
        public boolean parseTitle(String title, MsnSpectrum spectrum) {

            Matcher matcher = pattern.matcher(title);

            if (matcher.find()) {

                spectrum.addScanNumber(Integer.parseInt(matcher.group(1)));

                return true;
            } else {

                return false;
            }
        }

    }

    private static class TitleParser4 implements TitleParser {

        private final Pattern scanNumberPattern = Pattern.compile("survey:S(\\d+)");
        private final Pattern retentionTimePattern = Pattern.compile("start:(\\d+\\.?\\d+)[,\\s]end:(\\d+\\.?\\d+)");
        private final Pattern precursorIntensityPattern = Pattern.compile("parent_area:(\\d+\\.?\\d+)");

        @Override
        public boolean parseTitle(String title, MsnSpectrum spectrum) {

            Matcher scanNumberMatcher = scanNumberPattern.matcher(title);
            Matcher retentionTimeMatcher = retentionTimePattern.matcher(title);
            Matcher precursorIntensityMatcher = precursorIntensityPattern.matcher(title);

            if (scanNumberMatcher.find() && retentionTimeMatcher.find() && precursorIntensityMatcher.find()) {

                spectrum.addScanNumber(Integer.parseInt(scanNumberMatcher.group(1)));

                double rtStart = Double.parseDouble(retentionTimeMatcher.group(1));
                double rtEnd = Double.parseDouble(retentionTimeMatcher.group(2));

                if (rtStart == rtEnd) spectrum.addRetentionTime(new RetentionTimeDiscrete(rtStart, TimeUnit.SECOND));
                else spectrum.addRetentionTime(new RetentionTimeInterval(rtStart, rtEnd, TimeUnit.SECOND));

                spectrum.getPrecursor().setIntensity(Double.parseDouble(precursorIntensityMatcher.group(1)));


                return true;
            } else {

                return false;
            }
        }

    }
}
