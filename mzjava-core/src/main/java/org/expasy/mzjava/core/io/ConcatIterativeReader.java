/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.io;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * Combines multiple IterativeReaders into a single IterativeReader.
 *
 * @author Oliver Horlacher
 * @version 1.0
 */
public class ConcatIterativeReader<T> implements IterativeReader<T> {

    private boolean closed = false;

    private final Iterator<IterativeReader<T>> inputs;
    private IterativeReader<T> current = emptyIterator();

    /**
     * Create an IterativeReader that iterates over the readers in <code>readers</code>
     *
     * @param readers the readers to combine into one IterativeReader
     */
    public ConcatIterativeReader(IterativeReader<T>... readers) {

        checkNotNull(readers);

        this.inputs = Arrays.asList(readers).iterator();
    }

    /**
     * Create an IterativeReader that iterates over the readers in <code>inputs</code>
     *
     * @param inputs an iterator that contains the readers that are to be concatenated
     */
    public ConcatIterativeReader(Collection<IterativeReader<T>> inputs) {

        this.inputs = inputs.iterator();
    }

    @Override
    public boolean hasNext() {

        checkState(!closed);

        boolean currentHasNext;
        while (!(currentHasNext = checkNotNull(current).hasNext()) && inputs.hasNext()) {

            try {
                current.close();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
            current = inputs.next();
        }
        return currentHasNext;
    }

    @Override
    public T next() throws IOException {

        checkState(!closed);

        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        return current.next();
    }

    @Override
    public void close() throws IOException {

        current.close();
        while (inputs.hasNext()) {

            inputs.next().close();
        }
        closed = true;
    }

    /**
     * Returns the empty Iterative reader.
     */
    // Casting to any type is safe since there are no actual elements.
    @SuppressWarnings("unchecked")
    private IterativeReader<T> emptyIterator() {

        return (IterativeReader<T>) IterativeReader.EMPTY_READER;
    }
}
