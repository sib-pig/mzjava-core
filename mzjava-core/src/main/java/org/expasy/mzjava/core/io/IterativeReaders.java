/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class IterativeReaders {

    public static <T> List<T> toArrayList(IterativeReader<T> reader) {

        List<T> list = new ArrayList<>();

        try {
            while (reader.hasNext()) {

                list.add(reader.next());
            }
            reader.close();
        } catch (IOException e) {

            throw new IllegalStateException(e);
        }

        return list;
    }

    public static <T> IterativeReader<T> fromCollection(Collection<T> collection) {

        return new IteratorReader<>(collection.iterator());
    }

    public static <T> IterativeReader<T> fromIterator(Iterator<T> it) {

        return new IteratorReader<>(it);
    }

    public static <T> IterativeReader<T> empty() {

        //Casting i s save because reader is empty
        //noinspection unchecked
        return (IterativeReader<T>) IterativeReader.EMPTY_READER;
    }

    private static class IteratorReader<T> implements IterativeReader<T> {

        private final Iterator<T> it;

        public IteratorReader(Iterator<T> it) {

            this.it = it;
        }

        @Override
        public boolean hasNext() {

            return it.hasNext();
        }

        @Override
        public T next() throws IOException {

            return it.next();
        }

        @Override
        public void close() throws IOException {

            // Do nothing because the iterator cannot be closed
        }
    }
}
