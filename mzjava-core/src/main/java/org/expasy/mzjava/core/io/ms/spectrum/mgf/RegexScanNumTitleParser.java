/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.io.ms.spectrum.mgf;

import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Implementation of a title parser that uses a regex pattern to extract the scan number from the title
 *
 * @author nikitin
 * @version 1.0
 */
public class RegexScanNumTitleParser implements TitleParser {

    private static final Pattern CAPT_GROUP_PATTERN = Pattern.compile("(?<!\\\\)\\([^)]+\\)");

    private final Pattern pat;

    public RegexScanNumTitleParser(Pattern pat) {

        checkNotNull(pat);

        int count = countCapturingGroup(pat);

        checkArgument(count != 0, "regexp "+ pat.pattern()+" misses to capture scan number!");
        checkArgument(count == 1, "regexp "+ pat.pattern()+" contains too many capture groups (need only one to get scan number)!");

        this.pat = pat;
    }

    private static int countCapturingGroup(Pattern pat) {

        Matcher matcher = CAPT_GROUP_PATTERN.matcher(pat.pattern());

        int count=0;
        while (matcher.find()) {

            count++;
        }

        return count;
    }

    @Override
    public boolean parseTitle(String title, MsnSpectrum spectrum) {

        Matcher matcher = pat.matcher(title);

        if (matcher.find()) {

            spectrum.addScanNumber(Integer.parseInt(matcher.group(1)));
            return true;
        } else {

            return false;
        }
    }

}
