/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.transformer;

import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.AbstractPeakPairProcessor;

import java.util.List;

/**
 * Processor that scales down the peak list that has the larger ion current so that it has the same current as the
 * other peak list.
 *
 * Peak pairs where both peaks have an intensity that is < threshold are discarded
 *
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PeakPairIonCurrentNormalizer<X extends PeakAnnotation, Y extends PeakAnnotation> extends AbstractPeakPairProcessor<X, Y> {

    private final double threshold;

    private double scale;

    private enum ScaleCandidate {PEAK_LIST_X, PEAK_LIST_Y}
    private ScaleCandidate scaleCandidate;

    public PeakPairIonCurrentNormalizer(double threshold) {

        this.threshold = threshold;
    }

    @Override
    public void processPeakPair(final double centroid, final double xIntensity, final double yIntensity, final List<X> xAnnotations, final List<Y> yAnnotations) {

        double xScaled = xIntensity;
        double yScaled = yIntensity;
        if (scaleCandidate == ScaleCandidate.PEAK_LIST_X) {

            xScaled = xIntensity * scale;
        } else {

            yScaled = yIntensity * scale;
        }

        if (xScaled >= threshold || yScaled >= threshold) {

            sink.processPeakPair(centroid, xScaled, yScaled, xAnnotations, yAnnotations);
        }
    }

    @Override
    public void begin(PeakList<X> xPeakList, PeakList<Y> yPeakList) {

        if (xPeakList.getTotalIonCurrent() >= yPeakList.getTotalIonCurrent()) {

            scaleCandidate = ScaleCandidate.PEAK_LIST_X;
            scale = yPeakList.getTotalIonCurrent() / xPeakList.getTotalIonCurrent();
        } else {

            scaleCandidate = ScaleCandidate.PEAK_LIST_Y;
            scale = xPeakList.getTotalIonCurrent() / yPeakList.getTotalIonCurrent();
        }

        sink.begin(xPeakList, yPeakList);
    }

    @Override
    public void end() {

        sink.end();
    }
}
