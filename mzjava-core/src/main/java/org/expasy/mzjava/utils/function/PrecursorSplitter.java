package org.expasy.mzjava.utils.function;

import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;

/**
 * Splits a list of PeakList into sub lists. The input list is split wherever
 * the previous and next PeakList are not within tolerance.
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PrecursorSplitter extends ChargeMzSplitter<PeakList> {

    public PrecursorSplitter(Tolerance tolerance) {

        super(tolerance);

    }

    @Override
    protected double extractMz(PeakList value) {

        return value.getPrecursor().getMz();
    }

    @Override
    protected int extractCharge(PeakList value) {

        return value.getPrecursor().getCharge();
    }
}
