package org.expasy.mzjava.utils.function;

import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeDiscrete;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Splits a list of MsnSpectra into sub lists. The input list is split wherever
 * the previous and next MsnSpectra have a difference in retention time that is larger
 * than <code>minRT</code>.
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class RetentionTimeSplitter implements Splitter<MsnSpectrum> {

    //Minimum retention time difference in seconds
    private final RetentionTimeDiscrete retentionTolerance;

    /**
     *
     * @param retentionTolerance the minimum retention time in seconds
     */
    public RetentionTimeSplitter(RetentionTimeDiscrete retentionTolerance) {

        this.retentionTolerance = retentionTolerance;
    }

    @Override
    public <PL extends MsnSpectrum> List<List<PL>> split(List<PL> input) {

        if(input.isEmpty()) {

            return Collections.singletonList(input);
        }

        List<List<PL>> splits = new ArrayList<List<PL>>();

        int index0 = 0;
        PL last = input.get(0);
        int inputSize = input.size();
        for (int i = 1; i < inputSize; i++) {

            PL current = input.get(i);

            double lastRT = last.getRetentionTimes().getFirst().getTime();

            double currentRT = current.getRetentionTimes().getFirst().getTime();

            double delta = currentRT - lastRT;

            boolean split;
            if(delta >= retentionTolerance.getTime()){
                split = true;
            } else if(delta < 0) {
                throw new IllegalStateException("Input is not sorted by retention time");
            } else
                split = false;

            if(split) {

                splits.add(input.subList(index0, i));
                index0 = i;
            }

            last = current;
        }

        splits.add(input.subList(index0, inputSize));

        return splits;
    }
}
