package org.expasy.mzjava.utils.function;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public interface DoubleFunction<V> {

    double apply(V input);
}
