package org.expasy.mzjava.utils.function;

import org.expasy.mzjava.core.ms.Tolerance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Base class for splitter that splits on charge and m/z
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public abstract class ChargeMzSplitter<V> implements Splitter<V> {

    protected final Tolerance tolerance;

    public ChargeMzSplitter(Tolerance tolerance) {

        this.tolerance = tolerance;
    }

    @Override
    public <PL extends V> List<List<PL>> split(List<PL> input) {

        if(input.isEmpty()) {

            return Collections.singletonList(input);
        }

        List<List<PL>> splits = new ArrayList<List<PL>>();

        int index0 = 0;
        PL last = input.get(0);
        int inputSize = input.size();
        for (int i = 1; i < inputSize; i++) {

            PL current = input.get(i);

            double lastMz = extractMz(last);
            int lastCharge = extractCharge(last);

            double currentMz = extractMz(current);
            int currentCharge = extractCharge(current);

            Tolerance.Location location = tolerance.check(lastMz, currentMz);

            boolean split;
            if(lastCharge != currentCharge){
                split = true;
            } else if(currentMz < lastMz) {
                throw new IllegalStateException("Input is not sorted by precursor");
            } else
                split = location == Tolerance.Location.LARGER;

            if(split) {

                splits.add(input.subList(index0, i));
                index0 = i;
            }

            last = current;
        }

        splits.add(input.subList(index0, inputSize));

        return splits;
    }

    protected abstract double extractMz(V value);

    protected abstract int extractCharge(V value);
}
