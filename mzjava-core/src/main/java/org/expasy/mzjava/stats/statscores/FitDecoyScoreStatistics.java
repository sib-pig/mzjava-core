package org.expasy.mzjava.stats.statscores;

import gnu.trove.list.TDoubleList;
import gnu.trove.list.TFloatList;

/**
 * This method is appropriate if for separate decoy and target searches and if more than one hit
 * is considered. The decoy distribution is fitted into the target distribution using linear
 * regression.
 *
 * @author Markus Muller
 * @version 0.0
 */
public class FitDecoyScoreStatistics extends ScoreStatistics {

    protected float targetDecoyRatio;

    public FitDecoyScoreStatistics(float[] targetScores, float[] decoyScores) {
        super(targetScores,decoyScores);

        calcTargetDecoyRatio();
        calculateFDR();
        calculateQValue();
    }

    public FitDecoyScoreStatistics(float[] targetScores, float[] decoyScores, SortAction sortAction) {
        super(targetScores,decoyScores,sortAction);

        calcTargetDecoyRatio();
        calculateFDR();
        calculateQValue();
    }

    public FitDecoyScoreStatistics(double[] targetScores, double[] decoyScores) {
        super(targetScores,decoyScores);

        calcTargetDecoyRatio();
        calculateFDR();
        calculateQValue();
    }

    public FitDecoyScoreStatistics(double[] targetScores, double[] decoyScores, SortAction sortAction) {
        super(targetScores,decoyScores,sortAction);

        calcTargetDecoyRatio();
        calculateFDR();
        calculateQValue();
    }

    public FitDecoyScoreStatistics(TFloatList targetScores, TFloatList decoyScores) {
        super(targetScores,decoyScores);

        calcTargetDecoyRatio();
        calculateFDR();
        calculateQValue();
    }

    public FitDecoyScoreStatistics(TFloatList targetScores, TFloatList decoyScores, SortAction sortAction) {
        super(targetScores,decoyScores,sortAction);

        calcTargetDecoyRatio();
        calculateFDR();
        calculateQValue();
    }

    public FitDecoyScoreStatistics(TDoubleList targetScores, TDoubleList decoyScores) {
        super(targetScores,decoyScores);

        calcTargetDecoyRatio();
        calculateFDR();
        calculateQValue();
    }

    public FitDecoyScoreStatistics(TDoubleList targetScores, TDoubleList decoyScores, SortAction sortAction) {
        super(targetScores,decoyScores,sortAction);

        calcTargetDecoyRatio();
        calculateFDR();
        calculateQValue();
    }

    public float getTargetDecoyRatio() {
        if (targetDecoyRatio<0) calcTargetDecoyRatio();
        return targetDecoyRatio;
    }

    @Override
    protected void calculateFDR() {

        fdrValues = new float[targetScores.length];

        int iDecoy =  decoyScores.length-1;
        for (int i=targetScores.length-1;i>=0;i--) {
            float targetScore = targetScores[i];
            while (iDecoy>0 && decoyScores[iDecoy]>targetScore) iDecoy--;
            int nrTarget = targetScores.length-i;
            int nrDecoy = decoyScores.length-iDecoy-1;
            fdrValues[i] = targetDecoyRatio*nrDecoy/nrTarget;
        }
    }

    private void calcTargetDecoyRatio() {
        int nrTarget =  0;

        int nrDecoyForFit = Math.round(this.decoyScores.length*0.5f);

        for (int i=0;i<nrDecoyForFit;i++) {
            while (nrTarget<this.targetScores.length && this.targetScores[nrTarget]<this.decoyScores[i]) nrTarget++;
        }

        targetDecoyRatio = (1.0f*nrTarget)/nrDecoyForFit;

    }

}
