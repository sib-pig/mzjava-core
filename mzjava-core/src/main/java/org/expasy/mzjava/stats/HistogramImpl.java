/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.stats;


import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Range;
import org.expasy.mzjava.utils.PrimitiveArrayUtils;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;


/**
 * The default implementation of Histogram
 */
public class HistogramImpl implements Histogram {

    private String name;
    private double binWidth;
    private int binNumber;
    private Range<Double> range;
    private final Set<Integer> nonEmptyBinIndices;
    protected double cumulFrequencies;
    protected double[] mids, breaks;

    private double[] frequencies;

    public HistogramImpl() {

        name = "";
        range = Range.closedOpen(0., 0.);
        nonEmptyBinIndices = new TreeSet<Integer>();

        initBins(0);
    }

    /**
     * <h4>Remark:</h4> if (max-first) % binWidth &ne; 0 then max is extended to
     * the next correct bin.
     * <p/>
     *
     * @param binWidth the width of bins.
     * @param min      first bin lower bound (included).
     * @param max      last bin upper bound (excluded)
     */
    public HistogramImpl(double binWidth, double min, double max) {

        this();

        checkArgument(binWidth > 0, "binWidth=" + binWidth + ": bin width has to be strictly positive!");

        double diff = max - min;

        int currentBinNumber = (int) Math.floor(diff / binWidth);
        double mod = diff % binWidth;

        if (mod != 0) {
            currentBinNumber++;

            max = min + (binWidth * currentBinNumber);
        }

        init(currentBinNumber, binWidth, Range.closedOpen(min, max));
    }

    public HistogramImpl(int binNumber, double binWidth, double min) {

        this();
        init(binNumber, binWidth, min, min + (binWidth * binNumber));
    }

    public static HistogramImpl valueOfBinNumWidthMin(int binNumber,
                                                      double binWidth, double min) {

        return new HistogramImpl(binNumber, binWidth, min);
    }

    public static HistogramImpl valueOfBinWidthMinMax(double binWidth, double min,
                                                      double max) {

        return new HistogramImpl(binWidth, min, max);
    }

    protected void initBins(int binNumber) {

        this.frequencies = new double[binNumber];
    }

    private void init(int binNumber, double binWidth, double min, double max) {

        init(binNumber, binWidth, Range.closedOpen(min, max));
    }

    private void init(int binNumber, double binWidth, Range<Double> range) {

        checkArgument(binNumber > 0, "binNumber=" + binNumber + ": bin number has to be strictly positive!");
        checkArgument(binWidth > 0, "binWidth=" + binWidth + ": bin width has to be strictly positive!");

        this.binNumber = binNumber;
        this.range = range;
        this.binWidth = binWidth;

        initBins(binNumber);
    }

    @Override
    public HistogramImpl shallowCopy() {

        HistogramImpl hist = new HistogramImpl();

        hist.init(size(), getBinWidth(), getRange());

        return hist;
    }

    @Override
    public HistogramImpl copy() {

        HistogramImpl copy = shallowCopy();

        Set<Integer> nonEmptyBinIndices = getNonEmptyBinIndices();

        for (int i : nonEmptyBinIndices) {

            copy.addDataAtBin(getAbsoluteBinFreq(i), i);
            copy.getNonEmptyBinIndices().add(i);
        }

        copy.cumulFrequencies = cumulFrequencies;

        return copy;
    }

    @Override
    public void clear() {

        cumulFrequencies = 0;
        nonEmptyBinIndices.clear();

        for (int i = 0; i < frequencies.length; i++) {

            frequencies[i] = 0;
        }
    }

    /**
     * Add
     *
     * @param bins
     * @return
     */
    public boolean addBins(double[] bins) {

        checkNotNull(bins, "undefined bins!");
        checkArgument(bins.length == frequencies.length, "incompatible bin numbers!");

        for (int i = 0; i < bins.length; i++) {

            addDataAtBin(bins[i], i);
        }

        return true;
    }

    @Override
    public boolean addDataAtBin(double weight, int index) {

        if (weight == 0 || index == -1) {

            return false;
        }

        frequencies[index] += weight;

        return true;
    }

    @Override
    public double getAbsoluteBinFreq(int index) {

        if (index == -1) {

            return 0;
        }

        return frequencies[index];
    }

    public double[] getAbsoluteBinFreqs(double[] dest) {

        return PrimitiveArrayUtils.loadDoubles(frequencies, dest);
    }

    @Override
    public int getMostIntenseBinIndex() {

        return PrimitiveArrayUtils.indexMax(frequencies);
    }

    @Override
    public Histogram normalize(Normalization normalization) {

        Preconditions.checkNotNull(normalization);

        double newToOldRatio;

        switch (normalization.getType()) {
            case BIN_MAX:

                double max = getAbsoluteBinFreq(getMostIntenseBinIndex());
                newToOldRatio = normalization.getValue() / max;
                break;
            case BINS_CUMUL:

                newToOldRatio = normalization.getValue() / getCumulFreqs();
                break;
            default:
                throw new IllegalStateException("cannot normalise using " + normalization.getType());
        }

        HistogramImpl hist = copy();

        hist.cumulFrequencies = 0;

        for (int i = 0; i < hist.frequencies.length; i++) {

            if (hist.frequencies[i] > 0) {

                hist.frequencies[i] *= newToOldRatio;
                hist.cumulFrequencies += hist.frequencies[i];
            }
        }

        return hist;
    }

    @Override
    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    @Override
    public boolean isEmpty() {

        return cumulFrequencies == 0;
    }

    @Override
    public boolean addData(double value) {

        return addData(value, 1, null);
    }

    @Override
    public boolean addData(double value, double weight) {

        return addData(value, weight, null);
    }

    @Override
    public void addData(double[] values) {

        checkNotNull(values, "undefined values!");

        for (double value : values) {

            addData(value);
        }
    }

    @Override
    public void addData(double[] values, double[] weights) {

        checkNotNull(values, "undefined values!");
        checkNotNull(weights, "undefined weighs!");
        checkArgument(values.length == weights.length, "illegal numbers of values and weights: " + values.length
                + " values, " + weights.length + " weights!");

        for (int i = 0; i < values.length; i++) {

            addData(values[i], weights[i]);
        }
    }

    @Override
    public boolean addData(double value, double weight, Predicate<Double> predicate) {

        if (range.contains(value) && weight > 0
                && (predicate == null || predicate.apply(value))) {

            int index = getBinIndex(value);

            if (getAbsoluteBinFreq(index) == 0) {

                nonEmptyBinIndices.add(index);
            }

            // implemented by the concrete class
            addDataAtBin(weight, index);

            cumulFrequencies += weight;

            return true;
        }

        return false;
    }

    @Override
    public Range<Double> getRange() {

        return range;
    }

    @Override
    public int size() {

        return binNumber;
    }

    @Override
    public Set<Integer> getNonEmptyBinIndices() {

        return nonEmptyBinIndices;
    }

    @Override
    public double getBinWidth() {

        return binWidth;
    }

    @Override
    public double getRelativeBinFreq(int index) {

        if (getCumulFreqs() != 0) {

            return getAbsoluteBinFreq(index) / getCumulFreqs();
        }

        return 0;
    }

    @Override
    public double getRelativeBinFreq(int fromIncluded, int toExcluded) {

        if (getCumulFreqs() != 0) {

            return getCumulFreqs(fromIncluded, toExcluded) / getCumulFreqs();
        }

        return 0;
    }

    @Override
    public double getCumulFreqs() {

        return cumulFrequencies;
    }

    @Override
    public double getCumulFreqs(int fromIncluded, int toExcluded) {

        checkArgument(fromIncluded >= 0 && fromIncluded < binNumber);
        checkArgument(toExcluded > 0 && toExcluded <= binNumber);

        if (fromIncluded == 0 && toExcluded == binNumber) {

            return cumulFrequencies;
        }

        double sum = 0;

        for (int i = fromIncluded; i < toExcluded; i++) {

            sum += getAbsoluteBinFreq(i);
        }

        return sum;
    }

    @Override
    public int getBinIndex(double value) {

        if (!range.contains(value)) {

            return -1;
        }

        return (int) Math.floor((value - range.lowerEndpoint()) / getBinWidth());
    }

    @Override
    public double[] getBreaks(double[] dest) {

        if (breaks == null) {

            breaks = calculateBreaks();
        }

        return PrimitiveArrayUtils.loadDoubles(breaks, dest);
    }

    @Override
    public double[] getMids(double[] dest) {

        if (mids == null) {

            mids = calculateMids();
        }

        return PrimitiveArrayUtils.loadDoubles(mids, dest);
    }

    private double[] calculateBreaks() {

        double[] breaks = new double[binNumber + 1];

        for (int i = 0; i <= binNumber; i++) {

            breaks[i] = range.lowerEndpoint() + (i * binWidth);
        }

        return breaks;
    }

    private double[] calculateMids() {

        double[] mids = new double[binNumber];

        for (int i = 0; i < binNumber; i++) {

            mids[i] = range.lowerEndpoint() + ((i + 0.5) * binWidth);
        }

        return mids;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HistogramImpl histogram = (HistogramImpl) o;

        if (binNumber != histogram.binNumber) return false;
        if (Double.compare(histogram.binWidth, binWidth) != 0) return false;
        if (Double.compare(histogram.cumulFrequencies, cumulFrequencies) != 0) return false;
        if (!Arrays.equals(breaks, histogram.breaks)) return false;
        if (!Arrays.equals(frequencies, histogram.frequencies)) return false;
        if (!Arrays.equals(mids, histogram.mids)) return false;
        if (name != null ? !name.equals(histogram.name) : histogram.name != null) return false;
        if (nonEmptyBinIndices != null ? !nonEmptyBinIndices.equals(histogram.nonEmptyBinIndices) : histogram.nonEmptyBinIndices != null)
            return false;
        if (range != null ? !range.equals(histogram.range) : histogram.range != null) return false;

        return true;
    }

    @Override
    public int hashCode() {

        int result;
        long temp;
        result = name != null ? name.hashCode() : 0;
        temp = Double.doubleToLongBits(binWidth);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + binNumber;
        result = 31 * result + (range != null ? range.hashCode() : 0);
        result = 31 * result + (nonEmptyBinIndices != null ? nonEmptyBinIndices.hashCode() : 0);
        temp = Double.doubleToLongBits(cumulFrequencies);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (mids != null ? Arrays.hashCode(mids) : 0);
        result = 31 * result + (breaks != null ? Arrays.hashCode(breaks) : 0);
        result = 31 * result + (frequencies != null ? Arrays.hashCode(frequencies) : 0);
        return result;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder(getClass().getSimpleName());

        if (getName().length() > 0) {
            sb.append(" ");
            sb.append(getName());
        }
        sb.append("{range=").append(getRange());
        sb.append(", bin width=").append(getBinWidth());
        sb.append(", bin#=").append(size());

        sb.append(":");

        getBreaks(null);

        for (int index : getNonEmptyBinIndices()) {
            sb.append("[").append(breaks[index]).append("=").append(getAbsoluteBinFreq(index)).append("],");
        }
        sb.append(" cumulfreqs=").append(getCumulFreqs());
        sb.append("}");

        return sb.toString();
    }

}
