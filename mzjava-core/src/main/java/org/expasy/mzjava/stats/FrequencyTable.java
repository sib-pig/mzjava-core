/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.stats;

import com.google.common.base.Preconditions;
import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.procedure.TIntIntProcedure;
import gnu.trove.set.hash.TIntHashSet;

import java.util.Arrays;

/**
 * A table to hold the frequency distribution of values from one variable.
 * <p>
 * Each entry in the table contains the frequency or count of the occurrences of values within a particular interval,
 * and in this way, the table summarizes the distribution of values in the sample.
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class FrequencyTable {

    private final double binSize;

    private int count = 0;

    private final TIntIntHashMap bins = new TIntIntHashMap();
    private int minBinId = Integer.MAX_VALUE;
    private int maxBinId = 0;

    private String name = "new";

    /**
     * Construct a new FrequencyTable that has intervals that have a width of <code>binSize</code>
     *
     * @param binSize the width of the intervals
     */
    public FrequencyTable(double binSize) {

        this.binSize = binSize;
    }

    /**
     * Construct a new FrequencyTable that has intervals that have a width of <code>binSize</code>
     *
     * @param binSize the width of the intervals
     * @param name the name of the table
     */
    public FrequencyTable(double binSize, String name) {

        this.binSize = binSize;
        this.name = name;
    }

    /**
     * Add a value to this table
     *
     * @param value the value to add
     */
    public void add(double value) {

        int index = getBinId(value);

        count++;

        minBinId = Math.min(index, minBinId);
        maxBinId = Math.max(index, maxBinId);

        int count = bins.get(index) + 1;

        bins.put(index, count);
    }

    /**
     * Return the total number of values in this table.
     *
     * @return the total number of values in this table
     */
    public int getTotal() {

        return count;
    }

    /**
     * Returns an array that holds the ids of the bins (intervals) that contain values.
     *
     * @return an array that holds the ids of the bins (intervals) that contain values
     */
    public int[] getBinIds() {

        int[] keys = bins.keys();

        Arrays.sort(keys);

        return keys;
    }

    /**
     * Return the number of values that fall in the bin (interval) for the given <code>binId</code>
     *
     * @param binId the bin for which the frequency is to be returned
     * @return the number of values that fall in the bin (interval) for the given <code>binId</code>
     */
    public int getFrequency(int binId) {

        return bins.get(binId);
    }

    /**
     * Return the id of the bin (interval) that the <code>value</code> falls in.
     *
     * @param value the vale
     * @return the id of the bin (interval) that the <code>value</code> falls in
     */
    public int getBinId(double value) {

        return (int)(value/binSize);
    }

    /**
     * Return the center value of the bin at index <code>binId</code>
     *
     * @param binId the index of the bin
     * @return the center value of the bin at index <code>binId</code>
     */
    public double getCenter(int binId) {

        if (binId >= 0) {
            return getMax(binId) - binSize /2;
        } else {
            return getMin(binId) - binSize /2;
        }
    }

    /**
     * Return the min value of the bin at index <code>binId</code>
     *
     * @param binId the index of the bin
     * @return the min value of the bin at index <code>binId</code>
     */
    public double getMin(int binId) {

        return binId * binSize;
    }

    /**
     * Return the max value of the bin at index <code>binId</code>
     *
     * @param binId the index of the bin
     * @return the max value of the bin at index <code>binId</code>
     */
    public double getMax(int binId) {

        return (binId * binSize) + binSize;
    }

    /**
     * Return the id of the smallest bin that contains a value
     *
     * @return the id of the smallest bin that contains a value
     */
    public int getMinBinId() {

        return minBinId;
    }

    /**
     * Return the id of the largest bin that contains a value
     *
     * @return the id of the largest bin that contains a value
     */
    public int getMaxBinId() {

        return maxBinId;
    }

    /**
     * Return the bin size.
     *
     * @return the bin size
     */
    public double getBinSize() {

        return binSize;
    }

    @Override
    public String toString() {

        return toString(false);
    }

    /**
     * Returns a string representation of this FrequencyTable
     *
     * @param normalise true if the frequencies are to be normalized
     * @return string representation of this FrequencyTable
     */
    public String toString(boolean normalise) {

        StringBuilder buff = new StringBuilder();

        for(int binId : getBinIds()) {

            buff.append(getMin(binId));
            buff.append('\t');
            if (normalise) {

                buff.append(getFrequency(binId)/(double)count);
            } else {

                buff.append(getFrequency(binId));
            }
            buff.append('\n');
        }

        return buff.toString();
    }

    /**
     * Return the name of this FrequencyTable
     *
     * @return the name of this FrequencyTable
     */
    public String getName() {

        return name;
    }

    /**
     * Summarize the tables in one string
     *
     * @param countName the name of the values
     * @param tables the tables
     * @return the tables in one string
     */
    public static String toString(String countName, FrequencyTable... tables) {

        if(tables == null || tables.length == 0) return "null";

        double binSize = tables[0].getBinSize();
        for (int i = 1; i < tables.length; i++) {

            if(tables[i].getBinSize() != binSize) throw new IllegalStateException("All bin sizes need to be the same");

        }

        long[] totals = new long[tables.length];
        StringBuilder buff = new StringBuilder(countName);

        TIntHashSet idSet = new TIntHashSet();
        for (int i = 0; i < tables.length; i++) {

            FrequencyTable table = tables[i];
            totals[i] = table.getTotal();
            idSet.addAll(table.getBinIds());
            buff.append('\t');
            buff.append(table.getName());
        }

        boolean[] started = new boolean[tables.length];

        int[] ids = idSet.toArray();
        Arrays.sort(ids);
        for(int id : ids) {

            buff.append('\n');
            buff.append(tables[0].getCenter(id));

            for (int i = 0; i < tables.length; i++) {

                FrequencyTable table = tables[i];

                buff.append('\t');
                final double v = table.getFrequency(id) / (double) totals[i];
                if (started[i] || v > 0) {

                    buff.append(v);
                    started[i] = true;
                }
            }
        }

        buff.append('\n');
        buff.append("N");

        for (long total : totals) {

            buff.append('\t');
            buff.append(total);
        }

        return buff.toString();
    }

    /**
     * Summarize the tables in one string without normalizing the frequencies
     *
     * @param countName the name of the values
     * @param tables the tables
     * @return the tables in one string without normalizing the frequencies
     *
     * @noinspection ForLoopReplaceableByForEach
     */
    public static String toStringNotNormalize(String countName, FrequencyTable... tables) {

        if (tables == null || tables.length == 0) return "null";

        double binSize = tables[0].getBinSize();
        for (int i = 1; i < tables.length; i++) {

            if (tables[i].getBinSize() != binSize)
                throw new IllegalStateException("All bucket sizes need to be the same");
        }

        StringBuilder buff = new StringBuilder(countName);

        TIntHashSet idSet = new TIntHashSet();
        for (int i = 0; i < tables.length; i++) {

            FrequencyTable table = tables[i];
            idSet.addAll(table.getBinIds());
            buff.append('\t');
            buff.append(table.getName());
        }

        int[] ids = idSet.toArray();
        Arrays.sort(ids);
        for (int id : ids) {

            buff.append('\n');
            buff.append(tables[0].getCenter(id));

            for (int i = 0; i < tables.length; i++) {

                FrequencyTable table = tables[i];

                buff.append('\t');
                buff.append(table.getFrequency(id));
            }
        }

        return buff.toString();
    }

    /**
     * Create an array of TDoubleArrayList that contains the frequencies from the tables
     *
     * @param tables the tables
     * @return array of TDoubleArrayList that contains the frequencies from the tables
     */
    public static TDoubleArrayList[] toTable(FrequencyTable... tables) {

        if (tables == null || tables.length == 0) return new TDoubleArrayList[0];

        double binSize = tables[0].getBinSize();
        for (int i = 1; i < tables.length; i++) {

            if (tables[i].getBinSize() != binSize)
                throw new IllegalStateException("All bucket sizes need to be the same");
        }

        TIntHashSet idSet = new TIntHashSet();
        for (FrequencyTable table : tables) {

            idSet.addAll(table.getBinIds());
        }
        int[] ids = idSet.toArray();
        Arrays.sort(ids);

        TDoubleArrayList[] outTable = new TDoubleArrayList[ids.length];
        for (int i = 0; i < outTable.length; i++) {

            int binId = ids[i];
            TDoubleArrayList row = new TDoubleArrayList();

            row.add(tables[0].getMin(binId));

            for(FrequencyTable table : tables)
                row.add(table.getFrequency(binId));

            outTable[i] = row;
        }

        return outTable;
    }

    public void addAll(FrequencyTable frequencyTable){

        Preconditions.checkNotNull(frequencyTable);
        Preconditions.checkArgument(binSize == frequencyTable.binSize);

        count += frequencyTable.count;

        frequencyTable.bins.forEachEntry(new TIntIntProcedure() {
            @Override
            public boolean execute(int binId, int frequency) {

                bins.adjustOrPutValue(binId, frequency, frequency);
                minBinId = Math.min(binId, minBinId);
                maxBinId = Math.max(binId, maxBinId);
                return true;
            }
        });
    }
}
