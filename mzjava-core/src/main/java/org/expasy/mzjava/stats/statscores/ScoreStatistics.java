package org.expasy.mzjava.stats.statscores;

import com.google.common.base.Preconditions;
import gnu.trove.impl.unmodifiable.TUnmodifiableFloatList;
import gnu.trove.list.TDoubleList;
import gnu.trove.list.TFloatList;
import gnu.trove.list.array.TFloatArrayList;

import java.util.Arrays;

/**
 * @author Markus Muller
 * @author Oliver Horlacher
 * @version 0.0
 */
public abstract class ScoreStatistics {

    public enum SortAction {DO_SORT,DONT_SORT,SORT_IF_UNSORTED}

    protected float[] targetScores;
    protected float[] decoyScores;
    protected float[] fdrValues;
    protected float[] qValues;

    protected ScoreStatistics() {

        init();
    }

    public ScoreStatistics(float[] targetScores, float[] decoyScores) {
        this(targetScores, decoyScores, SortAction.SORT_IF_UNSORTED);
    }


    public ScoreStatistics(float[] targetScores, float[] decoyScores, SortAction sortAction) {

        Preconditions.checkNotNull(targetScores);
        Preconditions.checkNotNull(decoyScores);
        Preconditions.checkArgument(targetScores.length > 0);
        Preconditions.checkArgument(decoyScores.length > 0);

        init();

        this.targetScores = Arrays.copyOf(targetScores, targetScores.length);
        this.decoyScores = Arrays.copyOf(decoyScores, decoyScores.length);

        sort(sortAction);
    }

    public ScoreStatistics(double[] targetScores, double[] decoyScores) {
        this(targetScores, decoyScores, SortAction.SORT_IF_UNSORTED);
    }

    public ScoreStatistics(double[] targetScores, double[] decoyScores, SortAction sortAction) {

        Preconditions.checkNotNull(targetScores);
        Preconditions.checkNotNull(decoyScores);
        Preconditions.checkArgument(targetScores.length > 0);
        Preconditions.checkArgument(decoyScores.length > 0);

        init();

        this.targetScores = new float[targetScores.length];
        for (int i=0;i<this.targetScores.length;i++) {
            this.targetScores[i] = (float) targetScores[i];
        }

        this.decoyScores = new float[decoyScores.length];
        for (int i=0;i<this.decoyScores.length;i++) {
            this.decoyScores[i] = (float) decoyScores[i];
        }

        sort(sortAction);

    }


    public ScoreStatistics(TFloatList targetScores, TFloatList decoyScores) {
        this(targetScores, decoyScores, SortAction.SORT_IF_UNSORTED);
    }

    public ScoreStatistics(TFloatList targetScores, TFloatList decoyScores, SortAction sortAction) {

        Preconditions.checkNotNull(targetScores);
        Preconditions.checkNotNull(decoyScores);
        Preconditions.checkArgument(!targetScores.isEmpty());
        Preconditions.checkArgument(!decoyScores.isEmpty());

        init();

        this.targetScores = targetScores.toArray();
        this.decoyScores = decoyScores.toArray();

        sort(sortAction);
    }

    public ScoreStatistics(TDoubleList targetScores, TDoubleList decoyScores) {
        this(targetScores, decoyScores, SortAction.SORT_IF_UNSORTED);
    }

    public ScoreStatistics(TDoubleList targetScores, TDoubleList decoyScores, SortAction sortAction) {

        Preconditions.checkNotNull(targetScores);
        Preconditions.checkNotNull(decoyScores);
        Preconditions.checkArgument(!targetScores.isEmpty());
        Preconditions.checkArgument(!decoyScores.isEmpty());

        init();

        this.targetScores = new float[targetScores.size()];
        for (int i=0;i<this.targetScores.length;i++) {
            this.targetScores[i] = (float) targetScores.get(i);
        }

        this.decoyScores = new float[decoyScores.size()];
        for (int i=0;i<this.decoyScores.length;i++) {
            this.decoyScores[i] = (float) decoyScores.get(i);
        }

        sort(sortAction);
    }

    private boolean isSorted(float[] values) {

        for (int i=1;i<values.length;i++) {
            if (values[i-1] > values[i]) return false;
        }

        return true;
    }

    private void sort(SortAction sortAction) {

        if (sortAction == SortAction.DO_SORT) {
            Arrays.sort(targetScores);
            Arrays.sort(decoyScores);
        } else if (sortAction == SortAction.SORT_IF_UNSORTED) {

            if (!isSorted(targetScores))  Arrays.sort(targetScores);
            if (!isSorted(decoyScores))  Arrays.sort(decoyScores);
        }

    }

    public float getScoreForQValue(float qValue) {

        int indexLeft = 0;
        int indexRight = qValues.length-1;

        if (qValue>-qValues[indexLeft]) return targetScores[indexLeft];
        if (qValue<-qValues[indexRight]) return targetScores[indexRight];

        int index = getLowestIndex(qValue);

        float score;

        if (index>=0) {
            return targetScores[index];
        } else {
            indexLeft = -index-2;
            if (indexLeft<0) {
                score = targetScores[0];
                return score;
            }
            indexRight = -index-1;
            if (indexRight>qValues.length-1) {
                score = targetScores[qValues.length-1];
                return score;
            }
            return targetScores[indexLeft]+(-qValue-qValues[indexLeft])/(qValues[indexRight]-qValues[indexLeft])*(targetScores[indexRight]-targetScores[indexLeft]);
        }
    }

    private int getLowestIndex(float qValue) {

        int index = Arrays.binarySearch(qValues,-qValue);

        // set index to samllest possible value
        if (index>=0) {
            float tmpQValue = qValues[index];
            for (int i=index-1;i>=0;i--) {
                if (qValues[i]<tmpQValue) {
                    index = i+1;
                    break;
                }
            }
        } else {
            float tmpQValue = 0;
            if (-index-1<qValues.length-1) tmpQValue = qValues[-index-1];
            for (int i=-index-2;i>=0;i--) {
                if (qValues[i]<tmpQValue) {
                    index = -i-2;
                    break;
                }
            }

        }

        return index;
    }

    public float getQValue(float score){

        if (score<targetScores[0]) return -qValues[0];
        if (score>targetScores[targetScores.length-1]) return -qValues[targetScores.length-1];

        int index = Arrays.binarySearch(targetScores,score);

        if (index>=0) {
            return  -qValues[index];
        } else {
            int indexLeft = -index-2;
            if (indexLeft<0) {
                return -qValues[0];
            }
            int indexRight = -index-1;
            if (indexRight>qValues.length-1) {
                return  -qValues[qValues.length-1];
            }
            return  -qValues[indexLeft]-(score-targetScores[indexLeft])/(targetScores[indexRight]-targetScores[indexLeft])*(qValues[indexRight]-qValues[indexLeft]);
        }
    }

    protected void calculateQValue() {
        // set qValues to their negative values make them sorted in increasing order
        // this allows using Arrays.binarySearch later

        if (fdrValues == null) calculateFDR();

        qValues = new float[fdrValues.length];

        if (fdrValues.length == 0) return;

        qValues[0] = -fdrValues[0];
        for (int i = 1; i < fdrValues.length; i++) {
            if (fdrValues[i] > -qValues[i - 1]) qValues[i] = qValues[i - 1];
            else qValues[i] = -fdrValues[i];
        }
    }


    private int getNrHigherScores(float[] scores, float threshold){

        if (threshold<scores[0]) return scores.length;
        if (threshold>scores[scores.length-1]) return 0;

        int index = Arrays.binarySearch(scores,threshold);

        if (index>=0) {
            return scores.length-index;
        } else {
            int indexLeft = -index-2;
            if (indexLeft<0) {
                return scores.length;
            }
            int indexRight = -index-1;
            if (indexRight>scores.length-1) {
                return 0;
            }
            if (threshold-scores[indexLeft] < scores[indexRight]-threshold) return scores.length-indexLeft;
            else return scores.length-indexRight;
        }
    }

    public int getNrTargetScores(float score){

        return getNrHigherScores(targetScores,score);
    }

    public int getNrDecoyScores(float score){

        return getNrHigherScores(decoyScores,score);
    }

    public TFloatList getTargetScores() {

        return new TUnmodifiableFloatList(TFloatArrayList.wrap(targetScores));
    }

    public TFloatList getDecoyScores() {

        return new TUnmodifiableFloatList(TFloatArrayList.wrap(decoyScores));
    }

    protected abstract void calculateFDR();

    private void init() {

        targetScores = null;
        decoyScores = null;
        fdrValues = null;
        qValues = null;
    }

}
