/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.stats;

import java.text.NumberFormat;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public abstract class ROCTable {

    protected double[] thresholds;
    protected int[] truePositives;
    protected int[] falsePositives;
    protected int[] trueNegatives;
    protected int[] falseNegatives;

    public ROCTable(double min, double max, double thresholdIncrements) {

        int count = (int)((max - min)/thresholdIncrements) + 1;

        thresholds = new double[count];
        truePositives = new int[thresholds.length];
        falsePositives = new int[thresholds.length];
        trueNegatives = new int[thresholds.length];
        falseNegatives = new int[thresholds.length];

        double t = min;
        for (int i = 0; i < thresholds.length; i++) {

            thresholds[i] = t;
            t += thresholdIncrements;
        }
    }

    public abstract void add(double score, Classification classification);

    @Override
    public String toString() {

        StringBuilder buff = new StringBuilder();

        NumberFormat format = NumberFormat.getInstance();

        buff.append("threshold\ttp\tfp\ttn\tfn\terror\tfp rate\ttp rate\n");

        for (int i = 0; i < thresholds.length; i++) {

            int tp = truePositives[i];
            int fp = falsePositives[i];
            int tn = trueNegatives[i];
            int fn = falseNegatives[i];

            buff.append(format.format(thresholds[i]));
            buff.append('\t');
            buff.append(tp);
            buff.append('\t');
            buff.append(fp);
            buff.append('\t');
            buff.append(tn);
            buff.append('\t');
            buff.append(fn);
            buff.append('\t');
            buff.append((fp + fn) / (double) (tp + fp + tn + fn));
            buff.append('\t');
            buff.append(fp / (double) (fp + tn));
            buff.append('\t');
            buff.append(tp / (double) (tp + fn));
            buff.append('\n');
        }

        return buff.toString();
    }

    public int getTruePositive(int index) {

        return truePositives[index];
    }

    public int getTrueNegative(int index) {

        return trueNegatives[index];
    }

    public int getFalsePositive(int index) {

        return falsePositives[index];
    }

    public int getFalseNegative(int index) {

        return falseNegatives[index];
    }

    public double getThreshold(int index) {

        return thresholds[index];
    }

    public double getError(int index) {

        int tp = truePositives[index];
        int fp = falsePositives[index];
        int tn = trueNegatives[index];
        int fn = falseNegatives[index];

        return (fp + fn) / (double) (tp + fp + tn + fn);
    }

    public double getFalsePositiveRate(int index) {

        int fp = falsePositives[index];
        int tn = trueNegatives[index];

        return fp / (double) (fp + tn);
    }

    public double getTruePositiveRate(int index) {

        int tp = truePositives[index];
        int fn = falseNegatives[index];

        return tp / (double) (tp + fn);
    }

    public int size() {

        return thresholds.length;
    }
}
