/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.stats;


import com.google.common.base.Predicate;
import com.google.common.collect.Range;
import org.expasy.mzjava.utils.Copyable;

import java.util.Set;


/**
 * Histogram groups numbers into bins (class intervals) of same width.
 * It can represent standard frequency Histograms where each bin contain the number of occurrence of all values that fall into it
 * or can represent weighed Histograms where each bin contain the sum of weighted values that fall into it.
 *
 * @author nikitin
 * @version 1.0
 */
public interface Histogram extends Copyable<Histogram> {

    /**
     * Get its name
     */
    String getName();

    /**
     * Copy the bin structure only
     */
    Histogram shallowCopy();

    /**
     * Empty data
     */
    void clear();

    /**
     * Return true if no data
     */
    boolean isEmpty();

    /**
     * Get the range of defined values (first to last bin)
     */
    Range<Double> getRange();

    /**
     * Get the number of bins (n)
     */
    int size();

    /**
     * Get the bin width
     */
    double getBinWidth();

    /**
     * Add the data into this histogram if the given predicate is true
     */
    boolean addData(double value, double weight, Predicate<Double> predicate);

    /**
     * Add value contributing with weight
     */
    boolean addData(double value, double weight);

    /**
     * Add one occurrence of value
     */
    boolean addData(double value);

    /**
     * Add one occurrence of each value
     */
    void addData(double[] values);

    void addData(double[] values, double[] weights);

    /**
     * Add data at given bin
     */
    boolean addDataAtBin(double weight, int index);

    /**
     * Get the set of non-empty bins
     */
    Set<Integer> getNonEmptyBinIndices();

    /**
     * Get the bin index containing the given value (else must return -1)
     */
    int getBinIndex(double value);

    /**
     * Get the most intense bin index
     */
    int getMostIntenseBinIndex();

    /**
     * Get the absolute frequency at given bin
     */
    double getAbsoluteBinFreq(int index);

    double[] getAbsoluteBinFreqs(double[] dest);

    /**
     * Get the relative (normalized by the cumulative weights) weighted frequencies at given bin
     */
    double getRelativeBinFreq(int index);

    /**
     * Get the relative (normalized by the cumulative weights) weighted frequencies between bins
     */
    double getRelativeBinFreq(int fromIncluded, int toExcluded);

    /**
     * Get the counts of the cumulative number of observations in all bins
     */
    double getCumulFreqs();

    /**
     * Get the counts of the cumulative number of weighed observations in all of the bins in the specifed range
     */
    double getCumulFreqs(int fromIncluded, int toExcluded);

    /**
     * Get the n+1 cells boundaries
     */
    double[] getBreaks(double[] dest);

    /**
     * Get the n cell midpoints
     */
    double[] getMids(double[] dest);

    /**
     * Normalize histogram (weighted) frequencies
     */
    Histogram normalize(Normalization normalization);

    /**
     * Type of Histogram Normalization
     */
    public static class Normalization {

        public enum NormType {

            // each bin normalized on cumulated intensities
            // Normalizes an array to make it sum to a specified value. Returns
            // the result of the transformation x |-> x * normalizedSum / sum
            BINS_CUMUL,

            // set value to max bin -> transform other bins
            BIN_MAX
        }

        private final NormType normType;
        private final double normValue;

        public Normalization(NormType normType, double normValue) {

            this.normType = normType;
            this.normValue = normValue;
        }

        public NormType getType() {

            return normType;
        }

        public double getValue() {

            return normValue;
        }

        @Override
        public boolean equals(Object o) {

            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Normalization that = (Normalization) o;

            if (Double.compare(that.normValue, normValue) != 0) return false;
            if (normType != that.normType) return false;

            return true;
        }

        @Override
        public int hashCode() {

            int result;
            long temp;
            result = normType != null ? normType.hashCode() : 0;
            temp = normValue != +0.0d ? Double.doubleToLongBits(normValue) : 0L;
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }

        @Override
        public String toString() {

            //noinspection StringBufferReplaceableByString
            return new StringBuilder().append("Normalization{").append("normType=").append(normType).append(", normValue=").append(normValue).append('}').toString();
        }
    }
}
