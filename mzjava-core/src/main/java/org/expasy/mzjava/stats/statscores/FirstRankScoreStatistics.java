package org.expasy.mzjava.stats.statscores;

import gnu.trove.list.TDoubleList;
import gnu.trove.list.TFloatList;

/**
 * This class calculates FDRs and qValues for searches where each spectrum matches only one - either target or
 * decoy - entry. False matches to the target entries must have equal probability as matches to decoy entries.
 * This is for example approximately the case when using concatenated target decoy databases of the same size.
 *
 * @author Markus Muller
 * @version 0.0
 */
public class FirstRankScoreStatistics extends ScoreStatistics {

    public FirstRankScoreStatistics(float[] targetScores, float[] decoyScores) {
        super(targetScores, decoyScores);

        calculateFDR();
        calculateQValue();
    }

    public FirstRankScoreStatistics(float[] targetScores, float[] decoyScores, SortAction sortAction) {
        super(targetScores, decoyScores, sortAction);

        calculateFDR();
        calculateQValue();
    }

    public FirstRankScoreStatistics(double[] targetScores, double[] decoyScores) {
        super(targetScores, decoyScores);

        calculateFDR();
        calculateQValue();
    }

    public FirstRankScoreStatistics(double[] targetScores, double[] decoyScores, SortAction sortAction) {
        super(targetScores, decoyScores, sortAction);

        calculateFDR();
        calculateQValue();
    }

    public FirstRankScoreStatistics(TFloatList targetScores, TFloatList decoyScores) {
        super(targetScores, decoyScores);

        calculateFDR();
        calculateQValue();
    }

    public FirstRankScoreStatistics(TFloatList targetScores, TFloatList decoyScores, SortAction sortAction) {
        super(targetScores, decoyScores, sortAction);

        calculateFDR();
        calculateQValue();
    }

    public FirstRankScoreStatistics(TDoubleList targetScores, TDoubleList decoyScores) {
        super(targetScores, decoyScores);

        calculateFDR();
        calculateQValue();
    }

    public FirstRankScoreStatistics(TDoubleList targetScores, TDoubleList decoyScores, SortAction sortAction) {
        super(targetScores, decoyScores, sortAction);

        calculateFDR();
        calculateQValue();
    }


    @Override
    protected void calculateFDR() {

        if (targetScores==null || decoyScores==null) return;

        fdrValues = new float[targetScores.length];

        int iDecoy =  decoyScores.length-1;
        for (int i=targetScores.length-1;i>=0;i--) {
            float targetScore = targetScores[i];
            while (iDecoy>0 && decoyScores[iDecoy]>targetScore) iDecoy--;
            int nrTarget = targetScores.length-i;
            int nrDecoy = decoyScores.length-iDecoy-1;
            fdrValues[i] = (2f*nrDecoy)/(nrTarget+nrDecoy);
        }
    }

}
