package org.expasy.mzjava.core.ms.cluster;

import org.junit.Assert;
import org.junit.Test;

public class SimEdgeTest {

    @Test(expected = IllegalStateException.class)
    public void testLoop() throws Exception {

        new SimEdge<>("A1", "A1", 1.0);
    }

    @Test
    public void testGetOther() throws Exception {

        SimEdge<String> edge = new SimEdge<>("A1", "A2", 0.56);
        Assert.assertEquals("A1", edge.getOther("A2"));
        Assert.assertEquals("A2", edge.getOther("A1"));
    }

    @Test(expected = IllegalStateException.class)
    public void testGetOtherNotPresent() throws Exception {

        SimEdge<String> edge = new SimEdge<>("A1", "A2", 0.56);
        Assert.assertEquals("A1", edge.getOther("B1"));
    }

    @Test
    public void testEqualsAndHashCode() throws Exception {

        SimEdge<String> edge1 = new SimEdge<>("A1", "A2", 0.56);
        SimEdge<String> edge2 = new SimEdge<>("A2", "A1", 0.56);
        SimEdge<String> edge3 = new SimEdge<>("A1", "A2", 0.68);
        SimEdge<String> edge4 = new SimEdge<>("A1", "B1", 0.56);

        Assert.assertEquals(true, edge1.equals(edge2));
        Assert.assertEquals(true, edge2.equals(edge1));
        Assert.assertEquals(edge1.hashCode(), edge2.hashCode());

        Assert.assertEquals(false, edge1.equals(edge3));
        Assert.assertEquals(false, edge3.equals(edge1));

        Assert.assertEquals(false, edge1.equals(edge4));
        Assert.assertEquals(false, edge4.equals(edge1));
    }
}