/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.peaklist.peaktransformer;

import org.expasy.mzjava.core.ms.peaklist.MockPeakSink;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class UnitVectorNormalizerTest {

    @Test
    public void test() {

        UnitVectorNormalizer<PeakAnnotation> normalize = new UnitVectorNormalizer<PeakAnnotation>();
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();

        normalize.setSink(sink);

        runProcessor(normalize,
                new double[]{125.6, 458.6, 459.5, 178.2},
                new double[]{125, 457, 368, 2045});

        Assert.assertArrayEquals(new double[]{125.6, 458.6, 459.5, 178.2}, sink.getMzList(), 0.00001);
        Assert.assertArrayEquals(new double[]{0.0586530, 0.2144353, 0.1726744, 0.9595629}, sink.getIntensityList(), 0.00001);
    }

    @Test
    public void testEmpty() {

        UnitVectorNormalizer<PeakAnnotation> normalize = new UnitVectorNormalizer<PeakAnnotation>();
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();

        normalize.setSink(sink);

        runProcessor(normalize,
                new double[]{},
                new double[]{});

        Assert.assertArrayEquals(new double[0], sink.getMzList(), 0.00001);
        Assert.assertArrayEquals(new double[0], sink.getIntensityList(), 0.00001);

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getEndCalled());
    }

    private void runProcessor(UnitVectorNormalizer<PeakAnnotation> processor, double[] keys, double[] values) {

        processor.start(keys.length);

        for (int i = 0; i < keys.length; i++) {

            List<PeakAnnotation> objects = Collections.emptyList();
            processor.processPeak(keys[i], values[i], objects);
        }

        processor.end();
    }
}