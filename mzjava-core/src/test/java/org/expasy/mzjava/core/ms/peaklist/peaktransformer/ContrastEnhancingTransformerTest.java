package org.expasy.mzjava.core.ms.peaklist.peaktransformer;

import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Markus
 * @version 1.0
 */
public class ContrastEnhancingTransformerTest {
    double[] masses =
            new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 20, 21, 22, 23, 24, 25, 26};
    double[] intensities =
            new double[]{2, 4, 6, 8, 10, 1, 3, 5, 7, 9.5, 6.5, 8.5, 10.5, 1.5,
                    3.5, 5.5};

    double[] masses2 =
            new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
    double[] intensities2 =
            new double[]{0, 0, 0, 0, 10, 0, 0, 0, 0, 10, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0};

    @Test
    public void testMovingAvgTransformer() {

        ContrastEnhancingTransformer<PeakAnnotation> smoother = new ContrastEnhancingTransformer<PeakAnnotation>(2.3);
        DoublePeakList<PeakAnnotation> expPeakList = new DoublePeakList<PeakAnnotation>();
        expPeakList.addSorted(masses, intensities, masses.length);

        expPeakList.apply(smoother);

        double[] mvAvgIntensities =
                new double[]{2 - 4, -1, 0, 8 - 29.0 / 5, 10 - 28.0 / 5,
                        1 - 27.0 / 5, 3 - 26.0 / 5, 5 - 16.0 / 4, 7 - 15.0 / 3,
                        9.5 - 24.5 / 3, 6.5 - (9.5 + 6.5 + 8.5 + 10.5) / 4,
                        8.5 - (9.5 + 6.5 + 8.5 + 10.5 + 1.5) / 5,
                        10.5 - (6.5 + 8.5 + 10.5 + 1.5 + 3.5) / 5,
                        1.5 - (8.5 + 10.5 + 1.5 + 3.5 + 5.5) / 5,
                        3.5 - (10.5 + 1.5 + 3.5 + 5.5) / 4, 5.5 - (1.5 + 3.5 + 5.5) / 3};

        double[] smoothedInt = expPeakList.getIntensities(null);

        Assert.assertArrayEquals(mvAvgIntensities, smoothedInt, 0.0001);
    }

    @Test
    public void testMovingAvgTransformer2() {

        ContrastEnhancingTransformer<PeakAnnotation> smoother = new ContrastEnhancingTransformer<PeakAnnotation>(5.0);
        DoublePeakList<PeakAnnotation> expPeakList = new DoublePeakList<PeakAnnotation>();
        expPeakList.addSorted(masses2, intensities2, masses2.length);

        expPeakList.apply(smoother);

        double[] mvAvgIntensities =
                new double[]{-10.0 / 6, -10.0 / 7, -10.0 / 8, -10.0 / 9, 10 - 20.0 / 10, -20.0 / 11, -20.0 / 11, -20.0 / 11, -20.0 / 11, 10 - 30.0 / 11, -20.0 / 11,
                        -20.0 / 11, -20.0 / 11, -20.0 / 11, 10 - 20.0 / 11, -10.0 / 10, -10.0 / 9, -10.0 / 8, -10.0 / 7, -10.0 / 6};

        double[] smoothedInt = expPeakList.getIntensities(null);

        Assert.assertArrayEquals(mvAvgIntensities, smoothedInt, 0.0001);
    }
}
