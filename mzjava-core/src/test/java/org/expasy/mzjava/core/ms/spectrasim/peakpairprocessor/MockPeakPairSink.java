/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor;

import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TIntArrayList;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;

import java.util.ArrayList;
import java.util.List;

/**
* @author Oliver Horlacher
* @version 1.0
*/
public class MockPeakPairSink<A extends PeakAnnotation> implements PeakPairSink<A, A>, DefaultPeakListAligner.OverlapListener {

    private TDoubleArrayList centroids = new TDoubleArrayList();
    private TDoubleArrayList xValues = new TDoubleArrayList();
    private TDoubleArrayList yValues = new TDoubleArrayList();
    private List<List<? extends A>> xAnnotations = new ArrayList<List<? extends A>>();
    private List<List<? extends A>> yAnnotations = new ArrayList<List<? extends A>>();

    private TIntArrayList overlaps = new TIntArrayList();
    private boolean beginCalled = false;
    private boolean endCalled = false;

    @Override
    public void processPeakPair(double centroid, double xIntensity, double yIntensity, List<A> xAnnotations, List<A> yAnnotations) {

        centroids.add(centroid);
        xValues.add(xIntensity);
        yValues.add(yIntensity);
        this.xAnnotations.add(xAnnotations);
        this.yAnnotations.add(yAnnotations);

        if(!beginCalled) throw new IllegalStateException("processPeakPair called before begin was called");
    }

    public void overlap() {

        overlaps.add(centroids.size());
    }

    public double[] getCentroids() {

        return centroids.toArray();
    }

    public double[] getXvalues() {

        return xValues.toArray();
    }

    public double[] getYvalues() {

        return yValues.toArray();
    }

    public int[] getOverlaps() {

        return overlaps.toArray();
    }

    public List<List<? extends A>> getAnnotationsX() {

        return xAnnotations;
    }

    public List<List<? extends A>> getAnnotationsY() {

        return yAnnotations;
    }

    @Override
    public void begin(PeakList<A> xPeakList, PeakList<A> yPeakList) {

        beginCalled = true;
    }

    public void end() {

        endCalled = true;
    }

    public boolean wasBeginCalled() {

        return beginCalled;
    }

    public boolean wasEndCalled() {

        return endCalled;
    }
}
