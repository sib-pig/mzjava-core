/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor;

import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class DefaultPeakListAlignerTest {

    @Test
    public void testOverlap() {

        PeakList<PeakAnnotation> vX = createVector(
                new double[]{125.3},
                new double[]{100},
                new PeakAnnotation[]{createAnnotation(1)}
        );
        PeakList<PeakAnnotation> vY = createVector(
                new double[]{125.2, 125.4},
                new double[]{20, 200},
                new PeakAnnotation[]{createAnnotation(2), createAnnotation(3)});

        MockPeakPairSink<PeakAnnotation> callback = new MockPeakPairSink<PeakAnnotation>();
        DefaultPeakListAligner<PeakAnnotation, PeakAnnotation> vectorizer = new DefaultPeakListAligner<PeakAnnotation, PeakAnnotation>(new AbsoluteTolerance(0.1), callback);

        vectorizer.setOverlapListener(callback);

        vectorizer.setSink(callback);
        vectorizer.align(vX, vY);

        checkArray(callback.getCentroids(), 125.3, 125.4);
        checkArray(callback.getXvalues(), 100, 0);
        checkArray(callback.getYvalues(), 20, 200);
        
        checkAnnotations(callback.getAnnotationsX(), 1, -1);
        checkAnnotations(callback.getAnnotationsY(), 2, 3);

        checkArray(callback.getOverlaps(), 1);

        Assert.assertTrue(callback.wasBeginCalled());
        Assert.assertTrue(callback.wasEndCalled());
    }

    private PeakAnnotation createAnnotation(int charge) {

        PeakAnnotation mock = Mockito.mock(PeakAnnotation.class);
        when(mock.getCharge()).thenReturn(charge);
        return mock;
    }

    @Test
    public void testOverlap2() {

        PeakList<PeakAnnotation> vX = createVector(
                new double[]{125.3, 500.63},
                new double[]{100, 400},
                new PeakAnnotation[2]);
        PeakList<PeakAnnotation> vY = createVector(
                new double[]{125.2, 125.4},
                new double[]{20, 200},
                new PeakAnnotation[2]);

        DefaultPeakListAligner<PeakAnnotation, PeakAnnotation> vectorizer = new DefaultPeakListAligner<PeakAnnotation, PeakAnnotation>(new AbsoluteTolerance(0.1));

        MockPeakPairSink<PeakAnnotation> callback = new MockPeakPairSink<PeakAnnotation>();
        vectorizer.setOverlapListener(callback);

        vectorizer.setSink(callback);
        vectorizer.align(vX, vY);

        checkArray(callback.getCentroids(), 125.3, 125.4, 500.63);
        checkArray(callback.getXvalues(), 100, 0, 400);
        checkArray(callback.getYvalues(), 20, 200, 0);

        checkArray(callback.getOverlaps(), 1);

        Assert.assertTrue(callback.wasBeginCalled());
        Assert.assertTrue(callback.wasEndCalled());
    }

    @Test
    public void testDoubleOverlapX() {

        PeakList<PeakAnnotation> vX = createVector(
                new double[]{340.7588806152344},
                new double[]{10},
                new PeakAnnotation[1]);
        PeakList<PeakAnnotation> vY = createVector(
                new double[]{340.66839599609375, 340.742431640625},
                new double[]{10, 10},
                new PeakAnnotation[2]);

        DefaultPeakListAligner<PeakAnnotation, PeakAnnotation> vectorizer = new DefaultPeakListAligner<PeakAnnotation, PeakAnnotation>(new AbsoluteTolerance(0.1));

        MockPeakPairSink<PeakAnnotation> callback = new MockPeakPairSink<PeakAnnotation>();

        vectorizer.setSink(callback);
        vectorizer.align(vX, vY);

        checkArray(callback.getCentroids(), 340.66839599609375, 340.7588806152344);
        checkArray(callback.getXvalues(), 0, 10);
        checkArray(callback.getYvalues(), 10, 10);

        Assert.assertTrue(callback.wasBeginCalled());
        Assert.assertTrue(callback.wasEndCalled());
    }

    @Test
    public void testDoubleOverlapY() {

        PeakList<PeakAnnotation> vX = createVector(
                new double[]{340.66839599609375, 340.742431640625},
                new double[]{10, 10},
                new PeakAnnotation[2]);
        PeakList<PeakAnnotation> vY = createVector(
                new double[]{340.7588806152344},
                new double[]{10},
                new PeakAnnotation[1]);

        DefaultPeakListAligner<PeakAnnotation, PeakAnnotation> vectorizer = new DefaultPeakListAligner<PeakAnnotation, PeakAnnotation>(new AbsoluteTolerance(0.1));

        MockPeakPairSink<PeakAnnotation> callback = new MockPeakPairSink<PeakAnnotation>();

        vectorizer.setSink(callback);
        vectorizer.align(vX, vY);

        checkArray(callback.getCentroids(), 340.66839599609375, 340.742431640625);
        checkArray(callback.getXvalues(), 10, 10);
        checkArray(callback.getYvalues(), 0, 10);

        Assert.assertTrue(callback.wasBeginCalled());
        Assert.assertTrue(callback.wasEndCalled());
    }

    @Test
    public void testDoubleXY() {

        PeakList<PeakAnnotation> vX = createVector(
                new double[]{346.96, 347.17},
                new double[]{97.0, 31.0},
                new PeakAnnotation[2]);
        PeakList<PeakAnnotation> vY = createVector(
                new double[]{347.10, 347.22},
                new double[]{84.0, 93.0},
                new PeakAnnotation[2]);

        DefaultPeakListAligner<PeakAnnotation, PeakAnnotation> vectorizer = new DefaultPeakListAligner<PeakAnnotation, PeakAnnotation>(new AbsoluteTolerance(0.1));

        MockPeakPairSink<PeakAnnotation> callback = new MockPeakPairSink<PeakAnnotation>();

        vectorizer.setSink(callback);
        vectorizer.align(vX, vY);

        checkArray(callback.getCentroids(), 346.96, 347.1, 347.17);
        checkArray(callback.getXvalues(), 97, 0, 31);
        checkArray(callback.getYvalues(), 0, 84, 93);

        Assert.assertTrue(callback.wasBeginCalled());
        Assert.assertTrue(callback.wasEndCalled());
    }

    @Test
    public void testDoubleYX() {

        PeakList<PeakAnnotation> vX = createVector(
                new double[]{347.10, 347.22},
                new double[]{84.0, 93.0},
                new PeakAnnotation[2]);
        PeakList<PeakAnnotation> vY = createVector(
                new double[]{346.96, 347.17},
                new double[]{97.0, 31.0},
                new PeakAnnotation[2]);

        DefaultPeakListAligner<PeakAnnotation, PeakAnnotation> vectorizer = new DefaultPeakListAligner<PeakAnnotation, PeakAnnotation>(new AbsoluteTolerance(0.1));

        MockPeakPairSink<PeakAnnotation> callback = new MockPeakPairSink<PeakAnnotation>();

        vectorizer.setSink(callback);
        vectorizer.align(vX, vY);

        checkArray(callback.getCentroids(), 346.96, 347.1, 347.22);
        checkArray(callback.getXvalues(), 0, 84, 93);
        checkArray(callback.getYvalues(), 97, 0, 31);

        Assert.assertTrue(callback.wasBeginCalled());
        Assert.assertTrue(callback.wasEndCalled());
    }

    @Test
    public void testAlternate() {

        DefaultPeakListAligner<PeakAnnotation, PeakAnnotation> vectorizer = new DefaultPeakListAligner<PeakAnnotation, PeakAnnotation>(new AbsoluteTolerance(0.1));

        MockPeakPairSink<PeakAnnotation> callback = new MockPeakPairSink<PeakAnnotation>();
        vectorizer.setOverlapListener(callback);

        vectorizer.setSink(callback);
        vectorizer.align(
                createVector(new double[]{125.3, 158.63},
                        new double[]{100, 400},
                        new PeakAnnotation[2]),
                createVector(new double[]{512.25, 542.17},
                        new double[]{20, 200},
                        new PeakAnnotation[2]));

        checkArray(callback.getCentroids(), 125.3, 158.63, 512.25, 542.17);
        checkArray(callback.getXvalues(), 100, 400, 0, 0);
        checkArray(callback.getYvalues(), 0, 0, 20, 200);

        callback = new MockPeakPairSink<PeakAnnotation>();

        vectorizer.setOverlapListener(callback);
        vectorizer.setSink(callback);
        vectorizer.align(
                createVector(new double[]{512.25, 542.17},
                        new double[]{20, 200},
                        new PeakAnnotation[2]),
                createVector(new double[]{125.3, 158.63},
                        new double[]{100, 400},
                        new PeakAnnotation[2]));

        checkArray(callback.getCentroids(), 125.3, 158.63, 512.25, 542.17);
        checkArray(callback.getXvalues(), 0, 0, 20, 200);
        checkArray(callback.getYvalues(), 100, 400, 0, 0);

        Assert.assertTrue(callback.wasBeginCalled());
        Assert.assertTrue(callback.wasEndCalled());
    }

    private PeakList<PeakAnnotation> createVector(double[] centroids, double[] values, PeakAnnotation[] annotations) {

        PeakList<PeakAnnotation> peakList = new DoublePeakList<PeakAnnotation>();
        for (int i = 0; i < centroids.length; i++) {

            peakList.add(centroids[i], values[i]);
            PeakAnnotation annotation = annotations[i];

            if (annotation != null) {

                peakList.addAnnotation(i, annotation);
            }
        }

        return peakList;
    }

    private void checkArray(double[] actual, double... expected) {

        Assert.assertEquals("\nExpected: " + Arrays.toString(expected) +
                "\nActual  : " + Arrays.toString(actual),
                expected.length, actual.length);

        for (int i = 0; i < expected.length; i++) {

            Assert.assertEquals("\nExpected: " + Arrays.toString(expected) +
                    "\nActual  : " + Arrays.toString(actual)
                    , expected[i], actual[i], 0.0000001);
        }
    }

    private void checkArray(int[] actual, int... expected) {

        Assert.assertEquals("\nExpected: " + Arrays.toString(expected) +
                "\nActual  : " + Arrays.toString(actual),
                expected.length, actual.length);

        for (int i = 0; i < expected.length; i++) {

            Assert.assertEquals("\nExpected: " + Arrays.toString(expected) +
                    "\nActual  : " + Arrays.toString(actual)
                    , expected[i], actual[i]);
        }
    }
    
    private void checkAnnotations(List<List<? extends PeakAnnotation>> actual, int... expectedCharges) {

        Assert.assertEquals(expectedCharges.length, actual.size());

        for (int i = 0; i < expectedCharges.length; i++) {
            
            int expectedCharge = expectedCharges[i];
            List<? extends PeakAnnotation> annotations = actual.get(i);
            
            if(expectedCharge == -1) {
                
                Assert.assertEquals(0, annotations.size());
            } else {

                Assert.assertEquals(expectedCharge, annotations.get(0).getCharge());
            }
        }
    }

}
