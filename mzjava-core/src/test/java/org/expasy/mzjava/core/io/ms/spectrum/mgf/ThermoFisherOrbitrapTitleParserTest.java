/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.io.ms.spectrum.mgf;

import org.expasy.mzjava.core.ms.spectrum.RetentionTimeDiscrete;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeInterval;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeList;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberDiscrete;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberList;
import org.junit.Assert;
import org.junit.Test;
import org.openide.util.Lookup;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class ThermoFisherOrbitrapTitleParserTest {
    @Test
    public void testParseTitle() throws Exception {

        ThermoFisherOrbitrapTitleParser parser = new ThermoFisherOrbitrapTitleParser();

        String title = "msmsid:F000005,quan:0.000,start:0.026,end:0.026,survey:S000004,parent:458.700000,parent_area:103.5685,Qstart:0.026,Qend:0.026,AnalTime:111,Activation:cid";

        MsnSpectrum spectrum = new MsnSpectrum();

        boolean parsed = parser.parseTitle(title, spectrum);

        Assert.assertEquals(true, parsed);
        Assert.assertEquals(new ScanNumberList(new ScanNumberDiscrete(4)), spectrum.getScanNumbers());
        Assert.assertEquals(title, spectrum.getComment());
        Assert.assertEquals("cid", spectrum.getFragMethod());
        Assert.assertEquals(new RetentionTimeList(new RetentionTimeDiscrete(0.026, TimeUnit.HOUR)), spectrum.getRetentionTimes());
    }

    @Test
    public void testParseTitle2() throws Exception {

        ThermoFisherOrbitrapTitleParser parser = new ThermoFisherOrbitrapTitleParser();

        String title = "msmsid:F000005,quan:0.000,start:0.026,end:0.036,survey:S000004,parent:458.700000,parent_area:103.5685,Qstart:0.026,Qend:0.026,AnalTime:111,Activation:cid";

        MsnSpectrum spectrum = new MsnSpectrum();

        boolean parsed = parser.parseTitle(title, spectrum);

        Assert.assertEquals(true, parsed);
        Assert.assertEquals(new ScanNumberList(new ScanNumberDiscrete(4)), spectrum.getScanNumbers());
        Assert.assertEquals(title, spectrum.getComment());
        Assert.assertEquals("cid", spectrum.getFragMethod());
        Assert.assertEquals(new RetentionTimeList(new RetentionTimeInterval(0.026, 0.036, TimeUnit.HOUR)), spectrum.getRetentionTimes());
    }

    @Test
    public void testLookup() throws Exception {

        Lookup lookup = Lookup.getDefault();

        boolean found = false;
        for(TitleParser titleParsers : lookup.lookupAll(TitleParser.class)) {

            if(titleParsers instanceof ThermoFisherOrbitrapTitleParser) {

                found = true;
                break;
            }
        }

        Assert.assertEquals(true, found);
    }
}