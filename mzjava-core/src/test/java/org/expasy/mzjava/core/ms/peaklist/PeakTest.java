package org.expasy.mzjava.core.ms.peaklist;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author fnikitin
 * @author Oliver Horlacher
 */
public class PeakTest {

    @Test(expected = IllegalArgumentException.class)
	public void testBadConstrMzNeg() {

	    Peak.noIntensity(Double.NaN, 3);
	}

	@Test
	public void testConstrZeroCharge() {

		Peak p = Peak.noIntensity(189, 0);
		Assert.assertEquals(189, p.getMz(), 0.00);
	}

    @Test(expected = IllegalStateException.class)
    public void testMassForNoCharge() throws Exception {

        Peak peak = new Peak(463.87, 12);
        peak.getMass();
    }

    @Test
     public void testEquality() {

        Peak peak1 = Peak.noIntensity(189, 3);
        Peak peak2 = Peak.noIntensity(189, 3);

        Assert.assertEquals(0, peak1.compareTo(peak2));
        Assert.assertTrue(peak1.equals(peak2));
        Assert.assertEquals(peak1.hashCode(), peak2.hashCode());
    }

    @Test
    public void testEquality2() {

        Peak peak1 = Peak.noIntensity(189, -4);
        Peak peak2 = Peak.noIntensity(189, -3);

        Assert.assertEquals(1, peak1.compareTo(peak2));
        Assert.assertEquals(false, peak1.equals(peak2));
        Assert.assertNotEquals(peak1.hashCode(), peak2.hashCode());
    }

	@Test
	public void testCompChargeFirst() {

		Peak peak1 = Peak.noIntensity(189, 3);
		Peak peak2 = Peak.noIntensity(189, 2);

		Assert.assertTrue(peak1.compareTo(peak2) > 0);
	}

	@Test
	public void testCompMzSecond() {

		Peak peak1 = Peak.noIntensity(183, 2);
		Peak peak2 = Peak.noIntensity(189, 2);

		Assert.assertTrue(peak1.compareTo(peak2) < 0);
	}

	@Test
	public void testCompIntensityLast() {

		Peak peak1 = new Peak(189, 19, 2);
		Peak peak2 = new Peak(189, 29, 2);

		Assert.assertTrue(peak1.compareTo(peak2) < 0);
	}

    @Test
    public void testChargeList() throws Exception {

        final int[] constructorChargeList = {2, 3, 4};
        Peak peak = new Peak(67.9, 12.0, constructorChargeList);

        Assert.assertEquals(2, peak.getCharge());
        Assert.assertArrayEquals(new int[]{2, 3, 4}, peak.getChargeList());

        //Test that changing the charge array does not alter the charge list stored by the peak
        peak.getChargeList()[0] = 0;
        Assert.assertEquals(2, peak.getCharge());
        Assert.assertArrayEquals(new int[]{2, 3, 4}, peak.getChargeList());

        //Test that the array used during construction is copied
        constructorChargeList[0] = 0;
        Assert.assertEquals(2, peak.getCharge());
        Assert.assertArrayEquals(new int[]{2, 3, 4}, peak.getChargeList());
    }

    @Test (expected = IllegalArgumentException.class)
    public void testZeroCharge() throws Exception {

        new Peak(12.8, 4, 2, 0);
    }

    @Test
    public void testEmptyPeak() throws Exception {

        Peak peak = new Peak();

        Assert.assertEquals(0, peak.getMz(), 0.00000001);
        Assert.assertEquals(0, peak.getIntensity(), 0.00000001);
        Assert.assertEquals(0, peak.getCharge(), 0.00000001);
        Assert.assertArrayEquals(new int[]{}, peak.getChargeList());
        Assert.assertEquals(Polarity.UNKNOWN, peak.getPolarity());
    }

    @Test
    public void testCopyConstructor() throws Exception {

        Peak peak = new Peak(425.98, 34238.8, 2, -1, -9);
        Assert.assertEquals(425.98, peak.getMz(), 0.00000001);
        Assert.assertEquals(34238.8, peak.getIntensity(), 0.00000001);
        Assert.assertEquals(2, peak.getCharge(), 0.00000001);
        Assert.assertArrayEquals(new int[]{2, 1, 9}, peak.getChargeList());
        Assert.assertEquals(Polarity.POSITIVE, peak.getPolarity());
        Assert.assertEquals(851.96, peak.getMass(), 0.00000001);

        Peak copied = new Peak(peak);

        Assert.assertEquals(425.98, copied.getMz(), 0.00000001);
        Assert.assertEquals(34238.8, copied.getIntensity(), 0.00000001);
        Assert.assertEquals(2, copied.getCharge(), 0.00000001);
        Assert.assertArrayEquals(new int[]{2, 1, 9}, copied.getChargeList());
        Assert.assertEquals(Polarity.POSITIVE, copied.getPolarity());
        Assert.assertEquals(851.96, copied.getMass(), 0.00000001);
    }

    @Test
    public void testCopy() throws Exception {

        Peak peak = new Peak(425.98, 34238.8, 2, -1, -9);
        Assert.assertEquals(425.98, peak.getMz(), 0.00000001);
        Assert.assertEquals(34238.8, peak.getIntensity(), 0.00000001);
        Assert.assertEquals(2, peak.getCharge(), 0.00000001);
        Assert.assertArrayEquals(new int[]{2, 1, 9}, peak.getChargeList());
        Assert.assertEquals(Polarity.POSITIVE, peak.getPolarity());
        Assert.assertEquals(851.96, peak.getMass(), 0.00000001);

        Peak copied = peak.copy();

        Assert.assertEquals(425.98, copied.getMz(), 0.00000001);
        Assert.assertEquals(34238.8, copied.getIntensity(), 0.00000001);
        Assert.assertEquals(2, copied.getCharge(), 0.00000001);
        Assert.assertArrayEquals(new int[]{2, 1, 9}, copied.getChargeList());
        Assert.assertEquals(Polarity.POSITIVE, copied.getPolarity());
        Assert.assertEquals(851.96, copied.getMass(), 0.00000001);
    }

    @Test
    public void testSetIntensity() throws Exception {

        Peak peak = new Peak(451.9361, 12);
        Assert.assertEquals(12.0, peak.getIntensity(), 0.000000001);

        peak.setIntensity(45);
        Assert.assertEquals(45.0, peak.getIntensity(), 0.000000001);
    }
}
