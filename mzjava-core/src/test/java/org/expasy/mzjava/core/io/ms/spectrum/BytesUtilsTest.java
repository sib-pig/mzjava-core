package org.expasy.mzjava.core.io.ms.spectrum;

import org.expasy.mzjava.core.io.ms.spectrum.BytesUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author fnikitin
 *         Date: 3/4/14
 */
public class BytesUtilsTest {

    @Test
    public void testQuadToFloat() {

        float f = BytesUtils.quadToFloat((byte) 68, (byte) 50, (byte) -56, (byte) -10);

        Assert.assertEquals(715.14, f, 0.0001);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testQuadToFloatFewerBytes() {

        BytesUtils.quadToFloat((byte) 68, (byte) 50, (byte) -56);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testQuadToFloatMoreBytes() {

        BytesUtils.quadToFloat((byte) 68, (byte) 50, (byte) -56, (byte) -10, (byte) -10);
    }

    @Test
    public void testFloatToQuad() {

        byte[] expectedBytes= new byte[]{68, 50, -56, -10};

        byte[] quad = BytesUtils.floatToQuad(715.14f);

        Assert.assertEquals(4, quad.length);

        for (int i=0 ; i<4; i++) {

            Assert.assertEquals(expectedBytes[i], quad[i]);
        }
    }

    @Test
    public void testOctetToDouble() {

        double d = BytesUtils.octetToDouble((byte) 64, (byte) -122, (byte) 89, (byte) 30, (byte) -72, (byte) 81, (byte) -21, (byte) -123);

        Assert.assertEquals(715.14, d, 0.00000001);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testOctetToDoubleFewerBytes() {

        BytesUtils.octetToDouble((byte) 64, (byte) -122, (byte) 89, (byte) 30, (byte) -72, (byte) 81, (byte) -21);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testOctetToDoubleMoreBytes() {

        BytesUtils.octetToDouble((byte) 64, (byte) -122, (byte) 89, (byte) 30, (byte) -72, (byte) 81, (byte) -21, (byte) -123, (byte) -123);
    }

    @Test
    public void testDoubleToOctet() {

        byte[] expectedBytes = new byte[]{64, -122, 89, 30, -72, 81, -21, -123};

        byte[] octet = BytesUtils.doubleToOctet(715.14);

        Assert.assertEquals(8, octet.length);

        for (int i=0 ; i<8; i++) {

            Assert.assertEquals(expectedBytes[i], octet[i]);
        }
    }

    @Test
    public void testQuadsToFloats() throws Exception {

        float[] expected = new float[] {1553.31f, 656.35f, 1588.40f, 667.56f, 1601.79f, 543.23f, 1659.21f, 614.62f, 1709.63f, 715.14f};

        byte[] bytes = new byte[40];

        for (int i=0 ; i<expected.length ; i++) {
            byte[] tmp = BytesUtils.floatToQuad(expected[i]);

            bytes[4*i] = tmp[0];
            bytes[4*i+1] = tmp[1];
            bytes[4*i+2] = tmp[2];
            bytes[4*i+3] = tmp[3];
        }
        float[] floats = BytesUtils.quadsToFloats(bytes, null);

        Assert.assertArrayEquals(expected, floats, 0.01f);
    }

    @Test
    public void testQuadsToDoubles() throws Exception {

        double[] expected = new double[] {1553.31, 656.35, 1588.40, 667.56, 1601.79, 543.23, 1659.21, 614.62, 1709.63, 715.14};

        byte[] bytes = new byte[40];

        for (int i=0 ; i<expected.length ; i++) {
            byte[] tmp = BytesUtils.floatToQuad((float) expected[i]);

            bytes[4*i] = tmp[0];
            bytes[4*i+1] = tmp[1];
            bytes[4*i+2] = tmp[2];
            bytes[4*i+3] = tmp[3];
        }
        double[] doubles = BytesUtils.quadsToDoubles(bytes, null);

        Assert.assertArrayEquals(expected, doubles, 0.01);
    }

    @Test
    public void testFloatsToQuads() {

        byte[] expected = new byte[] {68, -62, 41, -20, 68, 36, 22, 102, 68, -58, -116, -51, 68, 38, -29, -41, 68, -56, 57, 72, 68, 7, -50, -72, 68, -49, 102, -72, 68, 25, -89, -82, 68, -43, -76, 41, 68, 50, -56, -10};
        float[] floats = new float[] {1553.31f, 656.35f, 1588.40f, 667.56f, 1601.79f, 543.23f, 1659.21f, 614.62f, 1709.63f, 715.14f};

        byte[] bytes = BytesUtils.floatsToQuads(floats);

        Assert.assertArrayEquals(expected, bytes);
    }

    @Test
    public void testOctetsToDoubles() throws Exception {

        double[] expected = new double[] {1553.31, 656.35, 1588.40, 667.56, 1601.79, 543.23, 1659.21, 614.62, 1709.63, 715.14};

        byte[] bytes = new byte[80];

        for (int i=0 ; i<expected.length ; i++) {
            byte[] tmp = BytesUtils.doubleToOctet(expected[i]);

            bytes[8*i] = tmp[0];
            bytes[8*i+1] = tmp[1];
            bytes[8*i+2] = tmp[2];
            bytes[8*i+3] = tmp[3];
            bytes[8*i+4] = tmp[4];
            bytes[8*i+5] = tmp[5];
            bytes[8*i+6] = tmp[6];
            bytes[8*i+7] = tmp[7];
        }
        double[] doubles = BytesUtils.octetsToDoubles(bytes, null);

        Assert.assertArrayEquals(expected, doubles, 0.01);
    }

    @Test
    public void testDoublesToOctets() {

        byte[] expected = new byte[] {64, -104, 69, 61, 112, -93, -41, 10, 64, -124, -126, -52, -52, -52, -52, -51, 64, -104, -47, -103, -103, -103, -103, -102, 64, -124, -36, 122, -31, 71, -82, 20, 64, -103, 7, 40, -11, -62, -113, 92, 64, -128, -7, -41, 10, 61, 112, -92, 64, -103, -20, -41, 10, 61, 112, -92, 64, -125, 52, -11, -62, -113, 92, 41, 64, -102, -74, -123, 30, -72, 81, -20, 64, -122, 89, 30, -72, 81, -21, -123};
        double[] doubles = new double[] {1553.31, 656.35, 1588.40, 667.56, 1601.79, 543.23, 1659.21, 614.62, 1709.63, 715.14};

        byte[] bytes = BytesUtils.doublesToOctets(doubles);

        Assert.assertArrayEquals(expected, bytes);
    }

    @Test
    public void testUncompress() throws Exception {

        int peakNumber = 32;
        int compressedLen = 267;
        int precision = 32;

        /*
        // OR20070924_S_mix7_02.mzXML
        // <scan num="15"
        //  peaksCount="32"
        // <peaks
        //  precision=32
        //  compressionType="zlib"
        //  compressedLen="267"
        String code = "eJwBAAH//kOaby1Eb9CEQ5pwYkQaAnZDoDj3RA" +
                "0GHEOgqp5EKTdHQ6g5PkP9P+RDr9e7RA0L5EPCm5ZD/916" +
                "Q+wblkPtCnlD9deHRESfh0QAgM9D+ppzRBHzVEQDCxdELrBb" +
                "RA3cj0QvJ+tEAUaMRDEBZUR3dg5ETcdMRA64tkRN/3VEFdN8RGU" +
                "5bUQHuTZEdXyeRAkhIESFoblEA3BKRIYdK0P0UXlEiHzkRCISIkSKqU" +
                "9EF25nRJBJt0QXsUBEkVkhRAAzKUSXDJtEA9MtRJ4ymEQZlutEo3H3RBpa" +
                "METCKf9EJBbrRMaM40Qm5DdEyDlmRAfO3ETPZr1EGagTRNW0SkQyyWyB3WZt";

        // base 64 -> bytes
        byte[] bytes = Base64Utils.decode(code);
        */
        byte[] expected = new byte[] {67, -102, 111, 45, 68, 111, -48, -124, 67, -102, 112, 98, 68, 26, 2, 118, 67, -96, 56, -9, 68, 13, 6, 28, 67, -96, -86, -98, 68, 41, 55, 71, 67, -88, 57, 62, 67, -3, 63, -28, 67, -81, -41, -69, 68, 13, 11, -28, 67, -62, -101, -106, 67, -1, -35, 122, 67, -20, 27, -106, 67, -19, 10, 121, 67, -11, -41, -121, 68, 68, -97, -121, 68, 0, -128, -49, 67, -6, -102, 115, 68, 17, -13, 84, 68, 3, 11, 23, 68, 46, -80, 91, 68, 13, -36, -113, 68, 47, 39, -21, 68, 1, 70, -116, 68, 49, 1, 101, 68, 119, 118, 14, 68, 77, -57, 76, 68, 14, -72, -74, 68, 77, -1, 117, 68, 21, -45, 124, 68, 101, 57, 109, 68, 7, -71, 54, 68, 117, 124, -98, 68, 9, 33, 32, 68, -123, -95, -71, 68, 3, 112, 74, 68, -122, 29, 43, 67, -12, 81, 121, 68, -120, 124, -28, 68, 34, 18, 34, 68, -118, -87, 79, 68, 23, 110, 103, 68, -112, 73, -73, 68, 23, -79, 64, 68, -111, 89, 33, 68, 0, 51, 41, 68, -105, 12, -101, 68, 3, -45, 45, 68, -98, 50, -104, 68, 25, -106, -21, 68, -93, 113, -9, 68, 26, 90, 48, 68, -62, 41, -1, 68, 36, 22, -21, 68, -58, -116, -29, 68, 38, -28, 55, 68, -56, 57, 102, 68, 7, -50, -36, 68, -49, 102, -67, 68, 25, -88, 19, 68, -43, -76, 74, 68, 50, -55, 108};

        byte[] bytes = new byte[]{120, -100, 1, 0, 1, -1, -2, 67, -102, 111, 45, 68, 111, -48, -124, 67, -102, 112, 98, 68, 26, 2, 118, 67, -96, 56, -9, 68, 13, 6, 28, 67, -96, -86, -98, 68, 41, 55, 71, 67, -88, 57, 62, 67, -3, 63, -28, 67, -81, -41, -69, 68, 13, 11, -28, 67, -62, -101, -106, 67, -1, -35, 122, 67, -20, 27, -106, 67, -19, 10, 121, 67, -11, -41, -121, 68, 68, -97, -121, 68, 0, -128, -49, 67, -6, -102, 115, 68, 17, -13, 84, 68, 3, 11, 23, 68, 46, -80, 91, 68, 13, -36, -113, 68, 47, 39, -21, 68, 1, 70, -116, 68, 49, 1, 101, 68, 119, 118, 14, 68, 77, -57, 76, 68, 14, -72, -74, 68, 77, -1, 117, 68, 21, -45, 124, 68, 101, 57, 109, 68, 7, -71, 54, 68, 117, 124, -98, 68, 9, 33, 32, 68, -123, -95, -71, 68, 3, 112, 74, 68, -122, 29, 43, 67, -12, 81, 121, 68, -120, 124, -28, 68, 34, 18, 34, 68, -118, -87, 79, 68, 23, 110, 103, 68, -112, 73, -73, 68, 23, -79, 64, 68, -111, 89, 33, 68, 0, 51, 41, 68, -105, 12, -101, 68, 3, -45, 45, 68, -98, 50, -104, 68, 25, -106, -21, 68, -93, 113, -9, 68, 26, 90, 48, 68, -62, 41, -1, 68, 36, 22, -21, 68, -58, -116, -29, 68, 38, -28, 55, 68, -56, 57, 102, 68, 7, -50, -36, 68, -49, 102, -67, 68, 25, -88, 19, 68, -43, -76, 74, 68, 50, -55, 108, -127, -35, 102, 109};

        // 2 values per peak, precision/8 bytes per value (256 here)
        int uncompressedLen = (peakNumber * precision/4);

        // uncompression
        bytes = BytesUtils.uncompress(bytes, compressedLen, uncompressedLen);

        Assert.assertArrayEquals(expected, bytes);
    }

    @Test
    public void testCompress() throws Exception {

        byte[] expected = new byte[]{120, -100, 1, 0, 1, -1, -2, 67, -102, 111, 45, 68, 111, -48, -124, 67, -102, 112, 98, 68, 26, 2, 118, 67, -96, 56, -9, 68, 13, 6, 28, 67, -96, -86, -98, 68, 41, 55, 71, 67, -88, 57, 62, 67, -3, 63, -28, 67, -81, -41, -69, 68, 13, 11, -28, 67, -62, -101, -106, 67, -1, -35, 122, 67, -20, 27, -106, 67, -19, 10, 121, 67, -11, -41, -121, 68, 68, -97, -121, 68, 0, -128, -49, 67, -6, -102, 115, 68, 17, -13, 84, 68, 3, 11, 23, 68, 46, -80, 91, 68, 13, -36, -113, 68, 47, 39, -21, 68, 1, 70, -116, 68, 49, 1, 101, 68, 119, 118, 14, 68, 77, -57, 76, 68, 14, -72, -74, 68, 77, -1, 117, 68, 21, -45, 124, 68, 101, 57, 109, 68, 7, -71, 54, 68, 117, 124, -98, 68, 9, 33, 32, 68, -123, -95, -71, 68, 3, 112, 74, 68, -122, 29, 43, 67, -12, 81, 121, 68, -120, 124, -28, 68, 34, 18, 34, 68, -118, -87, 79, 68, 23, 110, 103, 68, -112, 73, -73, 68, 23, -79, 64, 68, -111, 89, 33, 68, 0, 51, 41, 68, -105, 12, -101, 68, 3, -45, 45, 68, -98, 50, -104, 68, 25, -106, -21, 68, -93, 113, -9, 68, 26, 90, 48, 68, -62, 41, -1, 68, 36, 22, -21, 68, -58, -116, -29, 68, 38, -28, 55, 68, -56, 57, 102, 68, 7, -50, -36, 68, -49, 102, -67, 68, 25, -88, 19, 68, -43, -76, 74, 68, 50, -55, 108, -127, -35, 102, 109};

        byte[] bytes = new byte[] {67, -102, 111, 45, 68, 111, -48, -124, 67, -102, 112, 98, 68, 26, 2, 118, 67, -96, 56, -9, 68, 13, 6, 28, 67, -96, -86, -98, 68, 41, 55, 71, 67, -88, 57, 62, 67, -3, 63, -28, 67, -81, -41, -69, 68, 13, 11, -28, 67, -62, -101, -106, 67, -1, -35, 122, 67, -20, 27, -106, 67, -19, 10, 121, 67, -11, -41, -121, 68, 68, -97, -121, 68, 0, -128, -49, 67, -6, -102, 115, 68, 17, -13, 84, 68, 3, 11, 23, 68, 46, -80, 91, 68, 13, -36, -113, 68, 47, 39, -21, 68, 1, 70, -116, 68, 49, 1, 101, 68, 119, 118, 14, 68, 77, -57, 76, 68, 14, -72, -74, 68, 77, -1, 117, 68, 21, -45, 124, 68, 101, 57, 109, 68, 7, -71, 54, 68, 117, 124, -98, 68, 9, 33, 32, 68, -123, -95, -71, 68, 3, 112, 74, 68, -122, 29, 43, 67, -12, 81, 121, 68, -120, 124, -28, 68, 34, 18, 34, 68, -118, -87, 79, 68, 23, 110, 103, 68, -112, 73, -73, 68, 23, -79, 64, 68, -111, 89, 33, 68, 0, 51, 41, 68, -105, 12, -101, 68, 3, -45, 45, 68, -98, 50, -104, 68, 25, -106, -21, 68, -93, 113, -9, 68, 26, 90, 48, 68, -62, 41, -1, 68, 36, 22, -21, 68, -58, -116, -29, 68, 38, -28, 55, 68, -56, 57, 102, 68, 7, -50, -36, 68, -49, 102, -67, 68, 25, -88, 19, 68, -43, -76, 74, 68, 50, -55, 108};

        // uncompression
        bytes = BytesUtils.compress(bytes);

        Assert.assertArrayEquals(expected, bytes);
    }
}
