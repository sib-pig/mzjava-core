/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.io.ms.spectrum.mgf;

import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class AnalystTitleParserTest {

    @Test
    public void test() throws Exception {

        AnalystTitleParser titleParser = new AnalystTitleParser();

        MsnSpectrum spectrum = new MsnSpectrum();

        boolean parsed = titleParser.parseTitle("File: 4-way iTRAQ Jun07 100mM.wiff, Sample: 4-way iTRAQ Jun07 100mM (sample number 1), Elution: 7.98 min, Period: 1, Cycle(s): 439 (Experiment 2) (Charge not auto determined)", spectrum);

        Assert.assertEquals(true, parsed);
        Assert.assertEquals(478.8, spectrum.getRetentionTimes().get(0).getTime(), 0.01);
    }

    @Test
    public void test2() {

        AnalystTitleParser titleParser = new AnalystTitleParser();

        MsnSpectrum spectrum = new MsnSpectrum();

        boolean parsed = titleParser.parseTitle("Cmpd 11, +MSn(786.25) 16.0 min", spectrum);
        Assert.assertEquals(false, parsed);
        Assert.assertEquals(0, spectrum.getRetentionTimes().size());
    }
}
