/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.peaklist;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeakListMergerTest {

    @Test
    public void testMergeOne() throws Exception {

        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<>();

        PeakListMerger<PeakAnnotation> merger = new PeakListMerger<>();
        merger.setSink(sink);

        PeakList<PeakAnnotation> list1 = new FloatPeakList<>();
        list1.add(1, 1);
        list1.add(3, 1);
        list1.add(5, 1);
        list1.add(6, 1);

        //Generics array
        //noinspection unchecked
        merger.merge(Arrays.asList(list1));

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getEndCalled());

        Assert.assertArrayEquals(new double[]{1, 3, 5, 6}, sink.getMzList(), 0.0001);
    }

    @Test
    public void testMerge() throws Exception {

        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<>();

        PeakListMerger<PeakAnnotation> merger = new PeakListMerger<>();
        merger.setSink(sink);

        PeakList<PeakAnnotation> list1 = new FloatPeakList<>();
        list1.add(1, 1);
        list1.add(3, 1);
        list1.add(5, 1);
        list1.add(6, 1);

        PeakList<PeakAnnotation> list2 = new FloatPeakList<>();
        list2.add(2, 2);
        list2.add(4, 2);
        list2.add(6, 2);

        //Generics array
        //noinspection unchecked
        merger.merge(Arrays.asList(list2, list1));

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getEndCalled());

        Assert.assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6, 6}, sink.getMzList(), 0.0001);
    }

    @Test
    public void testMerge2() throws Exception {

        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<>();

        PeakListMerger<PeakAnnotation> merger = new PeakListMerger<>();
        merger.setSink(sink);

        PeakList<PeakAnnotation> list1 = new FloatPeakList<>();
        list1.add(1, 1);
        list1.add(3, 1);

        PeakList<PeakAnnotation> list2 = new FloatPeakList<>();
        list2.add(4, 2);
        list2.add(6, 2);

        //Generics array
        //noinspection unchecked
        merger.merge(Arrays.asList(list2, list1));

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getEndCalled());

        Assert.assertArrayEquals(new double[]{1, 3, 4, 6}, sink.getMzList(), 0.0001);
    }

    @Test
    public void testMergeRandom() throws Exception {

        double[] mzs = new double[5000];

        Random random = new Random();

        for (int i = 0; i < mzs.length; i++) {

            mzs[i] = random.nextDouble() * 1500;
        }

        Arrays.sort(mzs);

        int partitions = 50;

        List<PeakList<PeakAnnotation>> spectra = new ArrayList<>(partitions);

        for(int i = 0; i < partitions; i++) {

            spectra.add(new DoubleConstantPeakList<>(i + 1));
        }

        for (double mz : mzs) {

            int spectrum = random.nextInt(partitions);

            spectra.get(spectrum).add(mz, 1);
        }

        for(PeakList peakList : spectra) {

            peakList.trimToSize();
        }

        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<>();

        PeakListMerger<PeakAnnotation> merger = new PeakListMerger<>();
        merger.setSink(sink);

        merger.merge(spectra);

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getEndCalled());

        Assert.assertArrayEquals(mzs, sink.getMzList(), 0.00000001);
    }
}
