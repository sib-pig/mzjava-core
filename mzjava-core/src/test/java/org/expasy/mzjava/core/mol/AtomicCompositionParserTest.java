/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.mol;

import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import org.junit.Assert;
import org.junit.Test;

public class AtomicCompositionParserTest {

    AtomicCompositionParser parser = AtomicCompositionParser.getInstance();

    // Compare mass against this site:
    // http://www.bmrb.wisc.edu/metabolomics/mol_mass.php
    // http://en.wikipedia.org/wiki/Chemical_file_format
    @Test
    public void parseNBuild() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();
        int charge = parser.parse("C[13]2H-3ON-1(2+)", atomCounterMap);

        Assert.assertEquals(2, charge);
        Assert.assertEquals(4, atomCounterMap.size());
        Assert.assertEquals(2, atomCounterMap.get(PeriodicTable.getInstance().getAtom(AtomicSymbol.C, 13)));
        Assert.assertEquals(-3, atomCounterMap.get(PeriodicTable.H));
        Assert.assertEquals(1, atomCounterMap.get(PeriodicTable.O));
        Assert.assertEquals(-1, atomCounterMap.get(PeriodicTable.N));
    }

    @Test
    public void parseNBuild2() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();
        int charge = parser.parse("NH3", atomCounterMap);

        Assert.assertEquals(0, charge);
        Assert.assertEquals(2, atomCounterMap.size());
        Assert.assertEquals(3, atomCounterMap.get(PeriodicTable.H));
        Assert.assertEquals(1, atomCounterMap.get(PeriodicTable.N));

        charge = parser.parse("N[15]H3", atomCounterMap);

        Assert.assertEquals(0, charge);
        Assert.assertEquals(2, atomCounterMap.size());
        Assert.assertEquals(1, atomCounterMap.get(PeriodicTable.getInstance().getAtom(AtomicSymbol.N, 15)));
        Assert.assertEquals(3, atomCounterMap.get(PeriodicTable.H));
    }

    @Test
    public void parseNBuildFormulaWithInternalRepetition() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();
        int charge = parser.parse("PO4H2(CH2)12CH3", atomCounterMap);
        Assert.assertEquals(0, charge);
        Assert.assertEquals(4, atomCounterMap.size());
        Assert.assertEquals(1, atomCounterMap.get(PeriodicTable.P));
        Assert.assertEquals(13, atomCounterMap.get(PeriodicTable.C));
        Assert.assertEquals(29, atomCounterMap.get(PeriodicTable.H));
        Assert.assertEquals(4, atomCounterMap.get(PeriodicTable.O));

        charge = parser.parse("PO4H2(C[13]H2)12C[13]H3", atomCounterMap);
        Assert.assertEquals(0, charge);
        Assert.assertEquals(4, atomCounterMap.size());
        Assert.assertEquals(1, atomCounterMap.get(PeriodicTable.P));
        Assert.assertEquals(13, atomCounterMap.get(PeriodicTable.getInstance().getAtom(AtomicSymbol.C, 13)));
        Assert.assertEquals(29, atomCounterMap.get(PeriodicTable.H));
        Assert.assertEquals(4, atomCounterMap.get(PeriodicTable.O));
    }

    @Test
    public void parseSimpleFormula() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();
        int charge =  parser.parse("H3O(+)",atomCounterMap);

        Assert.assertEquals(1, charge);
        Assert.assertEquals(2, atomCounterMap.size());
        Assert.assertEquals(3, atomCounterMap.get(PeriodicTable.H));
        Assert.assertEquals(1, atomCounterMap.get(PeriodicTable.O));
    }

    @Test
    public void parseSimpleParticle() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();
        int charge = parser.parse("H", atomCounterMap);
        Assert.assertEquals(0, charge);
        Assert.assertEquals(1, atomCounterMap.size());
        Assert.assertEquals(1, atomCounterMap.get(PeriodicTable.H));

        charge = parser.parse("H(+)", atomCounterMap); // H-(e-)
        Assert.assertEquals(1, charge);
        Assert.assertEquals(1, atomCounterMap.size());
        Assert.assertEquals(1, atomCounterMap.get(PeriodicTable.H));

        charge  = parser.parse("H(-)", atomCounterMap);// H+(e-)
        Assert.assertEquals(-1, charge);
        Assert.assertEquals(1, atomCounterMap.size());
        Assert.assertEquals(1, atomCounterMap.get(PeriodicTable.H));
    }

    @Test
    public void parseSimpleParticle2() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();
        int charge = parser.parse("H-1", atomCounterMap);
        Assert.assertEquals(0, charge);
        Assert.assertEquals(1, atomCounterMap.size());
        Assert.assertEquals(-1, atomCounterMap.get(PeriodicTable.H));
    }

    @Test
    public void parseSimpleParticle3() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();
        int charge = parser.parse("H+1", atomCounterMap);
        Assert.assertEquals(0, charge);
        Assert.assertEquals(1, atomCounterMap.size());
        Assert.assertEquals(1, atomCounterMap.get(PeriodicTable.H));
    }

    @Test (expected = IllegalArgumentException.class)
    public void parseSimpleParticleBadAtomCountFormat() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();
        parser.parse("H++1", atomCounterMap);
    }

    @Test
    public void parseSimpleEmptyParticle() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();
        int charge = parser.parse("", atomCounterMap);

        Assert.assertEquals(0, charge);
        Assert.assertEquals(0, atomCounterMap.size());
    }

    @Test
    public void parseGroup1() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();
        int charge = parser.parse("(CH2)", atomCounterMap);

        Assert.assertEquals(0, charge);
        Assert.assertEquals(2, atomCounterMap.size());
        Assert.assertEquals(1, atomCounterMap.get(PeriodicTable.C));
        Assert.assertEquals(2, atomCounterMap.get(PeriodicTable.H));
    }

    @Test
    public void parseGroup() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();
        int charge = parser.parse("(CH2)10", atomCounterMap);

        Assert.assertEquals(0, charge);
        Assert.assertEquals(2, atomCounterMap.size());
        Assert.assertEquals(10, atomCounterMap.get(PeriodicTable.C));
        Assert.assertEquals(20, atomCounterMap.get(PeriodicTable.H));
    }

    @Test
    public void parseGroupSigned() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();
        int charge = parser.parse("(CH2)+10", atomCounterMap);

        Assert.assertEquals(0, charge);
        Assert.assertEquals(2, atomCounterMap.size());
        Assert.assertEquals(10, atomCounterMap.get(PeriodicTable.C));
        Assert.assertEquals(20, atomCounterMap.get(PeriodicTable.H));
    }

    @Test
    public void parseGroupNeg() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();
        int charge = parser.parse("(CH2)-10", atomCounterMap);

        Assert.assertEquals(0, charge);
        Assert.assertEquals(2, atomCounterMap.size());
        Assert.assertEquals(-10, atomCounterMap.get(PeriodicTable.C));
        Assert.assertEquals(-20, atomCounterMap.get(PeriodicTable.H));
    }

    @Test
    public void parseGroupTricky() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();
        int charge = parser.parse("(C-1H-2)-10", atomCounterMap);

        Assert.assertEquals(0, charge);
        Assert.assertEquals(2, atomCounterMap.size());
        Assert.assertEquals(10, atomCounterMap.get(PeriodicTable.C));
        Assert.assertEquals(20, atomCounterMap.get(PeriodicTable.H));
    }

    @Test
    public void parseMinusProton() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();
        int charge = parser.parse("H-1(-)", atomCounterMap);

        Assert.assertEquals(-1, charge);
        Assert.assertEquals(1, atomCounterMap.size());
        Assert.assertEquals(-1, atomCounterMap.get(PeriodicTable.H));
    }
}
