/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.peaklist.peakfilter;

import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.IdentityPeakProcessor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

/**
 * @author mmueller
 * @version 1.0
 */
public class NPeaksPerSlidingWindowFilterTest {

    double[] masses =
            new double[] {1, 2, 3, 4.6,  5, 6, 7, 8, 9, 10, 10.3, 11.5, 13, 14, 15.7, 16};
    double[] intensities =
            new double[] {2, 4, 6,   8, 10, 1, 3, 5, 8,  9,    6,    8, 10,  1,    3,  5};

    double[] masses2 =
            new double[] { 995.465, 997.549,1002.213,1009.445,1012.809,1015.269,1023.958,1024.746,1032.158,1038.543,
                          1049.680,1055.237,1056.236,1061.347,1063.269,1069.886,1074.268,1075.122,1080.235,1088.610,
                          1094.540,1096.541,1102.804,1107.303,1112.495,1117.861,1120.826,1125.643,1129.048,1136.221,
                          1138.760,1141.752,1149.159,1154.643,1167.231,1168.390,1172.392,1177.803,1185.206,1191.259,
                          1192.455,1199.366,1209.212,1210.209,1216.670};
    double[] intensities2 =
            new double[] {  18.288,  10.924,   6.442,  17.300,  17.788,  18.947,  21.898,  11.378,  22.440,  10.302,
                            50.827,  17.054,  22.280,  11.892,   7.995,  10.341, 215.683,  88.473,  18.228,  13.208,
                             3.964,   5.049,   2.980,   1.780,  13.860,   5.808,   5.583,   2.364,   1.033,  13.762,
                            21.549,   5.541,  14.566,  19.965, 239.495,  40.558,  21.821,   9.397,1820.518, 630.056,
            	            23.832,   3.843,   6.159, 383.467,  58.726};

    PeakList<PeakAnnotation> expPeakList,expPeakList2, newPL;
    NPeaksPerSlidingWindowFilter<PeakAnnotation> perSlidingWindowFilter1Sliding, perSlidingWindowFilter2Sliding, perSlidingWindowFilter3Sliding, perSlidingWindowFilter4Sliding,
		    perSlidingWindowFilter5Sliding, perSlidingWindowFilter6Sliding, perSlidingWindowFilter7Sliding;

    @Before
    public void init() {

        newPL = new DoublePeakList<PeakAnnotation>();
        expPeakList = new DoublePeakList<PeakAnnotation>();
        expPeakList.addSorted(masses, intensities, masses.length);
        expPeakList2 = new DoublePeakList<PeakAnnotation>();
        expPeakList2.addSorted(masses2, intensities2, masses2.length);
        perSlidingWindowFilter1Sliding = new NPeaksPerSlidingWindowFilter<PeakAnnotation>(1, 2.0, 0.0);
        perSlidingWindowFilter2Sliding = new NPeaksPerSlidingWindowFilter<PeakAnnotation>(2, 2.0, 0.0);
        perSlidingWindowFilter3Sliding = new NPeaksPerSlidingWindowFilter<PeakAnnotation>(2, 5.0, 1.2);
        perSlidingWindowFilter4Sliding = new NPeaksPerSlidingWindowFilter<PeakAnnotation>(2, 2.0, 0.5);
        perSlidingWindowFilter5Sliding = new NPeaksPerSlidingWindowFilter<PeakAnnotation>(1, 0.001, 0.0);
        perSlidingWindowFilter6Sliding = new NPeaksPerSlidingWindowFilter<PeakAnnotation>(1, 0.001, 10.0);
        perSlidingWindowFilter7Sliding = new NPeaksPerSlidingWindowFilter<PeakAnnotation>(2, 10.0, 1.2);
    }

    @Test
    public void testOneFilter1() {

        newPL = expPeakList.copy(new IdentityPeakProcessor<PeakAnnotation>());
        newPL.apply(perSlidingWindowFilter1Sliding);
        final double[] ms = newPL.getMzs(null);
        Assert.assertTrue(Arrays.equals(new double[]{5, 10, 13, 16}, ms));
    }

    @Test
    public void testOneFilter2() {

        newPL = expPeakList.copy(new IdentityPeakProcessor<PeakAnnotation>());
        newPL.apply(perSlidingWindowFilter2Sliding);
        final double[] ms = newPL.getMzs(null);
        Assert.assertTrue(Arrays.equals(new double[] {2.0, 4.6, 5.0, 9.0, 10.0,
                13.0, 15.7, 16.0}, ms));
    }

    @Test
    public void testOneFilter3() {

        newPL = expPeakList.copy(new IdentityPeakProcessor<PeakAnnotation>());
        newPL.apply(perSlidingWindowFilter3Sliding);
        final double[] ms = newPL.getMzs(null);
        Assert.assertTrue(Arrays.equals(new double[] {4.6, 5, 6, 13, 14}, ms));
    }

    @Test
    public void testOneFilter4() {

        newPL = expPeakList.copy(new IdentityPeakProcessor<PeakAnnotation>());
        newPL.apply(perSlidingWindowFilter4Sliding);
        final double[] ms = newPL.getMzs(null);
        Assert.assertTrue(Arrays.equals(new double[] {2.0, 4.6, 5.0, 9.0, 10.0,
                10.3, 13.0, 15.7, 16.0}, ms));
    }
    @Test
    public void testOneFilter5() {

        newPL = expPeakList.copy(new IdentityPeakProcessor<PeakAnnotation>());
        newPL.apply(perSlidingWindowFilter5Sliding);
        final double[] ms = newPL.getMzs(null);
        Assert.assertTrue(Arrays.equals(masses, ms));
    }

    @Test
    public void testOneFilter6() {

        newPL = expPeakList.copy(new IdentityPeakProcessor<PeakAnnotation>());
        newPL.apply(perSlidingWindowFilter6Sliding);
        final double[] ms = newPL.getMzs(null);
        Assert.assertTrue(Arrays.equals(masses, ms));
    }

    @Test
    public void testOneFilterReal() {

        newPL = expPeakList2.copy(new IdentityPeakProcessor<PeakAnnotation>());
        newPL.apply(perSlidingWindowFilter7Sliding);
        final double[] ms = newPL.getMzs(null);
        double[] res = new double[]{995.465,997.549,1012.809,1015.269,1023.958,1032.158,1038.543,1049.68,1056.236,
                                1074.268,1075.122,1088.61,1096.541,1112.495,1117.861,1136.221,1138.76,1149.159,
                                1154.643,1167.231,1168.39,1185.206,1191.259,1210.209,1216.67};
        Assert.assertArrayEquals(res, ms, 0.0001);
    }
}
