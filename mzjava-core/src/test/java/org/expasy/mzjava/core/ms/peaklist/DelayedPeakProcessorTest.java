/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.peaklist;

import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.map.hash.TIntObjectHashMap;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.spectrum.MockPeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class DelayedPeakProcessorTest {

    @Test
    public void test() throws Exception {

        MockDelayedPeakProcessor peakProcessor = new MockDelayedPeakProcessor();
        List<MockPeakAnnotation> empty = Collections.emptyList();

        MockPeakSink<MockPeakAnnotation> sink = new MockPeakSink<MockPeakAnnotation>();
        peakProcessor.setSink(sink);

        peakProcessor.start(3);

        peakProcessor.processPeak(124.87, 3498, empty);
        peakProcessor.processPeak(876.87, 12, Collections.singletonList(new MockPeakAnnotation(IonType.a, 2, 2)));
        peakProcessor.processPeak(1174.82, 100, empty);

        peakProcessor.end();

        TIntObjectHashMap<List<MockPeakAnnotation>> annotationMap = new TIntObjectHashMap<List<MockPeakAnnotation>>();
        annotationMap.put(1, Collections.singletonList(new MockPeakAnnotation(IonType.a, 2, 2)));

        peakProcessor.check(
                new TDoubleArrayList(new double[]{124.87, 876.87, 1174.82}),
                new TDoubleArrayList(new double[]{3498, 12, 100}),
                        annotationMap
                );

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getStartCalled());
    }

    @Test
    public void testClearingOnStart() throws Exception {

        MockDelayedPeakProcessor peakProcessor = new MockDelayedPeakProcessor();
        List<MockPeakAnnotation> empty = Collections.emptyList();

        MockPeakSink<MockPeakAnnotation> sink = new MockPeakSink<MockPeakAnnotation>();
        peakProcessor.setSink(sink);

        peakProcessor.start(3);

        peakProcessor.processPeak(876.87, 12, Collections.singletonList(new MockPeakAnnotation(IonType.a, 2, 2)));

        peakProcessor.end();

        //Process next peak list
        peakProcessor.start(3);

        peakProcessor.processPeak(124.87, 3498, empty);
        peakProcessor.processPeak(876.87, 12, Collections.singletonList(new MockPeakAnnotation(IonType.a, 2, 2)));
        peakProcessor.processPeak(1174.82, 100, empty);

        peakProcessor.end();

        TIntObjectHashMap<List<MockPeakAnnotation>> annotationMap = new TIntObjectHashMap<List<MockPeakAnnotation>>();
        annotationMap.put(1, Collections.singletonList(new MockPeakAnnotation(IonType.a, 2, 2)));

        peakProcessor.check(
                new TDoubleArrayList(new double[]{124.87, 876.87, 1174.82}),
                new TDoubleArrayList(new double[]{3498, 12, 100}),
                annotationMap
        );

        Assert.assertEquals(2, sink.getStartCalled());
        Assert.assertEquals(2, sink.getEndCalled());
    }

    private static class MockDelayedPeakProcessor extends DelayedPeakProcessor<MockPeakAnnotation, MockPeakAnnotation> {

        //The cached m/z's
        private TDoubleArrayList mzList;
        //The cached intensities
        private TDoubleArrayList intensityList;
        //The cached annotations
        private TIntObjectHashMap<List<MockPeakAnnotation>> annotationMap;

        public void check(TDoubleArrayList expectedMzs, TDoubleArrayList expectedIntensities, TIntObjectHashMap<List<MockPeakAnnotation>> expectedAnnotations) {

            Assert.assertEquals(expectedMzs, mzList);
            Assert.assertEquals(expectedIntensities, intensityList);
            Assert.assertEquals(expectedAnnotations, annotationMap);
        }

        @Override
        protected void processCached(TDoubleArrayList mzList, TDoubleArrayList intensityList, TIntObjectHashMap<List<MockPeakAnnotation>> annotationMap) {

            this.mzList = mzList;
            this.intensityList = intensityList;
            this.annotationMap = annotationMap;
        }
    }
}
