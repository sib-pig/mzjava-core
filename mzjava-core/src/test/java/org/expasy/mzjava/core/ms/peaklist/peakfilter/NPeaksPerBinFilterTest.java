/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.peaklist.peakfilter;

import org.expasy.mzjava.core.ms.peaklist.MockPeakSink;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class NPeaksPerBinFilterTest {

    @Test
    public void test() {

        NPeaksPerBinFilter<PeakAnnotation> binFilter = new NPeaksPerBinFilter<PeakAnnotation>(3, 500);
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();
        binFilter.setSink(sink);

        List<PeakAnnotation> annotations = Collections.emptyList();
        
        binFilter.start(31);

        binFilter.processPeak(104, 77, annotations);
        binFilter.processPeak(144, 932, annotations);
        binFilter.processPeak(166, 604, annotations);
        binFilter.processPeak(187, 394, annotations);
        binFilter.processPeak(219, 541, annotations);
        binFilter.processPeak(363, 684, annotations);
        binFilter.processPeak(467, 653, annotations);
        binFilter.processPeak(469, 412, annotations);
        binFilter.processPeak(478, 990, annotations);

        binFilter.processPeak(548, 176, annotations);
        binFilter.processPeak(591, 528, annotations);
        binFilter.processPeak(749, 615, annotations);
        binFilter.processPeak(754, 394, annotations);
        binFilter.processPeak(763, 232, annotations);
        binFilter.processPeak(834, 639, annotations);
        binFilter.processPeak(866, 166, annotations);
        binFilter.processPeak(925, 684, annotations);
        binFilter.processPeak(927, 198, annotations);
        binFilter.processPeak(980, 878, annotations);
        binFilter.processPeak(982, 290, annotations);

        binFilter.processPeak(1011, 984, annotations);
        binFilter.processPeak(1015, 480, annotations);
        binFilter.processPeak(1074, 202, annotations);
        binFilter.processPeak(1241, 815, annotations);
        binFilter.processPeak(1249, 997, annotations);
        binFilter.processPeak(1308, 224, annotations);
        binFilter.processPeak(1319, 795, annotations);
        binFilter.processPeak(1360, 301, annotations);
        binFilter.processPeak(1416, 719, annotations);

        binFilter.processPeak(1526, 959, annotations);
        binFilter.processPeak(1546, 232, annotations);

        binFilter.end();

        Assert.assertArrayEquals(new double[]{144, 363, 478, 834, 925, 980, 1011, 1241, 1249, 1526, 1546}, sink.getMzList(), 0.000001);
        Assert.assertArrayEquals(new double[]{932, 684, 990, 639, 684, 878,  984,  815,  997,  959,  232}, sink.getIntensityList(), 0.000001);

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getEndCalled());
    }
}
