/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.transformer;

import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.DefaultPeakListAligner;
import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.MockPeakPairSink;
import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PeakPairIonCurrentNormalizerTest {

    private final double delta = 0.000000001;

    @Test
    public void test1() {

        MockPeakPairSink<PeakAnnotation> sink = new MockPeakPairSink<>();
        MockProcessor processor = new MockProcessor(new PeakPairIonCurrentNormalizer<>(6), sink);
        processor.process(makeVector(new int[]{1, 2, 3}, new double[]{100, 200, 300}),
                makeVector(new int[]{1, 2, 3}, new double[]{100, 200, 300}));

        Assert.assertArrayEquals(new double[]{1, 2, 3}, sink.getCentroids(), delta);
        Assert.assertArrayEquals(new double[]{100.0, 200.0, 300.0}, sink.getXvalues(), delta);
        Assert.assertArrayEquals(new double[]{100.0, 200.0, 300.0}, sink.getYvalues(), delta);
    }

    @Test
    public void test2() {

        MockPeakPairSink<PeakAnnotation> sink = new MockPeakPairSink<>();
        MockProcessor processor = new MockProcessor(new PeakPairIonCurrentNormalizer<>(6), sink);
        processor.process(makeVector(new int[]{1, 2, 3}, new double[]{100, 2, 300}),
                makeVector(new int[]{1,    3}, new double[]{100,    302}));

        Assert.assertArrayEquals(new double[]{1, 3}, sink.getCentroids(), delta);

        Assert.assertArrayEquals(new double[]{100.0, 300.0}, sink.getXvalues(), delta);

        Assert.assertArrayEquals(new double[]{100.0, 302.0}, sink.getYvalues(), delta);
    }

    @Test
    public void test3() {

        MockPeakPairSink<PeakAnnotation> sink = new MockPeakPairSink<>();
        MockProcessor processor = new MockProcessor(new PeakPairIonCurrentNormalizer<>(6), sink);
        processor.process(makeVector(new int[]{1, 2, 3}, new double[]{10, 2, 3}),
                makeVector(new int[]{4, 5, 6}, new double[]{4, 5, 6}));

        Assert.assertArrayEquals(new double[]{1, 6}, sink.getCentroids(), delta);

        Assert.assertArrayEquals(new double[]{10, 0}, sink.getXvalues(), delta);

        Assert.assertArrayEquals(new double[]{0, 6}, sink.getYvalues(), delta);
    }

    @Test
    public void test4() {

        MockPeakPairSink<PeakAnnotation> sink = new MockPeakPairSink<>();
        MockProcessor processor = new MockProcessor(new PeakPairIonCurrentNormalizer<>(6), sink);
        processor.process(makeVector(new int[]{4, 5, 6}, new double[]{4, 5, 6}),
                makeVector(new int[]{1, 2, 3}, new double[]{10, 2, 3}));

        Assert.assertArrayEquals(new double[]{1, 6}, sink.getCentroids(), delta);

        Assert.assertArrayEquals(new double[]{0, 6}, sink.getXvalues(), delta);

        Assert.assertArrayEquals(new double[]{10, 0}, sink.getYvalues(), delta);
    }

    @Test
    public void test5() {

        MockPeakPairSink<PeakAnnotation> sink = new MockPeakPairSink<>();
        MockProcessor processor = new MockProcessor(new PeakPairIonCurrentNormalizer<>(6), sink);
        processor.process(makeVector(new int[]{4, 5, 6}, new double[]{1, 1, 1}),
                makeVector(new int[]{1, 2, 3}, new double[]{1, 1, 1}));

        Assert.assertArrayEquals(new double[]{}, sink.getCentroids(), delta);

        Assert.assertArrayEquals(new double[]{}, sink.getXvalues(), delta);

        Assert.assertArrayEquals(new double[]{}, sink.getYvalues(), delta);
    }

    @Test
    public void test6() {

        MockPeakPairSink<PeakAnnotation> sink = new MockPeakPairSink<>();
        MockProcessor processor = new MockProcessor(new PeakPairIonCurrentNormalizer<>(6), sink);
        processor.process(makeVector(new int[]{4, 5, 6}, new double[]{4000, 5000, 6000}),
                makeVector(new int[]{1, 2, 3}, new double[]{1000, 2000, 3000}));

        Assert.assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, sink.getCentroids(), delta);

        Assert.assertArrayEquals(new double[]{0.0, 0.0, 0.0, 1600, 2000, 2400}, sink.getXvalues(), delta);

        Assert.assertArrayEquals(new double[]{1000, 2000, 3000, 0.0, 0.0, 0.0}, sink.getYvalues(), delta);
    }

    @Test
    public void test7() {

        MockPeakPairSink<PeakAnnotation> sink = new MockPeakPairSink<>();
        MockProcessor processor = new MockProcessor(new PeakPairIonCurrentNormalizer<>(6), sink);
        processor.process(makeVector(new int[]{1, 2, 3}, new double[]{1000, 2000, 3000}),
                makeVector(new int[]{4, 5, 6}, new double[]{4000, 5000, 6000}));

        Assert.assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, sink.getCentroids(), delta);

        Assert.assertArrayEquals(new double[]{1000, 2000, 3000, 0.0, 0.0, 0.0}, sink.getXvalues(), delta);

        Assert.assertArrayEquals(new double[]{0.0, 0.0, 0.0, 1600, 2000, 2400}, sink.getYvalues(), delta);
    }

    @Test
    public void testNormalizeExpected() {

        MockPeakPairSink<PeakAnnotation> sink = new MockPeakPairSink<>();
        MockProcessor processor = new MockProcessor(new PeakPairIonCurrentNormalizer<>(6), sink);

        processor.process(makeVector(new int[]{1, 2}, new double[]{29, 1}),
                makeVector(new int[]{1, 2}, new double[]{ 0, 6}));

        Assert.assertArrayEquals(new double[]{2}, sink.getCentroids(), delta);

        Assert.assertArrayEquals(new double[]{0.2}, sink.getXvalues(), delta);

        Assert.assertArrayEquals(new double[]{6}, sink.getYvalues(), delta);
    }

    private PeakList<PeakAnnotation> makeVector(int[] binIds, double[] values) {

        Assert.assertEquals(binIds.length, values.length);

        PeakList<PeakAnnotation> vector = new DoublePeakList<>(binIds.length);

        for (int i = 0; i < binIds.length; i++) {

            vector.add(binIds[i], values[i]);
        }

        return vector;
    }

    private static class MockProcessor {

        private final DefaultPeakListAligner<PeakAnnotation, PeakAnnotation> merge = new DefaultPeakListAligner<>(new AbsoluteTolerance(0.5));

        public MockProcessor(PeakPairIonCurrentNormalizer<PeakAnnotation, PeakAnnotation> normalizer, MockPeakPairSink<PeakAnnotation> sink) {

            merge.setSink(normalizer);
            normalizer.setSink(sink);
        }

        public void process(PeakList<PeakAnnotation> vectorX, PeakList<PeakAnnotation> vectorY) {

            merge.align(vectorX, vectorY);
        }
    }
}
