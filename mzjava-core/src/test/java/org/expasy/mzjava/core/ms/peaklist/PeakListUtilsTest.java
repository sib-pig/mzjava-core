package org.expasy.mzjava.core.ms.peaklist;

import org.junit.Assert;
import org.junit.Test;

public class PeakListUtilsTest {

    @Test
    public void testCalculateDoubleLength() throws Exception {

        Assert.assertEquals(6.245, PeakListUtils.calcLength(new double[]{2, 5, 3, 1, 6, 7, 3}, 4), 0.00001);
    }

    @Test
    public void testCalculateFloatLength() throws Exception {

        Assert.assertEquals(6.245, PeakListUtils.calcLength(new float[]{2, 5, 3, 1, 6, 7, 3}, 4), 0.00001);
    }
}