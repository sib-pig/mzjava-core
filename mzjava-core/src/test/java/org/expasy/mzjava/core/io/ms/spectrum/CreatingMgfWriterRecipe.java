package org.expasy.mzjava.core.io.ms.spectrum;

import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.io.ms.spectrum.MgfWriter;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

// # $RECIPE$ $NUMBER$ - Writing Mass Spectrometry data #
//
// ## $PROBLEM$ ##
// You want to write MS data to character streams.
//
// ## $SOLUTION$ ##
//
// MS writer wrap a *[java.io.Writer](http://docs.oracle.com/javase/6/docs/api/index.html?java/io/Writer.html)*
// and need a *PeakList* precision.
public class CreatingMgfWriterRecipe {

// MS writer can be associated with any *java.io.Writer*
    @Test
    public void newWriter() throws IOException {

        //<SNIP>
        MgfWriter writer = new MgfWriter(new StringWriter(), PeakList.Precision.DOUBLE);
        //</SNIP>

        Assert.assertNotNull(writer);
    }

// Writing spectra into the writer
    @Test
    public void write() throws IOException {

        MgfWriter writer = new MgfWriter(new StringWriter(), PeakList.Precision.DOUBLE);

        //<SNIP>
        List<MsnSpectrum> spectra = new ArrayList<MsnSpectrum>();

        for (MsnSpectrum spectrum : spectra)  {

            writer.write(spectrum);
        }

        writer.close();
        //</SNIP>
    }

// ## $DISCUSSION$ ##

// ## $RELATED$ ##

}