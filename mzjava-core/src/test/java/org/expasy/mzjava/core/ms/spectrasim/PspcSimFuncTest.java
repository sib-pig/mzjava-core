/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim;

import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PspcSimFuncTest {

    @Test
    public void testCalcSimilarity() throws Exception {

        PeakList<PeakAnnotation> vX = makePeakList(new double[]{1, 2, 3, 4, 5, 6, 7, 8},
                new double[]{100, 100, 100, 100, 100, 100, 100, 100});
        PeakList<PeakAnnotation> vY = makePeakList(new double[]{1, 2, 3, 4, 5, 6, 7, 8},
                new double[]{100, 0, 100, 0, 100, 0, 100, 0});

        PspcSimFunc<PeakAnnotation, PeakAnnotation> simFunc = new PspcSimFunc<PeakAnnotation, PeakAnnotation>(0, new AbsoluteTolerance(0.1));

        Assert.assertEquals(0.6666666, simFunc.calcSimilarity(vX, vY), 0.000001);
    }

    @Test
    public void testCalcSimilarity2() throws Exception {

        PeakList<PeakAnnotation> vX = makePeakList(new double[]{1, 2, 3, 4, 5, 6, 7, 8},
                new double[]{0, 100, 0, 100, 0, 100, 100, 100});
        PeakList<PeakAnnotation> vY = makePeakList(new double[]{1, 2, 3, 4, 5, 6, 7, 8},
                new double[]{100, 0, 100, 0, 100, 0, 0, 100});

        PspcSimFunc<PeakAnnotation, PeakAnnotation> simFunc = new PspcSimFunc<PeakAnnotation, PeakAnnotation>(0, new AbsoluteTolerance(0.1));

        Assert.assertEquals(0.222222, simFunc.calcSimilarity(vX, vY), 0.000001);
    }

    private PeakList<PeakAnnotation> makePeakList(double[] centroids, double[] values){

        PeakList<PeakAnnotation> vector = new DoublePeakList<PeakAnnotation>(centroids.length);

        for (int i = 0; i < centroids.length; i++) {

            vector.add(centroids[i], values[i]);
        }

        return vector;
    }
}
