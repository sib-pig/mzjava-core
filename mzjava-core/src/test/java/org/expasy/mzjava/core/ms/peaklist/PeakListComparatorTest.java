package org.expasy.mzjava.core.ms.peaklist;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class PeakListComparatorTest {
    @Test
    public void testPeakListComp() throws Exception {
        PeakList pl1 = newPeakList(100.0, 1);
        PeakList pl2 = newPeakList(100.0, 1);
        PeakList pl3 = newPeakList(100.0, 2);
        PeakList pl4 = newPeakList(99.9999999, 1);
        PeakList pl5 = newPeakList(100.00000001, 1);

        PeakListComparator comparator = new PeakListComparator();
        Assert.assertTrue(comparator.compare(pl1,pl2)==0);
        Assert.assertTrue(comparator.compare(pl1,pl3)<0);
        Assert.assertTrue(comparator.compare(pl1,pl4)>0);
        Assert.assertTrue(comparator.compare(pl1,pl5)<0);
    }

    private PeakList newPeakList(double mz, int charge) {

        FloatConstantPeakList peakList = new FloatConstantPeakList(1);
        peakList.getPrecursor().setMzAndCharge(mz, charge);
        return peakList;
    }
}
