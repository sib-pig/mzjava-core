package org.expasy.mzjava.core.mol;

import org.junit.Assert;
import org.junit.Test;

public class AtomTest {

    @Test(expected = NullPointerException.class)
    public void testConstructorNullSymbol() throws Exception {

        new Atom(null, 1, 1, 0.9, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorNegativeMass() throws Exception {

        new Atom(AtomicSymbol.H, 1, -1, 0.9, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorNegativeAbundance() throws Exception {

        new Atom(AtomicSymbol.H, 1, 1, -0.1, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorLargerThan1Abundance() throws Exception {

        new Atom(AtomicSymbol.H, 1, 1, 1.5, true);
    }

    @Test
    public void testEquals() throws Exception {

        Assert.assertEquals(true, PeriodicTable.H.equals(new Atom(AtomicSymbol.H, 1, 1.007825032, 0.99985, true)));
        Assert.assertEquals(false, PeriodicTable.H.equals(PeriodicTable.C));
    }

    @Test
    public void testHashCode() throws Exception {

        Assert.assertEquals(PeriodicTable.H.hashCode(), new Atom(AtomicSymbol.H, 1, 1.007825032, 0.99985, true).hashCode());
        Assert.assertNotEquals(PeriodicTable.H.hashCode(), PeriodicTable.C.hashCode());
    }

    @Test
    public void testToString() throws Exception {

        Assert.assertEquals("C[13]", PeriodicTable.C13.toString());
        Assert.assertEquals("C", PeriodicTable.C.toString());
    }

    @Test
    public void testCompareTo() throws Exception {

        Atom h = PeriodicTable.H;
        Atom c13 = PeriodicTable.C13;
        Atom c = PeriodicTable.C;

        Assert.assertEquals(0, h.compareTo(h));
        Assert.assertEquals(1, c13.compareTo(c));
        Assert.assertEquals(-1, c.compareTo(c13));
    }
}