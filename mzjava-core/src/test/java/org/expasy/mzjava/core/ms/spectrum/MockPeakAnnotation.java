package org.expasy.mzjava.core.ms.spectrum;

import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MockPeakAnnotation implements PeakAnnotation {

    private final IonType ionType;
    private final int charge;
    private final int id;

    public MockPeakAnnotation(IonType ionType, int charge, int id) {

        Preconditions.checkNotNull(ionType);
        this.ionType = ionType;

        this.charge = charge;
        this.id = id;
    }

    @Override
    public PeakAnnotation copy() {

        return new MockPeakAnnotation(ionType, charge, id);
    }

    public int getCharge() {

        return charge;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MockPeakAnnotation that = (MockPeakAnnotation) o;

        return charge == that.charge && id == that.id && ionType == that.ionType;
    }

    @Override
    public int hashCode() {

        int result = ionType.hashCode();
        result = 31 * result + charge;
        result = 31 * result + id;
        return result;
    }

    @Override
    public String toString() {

        return "MockPeakAnnotation{" +
                "ionType=" + ionType +
                ", charge=" + charge +
                ", id=" + id +
                '}';
    }

    public IonType getIonType() {

        return ionType;
    }

    public int getId() {

        return id;
    }
}
