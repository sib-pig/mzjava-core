package org.expasy.mzjava.core.ms.peaklist.peakfilter;

import org.expasy.mzjava.utils.IntervalList;
import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Markus
 * @version 1.0
 */
public class MzRangeFilterTest {
    double[] masses =
            new double[] { 995.465, 997.549,1002.213,1009.445,1012.809,1015.269,1023.958,1024.746,1032.158,1038.543,
                    1049.680,1055.237,1056.236,1061.347,1063.269,1069.886,1074.268,1075.122,1080.235,1088.610,
                    1094.540,1096.541,1102.804,1107.303,1112.495,1117.861,1120.826,1125.643,1129.048,1136.221,
                    1138.760,1141.752,1149.159,1154.643,1167.231,1168.390,1172.392,1177.803,1185.206,1191.259,
                    1192.455,1199.366,1209.212,1210.209,1216.670};
    double[] intensities =
            new double[] {  18.288,  10.924,   6.442,  17.300,  17.788,  18.947,  21.898,  11.378,  22.440,  10.302,
                    50.827,  17.054,  22.280,  11.892,   7.995,  10.341, 215.683,  88.473,  18.228,  13.208,
                    3.964,   5.049,   2.980,   1.780,  13.860,   5.808,   5.583,   2.364,   1.033,  13.762,
                    21.549,   5.541,  14.566,  19.965, 239.495,  40.558,  21.821,   9.397,1820.518, 630.056,
                    23.832,   3.843,   6.159, 383.467,  58.726};

    @Test
    public void test1() {
        DoublePeakList<PeakAnnotation> expPeakList = new DoublePeakList<PeakAnnotation>();
        expPeakList.addSorted(masses, intensities, masses.length);
        double[] selection = new double[]{995.465,997.549,1012.809,1015.269,1023.958,1032.158,1038.543,1049.68,1056.236,
                1074.268,1075.122,1088.61,1096.541,1112.495,1117.861,1136.221,1138.76,1149.159,
                1154.643,1167.231,1168.39,1185.206,1191.259,1210.209,1216.67};

        IntervalList intervals = new IntervalList();

        //noinspection ForLoopReplaceableByForEach
        for (int i=0;i<selection.length;i++){
            intervals.addInterval(selection[i]-0.001,selection[i]+0.001);
        }

        MzRangeFilter<PeakAnnotation> mzRangeFilter = new MzRangeFilter<PeakAnnotation>(intervals, true);
        expPeakList.apply(mzRangeFilter);

        final double[] ms = expPeakList.getMzs(null);
        Assert.assertArrayEquals(selection, ms, 0.0001);
    }

    @Test
    public void test2() {
        DoublePeakList<PeakAnnotation> expPeakList = new DoublePeakList<PeakAnnotation>();
        expPeakList.addSorted(masses, intensities, masses.length);
        double[] selection = new double[]{995.465,997.549,1012.809,1015.269,1023.958,1032.158,1038.543,1049.68,1056.236,
                1074.268,1075.122,1088.61,1096.541,1112.495,1117.861,1136.221,1138.76,1149.159,
                1154.643,1167.231,1168.39,1185.206,1191.259,1210.209,1216.67};
        double[] res =
            new double[] { 1002.213,1009.445,1024.746,
                1055.237,1061.347,1063.269,1069.886,1080.235,
                1094.540,1102.804,1107.303,1120.826,1125.643,1129.048,
                1141.752,1172.392,1177.803,
                1192.455,1199.366,1209.212};

        IntervalList intervals = new IntervalList();

        //noinspection ForLoopReplaceableByForEach
        for (int i=0;i<selection.length;i++){
            intervals.addInterval(selection[i]-0.001,selection[i]+0.001);
        }

        MzRangeFilter<PeakAnnotation> mzRangeFilter = new MzRangeFilter<PeakAnnotation>(intervals, false);
        expPeakList.apply(mzRangeFilter);
        final double[] ms = expPeakList.getMzs(null);
        Assert.assertArrayEquals(res, ms, 0.0001);
    }

    @Test
    public void test3() {
        DoublePeakList<PeakAnnotation> expPeakList = new DoublePeakList<PeakAnnotation>();
        expPeakList.addSorted(masses, intensities, masses.length);
        IntervalList intervals = new IntervalList();
        intervals.addInterval(0.0,800);
        intervals.addInterval(900.0,1000.0);
        intervals.addInterval(1168.39,1210.209);
        double[] res = new double[] { 995.465, 997.549,1168.390,1172.392,1177.803,1185.206,1191.259,
                                      1192.455,1199.366,1209.212,1210.209};

        MzRangeFilter<PeakAnnotation> mzRangeFilter = new MzRangeFilter<PeakAnnotation>(intervals, true);
        expPeakList.apply(mzRangeFilter);
        final double[] ms = expPeakList.getMzs(null);
        Assert.assertArrayEquals(res, ms, 0.0001);
    }

    @Test
    public void test4() {
        DoublePeakList<PeakAnnotation> expPeakList = new DoublePeakList<PeakAnnotation>();
        expPeakList.addSorted(masses, intensities, masses.length);
        IntervalList intervals = new IntervalList();
        intervals.addInterval(0.0,800);
        intervals.addInterval(900.0,1000.0);
        intervals.addInterval(1168.39,1210.209);
        double[] res =
                new double[] { 1002.213,1009.445,1012.809,1015.269,1023.958,1024.746,1032.158,1038.543,
                        1049.680,1055.237,1056.236,1061.347,1063.269,1069.886,1074.268,1075.122,1080.235,1088.610,
                        1094.540,1096.541,1102.804,1107.303,1112.495,1117.861,1120.826,1125.643,1129.048,1136.221,
                        1138.760,1141.752,1149.159,1154.643,1167.231,1216.670};

        MzRangeFilter<PeakAnnotation> mzRangeFilter = new MzRangeFilter<PeakAnnotation>(intervals, false);
        expPeakList.apply(mzRangeFilter);
        final double[] ms = expPeakList.getMzs(null);
        Assert.assertArrayEquals(res, ms, 0.0001);
    }
}

