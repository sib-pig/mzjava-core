package org.expasy.mzjava.core.io.ms.spectrum;

import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.xmlunit.builder.Input;
import org.xmlunit.matchers.CompareMatcher;

import javax.xml.stream.XMLInputFactory;
import javax.xml.transform.Source;
import java.io.File;
import java.io.IOException;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MzxmlReaderTest3 extends AbstractReaderTest<PeakAnnotation, MsnSpectrum> {

    private MzxmlReader reader;

    @Before
    public void setup() throws IOException {

        File file = new File(getClass().getResource("mzxml_test_2.mzXML").getFile());

        reader = new MzxmlReader(file, PeakList.Precision.DOUBLE){
            @Override
            protected XMLInputFactory newXmlInputFactory() {

                final XMLInputFactory xmlif = super.newXmlInputFactory();
                xmlif.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, false);
                return xmlif;
            }
        };
    }

    @Test
    public void testHeader() throws Exception {

        String expectedHeader = "<msRun startTime='PT5.328S' endTime='PT3599.61S' scanCount='1398'>\n" +
                "        <parentFile fileSha1='c2798b237a59c3a28401e8174db13ea704406f03' fileType='RAWData' fileName='file:///D:/2008/080109 BH PGM TEST/080109 PGM TEST1.D/Analysis.yep'></parentFile>\n" +
                "        <msInstrument msInstrumentID='1'>\n" +
                "            <msManufacturer category='msManufacturer' value='instrument model'></msManufacturer>\n" +
                "            <msModel category='msModel' value='Bruker Daltonics instrument model'></msModel>\n" +
                "        </msInstrument>\n" +
                "        <dataProcessing>\n" +
                "            <software name='ProteoWizard' type='conversion' version='3.0.3421'></software>\n" +
                "            <processingOperation name='Conversion to mzML'></processingOperation>\n" +
                "        </dataProcessing>\n" +
                "        ";

        final String actualHeader = reader.getHeader();
        reader.close();

        final Source expected = Input.fromString(expectedHeader + "</msRun>").build();
        final Source actual = Input.fromString(actualHeader + "</msRun>").build();
        Assert.assertThat(expected, CompareMatcher.isSimilarTo(actual).throwComparisonFailure());
    }

    @Test
    public void test() throws Exception {

        reader.next();
        reader.next();
        reader.next();
        reader.next();
        Assert.assertFalse(reader.hasNext());

        reader.close();
    }
}
