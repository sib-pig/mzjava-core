package org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.transformer;

import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.MockPeakPairSink;
import org.junit.Assert;
import org.junit.Test;
import java.util.Collections;
import java.util.List;

import static java.lang.Math.log10;

/**
 * @author Aivett Bilbao
 * @version 1.0
 */
public class PeakPairIntensityLog10TransformerTest {

    @Test
    public void testProcessPeakPair() throws Exception {

        PeakPairIntensityLog10Transformer<PeakAnnotation, PeakAnnotation> processor = new PeakPairIntensityLog10Transformer<PeakAnnotation, PeakAnnotation>();

        MockPeakPairSink<PeakAnnotation> sink = new MockPeakPairSink<PeakAnnotation>();
        processor.setSink(sink);
        List<PeakAnnotation> empty = Collections.emptyList();

        processor.begin(null, null);

        processor.processPeakPair(1.1, 4, 14, empty, empty);
        processor.processPeakPair(1.2, 44, 3, empty, empty);
        processor.processPeakPair(1.3, 12, 8, empty, empty);
        processor.processPeakPair(1.4, 77, 2, empty, empty);

        processor.end();

        Assert.assertArrayEquals(new double[]{1.1, 1.2, 1.3, 1.4}, sink.getCentroids(), 0.00000001);
        Assert.assertArrayEquals(new double[]{log10(4 + 1), log10(44 + 1), log10(12 + 1), log10(77 + 1)}, sink.getXvalues(), 0.00000001);
        Assert.assertArrayEquals(new double[]{log10(14 + 1), log10(3 + 1), log10(8 + 1), log10(2 + 1)}, sink.getYvalues(), 0.00000001);

        Assert.assertTrue(sink.wasBeginCalled());
        Assert.assertTrue(sink.wasEndCalled());
    }

    @Test
    public void testProcessPeakPairUndefLog10() throws Exception {

        PeakPairIntensityLog10Transformer<PeakAnnotation, PeakAnnotation> processor = new PeakPairIntensityLog10Transformer<PeakAnnotation, PeakAnnotation>();
        MockPeakPairSink<PeakAnnotation> sink = new MockPeakPairSink<PeakAnnotation>();
        processor.setSink(sink);
        List<PeakAnnotation> empty = Collections.emptyList();

        processor.begin(null, null);

        processor.processPeakPair(1.1, -10, 10, empty, empty);
        processor.processPeakPair(50.1, -10, 0, empty, empty);

        Assert.assertArrayEquals(new double[]{0, 0}, sink.getXvalues(), 0.00000001);
        Assert.assertArrayEquals(new double[]{Math.log10(10 + 1), 0}, sink.getYvalues(), 0.00000001);
    }
}