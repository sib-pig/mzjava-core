/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.peaklist;

import org.expasy.mzjava.core.ms.spectrum.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class AbstractPeakProcessorTest {

    @Test
    public void testInPlace() throws Exception {

        MockPeakProcessor processor = new MockPeakProcessor();

        PeakList<MockPeakAnnotation> peakList = new DoublePeakList<MockPeakAnnotation>();
        peakList.add(356.7385, 98);
        peakList.add(443.9525, 12);

        peakList.apply(processor);

        Assert.assertSame(peakList, peakList);
        Assert.assertTrue("Start was not called", processor.startCalled);
        Assert.assertEquals(2, processor.size);
        Assert.assertTrue("End was not called", processor.endCalled);
    }

    @Test
    public void testCopyAnnotation() throws Exception {

        MockPeakProcessor processor = new MockPeakProcessor();

        MockPeakAnnotation annotation1 = new MockPeakAnnotation(IonType.b, 1, 3);
        MockPeakAnnotation annotation2 = new MockPeakAnnotation(IonType.y, 1, 2);

        PeakList<MockPeakAnnotation> peakList = new DoublePeakList<MockPeakAnnotation>();
        peakList.add(356.7385, 98);
        peakList.addAnnotation(0, annotation1);
        peakList.add(443.9525, 12);
        peakList.addAnnotation(1, annotation2);

        Peak precursor = peakList.getPrecursor();
        precursor.setMzAndCharge(1763.2154, 1);
        precursor.setIntensity(55);

        peakList.apply(processor);

        Assert.assertTrue("Start was not called", processor.startCalled);
        Assert.assertEquals(2, processor.size);
        Assert.assertTrue("End was not called", processor.endCalled);

        Assert.assertEquals(annotation1, peakList.getFirstAnnotation(0).get());
        Assert.assertNotSame(annotation1, peakList.getFirstAnnotation(0));

        Assert.assertEquals(annotation2, peakList.getFirstAnnotation(1).get());
        Assert.assertNotSame(annotation1, peakList.getFirstAnnotation(1));

        precursor = peakList.getPrecursor();
        Assert.assertEquals(1763.2154, precursor.getMz(), 0.0000001);
        Assert.assertEquals(1, precursor.getCharge());
        Assert.assertEquals(55, precursor.getIntensity(), 0.0000001);
    }

    private class MockPeakProcessor extends AbstractPeakProcessor<MockPeakAnnotation, MockPeakAnnotation> {

        int size = -1;
        boolean startCalled = false;
        boolean endCalled = false;

        @Override
        public void start(int size) {

            this.size = size;
            startCalled = true;

            sink.start(size);
        }

        @Override
        public void end() {

            endCalled = true;

            sink.end();
        }

        @Override
        public void processPeak(double mz, double intensity, List<MockPeakAnnotation> annotations) {

            sink.processPeak(mz, intensity, annotations);
        }
    }
}
