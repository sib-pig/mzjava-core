package org.expasy.mzjava.core.io.ms.spectrum;

import org.junit.Assert;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.junit.Before;
import org.junit.Test;

/**
 * User: fnikitin
 * Date: 7/12/12
 * Time: 11:14 AM
 */
public class PeakListPrecisionFormatTest {

	double mz;
	double intensity;

	@Before
	public void setup() {

		mz = 456.987654321987654321;
		intensity = 9999.99999999999999999;
	}

	@Test
	public void testGetMzFormatDouble() throws Exception {

		PeakList.Precision prec = PeakList.Precision.DOUBLE;

		Assert.assertEquals("456.9876543220", PeakListPrecisionFormat.getMzFormat(prec).format(mz));
	}

	@Test
	public void testGetIntensityFormat() throws Exception {
		PeakList.Precision prec = PeakList.Precision.DOUBLE;

		Assert.assertEquals("10000.0000000000", PeakListPrecisionFormat.getIntensityFormat(prec).format(intensity));
	}
}
