package org.expasy.mzjava.core.ms.peaklist.peaktransformer;

import cern.jet.random.Normal;
import cern.jet.random.engine.MersenneTwister;
import org.expasy.mzjava.core.ms.peaklist.MockPeakSink;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.AbstractMergePeakFilter;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.LibraryMergePeakFilter;
import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.LibPeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author mmueller
 * @version 1.0
 */
public class ArcsinhTransformerTest {
    double[] masses =
            new double[] { 995.465, 997.549,1002.213,1009.445,1012.809,1015.269,1023.958,1024.746,1032.158,1038.543,
                    1049.680,1055.237,1056.236,1061.347,1063.269,1069.886,1074.268,1075.122,1080.235,1088.610,
                    1094.540,1096.541,1102.804,1107.303,1112.495,1117.861,1120.826,1125.643,1129.048,1136.221,
                    1138.760,1141.752,1149.159,1154.643,1167.231,1168.390,1172.392,1177.803,1185.206,1191.259,
                    1192.455,1199.366,1209.212,1210.209,1216.670};
    double[] intensities =
            new double[] {  18.288,  10.924,   6.442,  17.300,  17.788,  18.947,  21.898,  11.378,  22.440,  10.302,
                    50.827,  17.054,  22.280,  11.892,   7.995,  10.341, 215.683,  88.473,  18.228,  13.208,
                    3.964,   5.049,   2.980,   1.780,  13.860,   5.808,   5.583,   2.364,   1.033,  13.762,
                    21.549,   5.541,  14.566,  19.965, 239.495,  40.558,  21.821,   9.397,1820.518, 630.056,
                    23.832,   3.843,   6.159, 383.467,  58.726};

    PeakList<PeakAnnotation> expPeakList, newPL,tmpPeakList;
    ArcsinhTransformer<PeakAnnotation> stabilizer;
    InverseArcsinhTransformer<PeakAnnotation> destabilizer;

    @Test
    public void test()
    {
        expPeakList = new DoublePeakList<PeakAnnotation>();
        expPeakList.addSorted(masses, intensities, masses.length);

        double gamma = 1.0;
        double a = 2.0;
        double b = 1.5;
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();
        stabilizer = new ArcsinhTransformer<PeakAnnotation>(gamma,a,b);
        stabilizer.setSink(sink);
        newPL = expPeakList.copy(new IdentityPeakProcessor<PeakAnnotation>());
        newPL.apply(stabilizer);

        double[] intens = newPL.getIntensities(null);
        double[] transIntensities = new double[intensities.length];

        for (int i=0;i<intensities.length;i++) {
            double intensity = a+intensities[i]*b;
            transIntensities[i] = gamma*Math.log(intensity+Math.sqrt(intensity*intensity+1.0));
        }

        Assert.assertArrayEquals(intens,transIntensities,0.0001);

        destabilizer = new InverseArcsinhTransformer<PeakAnnotation>(gamma,a,b);
        destabilizer.setSink(sink);
        newPL.apply(destabilizer);
        intens = newPL.getIntensities(null);

        Assert.assertArrayEquals(intens,intensities,0.0001);
    }
    @Test
    public void test2()
    {
        PeakList<LibPeakAnnotation> consPeakList = new DoublePeakList<LibPeakAnnotation>();

        MersenneTwister rd = new MersenneTwister();

        // create random variation in peak intensity according to quadratic noise model
        double c1 = 0.1;
        double c2 = 0.0;
        double c3 = 4.0;
        double[] intBuf = new double[masses.length];
        Normal[] nRandGens = new Normal[masses.length];
        for (int i=0;i<masses.length;i++) {
            double intensity = c1*intensities[i]+c2;
            double std = Math.sqrt(intensity*intensity + c3);

            Normal nrd = new Normal(0.0,std,rd);
            nRandGens[i] = nrd;
        }

        double gamma = 1.0/c1;
        double a = c2/Math.sqrt(c3);
        double b = c1/Math.sqrt(c3);
        stabilizer = new ArcsinhTransformer<PeakAnnotation>(gamma,a,b);
        for (int i=0;i<100;i++) {
            for (int j=0;j<masses.length;j++) {
                intBuf[j] = Math.max(intensities[j]+nRandGens[j].nextDouble(),0.0001);
            }

            tmpPeakList = new DoublePeakList<PeakAnnotation>();
            tmpPeakList.addSorted(masses, intBuf, masses.length);
            tmpPeakList.apply(stabilizer);
            consPeakList.addSorted(masses, tmpPeakList.getIntensities(null), masses.length);
        }

        LibraryMergePeakFilter<LibPeakAnnotation> filterMerge
                = new LibraryMergePeakFilter<LibPeakAnnotation>(0.01,0.02, AbstractMergePeakFilter.IntensityMode.MEAN_POS_INTENSITY,100);
        consPeakList.apply(filterMerge);

        double std = 0.0;
        LibPeakAnnotation annot;
        for (int i=0;i<masses.length;i++){
            annot = consPeakList.getAnnotations(i).get(0);
            std += annot.getIntensityStd();
        }
        std /= masses.length;

        for (int i=0;i<masses.length;i++){
            annot = consPeakList.getAnnotations(i).get(0);
            Assert.assertEquals(annot.getIntensityStd(),std,0.5);
        }
    }
}
