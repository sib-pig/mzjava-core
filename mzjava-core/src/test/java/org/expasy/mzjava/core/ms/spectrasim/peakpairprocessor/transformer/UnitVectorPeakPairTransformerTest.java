/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.transformer;

import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.MockPeakPairSink;
import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.PeakPairSink;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class UnitVectorPeakPairTransformerTest {

    @Test
    public void test() {

        UnitVectorPeakPairTransformer<PeakAnnotation, PeakAnnotation> normalize = new UnitVectorPeakPairTransformer<PeakAnnotation, PeakAnnotation>();
        MockPeakPairSink<PeakAnnotation> sink = new MockPeakPairSink<PeakAnnotation>();

        normalize.setSink(sink);

        process(normalize,
                new double[]{125.6, 458.6, 459.5, 178.2},
                new double[]{125, 457, 368, 2045},
                new double[]{575, 2102, 1693, 9407});

        Assert.assertArrayEquals(new double[]{125.6, 458.6, 459.5, 178.2}, sink.getCentroids(), 0.0001);
        Assert.assertArrayEquals(new double[]{0.0586530, 0.2144353, 0.1726744, 0.9595629}, sink.getXvalues(), 0.0001);
        Assert.assertArrayEquals(new double[]{0.0586530, 0.2144353, 0.1726744, 0.9595629}, sink.getYvalues(), 0.0001);
    }

    @Test
    public void testEmpty() {

        UnitVectorPeakPairTransformer<PeakAnnotation, PeakAnnotation> normalize = new UnitVectorPeakPairTransformer<PeakAnnotation, PeakAnnotation>();
        MockPeakPairSink<PeakAnnotation> sink = new MockPeakPairSink<PeakAnnotation>();

        normalize.setSink(sink);

       process(normalize,
               new double[]{},
               new double[]{},
               new double[]{});

        Assert.assertArrayEquals(new double[0], sink.getCentroids(), 0.0001);
        Assert.assertArrayEquals(new double[0], sink.getXvalues(), 0.0001);
        Assert.assertArrayEquals(new double[0], sink.getYvalues(), 0.0001);
    }

    public void process(PeakPairSink<PeakAnnotation, PeakAnnotation> sink, double[] centroids, double[] xValues, double[] yValues) {

        sink.begin(null, null);

        List<PeakAnnotation> empty = Collections.emptyList();
        for(int i = 0; i < centroids.length; i++) {

            sink.processPeakPair(centroids[i], xValues[i], yValues[i], empty, empty);
        }

        sink.end();
    }
}
