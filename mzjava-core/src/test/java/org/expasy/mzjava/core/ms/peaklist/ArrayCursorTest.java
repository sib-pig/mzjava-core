package org.expasy.mzjava.core.ms.peaklist;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author fnikitin
 * Date: 3/15/13
 */
public class ArrayCursorTest {

    @Test
    public void test() throws Exception {

        double[] mzs = {101.0909, 202.1079, 203.1045, 244.128, 254.1056, 255.191, 270.2388, 272.2092, 273.1984, 283.1682, 284.1255, 295.1564, 300.1432, 301.1701, 302.1986, 312.1813, 313.166, 313.3103, 314.1407, 330.185};
        double[] intensities = {15.4762, 21.4762, 5.3333, 14.381, 3.4286, 7.2381, 2.1905, 27.8095, 10.3333, 8.381, 4.381, 4.2857, 3.1905, 15.381, 3.3333, 7.0, 9.381, 3.2857, 7.0476, 17.5714};

        runTest(new ArrayCursor<PeakAnnotation>(mzs, intensities, mzs.length));
    }

    private void runTest(ArrayCursor<PeakAnnotation> cursor) {

        double mzDelta = 0.001;
        double intensityDelta = 0.001;

        Assert.assertEquals(330.185, cursor.lastMz(), mzDelta);
        Assert.assertEquals(17.5714, cursor.lastIntensity(), intensityDelta);

        Assert.assertEquals(20, cursor.size());

        Assert.assertTrue(cursor.next());
        Assert.assertEquals(101.0909, cursor.currMz(), mzDelta);
        Assert.assertEquals(15.4762, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(202.1079, cursor.currMz(), mzDelta);
        Assert.assertEquals(21.4762, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(203.1045, cursor.currMz(), mzDelta);
        Assert.assertEquals(5.3333, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(244.128, cursor.currMz(), mzDelta);
        Assert.assertEquals(14.381, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(254.1056, cursor.currMz(), mzDelta);
        Assert.assertEquals(3.4286, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(255.191, cursor.currMz(), mzDelta);
        Assert.assertEquals(7.2381, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(270.2388, cursor.currMz(), mzDelta);
        Assert.assertEquals(2.1905, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(272.2092, cursor.currMz(), mzDelta);
        Assert.assertEquals(27.8095, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(273.1984, cursor.currMz(), mzDelta);
        Assert.assertEquals(10.3333, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(283.1682, cursor.currMz(), mzDelta);
        Assert.assertEquals(8.381, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(284.1255, cursor.currMz(), mzDelta);
        Assert.assertEquals(4.381, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(295.1564, cursor.currMz(), mzDelta);
        Assert.assertEquals(4.2857, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(300.1432, cursor.currMz(), mzDelta);
        Assert.assertEquals(3.1905, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(301.1701, cursor.currMz(), mzDelta);
        Assert.assertEquals(15.381, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(302.1986, cursor.currMz(), mzDelta);
        Assert.assertEquals(3.3333, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(312.1813, cursor.currMz(), mzDelta);
        Assert.assertEquals(7.0, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(313.166, cursor.currMz(), mzDelta);
        Assert.assertEquals(9.381, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(313.3103, cursor.currMz(), mzDelta);
        Assert.assertEquals(3.2857, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(314.1407, cursor.currMz(), mzDelta);
        Assert.assertEquals(7.0476, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(330.185, cursor.currMz(), mzDelta);
        Assert.assertEquals(17.5714, cursor.currIntensity(), intensityDelta);
        Assert.assertFalse(cursor.next());

        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(314.1407, cursor.currMz(), mzDelta);
        Assert.assertEquals(7.0476, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(313.3103, cursor.currMz(), mzDelta);
        Assert.assertEquals(3.2857, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(313.166, cursor.currMz(), mzDelta);
        Assert.assertEquals(9.381, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(312.1813, cursor.currMz(), mzDelta);
        Assert.assertEquals(7.0, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(302.1986, cursor.currMz(), mzDelta);
        Assert.assertEquals(3.3333, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(301.1701, cursor.currMz(), mzDelta);
        Assert.assertEquals(15.381, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(300.1432, cursor.currMz(), mzDelta);
        Assert.assertEquals(3.1905, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(295.1564, cursor.currMz(), mzDelta);
        Assert.assertEquals(4.2857, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(284.1255, cursor.currMz(), mzDelta);
        Assert.assertEquals(4.381, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(283.1682, cursor.currMz(), mzDelta);
        Assert.assertEquals(8.381, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(273.1984, cursor.currMz(), mzDelta);
        Assert.assertEquals(10.3333, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(272.2092, cursor.currMz(), mzDelta);
        Assert.assertEquals(27.8095, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(270.2388, cursor.currMz(), mzDelta);
        Assert.assertEquals(2.1905, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(255.191, cursor.currMz(), mzDelta);
        Assert.assertEquals(7.2381, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(254.1056, cursor.currMz(), mzDelta);
        Assert.assertEquals(3.4286, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(244.128, cursor.currMz(), mzDelta);
        Assert.assertEquals(14.381, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(203.1045, cursor.currMz(), mzDelta);
        Assert.assertEquals(5.3333, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(202.1079, cursor.currMz(), mzDelta);
        Assert.assertEquals(21.4762, cursor.currIntensity(), intensityDelta);
        Assert.assertTrue(cursor.previous());
        Assert.assertEquals(101.0909, cursor.currMz(), mzDelta);
        Assert.assertEquals(15.4762, cursor.currIntensity(), intensityDelta);
    }

    @Test
    public void testMoveTo() {

        final double[] mzs = new double[]{10.0, 10.3};
        PeakCursor<PeakAnnotation> vector = new ArrayCursor<PeakAnnotation>(mzs, new double[]{1, 1}, mzs.length);

        vector.moveToClosest(10.1);
        Assert.assertEquals(10.0, vector.currMz(), 0.000001);

        vector.moveToClosest(10.2);
        Assert.assertEquals(10.3, vector.currMz(), 0.000001);

        vector.moveToClosest(100);
        Assert.assertEquals(10.3, vector.currMz(), 0.000001);
    }

    @Test
    public void testNext() {

        final double[] mzs = new double[]{12.5, 78.6, 158.9};
        PeakCursor<PeakAnnotation> vector = new ArrayCursor<PeakAnnotation>(mzs, new double[]{100, 200, 300}, mzs.length);

        vector.resetCursor();

        Assert.assertTrue(vector.next());
        Assert.assertEquals(12.5, vector.currMz(), 0.000001);
        Assert.assertEquals(100.0, vector.currIntensity(), 0.000001);
        Assert.assertTrue(vector.next());
        Assert.assertEquals(78.6, vector.currMz(), 0.000001);
        Assert.assertEquals(200.0, vector.currIntensity(), 0.000001);
        Assert.assertTrue(vector.next());
        Assert.assertEquals(158.9, vector.currMz(), 0.000001);
        Assert.assertEquals(300.0, vector.currIntensity(), 0.000001);
        Assert.assertFalse(vector.next());
    }

    @Test
    public void testNextWithCentroid() {

        final double[] mzs = new double[]{12.5, 78.6, 158.9, 241.68};
        PeakCursor<PeakAnnotation> vector = new ArrayCursor<PeakAnnotation>(mzs, new double[]{100, 200, 300, 400}, mzs.length);

        vector.resetCursor();

        //under on first curr was ? is now = 12.5
        Assert.assertTrue(vector.next(2));
        Assert.assertEquals(12.5, vector.currMz(), 0.000001);
        Assert.assertEquals(100.0, vector.currIntensity(), 0.000001);

        //under curr was 12.5 is now 78.6
        Assert.assertTrue(vector.next(58.59));
        Assert.assertEquals(78.6, vector.currMz(), 0.000001);
        Assert.assertEquals(200.0, vector.currIntensity(), 0.000001);

        //hold by under curr was 78.6 isn now 78.6
        Assert.assertTrue(vector.next(60.9));
        Assert.assertEquals(78.6, vector.currMz(), 0.000001);
        Assert.assertEquals(200.0, vector.currIntensity(), 0.000001);

        //exact curr was 78.6 is now 158.9
        Assert.assertTrue(vector.next(78.6));
        Assert.assertEquals(158.9, vector.currMz(), 0.000001);
        Assert.assertEquals(300.0, vector.currIntensity(), 0.000001);

        //over curr was 158.9 is now 241.68
        Assert.assertTrue(vector.next(500.91));
        Assert.assertEquals(241.68, vector.currMz(), 0.000001);
        Assert.assertEquals(400.0, vector.currIntensity(), 0.000001);

        Assert.assertFalse(vector.next());
    }

    @Test
    public void testEmpty() {

        final double[] mzs = new double[0];
        PeakCursor<PeakAnnotation> vector = new ArrayCursor<PeakAnnotation>(mzs, new double[0], mzs.length);

        vector.resetCursor();
        Assert.assertFalse(vector.next());
        Assert.assertFalse(vector.next(10));
    }

    @Test
    public void testMovePast() {

        final double[] mzs = new double[]{12.5, 52.5, 78.6, 158.9};
        PeakCursor<PeakAnnotation> vector = new ArrayCursor<PeakAnnotation>(mzs, new double[]{100, 200, 300, 400}, mzs.length);

        vector.resetCursor();

        vector.movePast(54);
        Assert.assertEquals(78.6, vector.currMz(), 0.000001);
        vector.movePast(12.5);
        Assert.assertEquals(52.5, vector.currMz(), 0.000001);
        vector.movePast(179);
        Assert.assertEquals(158.9, vector.currMz(), 0.000001);
    }

    @Test
    public void testMoveBefore() {

        final double[] mzs = new double[]{12.5, 52.5, 78.6, 158.9};
        PeakCursor<PeakAnnotation> vector = new ArrayCursor<PeakAnnotation>(mzs, new double[]{100, 200, 300, 400}, mzs.length);

        vector.resetCursor();

        vector.moveBefore(54);
        Assert.assertEquals(52.5, vector.currMz(), 0.000001);

        vector.moveBefore(179);
        Assert.assertEquals(158.9, vector.currMz(), 0.000001);

        vector.moveBefore(78);
        Assert.assertEquals(52.5, vector.currMz(), 0.000001);

        vector.moveBefore(12.5);

        vector.next();
        Assert.assertEquals(12.5, vector.currMz(), 0.000001);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetMzAtIndexOutOfBound() {

        final double[] mzs = new double[]{12.5, 78.6, 158.9};
        PeakCursor<PeakAnnotation> vector = new ArrayCursor<PeakAnnotation>(mzs, new double[]{100, 200, 300}, mzs.length);

        vector.resetCursor();
        Assert.assertEquals(12.5, vector.currMz(), 0.000001);
    }
}
