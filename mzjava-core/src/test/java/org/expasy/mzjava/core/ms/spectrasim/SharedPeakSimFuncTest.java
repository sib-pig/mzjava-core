/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim;

import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class SharedPeakSimFuncTest {

    @Test
    public void test() throws Exception {

        PeakList<PeakAnnotation> peakList1 = new DoublePeakList<PeakAnnotation>();
        peakList1.addSorted(
		        new double[]{592.0813696890917, 610.4389304593608, 819.528204667349, 1008.9731541854521, 1114.2026733872658, 1228.8277418928208, 1250.291877455562, 1458.4947369611507, 1617.97592028064, 1673.0009209859188, 1711.5858053298323, 1900.6064002325252, 1978.9769588099969, 2004.1818947649767, 2121.1340129945756, 2242.9549664843066, 2268.9075118303103, 2297.4721551530297, 2348.226927064944},
		        new double[]{8.0, 11.0, 7.0, 6.0, 21.0, 11.0, 2.0, 15.0, 14.0, 23.0, 4.0, 7.0, 18.0, 1.0, 26.0, 3.0, 24.0, 6.0, 12.0});

        PeakList<PeakAnnotation> peakList2 = new DoublePeakList<PeakAnnotation>();
        peakList2.addSorted(
		        new double[]{532.2181757006471, 592.0813696890917, 610.4389304593608, 819.528204667349, 1008.9731541854521, 1114.2026733872658, 1228.8277418928208, 1250.291877455562, 1458.4947369611507, 1617.97592028064, 1673.0009209859188, 1711.5858053298323, 1900.6064002325252, 1978.9769588099969, 2004.1818947649767, 2121.1340129945756, 2242.9549664843066, 2268.9075118303103, 2297.4721551530297, 2348.226927064944},
		        new double[]{6.0, 9.0, 21.0, 20.0, 1.0, 23.0, 9.0, 1.0, 7.0, 26.0, 12.0, 16.0, 9.0, 5.0, 7.0, 2.0, 9.0, 9.0, 5.0, 28.0});

        SimFunc<PeakAnnotation, PeakAnnotation> simFunc = new SharedPeakSimFunc<PeakAnnotation, PeakAnnotation>(0, new AbsoluteTolerance(0.5));

        double sim = simFunc.calcSimilarity(peakList1, peakList2);

        Assert.assertEquals(0.9743589743589743, sim, 0.000000001);
    }
}
