package org.expasy.mzjava.core.ms.cluster;

import com.google.common.collect.Sets;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.FloatConstantPeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrasim.SimFunc;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.*;

import static org.mockito.Mockito.when;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class SpectrumClusterEvaluatorTest {
    @Test
    public void testSpectrumClusterEvaluator1()
    {
        PeakList<PeakAnnotation> a1 = newPeakList(201.01, 1, "A1");
        PeakList<PeakAnnotation> a2 = newPeakList(201.02, 1, "A2");
        PeakList<PeakAnnotation> a3 = newPeakList(201.03, 1, "A3");

        PeakList<PeakAnnotation> b1 = newPeakList(201.01, 1, "B1");
        PeakList<PeakAnnotation> b2 = newPeakList(201.01, 1, "B2");

        PeakList<PeakAnnotation> c1 = newPeakList(201.02, 1, "C1");

        Tolerance tolerance = new AbsoluteTolerance(0.3);
        SimFunc<PeakAnnotation, PeakAnnotation> simFunc = Mockito.mock(SimFunc.class);

        when(simFunc.calcSimilarity(Mockito.any(PeakList.class), Mockito.any(PeakList.class))).thenReturn(0.0);

        when(simFunc.calcSimilarity(a1, a2)).thenReturn(0.5);
        when(simFunc.calcSimilarity(a2, a1)).thenReturn(0.5);
        when(simFunc.calcSimilarity(a1, a3)).thenReturn(0.6);
        when(simFunc.calcSimilarity(a3, a1)).thenReturn(0.6);
        when(simFunc.calcSimilarity(a2, a3)).thenReturn(0.7);
        when(simFunc.calcSimilarity(a3, a2)).thenReturn(0.7);

        when(simFunc.calcSimilarity(b1, b2)).thenReturn(0.8);
        when(simFunc.calcSimilarity(b1, c1)).thenReturn(0.29);
        when(simFunc.calcSimilarity(b2, c1)).thenReturn(0.1);
        when(simFunc.calcSimilarity(b2, b1)).thenReturn(0.8);
        when(simFunc.calcSimilarity(c1, b1)).thenReturn(0.29);
        when(simFunc.calcSimilarity(c1, b2)).thenReturn(0.1);

        List<PeakList<PeakAnnotation>> querySpectra = Arrays.asList(a1, a2, a3, b1, b2, c1);

        SpectrumClusterEvaluator<PeakAnnotation, PeakList<PeakAnnotation>> evaluator = new SpectrumClusterEvaluator<>(
                tolerance, simFunc, new MSTClusterBuilder<PeakList<PeakAnnotation>>(0.5));

        Set<PeakList<PeakAnnotation>> core = evaluator.evaluate(querySpectra);

        Assert.assertEquals(Sets.newHashSet(a1, a2, a3), core);
        Assert.assertEquals(evaluator.getMinSimScore(),0.5,0.00001);
        Assert.assertEquals(evaluator.getMaxSimScore(),0.7,0.00001);
        Assert.assertEquals(evaluator.getMeanSimScore(),0.6,0.00001);
        Assert.assertEquals(evaluator.getSdSimScore(),0.1,0.00001);
        Assert.assertEquals(evaluator.getNrMembersOrg(),6);
        Assert.assertEquals(evaluator.getNrMembersCleaned(),3);
    }

    @Test
    public void testSpectrumClusterEvaluator2()
    {
        PeakList<PeakAnnotation> a1 = newPeakList(201.01, 1, "A1");
        PeakList<PeakAnnotation> a2 = newPeakList(201.02, 1, "A2");
        PeakList<PeakAnnotation> a3 = newPeakList(201.03, 1, "A3");
        PeakList<PeakAnnotation> a4 = newPeakList(201.04, 1, "A4");
        PeakList<PeakAnnotation> a5 = newPeakList(201.05, 1, "A5");

        PeakList<PeakAnnotation> b1 = newPeakList(201.001, 1, "B1");
        PeakList<PeakAnnotation> b2 = newPeakList(201.002, 1, "B2");
        PeakList<PeakAnnotation> b3 = newPeakList(201.003, 1, "B3");

        Tolerance tolerance = new AbsoluteTolerance(0.3);
        SimFunc<PeakAnnotation, PeakAnnotation> simFunc = Mockito.mock(SimFunc.class);

        when(simFunc.calcSimilarity(Mockito.any(PeakList.class), Mockito.any(PeakList.class))).thenReturn(0.2);

        when(simFunc.calcSimilarity(a1, a2)).thenReturn(0.8);
        when(simFunc.calcSimilarity(a2, a1)).thenReturn(0.8);
        when(simFunc.calcSimilarity(a1, a3)).thenReturn(0.9);
        when(simFunc.calcSimilarity(a3, a1)).thenReturn(0.9);
        when(simFunc.calcSimilarity(a1, a4)).thenReturn(0.85);
        when(simFunc.calcSimilarity(a4, a1)).thenReturn(0.85);
        when(simFunc.calcSimilarity(a1, a5)).thenReturn(0.85);
        when(simFunc.calcSimilarity(a5, a1)).thenReturn(0.85);
        when(simFunc.calcSimilarity(a2, a3)).thenReturn(0.81);
        when(simFunc.calcSimilarity(a3, a2)).thenReturn(0.81);
        when(simFunc.calcSimilarity(a2, a4)).thenReturn(0.79);
        when(simFunc.calcSimilarity(a4, a2)).thenReturn(0.79);
        when(simFunc.calcSimilarity(a2, a5)).thenReturn(0.91);
        when(simFunc.calcSimilarity(a5, a2)).thenReturn(0.91);
        when(simFunc.calcSimilarity(a3, a4)).thenReturn(0.90);
        when(simFunc.calcSimilarity(a4, a3)).thenReturn(0.90);
        when(simFunc.calcSimilarity(a3, a5)).thenReturn(0.95);
        when(simFunc.calcSimilarity(a5, a3)).thenReturn(0.95);
        when(simFunc.calcSimilarity(a4, a5)).thenReturn(0.92);
        when(simFunc.calcSimilarity(a5, a4)).thenReturn(0.92);


        when(simFunc.calcSimilarity(b1, b2)).thenReturn(0.7);
        when(simFunc.calcSimilarity(b1, b3)).thenReturn(0.71);
        when(simFunc.calcSimilarity(b2, b3)).thenReturn(0.61);
        when(simFunc.calcSimilarity(b2, b1)).thenReturn(0.7);
        when(simFunc.calcSimilarity(b3, b1)).thenReturn(0.73);
        when(simFunc.calcSimilarity(b3, b2)).thenReturn(0.67);


        List<PeakList<PeakAnnotation>> querySpectra = Arrays.asList(a1, a2, a3, a4, a5, b1, b2, b3);

        SpectrumClusterEvaluator<PeakAnnotation, PeakList<PeakAnnotation>> evaluator = new SpectrumClusterEvaluator<>(
                tolerance, simFunc, new MSTClusterBuilder<PeakList<PeakAnnotation>>(0.5));

        Set<PeakList<PeakAnnotation>> core = evaluator.evaluate(querySpectra);

        Assert.assertEquals(Sets.newHashSet(a1, a2, a3, a4, a5), core);
        Assert.assertEquals(evaluator.getMinSimScore(),0.79,0.00001);
        Assert.assertEquals(evaluator.getMaxSimScore(),0.95,0.00001);
        double mean = (0.8+0.9+0.85+0.85+0.81+0.79+0.91+0.90+0.95+0.92)/10;
        Assert.assertEquals(evaluator.getMeanSimScore(),mean,0.00001);
        double sd = (0.8-mean)*(0.8-mean)+(0.9-mean)*(0.9-mean)+(0.85-mean)*(0.85-mean)+(0.85-mean)*(0.85-mean)+(0.81-mean)*(0.81-mean)+(0.79-mean)*(0.79-mean)+(0.91-mean)*(0.91-mean)+(0.9-mean)*(0.9-mean)+(0.95-mean)*(0.95-mean)+(0.92-mean)*(0.92-mean);
        sd = Math.sqrt(sd/9);
        Assert.assertEquals(evaluator.getSdSimScore(),sd,0.00001);
        Assert.assertEquals(evaluator.getNrMembersOrg(),8);
        Assert.assertEquals(evaluator.getNrMembersCleaned(),5);

    }

    @Test
    public void testSpectrumClusterEvaluator3()
    {
        PeakList<PeakAnnotation> a1 = newPeakList(201.01, 1, "A1");
        Tolerance tolerance = new AbsoluteTolerance(0.3);
        SimFunc<PeakAnnotation, PeakAnnotation> simFunc = Mockito.mock(SimFunc.class);

        when(simFunc.calcSimilarity(Mockito.any(PeakList.class), Mockito.any(PeakList.class))).thenReturn(0.2);


        List<PeakList<PeakAnnotation>> querySpectra = Arrays.asList(a1);

        SpectrumClusterEvaluator<PeakAnnotation, PeakList<PeakAnnotation>> evaluator = new SpectrumClusterEvaluator<>(
                tolerance,
                simFunc,
                new MSTClusterBuilder<PeakList<PeakAnnotation>>(0.5)
        );

        Set<PeakList<PeakAnnotation>> core = evaluator.evaluate(querySpectra);

        Assert.assertEquals(Collections.singleton(a1), core);
        Assert.assertEquals(evaluator.getMinSimScore(),0.0,0.00001);
        Assert.assertEquals(evaluator.getMaxSimScore(),0.0,0.00001);
        Assert.assertEquals(evaluator.getMeanSimScore(),0.0,0.00001);
        Assert.assertEquals(evaluator.getSdSimScore(),0.0,0.00001);
        Assert.assertEquals(evaluator.getNrMembersOrg(),1);
        Assert.assertEquals(evaluator.getNrMembersCleaned(),1);

    }

    private PeakList<PeakAnnotation> newPeakList(double mz, int charge, String idPrefix) {

        UUID id = UUID.fromString(idPrefix + "000000-0000-0000-0000-000000000000");

        PeakList<PeakAnnotation> peakList = new FloatConstantPeakList<PeakAnnotation>(1);
        peakList.setId(id);
        peakList.getPrecursor().setMzAndCharge(mz, charge);

        return peakList;
    }

}
