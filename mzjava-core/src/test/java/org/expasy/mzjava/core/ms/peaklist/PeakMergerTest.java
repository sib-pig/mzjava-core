package org.expasy.mzjava.core.ms.peaklist;

import cern.jet.random.Binomial;
import cern.jet.random.Uniform;
import cern.jet.random.engine.MersenneTwister;
import cern.jet.random.sampling.RandomSampler;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.AbstractMergePeakFilter;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.LibraryMergePeakFilter;
import org.expasy.mzjava.core.ms.spectrum.LibPeakAnnotation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

/**
 * Class to test MergePeakFilter
 *
 * @author Markus Muller
 * @version 0.0
 */
public class PeakMergerTest {
    double[] masses =
            new double[]{995.465, 997.549, 1002.213, 1009.445, 1012.809, 1015.269, 1023.958, 1024.746, 1032.158, 1038.543,
                    1049.680, 1055.237, 1056.236, 1061.347, 1063.269, 1069.886, 1074.268, 1075.122, 1080.235, 1088.610,
                    1094.540, 1096.541, 1102.804, 1107.303, 1112.495, 1117.861, 1120.826, 1125.643, 1129.048, 1136.221,
                    1138.760, 1141.752, 1149.159, 1154.643, 1167.231, 1168.390, 1172.392, 1177.803, 1185.206, 1191.259,
                    1192.455, 1199.366, 1209.212, 1210.209, 1216.670};
    double[] intensities =
            new double[]{18.288, 10.924, 6.442, 17.300, 17.788, 18.947, 21.898, 11.378, 22.440, 10.302,
                    50.827, 17.054, 22.280, 11.892, 7.995, 10.341, 215.683, 88.473, 18.228, 13.208,
                    3.964, 5.049, 2.980, 1.780, 13.860, 5.808, 5.583, 2.364, 1.033, 13.762,
                    21.549, 5.541, 14.566, 19.965, 239.495, 40.558, 21.821, 9.397, 1820.518, 630.056,
                    23.832, 3.843, 6.159, 383.467, 58.726};

    double[] masses2 = new double[masses.length];
    double[] masses3 = new double[masses.length];

    double[] masses4 =
            new double[]{99.998, 99.999, 100.0, 100.001, 100.002, 100.003, 100.05, 100.098, 100.099, 100.1, 100.101, 100.102};
    double[] intensities4 =
            new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};

    @Before
    public void init() {

        for (int i = 0; i < masses.length; i++) {
            masses2[i] = masses[i] + 0.01;
            masses3[i] = masses[i] - 0.01;
        }
    }

    @Test
    public void test() {

        PeakList<LibPeakAnnotation> consPeakList = new DoublePeakList<>();
        consPeakList.addSorted(masses, intensities, masses.length);
        consPeakList.addSorted(masses2, intensities, masses.length);
        consPeakList.addSorted(masses3, intensities, masses.length);

        LibraryMergePeakFilter<LibPeakAnnotation> filterMerge = new LibraryMergePeakFilter<>(0.2,0.5, AbstractMergePeakFilter.IntensityMode.MEAN_POS_INTENSITY,3);
        consPeakList.apply(filterMerge);

        final double[] ms = consPeakList.getMzs(null);
        final double[] ints = consPeakList.getIntensities(null);

        double std = Math.sqrt(2 * 0.01 * 0.01 / 3);
        LibPeakAnnotation annot;
        for (int i = 0; i < masses.length; i++) {

            Assert.assertEquals(masses[i], ms[i], 0.0001);
            Assert.assertEquals(intensities[i], ints[i], 0.0001);
            annot = consPeakList.getAnnotations(i).get(0);
            Assert.assertEquals(annot.getMergedPeakCount(), 3);
            Assert.assertEquals(annot.getMzStd(), std, 0.0001);
            Assert.assertEquals(annot.getIntensityStd(), 0.0, 0.0001);
        }
    }

    @Test
    public void test2() {
        PeakList<LibPeakAnnotation> consPeakList = new DoublePeakList<>();
        consPeakList.addSorted(masses4, intensities4, masses4.length);

        LibraryMergePeakFilter<LibPeakAnnotation> filterMerge = new LibraryMergePeakFilter<>(0.05,0.1, AbstractMergePeakFilter.IntensityMode.MEAN_POS_INTENSITY,1);
        consPeakList.apply(filterMerge);
        final double[] ms = consPeakList.getMzs(null);

        Assert.assertTrue(ms.length == 2);
        Assert.assertEquals(100.0, ms[0], 0.01);
        Assert.assertEquals(100.1, ms[1], 0.01);
    }


    @Test
    public void test3() {
        PeakList<LibPeakAnnotation> consPeakList = new DoublePeakList<>();
        consPeakList.addSorted(masses, intensities, masses.length);

        long[] idx = new long[masses.length];
        double[] massBuf = new double[masses.length];
        double[] intBuf = new double[masses.length];
        MersenneTwister rd = new MersenneTwister();
        Binomial brd = new Binomial(masses.length, 0.8, rd);
        Uniform nrd = new Uniform(-0.3, 0.3, rd);
        for (int i = 0; i < 100; i++) {
            int nrIdx = brd.nextInt();
            RandomSampler.sample(nrIdx, masses.length, nrIdx, 0, idx, 0, rd);
            for (int j = 0; j < nrIdx; j++) {
                int k = (int) idx[j];
                massBuf[j] = masses[k] + nrd.nextDouble();
                intBuf[j] = intensities[k] * (1.0 + nrd.nextDouble());
            }

            consPeakList.addSorted(massBuf, intBuf, nrIdx);
        }

        LibraryMergePeakFilter<LibPeakAnnotation> filterMerge = new LibraryMergePeakFilter<>(0.3,0.6, AbstractMergePeakFilter.IntensityMode.MEAN_POS_INTENSITY,100);
        consPeakList.apply(filterMerge);
        final double[] ms = consPeakList.getMzs(null);
        final double[] ints = consPeakList.getIntensities(null);

        Assert.assertTrue(ms.length == masses.length);
        for (int i = 0; i < masses.length; i++) {
            Assert.assertEquals(masses[i], ms[i], 0.1);
            Assert.assertEquals(intensities[i], ints[i], intensities[i] * 0.31);
        }
    }

    @Test
    public void test4() {
        PeakList<LibPeakAnnotation> consPeakList = new DoublePeakList<>();

        double[] massBuf = new double[20];
        double[] intBuf = new double[20];
        MersenneTwister rd = new MersenneTwister();
        Uniform nrd = new Uniform(-0.3, 0.3, rd);
        for (int j = 0; j < 5; j++) {
            for (int i = 0; i < 20; i++) {
                massBuf[i] = 1000.0 + j + nrd.nextDouble();
                intBuf[i] = intensities[i] * (1.0 + nrd.nextDouble());
            }
            Arrays.sort(massBuf);
            consPeakList.addSorted(massBuf, intBuf, 20);
            consPeakList.add(1000.0 + j + 0.5, 100.0);
        }

        LibraryMergePeakFilter<LibPeakAnnotation> filterMerge = new LibraryMergePeakFilter<>(0.3,0.6, AbstractMergePeakFilter.IntensityMode.MEAN_POS_INTENSITY,100);
        consPeakList.apply(filterMerge);
        final double[] ms = consPeakList.getMzs(null);

        Assert.assertTrue(ms.length == 5);
        for (int i = 0; i < 5; i++) {
            Assert.assertEquals(1000.0 + i, ms[i], 0.3);
        }
    }

    @Test
    public void test5() {
        PeakList<LibPeakAnnotation> consPeakList = new DoublePeakList<>();

        LibraryMergePeakFilter<LibPeakAnnotation> filterMerge = new LibraryMergePeakFilter<>(0.3,0.6, AbstractMergePeakFilter.IntensityMode.MEAN_POS_INTENSITY,100);
        consPeakList.apply(filterMerge);
        final double[] ms = consPeakList.getMzs(null);
        final double[] ints = consPeakList.getIntensities(null);

        Assert.assertTrue(ms.length == 0);
        Assert.assertTrue(ints.length == 0);
    }
}
