/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.peaklist;

import gnu.trove.list.TDoubleList;
import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.IdentityPeakProcessor;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PeakProcessorChainTest {

    @SuppressWarnings("unchecked")
    @Test
    public void testInitialize() throws Exception {

        PeakProcessorChain<PeakAnnotation> peakProcessorChain = new PeakProcessorChain<PeakAnnotation>();

        PeakProcessor<PeakAnnotation, PeakAnnotation> processor1 = mock(PeakProcessor.class);
        PeakProcessor<PeakAnnotation, PeakAnnotation> processor2 = mock(PeakProcessor.class);

        peakProcessorChain.add(processor1).add(processor2);

        PeakList<PeakAnnotation> peakList1 = mock(PeakList.class);
        when(peakList1.size()).thenReturn(12);

        PeakSink<PeakAnnotation> sink1 = mock(PeakSink.class);
        peakProcessorChain.process(peakList1, sink1);

        verify(processor1).setSink(processor2);
        verify(processor2).setSink(sink1);
        verify(processor1).start(12);
        verify(processor1, times(12)).processPeak(0, 0, Collections.<PeakAnnotation>emptyList());
        verify(processor1).end();

        PeakList<PeakAnnotation> peakList2 = mock(PeakList.class);
        when(peakList2.size()).thenReturn(13);

        reset(processor1);
        PeakSink<PeakAnnotation> sink2 = mock(PeakSink.class);
        peakProcessorChain.process(peakList2, sink2);
        verify(processor1).setSink(processor2);
        verify(processor2).setSink(sink2);
        verify(processor1).start(13);
        verify(processor1, times(13)).processPeak(0, 0, Collections.<PeakAnnotation>emptyList());
        verify(processor1).end();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testSingletonProcessor() throws Exception {

        PeakProcessorChain<PeakAnnotation> peakProcessorChain = new PeakProcessorChain<PeakAnnotation>();

        PeakProcessor<PeakAnnotation, PeakAnnotation> processor = mock(PeakProcessor.class);
        peakProcessorChain.add(processor);

        PeakList<PeakAnnotation> peakList = mock(PeakList.class);
        when(peakList.size()).thenReturn(12);

        PeakSink<PeakAnnotation> sink = mock(PeakSink.class);
        peakProcessorChain.process(peakList, sink);
        verify(processor).setSink(sink);
        verify(processor).start(12);
        verify(processor, times(12)).processPeak(0, 0, Collections.<PeakAnnotation>emptyList());
        verify(processor).end();
    }

    @Test
    public void testEmptyList() throws Exception {

        PeakProcessorChain<PeakAnnotation> peakProcessorChain = new PeakProcessorChain<PeakAnnotation>();
        Assert.assertEquals(true, peakProcessorChain.isEmpty());

        @SuppressWarnings("unchecked") //Mockito can't mock generics
        PeakList<PeakAnnotation> peakList = mock(PeakList.class);
        when(peakList.size()).thenReturn(12);

        @SuppressWarnings("unchecked") //Mockito can't mock generics
        PeakSink<PeakAnnotation> sink = mock(PeakSink.class);

        peakProcessorChain.process(peakList, sink);

        verify(sink).start(12);
        verify(sink, times(12)).processPeak(0, 0, Collections.<PeakAnnotation>emptyList());
        verify(sink).end();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testSharedProcessor() throws Exception {

        PeakProcessor<PeakAnnotation, PeakAnnotation> processor1 = new IdentityPeakProcessor<PeakAnnotation>();
        PeakProcessor<PeakAnnotation, PeakAnnotation> processor2 = new IdentityPeakProcessor<PeakAnnotation>();
        PeakProcessor<PeakAnnotation, PeakAnnotation> processor3 = new IdentityPeakProcessor<PeakAnnotation>();

        PeakProcessorChain<PeakAnnotation> chain1 = new PeakProcessorChain<PeakAnnotation>();
        PeakProcessorChain<PeakAnnotation> chain2 = new PeakProcessorChain<PeakAnnotation>();

        chain1.add(processor1).add(processor2);
        chain2.add(processor1).add(processor3);

        PeakList<PeakAnnotation> peaks = mock(PeakList.class);
        when(peaks.size()).thenReturn(1);
        when(peaks.getMz(0)).thenReturn(10.68);
        when(peaks.getIntensity(0)).thenReturn(12.0);

        PeakSink<PeakAnnotation> sink1 = mock(PeakSink.class);
        PeakSink<PeakAnnotation> sink2 = mock(PeakSink.class);

        chain1.process(peaks, sink1);

        chain2.process(peaks, sink2);

        verify(sink1).start(1);
        verify(sink1).processPeak(10.68, 12.0, Collections.<PeakAnnotation>emptyList());
        verify(sink1).end();

        verify(sink2).start(1);
        verify(sink2).processPeak(10.68, 12.0, Collections.<PeakAnnotation>emptyList());
        verify(sink2).end();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testProcessTList() throws Exception {

        PeakProcessor<PeakAnnotation, PeakAnnotation> peakProcessor = mock(PeakProcessor.class);
        PeakSink<PeakAnnotation> peakSink = mock(PeakSink.class);

        TDoubleList mzs =         new TDoubleArrayList(new double[]{1, 2, 3});
        TDoubleList intensities = new TDoubleArrayList(new double[]{10, 20, 30});
        PeakAnnotation annotation = mock(PeakAnnotation.class);
        TIntObjectMap<List<PeakAnnotation>> annotationMap = new TIntObjectHashMap<List<PeakAnnotation>>();
        annotationMap.put(1, Arrays.asList(annotation));

        PeakProcessorChain<PeakAnnotation> processorChain = new PeakProcessorChain<PeakAnnotation>()
                .add(peakProcessor);
        processorChain.process(mzs, intensities, annotationMap, peakSink);

        verify(peakProcessor).setSink(peakSink);
        verify(peakProcessor).start(3);
        verify(peakProcessor).processPeak(1, 10, Collections.<PeakAnnotation>emptyList());
        verify(peakProcessor).processPeak(2, 20, Arrays.asList(annotation));
        verify(peakProcessor).processPeak(3, 30, Collections.<PeakAnnotation>emptyList());
        verify(peakProcessor).end();
        verifyNoMoreInteractions(peakProcessor);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testProcessNumber() throws Exception {

        PeakProcessor<PeakAnnotation, PeakAnnotation> peakProcessor = mock(PeakProcessor.class);
        PeakSink<PeakAnnotation> peakSink = mock(PeakSink.class);

        Number[] mzs =         new Number[]{1, 2, 3};
        Number[] intensities = new Number[]{10, 20, 30};
        PeakAnnotation annotation = mock(PeakAnnotation.class);
        TIntObjectMap<List<PeakAnnotation>> annotationMap = new TIntObjectHashMap<List<PeakAnnotation>>();
        annotationMap.put(1, Arrays.asList(annotation));

        PeakProcessorChain<PeakAnnotation> processorChain = new PeakProcessorChain<PeakAnnotation>(peakProcessor);
        Assert.assertEquals(false, processorChain.isEmpty());

        processorChain.process(mzs, intensities, annotationMap, peakSink);

        verify(peakProcessor).setSink(peakSink);
        verify(peakProcessor).start(3);
        verify(peakProcessor).processPeak(1, 10, Collections.<PeakAnnotation>emptyList());
        verify(peakProcessor).processPeak(2, 20, Arrays.asList(annotation));
        verify(peakProcessor).processPeak(3, 30, Collections.<PeakAnnotation>emptyList());
        verify(peakProcessor).end();
        verifyNoMoreInteractions(peakProcessor);
    }
}
