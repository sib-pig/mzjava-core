/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrum;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class RetentionTimeListTest {

    @Test
    public void testCopy() throws Exception {

        final RetentionTimeDiscrete rt1 = new RetentionTimeDiscrete(12, TimeUnit.SECOND);
        final RetentionTimeDiscrete rt2 = new RetentionTimeDiscrete(56, TimeUnit.SECOND);
        final RetentionTimeDiscrete rt3 = new RetentionTimeDiscrete(876, TimeUnit.SECOND);

        RetentionTimeList retentionTime = new RetentionTimeList();
        retentionTime.add(rt1);
        retentionTime.add(rt2);
        retentionTime.add(rt3);

        RetentionTimeList copy = retentionTime.copy();
        assertEquals(12, copy.get(0).getTime(), 0.0000001);
        assertEquals(56, copy.get(1).getTime(), 0.0000001);
        assertEquals(876, copy.get(2).getTime(), 0.0000001);

        assertNotSame(rt1, copy.get(0));
        assertNotSame(rt2, copy.get(1));
        assertNotSame(rt3, copy.get(2));

        assertTrue(retentionTime.equals(copy));
        assertEquals(retentionTime.hashCode(), copy.hashCode());
    }

    @Test
    public void testContains() throws Exception {

        RetentionTimeList retentionTimeList = new RetentionTimeList();

        retentionTimeList.add(1, 56, TimeUnit.SECOND);

        assertEquals(true, retentionTimeList.contains(new RetentionTimeDiscrete(5, TimeUnit.SECOND)));
        assertEquals(false, retentionTimeList.contains(new RetentionTimeDiscrete(600, TimeUnit.SECOND)));
        assertEquals(true, retentionTimeList.contains(new RetentionTimeInterval(1, 56, TimeUnit.SECOND)));
        assertEquals(false, retentionTimeList.contains(new RetentionTimeInterval(40, 80, TimeUnit.SECOND)));
        assertEquals(false, retentionTimeList.contains(new RetentionTimeInterval(0, 500, TimeUnit.SECOND)));
    }
}
