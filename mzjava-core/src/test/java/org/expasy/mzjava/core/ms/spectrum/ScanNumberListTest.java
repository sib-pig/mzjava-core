/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrum;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class ScanNumberListTest {

    @Test
    public void testCopy() throws Exception {

        final ScanNumber rt1 = new ScanNumberDiscrete(12);
        final ScanNumber rt2 = new ScanNumberDiscrete(56);
        final ScanNumber rt3 = new ScanNumberDiscrete(876);

        ScanNumberList retentionTime = new ScanNumberList();
        retentionTime.add(rt1);
        retentionTime.add(rt2);
        retentionTime.add(rt3);

        ScanNumberList copy = retentionTime.copy();
        assertEquals(12, copy.get(0).getValue(), 0.0000001);
        assertEquals(56, copy.get(1).getValue(), 0.0000001);
        assertEquals(876, copy.get(2).getValue(), 0.0000001);

        Assert.assertTrue(retentionTime.equals(copy));
        assertEquals(retentionTime.hashCode(), copy.hashCode());
    }

    @Test
    public void testContains() throws Exception {

        ScanNumberList list = new ScanNumberList();
        list.add(40, 67);

        assertEquals(true, list.contains(new ScanNumberDiscrete(50)));
        assertEquals(false, list.contains(new ScanNumberDiscrete(30)));
        assertEquals(false, list.contains(new ScanNumberDiscrete(70)));

        assertEquals(true, list.contains(new ScanNumberInterval(45, 50)));
        assertEquals(false, list.contains(new ScanNumberInterval(3, 50)));
        assertEquals(false, list.contains(new ScanNumberInterval(60, 80)));
        assertEquals(false, list.contains(new ScanNumberInterval(30, 80)));
    }
}
