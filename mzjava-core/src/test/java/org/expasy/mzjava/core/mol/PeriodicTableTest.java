/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.mol;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PeriodicTableTest {

    @Test
    public void testFlyweight() throws Exception {

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        Assert.assertSame(periodicTable.getAtom(AtomicSymbol.H, 1), periodicTable.getAtom(AtomicSymbol.H, 1));
        Assert.assertSame(periodicTable.getAtom(AtomicSymbol.H, 2), periodicTable.getAtom(AtomicSymbol.H, 2));
        Assert.assertNotSame(periodicTable.getAtom(AtomicSymbol.H, 1), periodicTable.getAtom(AtomicSymbol.H, 2));
    }

    @Test
    public void testFlyweight2() throws Exception {

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        Atom hOne = periodicTable.getAtom(AtomicSymbol.H, 1);
        Atom hTwo = periodicTable.getAtom(AtomicSymbol.H);

        Assert.assertSame(hOne,  hTwo);
    }

    @Test
    public void getCarbon() throws ParseException {

        Assert.assertEquals(AtomicSymbol.C, PeriodicTable.getInstance().getAtom("C").getSymbol());
    }

    @Test
    public void testGetCarbon14() throws ParseException {

        Atom c14 = PeriodicTable.getInstance().getAtom(AtomicSymbol.valueOf("C"), 14);

        Assert.assertEquals(AtomicSymbol.C, c14.getSymbol());
        Assert.assertEquals(14, c14.getMassNumber());
        Assert.assertEquals(14.003241988, c14.getMass(), 0.0000000001);
    }

    @Test
    public void testHashSet() throws ParseException {

        Set<Atom> atoms = new HashSet<Atom>();
        atoms.add(PeriodicTable.getInstance().getAtom(AtomicSymbol.valueOf("C"), 12));
        atoms.add(PeriodicTable.getInstance().getAtom(AtomicSymbol.valueOf("C"), 13));
        atoms.add(PeriodicTable.getInstance().getAtom(AtomicSymbol.valueOf("C"), 14));
        atoms.add(PeriodicTable.getInstance().getAtom("C[14]"));
        atoms.add(PeriodicTable.getInstance().getAtom("C"));

        Assert.assertEquals(3, atoms.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetBadFormatAtom() throws ParseException {

        PeriodicTable.getInstance().getAtom("C(13)");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidAtom() throws Exception {

        PeriodicTable.getInstance().getAtom(AtomicSymbol.H, 12);
    }

    @Test
    public void testGetAtoms() throws Exception {

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        List<Atom> atoms = periodicTable.getAtoms(AtomicSymbol.Ca);

        Assert.assertEquals(6, atoms.size());

        //Check that atoms are sorted by mass
        double mass = 0;
        for(Atom atom : atoms) {

            Assert.assertTrue("expecting atoms to be sorted", mass < atom.getMass());
            mass = atom.getMass();
        }
    }

    @Test
    public void testToString1() {

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        Atom atom = periodicTable.getAtom("C[12]");

        Assert.assertEquals("C", atom.toString());
    }

    @Test
    public void testToString2() {

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        Atom atom = periodicTable.getAtom("C[13]");

        Assert.assertEquals("C[13]", atom.toString());
    }

    @Test
    public void testIsotope() throws Exception {

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        Atom atom = periodicTable.getAtom("C[13]");
        Assert.assertEquals(AtomicSymbol.C, atom.getSymbol());
        Assert.assertEquals(13, atom.getMassNumber());
    }

    @Test
    public void testIsotope2() throws Exception {

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        Atom atom = periodicTable.getAtom("C[13]");
        Assert.assertEquals(AtomicSymbol.C, atom.getSymbol());
        Assert.assertEquals(13, atom.getMassNumber());
    }

    @Test
    public void testIssue43() {

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        Atom atom = periodicTable.getAtom("C[13]");

        Atom atom2 = periodicTable.getAtom(atom.toString());

        Assert.assertSame(atom, atom2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalFormat() throws Exception {

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        periodicTable.getAtom("C[1a]");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalFormat2() throws Exception {

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        periodicTable.getAtom("C[14");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalFormat3() throws Exception {

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        periodicTable.getAtom("C14]");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalFormat5() throws Exception {

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        periodicTable.getAtom("C14[]");
    }
}
