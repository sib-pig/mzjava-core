package org.expasy.mzjava.core.ms.peaklist.peaktransformer;

import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Markus
 * @version 1.0
 */
public class HighestPeakPerBinNormalizerTest {
    double[] masses =
            new double[]{1, 2, 3, 4,  5, 6, 7, 8, 9,  20,  21,  22,   23,  24,  25,  26};
    double[] intensities =
            new double[]{2, 4, 6, 8, 10, 1, 3, 5, 7, 9.5, 6.5, 8.5, 10.5, 1.5, 3.5, 5.5};

    @Test
    public void localMaxIntensNorm() {

        HighestPeakPerBinNormalizer<PeakAnnotation> normalizer = new HighestPeakPerBinNormalizer<PeakAnnotation>(5.0,50.0);
        DoublePeakList<PeakAnnotation> expPeakList = new DoublePeakList<PeakAnnotation>();
        expPeakList.addSorted(masses, intensities, masses.length);

        expPeakList.apply(normalizer);

        double[] mvAvgIntensities =
                new double[]{50.0/8.0*2, 50.0/8.0*4, 50.0/8.0*6, 50.0, 50.0, 50.0/10.0*1, 50.0/10.0*3, 50.0/10.0*5, 50.0/10.0*7,
                        50.0/10.5*9.5, 50.0/10.5*6.5, 50.0/10.5*8.5, 50.0, 50.0/10.5*1.5, 50.0/5.5*3.5, 50.0};

        double[] smoothedInt = expPeakList.getIntensities(null);

        Assert.assertArrayEquals(mvAvgIntensities, smoothedInt, 0.0001);
    }

}
