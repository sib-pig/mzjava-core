/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.peaklist;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PeakListTest {

    @Test
    public void testIndexOf() throws Exception {

        List<PeakList<PeakAnnotation>> peakLists = build(new double[]{1, 2, 3, 4, 5, 6});

        for (PeakList<PeakAnnotation> peakList : peakLists) {

            Assert.assertEquals(peakList.getPrecision().toString(), 0, peakList.indexOf(1));
            Assert.assertEquals(peakList.getPrecision().toString(), 5, peakList.indexOf(6));
        }
    }

    private List<PeakList<PeakAnnotation>> build(double[] mzs) {

        double[] intensities = new double[mzs.length];
        Arrays.fill(intensities, 1);

        List<PeakList<PeakAnnotation>> peakLists = new ArrayList<PeakList<PeakAnnotation>>();

        for(PeakList.Precision precision : PeakList.Precision.values()) {

            peakLists.add(PeakListFactory.newPeakList(precision, mzs, intensities, mzs.length));
        }

        return peakLists;
    }
}
