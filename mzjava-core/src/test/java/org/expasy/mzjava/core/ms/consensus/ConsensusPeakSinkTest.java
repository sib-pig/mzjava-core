package org.expasy.mzjava.core.ms.consensus;

import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.LibPeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.UUID;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class ConsensusPeakSinkTest {

    @Test
    public void testConsensusSpectrumPeakFilter(){
        ConsensusSpectrum<LibPeakAnnotation> consensus = new ConsensusSpectrum<>(100, PeakList.Precision.DOUBLE,new HashSet<UUID>());

        ConsensusPeakSink<LibPeakAnnotation> filter = new ConsensusPeakSink<>(consensus,0.2,10);
        for (int i=0;i<100;i++) {
            filter.processPeak(100.0+i*10.0,10.0, Collections.singletonList(new LibPeakAnnotation(i, 1.0, 1.0)) );
        }

        Assert.assertEquals(consensus.size(),90);

    }
}
