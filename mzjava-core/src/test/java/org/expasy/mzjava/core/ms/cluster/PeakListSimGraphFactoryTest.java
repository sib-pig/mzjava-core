package org.expasy.mzjava.core.ms.cluster;

import com.google.common.base.Optional;
import com.google.common.collect.Sets;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.FloatConstantPeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrasim.SimFunc;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.mockito.Mockito.when;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeakListSimGraphFactoryTest {

    /**
     * A1--0.5--A2      B1--0.8--B2
     *  \      /         .      .
     *  0.6  0.7        0.29  0.1
     *   \  /              . .
     *    A3               C1
     *
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testThreshold0() throws Exception {

        PeakList<PeakAnnotation> a1 = newPeakList(201.01, 1, "A1");
        PeakList<PeakAnnotation> a2 = newPeakList(201.02, 1, "A2");
        PeakList<PeakAnnotation> a3 = newPeakList(201.03, 1, "A3");

        PeakList<PeakAnnotation> b1 = newPeakList(202.01, 1, "B1");
        PeakList<PeakAnnotation> b2 = newPeakList(202.01, 1, "B2");

        PeakList<PeakAnnotation> c1 = newPeakList(202.02, 1, "C1");

        Tolerance tolerance = new AbsoluteTolerance(0.3);
        SimFunc<PeakAnnotation, PeakAnnotation> simFunc = Mockito.mock(SimFunc.class);

        when(simFunc.calcSimilarity(Mockito.any(PeakList.class), Mockito.any(PeakList.class))).thenReturn(1.0);

        when(simFunc.calcSimilarity(a1, a2)).thenReturn(0.5);
        when(simFunc.calcSimilarity(a1, a3)).thenReturn(0.6);
        when(simFunc.calcSimilarity(a2, a3)).thenReturn(0.7);

        when(simFunc.calcSimilarity(b1, b2)).thenReturn(0.8);
        when(simFunc.calcSimilarity(b1, c1)).thenReturn(0.29);
        when(simFunc.calcSimilarity(b2, c1)).thenReturn(0.1);

        List<PeakList<PeakAnnotation>> querySpectra = Arrays.asList(a1, a2, a3, b1, b2, c1);

        SimilarityGraph<PeakList<PeakAnnotation>> simGraph = PeakListSimGraphFactory.build(querySpectra, simFunc, tolerance, 0);

        Set<SimEdge<PeakList<PeakAnnotation>>> edges = Sets.newHashSet(simGraph.getEdges());
        Assert.assertEquals(6, edges.size());

        Assert.assertEquals(0.5, simGraph.findEdge(a1, a2).get().getScore(), 0.000001);
        Assert.assertEquals(0.6, simGraph.findEdge(a1, a3).get().getScore(), 0.000001);
        Assert.assertEquals(0.7, simGraph.findEdge(a2, a3).get().getScore(), 0.000001);

        Assert.assertEquals(0.8, simGraph.findEdge(b1, b2).get().getScore(), 0.000001);
        Assert.assertEquals(0.29, simGraph.findEdge(b1, c1).get().getScore(), 0.000001);
        Assert.assertEquals(0.1, simGraph.findEdge(b2, c1).get().getScore(), 0.000001);
    }

    /**
     * A1--0.5--A2      B1--0.8--B2
     *  \      /         .      .
     *  0.6  0.7        0.29  0.1
     *   \  /              . .
     *    A3               C1
     *
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testThreshold0_3() throws Exception {

        PeakList<PeakAnnotation> a1 = newPeakList(201.01, 1, "A1");
        PeakList<PeakAnnotation> a2 = newPeakList(201.02, 1, "A2");
        PeakList<PeakAnnotation> a3 = newPeakList(201.03, 1, "A3");

        PeakList<PeakAnnotation> b1 = newPeakList(202.01, 1, "B1");
        PeakList<PeakAnnotation> b2 = newPeakList(202.01, 1, "B2");

        PeakList<PeakAnnotation> c1 = newPeakList(202.02, 1, "C1");

        Tolerance tolerance = new AbsoluteTolerance(0.3);
        SimFunc<PeakAnnotation, PeakAnnotation> simFunc = Mockito.mock(SimFunc.class);

        when(simFunc.calcSimilarity(Mockito.any(PeakList.class), Mockito.any(PeakList.class))).thenReturn(1.0);

        when(simFunc.calcSimilarity(a1, a2)).thenReturn(0.5);
        when(simFunc.calcSimilarity(a1, a3)).thenReturn(0.6);
        when(simFunc.calcSimilarity(a2, a3)).thenReturn(0.7);

        when(simFunc.calcSimilarity(b1, b2)).thenReturn(0.8);
        when(simFunc.calcSimilarity(b1, c1)).thenReturn(0.29);
        when(simFunc.calcSimilarity(b2, c1)).thenReturn(0.1);

        List<PeakList<PeakAnnotation>> querySpectra = Arrays.asList(a1, a2, a3, b1, b2, c1);

        SimilarityGraph<PeakList<PeakAnnotation>> simGraph = PeakListSimGraphFactory.build(querySpectra, simFunc, tolerance, 0.3);

        Set<SimEdge<PeakList<PeakAnnotation>>> edges = Sets.newHashSet(simGraph.getEdges());
        Assert.assertEquals(4, edges.size());

        Assert.assertEquals(0.5, simGraph.findEdge(a1, a2).get().getScore(), 0.000001);
        Assert.assertEquals(0.6, simGraph.findEdge(a1, a3).get().getScore(), 0.000001);
        Assert.assertEquals(0.7, simGraph.findEdge(a2, a3).get().getScore(), 0.000001);

        Assert.assertEquals(0.8, simGraph.findEdge(b1, b2).get().getScore(), 0.000001);
    }

    /**
     * A nodes are +1, b and c nodes +2
     *
     * A1--0.5--A2      B1--0.8--B2
     *  \      /         .      .
     *  0.6  0.7        0.29  0.1
     *   \  /              . .
     *    A3               C1
     *
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testCharge() throws Exception {

        PeakList<PeakAnnotation> a1 = newPeakList(201.01, 1, "A1");
        PeakList<PeakAnnotation> a2 = newPeakList(201.02, 1, "A2");
        PeakList<PeakAnnotation> a3 = newPeakList(201.03, 1, "A3");

        PeakList<PeakAnnotation> b1 = newPeakList(201.01, 2, "B1");
        PeakList<PeakAnnotation> b2 = newPeakList(201.02, 2, "B2");

        PeakList<PeakAnnotation> c1 = newPeakList(201.03, 2, "C1");

        Tolerance tolerance = new AbsoluteTolerance(0.3);
        SimFunc<PeakAnnotation, PeakAnnotation> simFunc = Mockito.mock(SimFunc.class);

        when(simFunc.calcSimilarity(Mockito.any(PeakList.class), Mockito.any(PeakList.class))).thenReturn(1.0);

        when(simFunc.calcSimilarity(a1, a2)).thenReturn(0.5);
        when(simFunc.calcSimilarity(a1, a3)).thenReturn(0.6);
        when(simFunc.calcSimilarity(a2, a3)).thenReturn(0.7);

        when(simFunc.calcSimilarity(b1, b2)).thenReturn(0.8);
        when(simFunc.calcSimilarity(b1, c1)).thenReturn(0.29);
        when(simFunc.calcSimilarity(b2, c1)).thenReturn(0.1);

        List<PeakList<PeakAnnotation>> querySpectra = Arrays.asList(a1, a2, a3, b1, b2, c1);

        SimilarityGraph<PeakList<PeakAnnotation>> simGraph = PeakListSimGraphFactory.build(querySpectra, simFunc, tolerance, 0.3);

        Set<SimEdge<PeakList<PeakAnnotation>>> edges = Sets.newHashSet(simGraph.getEdges());
        Assert.assertEquals(4, edges.size());
        Assert.assertEquals(6, simGraph.getVertexCount());
        Assert.assertEquals(0.5, simGraph.findEdge(a1, a2).get().getScore(), 0.000001);
        Assert.assertEquals(0.6, simGraph.findEdge(a1, a3).get().getScore(), 0.000001);
        Assert.assertEquals(0.7, simGraph.findEdge(a2, a3).get().getScore(), 0.000001);
        Assert.assertEquals(0.8, simGraph.findEdge(b1, b2).get().getScore(), 0.000001);

        Assert.assertEquals(Optional.<SimEdge<PeakList<PeakAnnotation>>>absent(), simGraph.findEdge(a1, b1));
        Assert.assertEquals(Optional.<SimEdge<PeakList<PeakAnnotation>>>absent(), simGraph.findEdge(a1, b2));
        Assert.assertEquals(Optional.<SimEdge<PeakList<PeakAnnotation>>>absent(), simGraph.findEdge(a1, c1));
        Assert.assertEquals(Optional.<SimEdge<PeakList<PeakAnnotation>>>absent(), simGraph.findEdge(a2, b1));
        Assert.assertEquals(Optional.<SimEdge<PeakList<PeakAnnotation>>>absent(), simGraph.findEdge(a2, b2));
        Assert.assertEquals(Optional.<SimEdge<PeakList<PeakAnnotation>>>absent(), simGraph.findEdge(a2, c1));
        Assert.assertEquals(Optional.<SimEdge<PeakList<PeakAnnotation>>>absent(), simGraph.findEdge(a3, b1));
        Assert.assertEquals(Optional.<SimEdge<PeakList<PeakAnnotation>>>absent(), simGraph.findEdge(a3, b2));
        Assert.assertEquals(Optional.<SimEdge<PeakList<PeakAnnotation>>>absent(), simGraph.findEdge(a3, c1));

        Assert.assertEquals(Optional.<SimEdge<PeakList<PeakAnnotation>>>absent(), simGraph.findEdge(b1, c1));
        Assert.assertEquals(Optional.<SimEdge<PeakList<PeakAnnotation>>>absent(), simGraph.findEdge(b2, c1));
    }

    /**
     * <pre>
     *     A1   B1   C1
     * </pre>
     *
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testDisconnectedGraph() throws Exception {

        PeakList a1 = newPeakList(201.01, 1, "A1");
        PeakList b1 = newPeakList(201.01, 1, "B1");
        PeakList c1 = newPeakList(201.01, 1, "C1");

        Tolerance tolerance = new AbsoluteTolerance(0.3);
        SimFunc<PeakAnnotation, PeakAnnotation> simFunc = Mockito.mock(SimFunc.class);
        when(simFunc.calcSimilarity(Mockito.any(PeakList.class), Mockito.any(PeakList.class))).thenReturn(0.0);

        List<PeakList> querySpectra = Arrays.asList(a1, b1, c1);

        SimilarityGraph<PeakList> simGraph = PeakListSimGraphFactory.build(querySpectra, simFunc, tolerance, 0.3);

        Assert.assertEquals(3, simGraph.getVertexCount());

        Set<SimEdge<PeakList>> edges = Sets.newHashSet(simGraph.getEdges());
        Assert.assertEquals(0, edges.size());
    }

    /**
     * A nodes are +1, b and c nodes +2
     *
     * A1--0.5--A2      B1--0.8--B2
     *  \      /         .      .
     *  0.6  0.7        0.29  0.1
     *   \  /              . .
     *    A3               C1
     *
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @Test(expected = IllegalStateException.class)
    public void testUnsortedMz() throws Exception {

        PeakList<PeakAnnotation> a1 = newPeakList(201.01, 1, "A1");
        PeakList<PeakAnnotation> a2 = newPeakList(201.02, 1, "A2");
        PeakList<PeakAnnotation> a3 = newPeakList(201.03, 1, "A3");

        PeakList<PeakAnnotation> b1 = newPeakList(201.01, 2, "B1");
        PeakList<PeakAnnotation> b2 = newPeakList(201.02, 2, "B2");

        PeakList<PeakAnnotation> c1 = newPeakList(201.03, 2, "C1");

        Tolerance tolerance = new AbsoluteTolerance(0.3);
        SimFunc<PeakAnnotation, PeakAnnotation> simFunc = Mockito.mock(SimFunc.class);

        when(simFunc.calcSimilarity(Mockito.any(PeakList.class), Mockito.any(PeakList.class))).thenReturn(1.0);

        when(simFunc.calcSimilarity(a1, a2)).thenReturn(0.5);
        when(simFunc.calcSimilarity(a1, a3)).thenReturn(0.6);
        when(simFunc.calcSimilarity(a2, a3)).thenReturn(0.7);

        when(simFunc.calcSimilarity(b1, b2)).thenReturn(0.8);
        when(simFunc.calcSimilarity(b1, c1)).thenReturn(0.29);
        when(simFunc.calcSimilarity(b2, c1)).thenReturn(0.1);

        List<PeakList<PeakAnnotation>> querySpectra = Arrays.asList(a3, a1, a2, c1, b2, b1);

        PeakListSimGraphFactory.build(querySpectra, simFunc, tolerance, 0.3);
    }

    /**
     * A nodes are +1, b and c nodes +2
     *
     * A1--0.5--A2      B1--0.8--B2
     *  \      /         .      .
     *  0.6  0.7        0.29  0.1
     *   \  /              . .
     *    A3               C1
     *
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @Test(expected = IllegalStateException.class)
    public void testUnsortedCharge() throws Exception {

        PeakList<PeakAnnotation> a1 = newPeakList(201.01, 1, "A1");
        PeakList<PeakAnnotation> a2 = newPeakList(201.02, 1, "A2");
        PeakList<PeakAnnotation> a3 = newPeakList(201.03, 1, "A3");

        PeakList<PeakAnnotation> b1 = newPeakList(201.01, 2, "B1");
        PeakList<PeakAnnotation> b2 = newPeakList(201.02, 2, "B2");

        PeakList<PeakAnnotation> c1 = newPeakList(201.03, 2, "C1");

        Tolerance tolerance = new AbsoluteTolerance(0.3);
        SimFunc<PeakAnnotation, PeakAnnotation> simFunc = Mockito.mock(SimFunc.class);

        when(simFunc.calcSimilarity(Mockito.any(PeakList.class), Mockito.any(PeakList.class))).thenReturn(1.0);

        when(simFunc.calcSimilarity(a1, a2)).thenReturn(0.5);
        when(simFunc.calcSimilarity(a1, a3)).thenReturn(0.6);
        when(simFunc.calcSimilarity(a2, a3)).thenReturn(0.7);

        when(simFunc.calcSimilarity(b1, b2)).thenReturn(0.8);
        when(simFunc.calcSimilarity(b1, c1)).thenReturn(0.29);
        when(simFunc.calcSimilarity(b2, c1)).thenReturn(0.1);

        List<PeakList<PeakAnnotation>> querySpectra = Arrays.asList(a1, b1, a2, b2, a3, c1);

        PeakListSimGraphFactory.build(querySpectra, simFunc, tolerance, 0.3);
    }

    private PeakList<PeakAnnotation> newPeakList(double mz, int charge, String idPrefix) {

        UUID id = UUID.fromString(idPrefix + "000000-0000-0000-0000-000000000000");

        PeakList<PeakAnnotation> peakList = new FloatConstantPeakList<PeakAnnotation>(1);
        peakList.setId(id);
        peakList.getPrecursor().setMzAndCharge(mz, charge);

        return peakList;
    }
}
