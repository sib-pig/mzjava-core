package org.expasy.mzjava.core.ms.cluster;

import edu.uci.ics.jung.algorithms.shortestpath.MinimumSpanningForest;
import edu.uci.ics.jung.graph.DelegateForest;
import edu.uci.ics.jung.graph.Forest;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.Tree;
import org.expasy.mzjava.utils.function.Procedure;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class MSTClusterBuilderTest {
    @Test
    public void testMSTClusterBuilder() {

        String nodeA1 = "nodeA1";
        String nodeA2 = "nodeA2";
        String nodeA3 = "nodeA3";
        String nodeA4 = "nodeA4";

        String nodeB1 = "nodeB1";
        String nodeB2 = "nodeB2";
        String nodeB3 = "nodeB3";

        String nodeC1 = "nodeC1";
        String nodeC2 = "nodeC2";

        DenseSimilarityGraph.Builder<String> builder = new DenseSimilarityGraph.Builder<>();

        SimEdge<String> edge1 = builder.add(nodeA1, nodeA2, 0.5);
        SimEdge<String> edge2 = builder.add(nodeA1, nodeA3, 0.5);
        SimEdge<String> edge3 = builder.add(nodeA1, nodeA4, 0.5);
        SimEdge<String> edge4 = builder.add(nodeA2, nodeA3, 0.9);
        SimEdge<String> edge5 = builder.add(nodeA2, nodeA4, 0.9);
        SimEdge<String> edge6 = builder.add(nodeA3, nodeA4, 0.9);

        SimEdge<String> edge7 = builder.add(nodeB1, nodeB2, 0.7);
        SimEdge<String> edge8 = builder.add(nodeB1, nodeB3, 0.7);
        SimEdge<String> edge9 = builder.add(nodeB2, nodeB3, 0.7);

        SimEdge<String> edge10 = builder.add(nodeA2, nodeB1, 0.5);
        SimEdge<String> edge11 = builder.add(nodeA2, nodeB3, 0.5);

        SimEdge<String> edge12 = builder.add(nodeC1, nodeC2, 0.5);


        DenseSimilarityGraph<String> simGraph = builder.build();

        MSTClusterBuilder clusterer = new MSTClusterBuilder(0.6);

        Collection<Set<String>> clusters = clusterer.cluster(simGraph);

        Assert.assertEquals(clusters.size(),5);
        for (Set<String> cluster : clusters) {

            if (cluster.contains(nodeA1)) {
                Assert.assertEquals(cluster.size(),1);
            }

            if (cluster.contains(nodeA2)) {
                Assert.assertEquals(cluster.size(),3);
                Assert.assertTrue(cluster.contains(nodeA3));
                Assert.assertTrue(cluster.contains(nodeA4));
            }

            if (cluster.contains(nodeB1)) {
                Assert.assertEquals(cluster.size(),3);
                Assert.assertTrue(cluster.contains(nodeB2));
                Assert.assertTrue(cluster.contains(nodeB3));
            }

            if (cluster.contains(nodeC1)) {
                Assert.assertEquals(cluster.size(),1);
            }

            if (cluster.contains(nodeC2)) {
                Assert.assertEquals(cluster.size(),1);
            }
        }

    }
    
    @Test
    public void testJungMSTAlgo() {
        // test minimal spanning tree algorithm
        // Example from 'Introduction to algorithms' by Corman, Leiserson, Rivest, and Stein. Figure 23.4

        String node1 = "node1";
        String node2 = "node2";
        String node3 = "node3";
        String node4 = "node4";
        String node5 = "node5";
        String node6 = "node6";
        String node7 = "node7";
        String node8 = "node8";
        String node9 = "node9";

        DenseSimilarityGraph.Builder<String> builder = new DenseSimilarityGraph.Builder<>();

        SimEdge<String> e1_2 = builder.add(node1, node2, 4.0);
        SimEdge<String> e2_3 = builder.add(node2, node3, 8.0);
        SimEdge<String> e3_4 = builder.add(node3, node4, 7.0);
        SimEdge<String> e4_5 = builder.add(node4, node5, 9.0);
        SimEdge<String> e5_6 = builder.add(node5, node6, 10.0);
        SimEdge<String> e6_7 = builder.add(node6, node7, 2.0);
        SimEdge<String> e7_8 = builder.add(node7, node8, 1.0);
        SimEdge<String> e8_9 = builder.add(node8, node9, 7.0);
        SimEdge<String> e1_8 = builder.add(node1, node8, 8.0);
        SimEdge<String> e2_8 = builder.add(node2, node8, 11.0);
        SimEdge<String> e3_9 = builder.add(node3, node9, 2.0);
        SimEdge<String> e3_6 = builder.add(node3, node6, 4.0);
        SimEdge<String> e4_6 = builder.add(node4, node6, 14.0);
        SimEdge<String> e7_9 = builder.add(node7, node9, 6.0);


        DenseSimilarityGraph<String> simGraph = builder.build();
        final Map<SimEdge<String>, Double> scoreMap = new HashMap<>();
        simGraph.forEachEdge(new Procedure<SimEdge<String>>() {
            @Override
            public void execute(SimEdge<String> simEdge) {
                scoreMap.put(simEdge, simEdge.getScore());
            }
        });

        JungSimGraphWrapper<String> graph = new JungSimGraphWrapper<>(simGraph);
        Graph<String, SimEdge<String>> uuidSimEdgeGraph = graph;

        MinimumSpanningForest<String,SimEdge<String>> mstforest =
                new MinimumSpanningForest<>(uuidSimEdgeGraph, new DelegateForest<String, SimEdge<String>>(), null, scoreMap);

        Forest<String,SimEdge<String>> forest = mstforest.getForest();

        Assert.assertTrue(forest.getTrees().size() == 1);

        for (Tree<String,SimEdge<String>> tree: forest.getTrees())    {
            Assert.assertEquals(tree.getVertexCount(), 9);
            Assert.assertTrue(tree.containsEdge(e1_2));
            Assert.assertTrue(tree.containsEdge(e1_8));
            Assert.assertTrue(tree.containsEdge(e3_4));
            Assert.assertTrue(tree.containsEdge(e3_6));
            Assert.assertTrue(tree.containsEdge(e3_9));
            Assert.assertTrue(tree.containsEdge(e4_5));
            Assert.assertTrue(tree.containsEdge(e6_7));
            Assert.assertTrue(tree.containsEdge(e7_8));

            Assert.assertFalse(tree.containsEdge(e2_3));
            Assert.assertFalse(tree.containsEdge(e2_8));
            Assert.assertFalse(tree.containsEdge(e8_9));
            Assert.assertFalse(tree.containsEdge(e7_9));
            Assert.assertFalse(tree.containsEdge(e4_6));
            Assert.assertFalse(tree.containsEdge(e5_6));
        }
    }
    
}
