package org.expasy.mzjava.core.ms.library;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.utils.function.Procedure;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IntervalSpectrumLibraryTest {

    /**
     * Library         365    478-479    567-567
     *                  |     |  /         \  |
     * Query   200    365    468            570     600
     */
    @Test
    public void testIterator() throws Exception {

        final PeakList pl_365 = mockSpectrum(365, 2);
        final PeakList pl_478 = mockSpectrum(478, 2);
        final PeakList pl_479 = mockSpectrum(479, 2);
        final PeakList pl_567a = mockSpectrum(567, 2);
        final PeakList pl_567b = mockSpectrum(567, 2);

        SpectrumLibrary<PeakList> lib = new IntervalSpectrumLibrary<>(-20, 20, Lists.newArrayList(pl_365, pl_478, pl_479, pl_567a, pl_567b));

        final Set<PeakList> found = new LinkedHashSet<>();
        final Procedure<PeakList> procedure = new Procedure<PeakList>() {
            @Override
            public void execute(PeakList peakList) {

                found.add(peakList);
            }
        };

        lib.forEach(new Peak(200, 564, 2), procedure);
        Assert.assertEquals(0, found.size());

        found.clear();
        lib.forEach(new Peak(365, 564, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl_365), found);

        found.clear();
        lib.forEach(new Peak(468, 564, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl_478, pl_479), found);

        found.clear();
        lib.forEach(new Peak(570, 564, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl_567a, pl_567b), found);

        found.clear();
        lib.forEach(new Peak(600, 564, 2), procedure);
        Assert.assertEquals(0, found.size());
    }

    @Test
    public void testMultipleCharges() throws Exception {

        final PeakList pl_1365 = mockSpectrum(1365, 1);
        final PeakList pl_1478 = mockSpectrum(1478, 1);
        final PeakList pl_479 = mockSpectrum(479, 2);
        final PeakList pl_567a = mockSpectrum(567, 2);
        final PeakList pl_567b = mockSpectrum(567, 2);

        SpectrumLibrary<PeakList> lib = new IntervalSpectrumLibrary<>(-20, 20, Lists.newArrayList(pl_1365, pl_1478, pl_479, pl_567a, pl_567b));

        final Set<PeakList> found = new LinkedHashSet<>();
        final Procedure<PeakList> procedure = new Procedure<PeakList>() {
            @Override
            public void execute(PeakList peakList) {

                found.add(peakList);
            }
        };

        found.clear();
        lib.forEach(new Peak(1361, 564, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(pl_1365), found);

        found.clear();
        lib.forEach(new Peak(470, 564, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl_479), found);
        found.clear();

        found.clear();
        lib.forEach(new Peak(576, 564, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl_567a, pl_567b), found);
    }

    @Test
    public void testEmptyReader() throws Exception {

        SpectrumLibrary<PeakList> library = new IntervalSpectrumLibrary<>(-0.2, 0.2, Collections.<PeakList>emptyList());

        final Set<PeakList> found = new LinkedHashSet<>();
        final Procedure<PeakList> procedure = new Procedure<PeakList>() {
            @Override
            public void execute(PeakList peakList) {

                found.add(peakList);
            }
        };
        library.forEach(new Peak(391.285003662109, 1.0, 2), procedure);
        Assert.assertEquals(0, found.size());

        library.forEach(new Peak(391.7109375, 1.0, 2), procedure);
        Assert.assertEquals(0, found.size());
    }

    @Test
    public void testTraverseOfEnd() throws Exception {

        final PeakList pl_1875_5 = mockSpectrum(1875.5, 2);
        final PeakList pl_1875_8 = mockSpectrum(1875.8, 2);
        final PeakList pl_1876_2 = mockSpectrum(1876.2, 2);
        final PeakList pl_1876_5 = mockSpectrum(1876.5, 2);

        SpectrumLibrary<PeakList> library = new IntervalSpectrumLibrary<>(-1.0, 0.6, Lists.newArrayList(pl_1875_5, pl_1875_8, pl_1876_2, pl_1876_5));

        final Set<PeakList> found = new LinkedHashSet<>();
        final Procedure<PeakList> procedure = new Procedure<PeakList>() {
            @Override
            public void execute(PeakList peakList) {

                found.add(peakList);
            }
        };

        library.forEach(new Peak(1875.5, 12, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl_1875_5, pl_1875_8), found);

        found.clear();
        library.forEach(new Peak(1875.8, 12, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl_1875_5, pl_1875_8, pl_1876_2), found);

        found.clear();
        library.forEach(new Peak(1876.2, 12, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl_1875_5, pl_1875_8, pl_1876_2, pl_1876_5), found);

        found.clear();
        library.forEach(new Peak(1876.81, 12, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl_1876_2, pl_1876_5), found);
    }

    private PeakList mockSpectrum(double mz, int charge) {

        PeakList peakList = mock(PeakList.class);
        when(peakList.getPrecursor()).thenReturn(new Peak(mz, 10, charge));
        when(peakList.toString()).thenReturn(mz + " " + charge + "+");

        return peakList;
    }
}