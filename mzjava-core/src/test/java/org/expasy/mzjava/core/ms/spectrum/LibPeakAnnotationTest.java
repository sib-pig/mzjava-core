package org.expasy.mzjava.core.ms.spectrum;

import org.expasy.mzjava.core.ms.spectrum.LibPeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class LibPeakAnnotationTest {

    @Test
    public void testEquals() throws Exception {
        LibPeakAnnotation annotation1 = new LibPeakAnnotation(3, 1.0, 100.0);
        LibPeakAnnotation annotation2 = new LibPeakAnnotation(3, 1.0, 100.0);
        LibPeakAnnotation annotation3 = new LibPeakAnnotation(1, 1.0, 100.0);

        Assert.assertNotSame(annotation1, annotation2);

        Assert.assertTrue(annotation1.equals(annotation2));
        Assert.assertTrue(annotation2.equals(annotation1));

        Assert.assertFalse(annotation1.equals(annotation3));
        Assert.assertFalse(annotation3.equals(annotation1));
    }

    @Test
    public void testHashCode() throws Exception {

        LibPeakAnnotation annotation1 = new LibPeakAnnotation(3, 1.0, 100.0);
        LibPeakAnnotation annotation2 = new LibPeakAnnotation(3, 1.0, 100.0);
        LibPeakAnnotation annotation3 = new LibPeakAnnotation(1, 1.0, 100.0);

        int hash1 = annotation1.hashCode();
        int hash2 = annotation2.hashCode();
        int hash3 = annotation3.hashCode();

        Assert.assertEquals(hash1, hash2);
        Assert.assertTrue(hash1 != hash3);
    }

    @Test
    public void testCopy() throws Exception {

        LibPeakAnnotation annotation = new LibPeakAnnotation(3, 1.0, 100.0);

        LibPeakAnnotation copyAnnotation = annotation.copy();
        LibPeakAnnotation copyConstructedAnnotation = new LibPeakAnnotation(annotation);

        Assert.assertNotSame(copyAnnotation, copyConstructedAnnotation);
        Assert.assertTrue(copyAnnotation.equals(copyConstructedAnnotation));
        Assert.assertTrue(copyConstructedAnnotation.equals(copyAnnotation));
    }
}
