package org.expasy.mzjava.core.ms.cluster;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import edu.uci.ics.jung.graph.util.Pair;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JungSimGraphWrapperTest {

    @Test(expected = UnsupportedOperationException.class)
    public void testAddEdge() throws Exception {

        newGraph().addEdge(new SimEdge<String>("B3", "A1", 0.5), "B3", "A1");
    }

    @Test
    public void testGetInEdges() throws Exception {

        @SuppressWarnings("unchecked")
        final HashSet<SimEdge<String>> expected = Sets.newHashSet(
                new SimEdge<String>("B1", "B2", 0.9),
                new SimEdge<String>("B1", "B3", 0.9),
                new SimEdge<String>("A1", "B1", 0.1));
        final HashSet<SimEdge<String>> actual = Sets.newHashSet(newGraph().getInEdges("B1"));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetOutEdges() throws Exception {

        @SuppressWarnings("unchecked")
        final HashSet<SimEdge<String>> expected = Sets.newHashSet(
                new SimEdge<String>("B1", "B2", 0.9),
                new SimEdge<String>("B1", "B3", 0.9),
                new SimEdge<String>("A1", "B1", 0.1));
        final HashSet<SimEdge<String>> actual = Sets.newHashSet(newGraph().getOutEdges("B1"));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetPredecessors() throws Exception {

        JungSimGraphWrapper<String> graph = newGraph();

        Collection<String> neighbours = graph.getPredecessors("B2");
        Assert.assertEquals(Sets.newHashSet("B1", "B3"), Sets.newHashSet(neighbours));
    }

    @Test
    public void testGetSuccessors() throws Exception {

        JungSimGraphWrapper<String> graph = newGraph();

        Collection<String> neighbours = graph.getSuccessors("B2");
        Assert.assertEquals(Sets.newHashSet("B1", "B3"), Sets.newHashSet(neighbours));
    }

    @Test
    public void testFindEdge() throws Exception {

        JungSimGraphWrapper<String> graph = newGraph();

        Assert.assertEquals(new SimEdge<String>("A1", "B1", 0.1), graph.findEdge("B1", "A1"));
        Assert.assertEquals(new SimEdge<String>("A1", "B1", 0.1), graph.findEdge("A1", "B1"));
        Assert.assertNull(graph.findEdge("A1", "B3"));
        Assert.assertNull(graph.findEdge("A12", "B3"));
    }

    @Test
    public void testGetEndpoints() throws Exception {

        JungSimGraphWrapper<String> graph = newGraph();

        Assert.assertEquals(new Pair<String>("A1", "B1"), graph.getEndpoints(new SimEdge<String>("A1", "B1", 0.1)));
    }

    @Test
    public void testGetSource() throws Exception {

        JungSimGraphWrapper<String> graph = newGraph();

        Assert.assertNull(graph.getSource(new SimEdge<String>("A1", "B1", 0.1)));
    }

    @Test
    public void testGetDest() throws Exception {

        JungSimGraphWrapper<String> graph = newGraph();

        Assert.assertNull(graph.getDest(new SimEdge<String>("A1", "B1", 0.1)));
    }

    @Test
    public void testIsSource() throws Exception {

        JungSimGraphWrapper<String> graph = newGraph();

        Assert.assertEquals(false, graph.isSource("A1", new SimEdge<String>("A1", "B1", 0.1)));
    }

    @Test
    public void testIsDest() throws Exception {

        JungSimGraphWrapper<String> graph = newGraph();

        Assert.assertEquals(false, graph.isDest("B1", new SimEdge<String>("A1", "B1", 0.1)));
    }

    @Test
    public void testGetEdges() throws Exception {

        JungSimGraphWrapper<String> graph = newGraph();

        @SuppressWarnings("unchecked")
        final HashSet<SimEdge<String>> expected = Sets.newHashSet(
                new SimEdge<String>("A1", "B1", 0.1),
                new SimEdge<String>("B1", "B2", 0.9),
                new SimEdge<String>("B1", "B3", 0.9),
                new SimEdge<String>("B2", "B3", 0.9)
        );

        Assert.assertEquals(expected, Sets.newHashSet(graph.getEdges()));
    }

    @Test
    public void testGetVertices() throws Exception {

        JungSimGraphWrapper<String> graph = newGraph();

        Assert.assertEquals(Sets.newHashSet("A1", "B1", "B2", "B3"), graph.getVertices());
    }

    @Test
    public void testContainsVertex() throws Exception {

        JungSimGraphWrapper<String> graph = newGraph();

        Assert.assertEquals(true, graph.containsVertex("B2"));
        Assert.assertEquals(false, graph.containsVertex("F2"));
    }

    @Test
    public void testContainsEdge() throws Exception {

        JungSimGraphWrapper<String> graph = newGraph();

        Assert.assertEquals(true, graph.containsEdge(new SimEdge<String>("A1", "B1", 0.1)));
        Assert.assertEquals(false, graph.containsEdge(new SimEdge<String>("A1", "B1", 0.4)));
    }

    @Test
    public void testGetEdgeCount() throws Exception {

        JungSimGraphWrapper<String> graph = newGraph();

        Assert.assertEquals(4, graph.getEdgeCount());
    }

    @Test
    public void testGetVertexCount() throws Exception {

        JungSimGraphWrapper<String> graph = newGraph();

        Assert.assertEquals(4, graph.getVertexCount());
    }

    @Test
    public void testGetNeighbors() throws Exception {

        JungSimGraphWrapper<String> graph = newGraph();

        Collection<String> neighbours = graph.getNeighbors("B2");
        Assert.assertEquals(Sets.newHashSet("B1", "B3"), Sets.newHashSet(neighbours));
        Assert.assertNull(graph.getNeighbors("C2"));
    }

    @Test
    public void testGetIncidentEdges() throws Exception {

        JungSimGraphWrapper<String> graph = newGraph();

        Collection<String> neighbours = graph.getNeighbors("B2");
        Assert.assertEquals(Sets.newHashSet("B1", "B3"), Sets.newHashSet(neighbours));
        Assert.assertNull(graph.getIncidentEdges("C2"));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testAddVertex() throws Exception {

        newGraph().addVertex("A2");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testRemoveVertex() throws Exception {

        newGraph().removeVertex("A1");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testRemoveEdge() throws Exception {

        newGraph().removeEdge(new SimEdge<String>("A1", "B1", 0.1));
    }

    @Test
    public void testNonCollectionIterable() throws Exception {

        //noinspection unchecked
        Iterator<String> iterator = mock(Iterator.class);
        when(iterator.hasNext()).thenReturn(true, true, false);
        when(iterator.next()).thenReturn("A1", "A2");

        //noinspection unchecked
        Iterable<String> iterable = mock(Iterable.class);
        when(iterable.iterator()).thenReturn(iterator);

        //noinspection unchecked
        SimilarityGraph<String> simGraph = mock(SimilarityGraph.class);
        when(simGraph.getVertices()).thenReturn(iterable);

        JungSimGraphWrapper<String> jungGraph = new JungSimGraphWrapper<String>(simGraph);
        Assert.assertEquals(Lists.newArrayList("A1", "A2"), jungGraph.getVertices());
    }

    /**
     *  <pre>
     *  A1
     *  |
     * 0.1
     *  |
     *  B1-0.90-B2
     *    \     /
     *   0.9  0.9
     *     \  /
     *      B3
     * </pre>
     */
    private JungSimGraphWrapper<String> newGraph(){

        String nodeA1 = "A1";

        String nodeB1 = "B1";
        String nodeB2 = "B2";
        String nodeB3 = "B3";

        DenseSimilarityGraph.Builder<String> builder = new DenseSimilarityGraph.Builder<String>();
        builder.add(nodeA1, nodeB1, 0.1);

        builder.add(nodeB1, nodeB2, 0.9);
        builder.add(nodeB1, nodeB3, 0.9);
        builder.add(nodeB2, nodeB3, 0.9);

        return new JungSimGraphWrapper<String>(builder.build());
    }
}