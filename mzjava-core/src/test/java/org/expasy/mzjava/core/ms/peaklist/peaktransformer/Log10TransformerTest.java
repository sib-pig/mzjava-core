/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.peaklist.peaktransformer;

import org.expasy.mzjava.core.ms.peaklist.MockPeakSink;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessor;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class Log10TransformerTest {

    @Test
    public void test() throws Exception {

        double[] mzs = new double[]{408.05784111354126, 514.6757533311302, 584.0217507817738, 784.234};
        double[] intensities = new double[]{99, 12.0, 0.0001, -1};

        Log10Transformer<PeakAnnotation> processor = new Log10Transformer<PeakAnnotation>();
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();
        processor.setSink(sink);

        runProcessor(processor, mzs, intensities);

        double[] actualMz = sink.getMzList();
        double[] actualIntensities = sink.getIntensityList();

        Assert.assertEquals(mzs.length, actualMz.length);

        Assert.assertEquals(2.0, actualIntensities[0], 0.000000000001);
        Assert.assertEquals(1.1139433523068367, actualIntensities[1], 0.000000000001);
        Assert.assertEquals(4.342727686266486E-5, actualIntensities[2], 0.000000000001);
        Assert.assertEquals(0, actualIntensities[3], 0.000000000001);

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getEndCalled());
    }

    private void runProcessor(PeakProcessor<PeakAnnotation, PeakAnnotation> processor, double[] keys, double[] values) {

        processor.start(keys.length);

        for (int i = 0; i < keys.length; i++) {

            List<PeakAnnotation> objects = Collections.emptyList();
            processor.processPeak(keys[i], values[i], objects);
        }

        processor.end();
    }
}
