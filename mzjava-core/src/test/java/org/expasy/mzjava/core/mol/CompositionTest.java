/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.mol;

import gnu.trove.map.hash.TObjectIntHashMap;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.util.Set;

import static org.expasy.mzjava.core.mol.AtomicSymbol.*;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class CompositionTest {

    @Test
    public void test() throws Exception {

        Composition composition = new Composition(new Composition.Builder(AtomicSymbol.C).add(AtomicSymbol.H, 3).charge(1).build());

        Assert.assertEquals("CH3(+)", composition.getFormula());
        Assert.assertEquals(1, composition.getCount(PeriodicTable.C));
        Assert.assertEquals(3, composition.getCount(PeriodicTable.H));
        Assert.assertEquals(15.0229261717, composition.getMolecularMass(), 0.00000000001);
        Assert.assertEquals(1, composition.getCharge());
    }

    @Test
    public void testNegative() throws Exception {

        Composition composition = new Composition(new Composition.Builder(AtomicSymbol.H, -2).add(AtomicSymbol.O, -1).build());

        Assert.assertEquals("H-2O-1", composition.getFormula());
        Assert.assertEquals(-2, composition.getCount(PeriodicTable.H));
        Assert.assertEquals(-1, composition.getCount(PeriodicTable.O));
        Assert.assertEquals(-18.010564686, composition.getMolecularMass(), 0.00000001);
        Assert.assertEquals(0, composition.getCharge());
    }

    @Test
    public void testEquals() throws Exception {

        Composition.Builder atomCounterMap1 = new Composition.Builder(AtomicSymbol.C, 2).add(AtomicSymbol.H, 6).add(AtomicSymbol.O, 1);
        Composition composition1 = new Composition(atomCounterMap1.build());


        Composition.Builder atomCounterMap2 = new Composition.Builder(AtomicSymbol.C, 2).add(AtomicSymbol.H, 6).add(AtomicSymbol.O, 1);
        Composition composition2 = new Composition(atomCounterMap2.build());

        Assert.assertEquals(composition1, composition2);
    }

    @Test
    public void testEquals2() throws Exception {

        Composition composition1 = Composition.parseComposition("");
        Composition composition2 = Composition.parseComposition("");

        Assert.assertEquals(composition1, composition2);
    }

    @Test
    public void testChargeMass() throws Exception {

        Composition composition = new Composition(new Composition.Builder(AtomicSymbol.H).build());
        Assert.assertEquals(PeriodicTable.H_MASS, composition.getMolecularMass(), 0.00000000001);

        composition = new Composition(new Composition.Builder(AtomicSymbol.H).charge(1).build());
        Assert.assertEquals(PeriodicTable.H_MASS - PeriodicTable.ELECTRON_MASS, composition.getMolecularMass(), 0.00000000001);

        composition = new Composition(new Composition.Builder(AtomicSymbol.H).charge(-1).build());
        Assert.assertEquals(PeriodicTable.H_MASS + PeriodicTable.ELECTRON_MASS, composition.getMolecularMass(), 0.00000000001);

        composition = new Composition(new Composition.Builder(AtomicSymbol.H).charge(2).build());
        Assert.assertEquals(PeriodicTable.H_MASS - PeriodicTable.ELECTRON_MASS * 2, composition.getMolecularMass(), 0.00000000001);

        composition = new Composition(new Composition.Builder(AtomicSymbol.H).charge(-2).build());
        Assert.assertEquals(PeriodicTable.H_MASS + PeriodicTable.ELECTRON_MASS * 2, composition.getMolecularMass(), 0.00000000001);
    }

    @Test
    public void testCountAll() throws Exception {

        Composition composition = new Composition(new Composition.Builder(AtomicSymbol.C).add(AtomicSymbol.H, 3).charge(1).build());

        Assert.assertEquals(4, composition.size());
        Assert.assertEquals(15.0229261717, composition.getMolecularMass(), 0.0000000001);
        Assert.assertEquals("CH3(+)", composition.getFormula());
    }

    @Test
    public void testAtomMapConstructor() throws Exception {

        Composition composition = new Composition(new Composition.Builder(AtomicSymbol.C).add(AtomicSymbol.H, 3).charge(1).build());

        Assert.assertEquals(4, composition.size());
        Assert.assertEquals(15.0229261717, composition.getMolecularMass(), 0.0000000001);
        Assert.assertEquals("CH3(+)", composition.getFormula());
    }

    @Test
    public void testParseComposition() throws ParseException {

        Composition molecule = Composition.parseComposition("CH3(CH2)2O(CH2)3OH(-)");

        Assert.assertEquals("CH3(CH2)2O(CH2)3OH(-)", molecule.getFormula());
    }

    @Test
    public void testParseSimplerMolecule() throws ParseException {

        Composition molecule = Composition.parseComposition("C H3");
        Assert.assertEquals("C H3", molecule.getFormula());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseComposition2() throws ParseException {

        Composition.parseComposition("CH3(hello)2O(CH2)3OH(-)");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseComposition3() throws ParseException {

        Composition.parseComposition("CH3(CH2)2O(CH2)3OHhello(-)");
    }

    @Test
    public void testParseComposition4() throws ParseException {

        Composition composition = Composition.parseComposition("C3C6");

        Assert.assertEquals(9, composition.getCount(PeriodicTable.C));
        Assert.assertEquals(9, composition.size());
    }

    @Test
    public void testParseComposition5() throws ParseException {

        Composition composition = Composition.parseComposition("C11H17N1O8");
        Assert.assertEquals(291.095416525, composition.getMolecularMass(), 0.000000000001);
    }

    @Test
    public void testNegativeGroupOcc() throws ParseException {

        Composition mol1 = Composition.parseComposition("(H2O)3(NH3)-3");
        Composition mol2 = Composition.parseComposition("N-3H-3O3");

        Assert.assertEquals(mol1, mol2);
    }

    @Test
    public void testParseCompositionMass() throws ParseException {

        Composition mol = Composition.parseComposition("CH4");
        Assert.assertEquals(16.03, mol.getMolecularMass(), 0.01);
    }

    @Test
    public void testParseCompositionNegativeMass() throws ParseException {

        Composition mol = Composition.parseComposition("C-1H-4");
        Assert.assertEquals(-16.03, mol.getMolecularMass(), 0.01);
    }

    // Compare mass against this site:
    // http://www.bmrb.wisc.edu/metabolomics/mol_mass.php
    // http://en.wikipedia.org/wiki/Chemical_file_format
    @Test
    public void parseNBuild() throws Exception {

        Composition molecule = Composition.parseComposition("C[13]2H-3ON-1(2+)");

        org.junit.Assert.assertEquals(24.97, molecule.getMolecularMass(), 0.01);
    }

    @Test
    public void parseNBuild2() throws Exception {

        Composition molecule = Composition.parseComposition("NH3");

        org.junit.Assert.assertEquals(17.026, molecule.getMolecularMass(), 0.001);

        molecule = Composition.parseComposition("N[15]H3");

        org.junit.Assert.assertEquals(18.02, molecule.getMolecularMass(), 0.01);
    }

    @Test
    public void parseNBuildFormulaWithInternalRepetition() throws Exception {

        Composition molecule = Composition.parseComposition("PO4H2(CH2)12CH3");
        Assert.assertEquals(280.1803, molecule.getMolecularMass(), 0.0001);

        molecule = Composition.parseComposition("PO4H2(C[13]H2)12C[13]H3");

        Assert.assertEquals(293.2239, molecule.getMolecularMass(), 0.0001);
    }

    @Test
    public void parseSimpleFormula() throws Exception {

        Composition molecule = Composition.parseComposition("H3O(+)");

        org.junit.Assert.assertEquals(19.0178, molecule.getMolecularMass(), 0.0001);
    }

    @Test
    public void parseSimpleParticle() throws Exception {

        Composition molecule = Composition.parseComposition("H");
        double hMass = 1.007825032;
        org.junit.Assert.assertEquals(hMass, molecule.getMolecularMass(),
                0.000000001);

        molecule = Composition.parseComposition("H(+)"); // H-(e-)
        double mass = molecule.getMolecularMass();

        org.junit.Assert.assertEquals(1.00727610769, mass, 0.000000001);
        org.junit.Assert.assertEquals(mass, hMass - PeriodicTable.ELECTRON_MASS, 0.000000001);

        molecule = Composition.parseComposition("H(-)");// H+(e-)
        mass = molecule.getMolecularMass();

        org.junit.Assert.assertEquals(1.0083739563, mass, 0.000000001);
        org.junit.Assert.assertEquals(mass, hMass + PeriodicTable.ELECTRON_MASS, 0.000000001);
    }

    @Test
    public void testGetAtoms() throws Exception {

        Composition molecule = Composition.parseComposition("PO4H2(CH2)12CH3");

        Set<Atom> atoms = molecule.getAtoms();
        Assert.assertEquals(4, atoms.size());
        Assert.assertEquals(true, atoms.contains(PeriodicTable.P));
        Assert.assertEquals(true, atoms.contains(PeriodicTable.O));
        Assert.assertEquals(true, atoms.contains(PeriodicTable.C));
        Assert.assertEquals(true, atoms.contains(PeriodicTable.H));
    }

    //Test that changing the atoms collection does not change the composition
    @Test (expected = UnsupportedOperationException.class)
    public void testImmutableAtomCollection() throws Exception {

        Composition molecule = Composition.parseComposition("PO4H2(CH2)12CH3");

        Set<Atom> atoms = molecule.getAtoms();
        atoms.clear();
    }

    @Test
    public void testGetFormula() throws Exception {

        Composition composition = Composition.parseComposition("H3C");

        Assert.assertEquals("H3C", composition.getFormula());
    }


    @Test
    public void testCopy() throws Exception {

        Composition composition = Composition.parseComposition("H3C");

        Assert.assertEquals("H3C", composition.getFormula());

        Composition copiedComposition = new Composition(composition);
        Assert.assertEquals("H3C", copiedComposition.getFormula());
        Assert.assertEquals(3, copiedComposition.getCount(PeriodicTable.H));
        Assert.assertEquals(1, copiedComposition.getCount(PeriodicTable.C));
        Assert.assertEquals(composition.hashCode(), copiedComposition.hashCode());
        Assert.assertEquals(true, composition.equals(copiedComposition));
        Assert.assertEquals(true, copiedComposition.equals(composition));
    }

    @Test
    public void testCopyConstructor() throws Exception {

        Composition composition = Composition.parseComposition("H3C");

        Assert.assertEquals("H3C", composition.getFormula());

        Composition copiedComposition = new Composition(composition);
        Assert.assertEquals("H3C", copiedComposition.getFormula());
        Assert.assertEquals(3, copiedComposition.getCount(PeriodicTable.H));
        Assert.assertEquals(1, copiedComposition.getCount(PeriodicTable.C));
        Assert.assertEquals(composition.hashCode(), copiedComposition.hashCode());
        Assert.assertEquals(true, composition.equals(copiedComposition));
        Assert.assertEquals(true, copiedComposition.equals(composition));
    }

    @Test
    public void testConstructor() throws Exception {

        Composition composition = new Composition(
                Composition.parseComposition("C"),
                Composition.parseComposition("H"),
                Composition.parseComposition("H"),
                Composition.parseComposition("H")
        );

        Assert.assertEquals("CH3", composition.getFormula());
    }

    @Test
    public void testToString() throws Exception {

        Composition composition = Composition.parseComposition("H3C");
        Assert.assertEquals(composition.getFormula(), composition.toString());
    }

    @Test
    public void test1() throws Exception {

        Composition.Builder builder = new Composition.Builder().add(C).add(O, 2);

        Assert.assertEquals(Composition.parseComposition("CO2"), builder.build());
    }

    @Test
    public void test2() throws Exception {

        Composition.Builder builder = new Composition.Builder(C).add(O, 2);

        Assert.assertEquals(Composition.parseComposition("CO2"), builder.build());
    }

    @Test
    public void test3() throws Exception {

        Composition.Builder builder = new Composition.Builder(C, 1).add(O, 2);

        Assert.assertEquals(Composition.parseComposition("CO2"), builder.build());
    }

    @Test
    public void test4() throws Exception {

        Composition.Builder builder = new Composition.Builder(C, -1).add(O, -2);

        Assert.assertEquals(Composition.parseComposition("C-1O-2"), builder.build());
    }

    @Test
    public void testIsotope1() throws Exception {

        Composition.Builder builder = new Composition.Builder().addIsotope(C, 13, -1).add(O, -2);

        Assert.assertEquals(Composition.parseComposition("C[13]-1O-2"), builder.build());
    }

    @Test
    public void testIsotope2() throws Exception {

        Composition.Builder builder = new Composition.Builder().addIsotope(C, 13).add(O, 2);

        Assert.assertEquals(Composition.parseComposition("C[13]O2"), builder.build());
    }

    @Test
    public void testCharge() throws Exception {

        Composition comp = new Composition.Builder(O).add(H).charge(-1).build();

        Assert.assertEquals(1, comp.getCount(PeriodicTable.O));
        Assert.assertEquals(1, comp.getCount(PeriodicTable.H));
        Assert.assertEquals(-1, comp.getCharge());
    }

    @Test
    public void testReuseBuild() throws Exception {

        Composition.Builder builder = new Composition.Builder(C, -1).add(O, -2);
        Composition composition1 = builder.build();
        Assert.assertEquals(Composition.parseComposition("C-1O-2"), composition1);

        builder.add(C, 2);
        builder.add(O, 4);
        Composition composition2 = builder.build();
        Assert.assertEquals(Composition.parseComposition("C1O2"), composition2);

        Assert.assertEquals(-1, composition1.getCount(PeriodicTable.getInstance().getAtom(C)));
        Assert.assertEquals(-2, composition1.getCount(PeriodicTable.getInstance().getAtom(O)));
    }

    @Test
    public void testMakeICATD2H8Formula() throws Exception {

        //  ICAT-D:2H(8):C20H26H[2]8N4O5S
        TObjectIntHashMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        atomCounterMap.put(periodicTable.getAtom(AtomicSymbol.C), 20);
        atomCounterMap.put(periodicTable.getAtom(AtomicSymbol.H), 26);
        atomCounterMap.put(periodicTable.getAtom(AtomicSymbol.H, 2), 8);
        atomCounterMap.put(periodicTable.getAtom(AtomicSymbol.N), 4);
        atomCounterMap.put(periodicTable.getAtom(AtomicSymbol.O), 5);
        atomCounterMap.put(periodicTable.getAtom(AtomicSymbol.S), 1);

        String formulaString = Composition.Builder.makeFormula(atomCounterMap, 0);

        Assert.assertEquals("C20H26H[2]8N4O5S", formulaString);
    }

    @Test
    public void testMakeChargedFormula() throws Exception {

        TObjectIntHashMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        atomCounterMap.put(periodicTable.getAtom(O), 1);
        atomCounterMap.put(periodicTable.getAtom(H), 1);

        String formulaString = Composition.Builder.makeFormula(atomCounterMap, -1);

        Assert.assertEquals("HO(-)", formulaString);
    }

    @Test
    public void testMakeChargedFormula2() throws Exception {

        TObjectIntHashMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        atomCounterMap.put(periodicTable.getAtom(H), 1);

        String formulaString = Composition.Builder.makeFormula(atomCounterMap, 1);

        Assert.assertEquals("H(+)", formulaString);
    }

    @Test
    public void testMakeChargedFormula3() throws Exception {

        TObjectIntHashMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        atomCounterMap.put(periodicTable.getAtom(S), 1);
        atomCounterMap.put(periodicTable.getAtom(O), 3);

        String formulaString = Composition.Builder.makeFormula(atomCounterMap, -2);

        Assert.assertEquals("O3S(2-)", formulaString);
    }

    @Test
    public void testMakeChargedFormula4() throws Exception {

        TObjectIntHashMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>();

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        atomCounterMap.put(periodicTable.getAtom(Fe), 1);

        String formulaString = Composition.Builder.makeFormula(atomCounterMap, 2);

        Assert.assertEquals("Fe(2+)", formulaString);
    }

    @Test
    public void testBuilderToString() throws Exception {

        Composition.Builder builder = new Composition.Builder(C, 20).add(H, 26).addIsotope(H, 2, 8).add(N, 4).add(O, 5).add(S);
        Assert.assertEquals("C20H26H[2]8N4O5S", builder.toString());
    }

    @Test
    public void testBuilderAddAll() throws Exception {

        Composition.Builder builder = new Composition.Builder(AtomicSymbol.P);

        builder.addAll(Composition.parseComposition("H3O4"));

        Composition composition = builder.build();

        PeriodicTable periodicTable = PeriodicTable.getInstance();
        Assert.assertEquals(3, composition.getCount(periodicTable.getAtom(AtomicSymbol.H)));
        Assert.assertEquals(1, composition.getCount(periodicTable.getAtom(AtomicSymbol.P)));
        Assert.assertEquals(4, composition.getCount(periodicTable.getAtom(AtomicSymbol.O)));
        Assert.assertEquals(8, composition.size());
    }

    @Test
    public void testIsEmpty() throws Exception {

        Assert.assertEquals(true, new Composition().isEmpty());
        Assert.assertEquals(false, new Composition.Builder().charge(1).build().isEmpty());
        Assert.assertEquals(false, new Composition.Builder(Pt, 12).build().isEmpty());
    }

    @Test
    public void testBuilderIsEmpty() throws Exception {

        Assert.assertEquals(true, new Composition.Builder().isEmpty());
        Assert.assertEquals(false, new Composition.Builder().charge(1).isEmpty());
        Assert.assertEquals(false, new Composition.Builder(Pt, 12).isEmpty());
    }

    @Test
    public void testGroups() throws Exception {

        Composition composition = Composition.parseComposition("(OH)-1");
        Assert.assertEquals(-1, composition.getCount(PeriodicTable.O));
        Assert.assertEquals(-1, composition.getCount(PeriodicTable.H));
    }


    @Test
    public void testElectron() throws Exception {

        Composition composition = Composition.parseComposition("(-)");

        Assert.assertEquals(PeriodicTable.ELECTRON_MASS, composition.getMolecularMass(), 0.000001);
        Assert.assertEquals(-1, composition.getCharge());
    }

    @Test
    public void testMinusProton() throws Exception {

        Composition composition = Composition.parseComposition("H-1(-)");

        Assert.assertEquals(-MassCalculator.PROTON_MASS, composition.getMolecularMass(), 0.000001);
        Assert.assertEquals(-1, composition.getCharge());
    }

    @Test
    public void testGetMassDefect() throws Exception {

        Composition composition = Composition.parseComposition("H");
        Assert.assertEquals(0.007825032, composition.getMassDefect(), 0.0000000001);

        composition = Composition.parseComposition("H-1");
        Assert.assertEquals(-0.007825032, composition.getMassDefect(), 0.0000000001);

        composition = Composition.parseComposition("H2O");
        Assert.assertEquals(0.010564686, composition.getMassDefect(), 0.0000000001);

        composition = Composition.parseComposition("(H2O)-1");
        Assert.assertEquals(-0.010564686, composition.getMassDefect(), 0.0000000001);
    }


    @Test
    public void testSubtractCompositions() throws Exception {
        Composition composition1 = Composition.parseComposition("H3C");
        Composition composition2 = Composition.parseComposition("H2");
        Assert.assertEquals("CH", Composition.subtractCompositions(composition1, composition2).getFormula());

        composition1 = Composition.parseComposition("H3C");
        composition2 = Composition.parseComposition("H3C");
        Assert.assertEquals("", Composition.subtractCompositions(composition1, composition2).getFormula());
    }

    @Test(expected=IllegalStateException.class)
    public void testSubtractCompositions2() throws Exception {
        Composition composition1 = Composition.parseComposition("H3C");
        Composition composition2 = Composition.parseComposition("H3CO");
        Composition.subtractCompositions(composition1, composition2);
    }

    @Test
    public void testSubtractCompositions3() throws Exception {
        Composition composition1 = Composition.parseComposition("H3C");
        Composition composition2 = Composition.parseComposition("H4C2");
        Assert.assertEquals("C-1H-1", Composition.subtractCompositions(composition1, composition2).getFormula());
    }

}

