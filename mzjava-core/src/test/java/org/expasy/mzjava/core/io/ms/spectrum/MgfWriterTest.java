package org.expasy.mzjava.core.io.ms.spectrum;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.utils.NumberFormatFactory;
import org.expasy.mzjava.core.ms.spectrum.*;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * User: fnikitin
 * Date: 7/10/12
 * Time: 5:14 PM
 */
public class MgfWriterTest {

    public static List<MsnSpectrum> setup(PeakList.Precision prec) {

        List<MsnSpectrum> spectra = new ArrayList<MsnSpectrum>();


        MsnSpectrum spectrum = new MsnSpectrum(prec);
        spectrum.setSpectrumIndex(0);
        spectrum.setComment("Cmpd 11, +MSn(786.25) 16.0 min");

        //Spectrum 0
        spectrum.addSorted(new double[]{800.42261, 801.40369, 804.4314, 806.37402, 809.42621, 810.42065, 813.4176, 817.41772, 822.42627, 823.42096},
		        new double[]{520.85944, 393.4707, 317.92062, 1502.3396, 258.33871, 268.10699, 289.59726, 329.18951, 323.10419, 260.3602});
        spectrum.setMsLevel(1);

        spectra.add(spectrum);

        //Spectrum 1

        spectrum = new MsnSpectrum(prec);
        spectrum.setComment("Cmpd 11, +MSn(786.25) 16.0 min");
        spectrum.setSpectrumIndex(1);
        spectrum.addRetentionTime(new RetentionTimeDiscrete(23., TimeUnit.SECOND));

        spectrum.addSorted(new double[]{197.725, 198.053, 199.141, 711.524, 715.513, 738.915, 739.374, 743.954, 744.386, 751.116, 768.535, 771.675, 772.725},
		        new double[]{96.0, 38.0, 34.0, 29.0, 48.0, 186.0, 81.0, 37.0, 88.0, 90.0, 462.0, 169.0, 70.0});
        spectrum.addAnnotation(0, Mockito.mock(PeakAnnotation.class));
        spectrum.setMsLevel(2);
        spectrum.setPrecursor(new Peak(786.24, 6955, 1));

        spectra.add(spectrum);

        return spectra;
    }

    @Test
    public void testWritingMzIntensityFloat() throws Exception {

        PeakList.Precision precision = PeakList.Precision.FLOAT;
        List<MsnSpectrum> spectra = setup(precision);

        Writer sw = new StringWriter();

        MgfParameters parameters = new MgfParameters(precision, spectra.get(0));
        MgfWriter writer = new MgfWriter(sw, parameters);

        writer.write(spectra.get(1));
        writer.close();

        String expectedString =
                "800.423\t520.859\n" +
                        "801.404\t393.471\n" +
                        "804.431\t317.921\n" +
                        "806.374\t1502.340\n" +
                        "809.426\t258.339\n" +
                        "810.421\t268.107\n" +
                        "813.418\t289.597\n" +
                        "817.418\t329.190\n" +
                        "822.426\t323.104\n" +
                        "823.421\t260.360\n" +
                        "\n" +
                        "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.240\t6955.000\n" +
                        "CHARGE=1+\n" +
                        "RTINSECONDS=23.00\n" +
                        "197.725\t96.000\n" +
                        "198.053\t38.000\n" +
                        "199.141\t34.000\n" +
                        "711.524\t29.000\n" +
                        "715.513\t48.000\n" +
                        "738.915\t186.000\n" +
                        "739.374\t81.000\n" +
                        "743.954\t37.000\n" +
                        "744.386\t88.000\n" +
                        "751.116\t90.000\n" +
                        "768.535\t462.000\n" +
                        "771.675\t169.000\n" +
                        "772.725\t70.000\n" +
                        "END IONS\n\n";

        Assert.assertEquals(expectedString, sw.toString());
    }

    @Test
    public void testWritingMzIntensityDouble() throws Exception {

        PeakList.Precision precision = PeakList.Precision.DOUBLE;
        List<MsnSpectrum> spectra = setup(precision);

        Writer sw = new StringWriter();

        MgfParameters parameters = new MgfParameters(precision, spectra.get(0));
        MgfWriter writer = new MgfWriter(sw, parameters);

        writer.write(spectra.get(1));

        writer.close();

        String expectedString =
                "800.4226100000\t520.8594400000\n" +
                        "801.4036900000\t393.4707000000\n" +
                        "804.4314000000\t317.9206200000\n" +
                        "806.3740200000\t1502.3396000000\n" +
                        "809.4262100000\t258.3387100000\n" +
                        "810.4206500000\t268.1069900000\n" +
                        "813.4176000000\t289.5972600000\n" +
                        "817.4177200000\t329.1895100000\n" +
                        "822.4262700000\t323.1041900000\n" +
                        "823.4209600000\t260.3602000000\n" +
                        "\n" +
                        "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.2400000000\t6955.0000000000\n" +
                        "CHARGE=1+\n" +
                        "RTINSECONDS=23.00\n" +
                        "197.7250000000\t96.0000000000\n" +
                        "198.0530000000\t38.0000000000\n" +
                        "199.1410000000\t34.0000000000\n" +
                        "711.5240000000\t29.0000000000\n" +
                        "715.5130000000\t48.0000000000\n" +
                        "738.9150000000\t186.0000000000\n" +
                        "739.3740000000\t81.0000000000\n" +
                        "743.9540000000\t37.0000000000\n" +
                        "744.3860000000\t88.0000000000\n" +
                        "751.1160000000\t90.0000000000\n" +
                        "768.5350000000\t462.0000000000\n" +
                        "771.6750000000\t169.0000000000\n" +
                        "772.7250000000\t70.0000000000\n" +
                        "END IONS\n" +
                        "\n";

        Assert.assertEquals(expectedString, sw.toString());
    }

    @Test
    public void testWritingMzDoubleIntensityInt() throws Exception {

        PeakList.Precision precision = PeakList.Precision.DOUBLE;
        List<MsnSpectrum> spectra = setup(precision);

        Writer sw = new StringWriter();

        MgfParameters parameters = new MgfParameters(NumberFormatFactory.DOUBLE_PRECISION, NumberFormatFactory.INT_PRECISION, Optional.<MsnSpectrum>of(spectra.get(0)));
        MgfWriter writer = new MgfWriter(sw, parameters);

        writer.write(spectra.get(1));
        writer.close();

        String expectedString =
                "800.4226100000\t521\n" +
                        "801.4036900000\t393\n" +
                        "804.4314000000\t318\n" +
                        "806.3740200000\t1502\n" +
                        "809.4262100000\t258\n" +
                        "810.4206500000\t268\n" +
                        "813.4176000000\t290\n" +
                        "817.4177200000\t329\n" +
                        "822.4262700000\t323\n" +
                        "823.4209600000\t260\n" +
                        "\n" +
                        "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.2400000000\t6955\n" +
                        "CHARGE=1+\n" +
                        "RTINSECONDS=23.00\n" +
                        "197.7250000000\t96\n" +
                        "198.0530000000\t38\n" +
                        "199.1410000000\t34\n" +
                        "711.5240000000\t29\n" +
                        "715.5130000000\t48\n" +
                        "738.9150000000\t186\n" +
                        "739.3740000000\t81\n" +
                        "743.9540000000\t37\n" +
                        "744.3860000000\t88\n" +
                        "751.1160000000\t90\n" +
                        "768.5350000000\t462\n" +
                        "771.6750000000\t169\n" +
                        "772.7250000000\t70\n" +
                        "END IONS\n" +
                        "\n";

        Assert.assertEquals(expectedString, sw.toString());
    }

    @Test
    public void testNegativeCharge() throws Exception {

        PeakList.Precision precision = PeakList.Precision.DOUBLE;
        MsnSpectrum spectrum = new MsnSpectrum(precision);
        spectrum.setId(UUID.fromString("deedeb8b-b554-4695-b299-c03ec5dff963"));
        spectrum.setMsLevel(2);
        spectrum.getPrecursor().setValues(2519.65, 50, -1);
        spectrum.add(12, 50);

        final StringWriter sw = new StringWriter();

        final MgfWriter writer = new MgfWriter(sw, precision);

        writer.write(spectrum);
        writer.close();

        String expected = "BEGIN IONS\n" +
                "PEPMASS=2519.6500000000\t50.0000000000\n" +
                "CHARGE=1-\n" +
                "12.0000000000\t50.0000000000\n" +
                "END IONS\n" +
                "\n";

        Assert.assertEquals(expected, sw.toString());
    }

    @Test
    public void testChargeList() throws Exception {

        PeakList.Precision precision = PeakList.Precision.DOUBLE;
        MsnSpectrum spectrum = new MsnSpectrum(precision);
        spectrum.setId(UUID.fromString("a4e65a2a-16e0-4048-8b06-6b085e90aab2"));
        spectrum.setMsLevel(2);
        spectrum.getPrecursor().setValues(2519.65, 50, -2, 2);
        spectrum.add(12, 50);

        final StringWriter sw = new StringWriter();

        final MgfWriter writer = new MgfWriter(sw, precision);

        writer.write(spectrum);
        writer.close();

        String expected = "BEGIN IONS\n" +
                "PEPMASS=2519.6500000000\t50.0000000000\n" +
                "CHARGE=2-, 2-\n" +
                "12.0000000000\t50.0000000000\n" +
                "END IONS\n" +
                "\n";

        Assert.assertEquals(expected, sw.toString());
    }

    @Test
    public void testWriteComment() throws Exception {

        PeakList.Precision precision = PeakList.Precision.DOUBLE;
        MsnSpectrum spectrum = new MsnSpectrum(precision);
        spectrum.setId(UUID.fromString("80de792e-5640-42f7-82e7-4d1d70096128"));
        spectrum.setMsLevel(2);
        spectrum.getPrecursor().setValues(2519.65, 50, -2, -2);
        spectrum.add(12, 50);

        final StringWriter sw = new StringWriter();

        final MgfWriter writer = new MgfWriter(sw, precision);

        writer.writeComment("This is the comment");
        writer.write(spectrum);
        writer.close();

        String expected = "# This is the comment\n" +
                "BEGIN IONS\n" +
                "PEPMASS=2519.6500000000\t50.0000000000\n" +
                "CHARGE=2-, 2-\n" +
                "12.0000000000\t50.0000000000\n" +
                "END IONS\n" +
                "\n";

        Assert.assertEquals(expected, sw.toString());
    }
}


