/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.peaklist;

import org.expasy.mzjava.core.ms.spectrum.MockPeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class DoubleConstantPeakListTest extends BasePeakListTest {

    public DoubleConstantPeakListTest() {

        super(PeakList.Precision.DOUBLE_CONSTANT);
    }

    private double[] getMzField(DoubleConstantPeakList<PeakAnnotation> peakList) throws NoSuchFieldException, IllegalAccessException {

        Field field = peakList.getClass().getDeclaredField("mzList");
        field.setAccessible(true);
        return (double[]) field.get(peakList);
    }

    @Override
    protected void checkPeakList(double[] expectedMzs, double[] expectedIntensities, PeakList<? extends PeakAnnotation> peakList) throws NoSuchFieldException, IllegalAccessException {

        peakList.trimToSize();
        double[] mzs = getMzField((DoubleConstantPeakList<PeakAnnotation>) peakList);
        Assert.assertEquals(expectedMzs.length, mzs.length);
        for (int i = 0; i < mzs.length; i++) {

            Assert.assertEquals(expectedMzs[i], mzs[i], deltaMz);
        }
    }

    private <A extends PeakAnnotation> DoubleConstantPeakList<A> newPeakList(int initialCapacity) {

        if(initialCapacity == 0) return new DoubleConstantPeakList<A>(1);
        else return new DoubleConstantPeakList<A>(1, initialCapacity);
    }

    @Override
    protected <A extends PeakAnnotation> int getIntensityArrayLength(PeakList<A> peakList) throws NoSuchFieldException, IllegalAccessException {

        //Delegate to mz as constant peak list has no intensity array
        return getMzArrayLength(peakList);
    }

    @Override
    protected <A extends PeakAnnotation> int getMzArrayLength(PeakList<A> peakList) throws NoSuchFieldException, IllegalAccessException {

        return getMzField((DoubleConstantPeakList<PeakAnnotation>) peakList).length;
    }

    protected <A extends PeakAnnotation> DoubleConstantPeakList<A> newPeakList() {

        return newPeakList(0);
    }

    /**
     * Does not call runTestEquals because constant peak lists only test m/z not intensities
     * @throws Exception
     */
    @Override
    @Test
    public void testEquals() throws Exception {

        PeakList<PeakAnnotation> peakList1 = newPeakList();
        PeakList<PeakAnnotation> peakList2 = newPeakList();

        build(peakList1, new double[]{4, 5, 6}, new double[]{1, 1, 1});
        build(peakList2, new double[]{4, 5, 6}, new double[]{2, 2, 2});

        Assert.assertEquals(true, peakList1.equals(peakList2));
    }

    /**
     * Does not call runTestEquals because constant peak lists only test m/z not intensities
     * @throws Exception
     */
    @Override
    @Test
    public void testHashCode() throws Exception {

        DoubleConstantPeakList<PeakAnnotation> peakList1 = newPeakList();
        DoubleConstantPeakList<PeakAnnotation> peakList2 = newPeakList();

        build(peakList1, new double[]{4, 5, 6}, new double[]{1, 1, 1});
        build(peakList2, new double[]{4, 5, 6}, new double[]{2, 2, 2});

        Assert.assertTrue(peakList1.hashCode() == peakList2.hashCode());
    }

    @Override
    @Test
    public void testGetIntensitiesArr() throws Exception {

        runTestGetIntensitiesArr(newPeakList());
    }

    @Override
    @Test
    public void testGetIntensities() throws Exception {

        PeakList<PeakAnnotation> peakList = newPeakList();

        runTestGetIntensities(peakList);
    }

    @Override
    @Test
    public void testGetIntensities2() throws Exception {

        runTestGetIntensities2(newPeakList());
    }

    @Override
    @Test
    public void testGetIntensities3() throws Exception {

        runTestGetIntensities3(newPeakList());
    }

    @Override
    @Test
    public void testGetIntensities4() throws Exception {

        runTestGetIntensities4(newPeakList());
    }

    @Override
    @Test
    public void testGetIntensities5() throws Exception {

        runTestGetIntensities5(newPeakList());
    }

    @Override
    @Test
    public void testGetMzs1() throws Exception {

        runTestGetMzs1(newPeakList());
    }

    @Override
    @Test
    public void testGetMzs2() throws Exception {

        runTestGetMzs2(newPeakList());
    }

    @Override
    @Test
    public void testGetMzs3() throws Exception {

        runTestGetMzs3(newPeakList());
    }

    @Override
    @Test
    public void testGetIntensityAt() throws Exception {

        PeakList<PeakAnnotation> peakList = newPeakList();

        runTestGetIntensityAt(peakList);
    }

    @Test
    public void testAddSame() {

        runTestAddSame(newPeakList());
    }

    @Override
    @Test
    public void testEnsureCapacity() throws NoSuchFieldException, IllegalAccessException {

        runTestEnsureCapacity(newPeakList());
    }

    @Override
    @Test
    public void testSetLoadFactor() throws NoSuchFieldException, IllegalAccessException {

        runTestSetLoadFactor(newPeakList());
    }

    @Override
    @Test
    public void testGetMostIntenseIndex() {

        runTestGetMostIntenseIndex(newPeakList(15));
    }

    @Override
    @Test
    public void testGetMostIntenseIndex2() {

        runTestGetMostIntenseIndex2(newPeakList(0));
    }

    @Override
    @Test
    public void testGetMostIntenseIndex3() {

        runTestGetMostIntenseIndex3(newPeakList(0));
    }

    @Override
    @Test
    public void testGetMostIntenseIndex4() {

        runTestGetMostIntenseIndex4(newPeakList(0));
    }

    @Override
    @Test
    public void testGetMostIntenseIndex5() {

        runTestGetMostIntenseIndex5(newPeakList(0));
    }

    @Override
    @Test
    public void testTotalIonCurrent() {

        DoubleConstantPeakList<PeakAnnotation> peakList = newPeakList();

        build(peakList, new double[]{58.3, 87.6, 894.6}, new double[]{1, 2, 3});

        Assert.assertEquals(3.0, peakList.getTotalIonCurrent(), 0.00001);
    }

    @Override
    @Test
    public void testCopyMzs() {

        runTestCopyMzs(newPeakList(0));
    }

    @Override
    @Test(expected = UnsupportedOperationException.class)
    public void testSetIntensity() {

        runTestSetIntensity(newPeakList());
    }

    @Override
    @Test
    public void testValueOf() {

        runTestValueOf(precision);
    }

    @Override
    @Test
    public void testValueOfWithIntensities() {

        runTestValueOfWithIntensities(precision);
    }

    @Override
    @Test
    public void testAddPeakWithAnnotation() throws Exception {

        runTestAddPeakWithAnnotation(new DoubleConstantPeakList<MockPeakAnnotation>(1));
    }

    @Override
    @Test
    public void testGetMostIntenseIndexWholeSpectra() throws Exception {

        PeakList peakList = new DoubleConstantPeakList(2);

        Assert.assertEquals(-1, peakList.getMostIntenseIndex());

        peakList.addSorted(new double[]{1, 2, 3, 4, 5, 6}, new double[]{1, 2, 5, 5, 4, 2});
        Assert.assertEquals(0, peakList.getMostIntenseIndex());
    }

    @Override
    @Test(expected = IndexOutOfBoundsException.class)
    public void testEmptyPeakListGetMz() throws Exception {

        PeakList<PeakAnnotation> peakList = newPeakList();
        runTestEmptyPeakListGetMz(peakList);
    }

    @Override
    @Test
    public void testEmptyPeakListGetMzs() throws Exception {

        runTestEmptyPeakListGetMzs(newPeakList());
    }

    @Override
    @Test(expected = IndexOutOfBoundsException.class)
    public void testEmptyPeakListGetIntensityAt() throws Exception {

        runTestEmptyPeakListGetIntensityAt(newPeakList());
    }

    @Override
    @Test
    public void testEmptyPeakListGetIntensities() throws Exception {

        runTestEmptyPeakListGetIntensities(newPeakList());
    }

    @Override
    @Test
    public void testClear() throws Exception {

        PeakList<PeakAnnotation> peakList = newPeakList();
        runTestClear(peakList);
    }

    @Override
    @Test
    public void testPrecision() throws Exception {

        PeakList<PeakAnnotation> peakList = newPeakList();
        PeakList.Precision precision = this.precision;
        runTestPrecision(peakList, precision);
    }

    @Test
    public void testBulkAdd() throws Exception {

        runTestBulkAdd(newPeakList());
    }

    @Override
    @Test
    public void testBulkAdd2() throws Exception {

        runTestBulkAdd2(newPeakList());
    }

    @Override
    @Test
    public void testBulkAdd3() throws Exception {

        runTestBulkAdd3(newPeakList());
    }

    @Override
    @Test
    public void testBulkAddException() throws Exception {

        runTestAddException(newPeakList());
    }

    @Override
    @Test
    public void testBulkAddException2() throws Exception {

        runTestBulkAddException2(newPeakList());
    }

    @Override
    @Test
    public void testBulkAddException3() throws Exception {

        runTestBulkAddException3(newPeakList());
    }

    @Override
    @Test
    public void testBulkAddException4() throws Exception {

        runTestBulkAddException4(newPeakList());
    }

    @Test
    public void testInsert() throws Exception {

        runTestInsert(newPeakList());
    }

    @Test
    public void testMerge() throws Exception {

        runTestMerge(newPeakList());
    }

    @Test
    public void testMerge2() {

        runTestMerge2(newPeakList());
    }

    @Test
    public void testMerge3() {

        runTestMerge3(newPeakList());
    }

    @Test
    public void testMerge4() {

        runTestMerge4(newPeakList());
    }

    @Test
    public void testMerge5() throws Exception {

        runTestMerge5(new DoubleConstantPeakList<MockPeakAnnotation>(1));
    }

    @Test
    public void testMerge6() throws Exception {

        runTestMerge6(new DoubleConstantPeakList<MockPeakAnnotation>(1), new DoubleConstantPeakList<MockPeakAnnotation>(1));
    }

    @Override
    public void testAddPeaks() throws Exception {

        runTestAddPeaks(new DoubleConstantPeakList<MockPeakAnnotation>(1), new DoubleConstantPeakList<PeakAnnotation>(1));
    }

    @Test
    public void testAddDuplicateWithAnnotation() throws Exception {

        runTestAddDuplicateWithAnnotation(new DoubleConstantPeakList<PeakAnnotation>(1), PeakAnnotation.class);
    }

    @Test
    public void testDoInsert() throws Exception {

        runTestDoInsert(newPeakList());
    }

    @Override
    public void testIndexOf() throws Exception {

        runTestIndexOf(new DoubleConstantPeakList(1));
    }

    @Override
    public void testGetClosestIndex() {

        runTestGetClosestIndex(new DoubleConstantPeakList(1));
    }
}