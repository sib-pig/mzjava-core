/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.io.ms.spectrum;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import gnu.trove.list.array.TIntArrayList;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.Polarity;
import org.expasy.mzjava.utils.URIBuilder;
import org.expasy.mzjava.core.ms.spectrum.*;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class MgfReaderTest extends AbstractReaderTest<PeakAnnotation, MsnSpectrum> {

    @Test
    public void testParseCharge() throws Exception {

        MgfReader reader = new MgfReader(new StringReader(""), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        TIntArrayList charges = reader.extractCharge("1");
        Assert.assertArrayEquals(new int[]{1}, charges.toArray());

        charges = reader.extractCharge("2 3");
        Assert.assertArrayEquals(new int[]{2, 3}, charges.toArray());

        charges = reader.extractCharge("1-");
        Assert.assertArrayEquals(new int[]{-1}, charges.toArray());

        charges = reader.extractCharge("+1-, -2+ and 3+");
        Assert.assertArrayEquals(new int[]{-1, 2, 3}, charges.toArray());

        charges = reader.extractCharge("-1-1-");
        Assert.assertArrayEquals(new int[]{-1, -1}, charges.toArray());

        charges = reader.extractCharge("-1-1");
        Assert.assertArrayEquals(new int[]{-1, 1}, charges.toArray());
    }

    @Test
    public void testParsePepMass() throws Exception {

        MgfReader reader = new MgfReader(new StringReader(""), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);
        MsnSpectrum spectrum = new MsnSpectrum(PeakList.Precision.DOUBLE);

        reader.parsePepMassTag("786.240 6955 1", spectrum);

        Peak precursor = spectrum.getPrecursor();
        assertEquals(786.240, precursor.getMz(), 0.001);
        assertEquals(6955.0, precursor.getIntensity(), 0.001);
        assertEquals(1, precursor.getCharge());

        spectrum = new MsnSpectrum(PeakList.Precision.DOUBLE);

        reader.parsePepMassTag("565.828 14150 2,3", spectrum);

        precursor = spectrum.getPrecursor();
        assertEquals(565.828, precursor.getMz(), 0.001);
        assertEquals(14150.0, precursor.getIntensity(), 0.001);
        Assert.assertArrayEquals(new int[]{2, 3}, precursor.getChargeList());
    }

    @Test
    public void testRetentionTime() throws Exception {

        MgfReader reader = new MgfReader(new StringReader(""), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);
        MsnSpectrum spectrum = new MsnSpectrum(PeakList.Precision.DOUBLE);

        reader.parseRetentionTimeTag("12.76-456.98", spectrum);

        RetentionTimeList rtList = spectrum.getRetentionTimes();

        assertEquals(1, rtList.size());

        RetentionTimeInterval rt = (RetentionTimeInterval) rtList.get(0);
        assertEquals(12.76, rt.getMinRetentionTime(), 0.01);
        assertEquals(456.98, rt.getMaxRetentionTime(), 0.01);
    }

    @Test
    public void testRead() throws Exception {

        String mgf =
                "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.240 6955 1\n" +
                        "CHARGE=1+\n" +
                        "197.725 96 0\n" +
                        "198.053 38 0\n" +
                        "199.141 34 0\n" +
                        "711.524 29 0\n" +
                        "715.513 48 0\n" +
                        "738.915 1.86E2 0\n" +
                        "739.374 81 1\n" +
                        "743.954 37 0\n" +
                        "744.386 88 0\n" +
                        "7.51116E2 90 0\n" +
                        "768.535 462 0\n" +
                        "771.675 169 1\n" +
                        "772.725 70 0\n" +
                        "END IONS";

        MgfReader reader = new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        Assert.assertTrue("mgf has one spectrum", reader.hasNext());

        MsnSpectrum spectrum = reader.next();

        reader.close();

        assertEquals(1, spectrum.getPrecursor().getCharge());
        assertEquals(786.24, spectrum.getPrecursor().getMz(), 0.00001);
        assertEquals(6955.0, spectrum.getPrecursor().getIntensity(), 0.00001);
        assertEquals("Cmpd 11, +MSn(786.25) 16.0 min", spectrum.getComment());

        checkPeak(197.725, 96, 0, spectrum);
        checkPeak(198.053, 38, 1, spectrum);
        checkPeak(199.141, 34, 2, spectrum);
        checkPeak(711.524, 29, 3, spectrum);
        checkPeak(715.513, 48, 4, spectrum);
        checkPeak(738.915, 1.86E2, 5, spectrum);
        checkPeak(739.374, 81, 6, spectrum);
        checkPeak(743.954, 37, 7, spectrum);
        checkPeak(744.386, 88, 8, spectrum);
        checkPeak(7.51116E2, 90, 9, spectrum);
        checkPeak(768.535, 462, 10, spectrum);
        checkPeak(771.675, 169, 11, spectrum);
        checkPeak(772.725, 70, 12, spectrum);
    }

    @Test
    public void testIssue66() throws Exception {

        String mgf = "BEGIN IONS\n" +
                "TITLE=sample=1 period=1 cycle=1 experiment=14\n" +
                "RTINSECONDS=1\n" +
                "PEPMASS=655.216931982634\n" +
                "74.06076846 15\n" +
                "99.05170831 15\n" +
                "130.0460095 15\n" +
                "END IONS";

        MgfReader reader = new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        Assert.assertTrue("mgf has one spectrum", reader.hasNext());

        MsnSpectrum spectrum = reader.next();

        reader.close();

        assertEquals(0, spectrum.getPrecursor().getCharge());
        assertEquals(655.216931982634, spectrum.getPrecursor().getMz(), 0.00000000000001);
        assertEquals(0, spectrum.getPrecursor().getIntensity(), 0.00001);
        assertEquals("sample=1 period=1 cycle=1 experiment=14", spectrum.getComment());
        assertEquals(1, spectrum.getRetentionTimes().getFirst().getTime(), 0.0000001);

        checkPeak(74.06076846, 15, 0, spectrum);
        checkPeak(99.05170831, 15, 1, spectrum);
        checkPeak(130.0460095, 15, 2, spectrum);
    }

    @Test
    public void testIssue66_2() throws Exception {

        String mgf = "BEGIN IONS\n" +
                "TITLE=sample=1 period=1 cycle=1 experiment=13\n" +
                "RTINSECONDS=0.928\n" +
                "PEPMASS=630.210899103787\n" +
                "142.9849912 15\n" +
                "156.0782752 15\n" +
                "159.0913177 15\n" +
                "207.0823782 15\n" +
                "END IONS\n" +
                "BEGIN IONS\n" +
                "TITLE=sample=1 period=1 cycle=1 experiment=14\n" +
                "RTINSECONDS=1\n" +
                "PEPMASS=655.216931982634\n" +
                "74.06076846 15\n" +
                "99.05170831 15\n" +
                "130.0460095 15\n" +
                "END IONS\n" +
                "BEGIN IONS\n" +
                "TITLE=sample=1 period=1 cycle=1 experiment=15\n" +
                "RTINSECONDS=1.071\n" +
                "PEPMASS=680.22253045018\n" +
                "58.07488868 15\n" +
                "101.0725161 15\n" +
                "102.0534628 15\n" +
                "123.0163917 15\n" +
                "133.0549299 15\n" +
                "145.1129122 15\n" +
                "END IONS";

        MgfReader reader = new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        Assert.assertTrue("mgf has three spectra", reader.hasNext());
        reader.next();
        Assert.assertTrue("mgf has three spectra", reader.hasNext());

        MsnSpectrum spectrum = reader.next();

        reader.close();

        assertEquals(0, spectrum.getPrecursor().getCharge());
        assertEquals(655.216931982634, spectrum.getPrecursor().getMz(), 0.00000000000001);
        assertEquals(0, spectrum.getPrecursor().getIntensity(), 0.00001);
        assertEquals("sample=1 period=1 cycle=1 experiment=14", spectrum.getComment());
        assertEquals(1, spectrum.getRetentionTimes().getFirst().getTime(), 0.0000001);

        checkPeak(74.06076846, 15, 0, spectrum);
        checkPeak(99.05170831, 15, 1, spectrum);
        checkPeak(130.0460095, 15, 2, spectrum);
    }

    @Test
    public void testAnalystTitle() throws Exception {

        String mgf = "BEGIN IONS\n" +
                "TITLE=File: 4-way iTRAQ Jun07 100mM.wiff, Sample: 4-way iTRAQ Jun07 100mM (sample number 1), Elution: 7.98 min, Period: 1, Cycle(s): 439 (Experiment 2) (Charge not auto determined)\n" +
                "RTINSECONDS=1\n" +
                "PEPMASS=655.216931982634\n" +
                "74.06076846 15\n" +
                "99.05170831 15\n" +
                "130.0460095 15\n" +
                "END IONS";

        MgfReader reader = new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        Assert.assertTrue("mgf has one spectrum", reader.hasNext());

        MsnSpectrum spectrum = reader.next();

        reader.close();

        assertEquals(0, spectrum.getPrecursor().getCharge());
        assertEquals(655.216931982634, spectrum.getPrecursor().getMz(), 0.00000000000001);
        assertEquals(0, spectrum.getPrecursor().getIntensity(), 0.00001);
        assertEquals("File: 4-way iTRAQ Jun07 100mM.wiff, Sample: 4-way iTRAQ Jun07 100mM (sample number 1), Elution: 7.98 min, Period: 1, Cycle(s): 439 (Experiment 2) (Charge not auto determined)", spectrum.getComment());
        assertEquals(478.8, spectrum.getRetentionTimes().getFirst().getTime(), 0.0000001);

        checkPeak(74.06076846, 15, 0, spectrum);
        checkPeak(99.05170831, 15, 1, spectrum);
        checkPeak(130.0460095, 15, 2, spectrum);
    }

    @Test
    public void testReadNoHasNext() throws Exception {

        String mgf =
                "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.240 6955 1\n" +
                        "CHARGE=1+\n" +
                        "197.725 96 0\n" +
                        "198.053 38 0\n" +
                        "199.141 34 0\n" +
                        "711.524 29 0\n" +
                        "715.513 48 0\n" +
                        "738.915 1.86E2 0\n" +
                        "739.374 81 1\n" +
                        "743.954 37 0\n" +
                        "744.386 88 0\n" +
                        "7.51116E2 90 0\n" +
                        "768.535 462 0\n" +
                        "771.675 169 1\n" +
                        "772.725 70 0\n" +
                        "END IONS";

        MgfReader reader = new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        MsnSpectrum spectrum = reader.next();

        assertEquals(1, spectrum.getPrecursor().getCharge());
        assertEquals(786.24, spectrum.getPrecursor().getMz(), 0.00001);
        assertEquals(6955.0, spectrum.getPrecursor().getIntensity(), 0.00001);

        checkPeak(197.725, 96, 0, spectrum);
        checkPeak(198.053, 38, 1, spectrum);
        checkPeak(199.141, 34, 2, spectrum);
        checkPeak(711.524, 29, 3, spectrum);
        checkPeak(715.513, 48, 4, spectrum);
        checkPeak(738.915, 1.86E2, 5, spectrum);
        checkPeak(739.374, 81, 6, spectrum);
        checkPeak(743.954, 37, 7, spectrum);
        checkPeak(744.386, 88, 8, spectrum);
        checkPeak(7.51116E2, 90, 9, spectrum);
        checkPeak(768.535, 462, 10, spectrum);
        checkPeak(771.675, 169, 11, spectrum);
        checkPeak(772.725, 70, 12, spectrum);
    }

    @Test
    public void testReadNegCharge() throws Exception {

        String mgf =
                "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.240 6955 1\n" +
                        "CHARGE=1-\n" +
                        "197.725 96 0\n" +
                        "END IONS\n";

        MgfReader reader = new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        Assert.assertTrue("expecting one spectrum", reader.hasNext());

        MsnSpectrum spectrum = reader.next();

        assertEquals(1, spectrum.getPrecursor().getCharge());
        assertEquals(Polarity.NEGATIVE, spectrum.getPrecursor().getPolarity());

    }

    @Test
    public void testReadNegCharge2() throws Exception {

        String mgf =
                "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.240 6955 1\n" +
                        "CHARGE=-1\n" +
                        "197.725 96 0\n" +
                        "END IONS";

        MgfReader reader = new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        Assert.assertTrue("mgf has one spectrum", reader.hasNext());

        MsnSpectrum spectrum = reader.next();

        assertEquals(1, spectrum.getPrecursor().getCharge());
        assertEquals(Polarity.NEGATIVE, spectrum.getPrecursor().getPolarity());
    }

    @Test
    public void testReadPosCharge() throws Exception {

        String mgf =
                "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.240 6955 1\n" +
                        "CHARGE=1+\n" +
                        "197.725 96 0\n" +
                        "END IONS";

        MgfReader reader = new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        Assert.assertTrue("mgf has one spectrum", reader.hasNext());

        MsnSpectrum spectrum = reader.next();

        assertEquals(1, spectrum.getPrecursor().getCharge());
    }

    @Test
    public void testReadPosCharge2() throws Exception {

        String mgf =
                "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.240 6955 1\n" +
                        "CHARGE=+1\n" +
                        "197.725 96 0\n" +
                        "END IONS";

        MgfReader reader = new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        Assert.assertTrue("mgf has one spectrum", reader.hasNext());

        MsnSpectrum spectrum = reader.next();

        assertEquals(1, spectrum.getPrecursor().getCharge());
    }

    @Test
    public void testReadPosCharge3() throws Exception {

        String mgf =
                "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.240 6955 1\n" +
                        "CHARGE=1\n" +
                        "197.725 96 0\n" +
                        "END IONS";

        MgfReader reader = new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        Assert.assertTrue("mgf has one spectrum", reader.hasNext());

        MsnSpectrum spectrum = reader.next();

        assertEquals(1, spectrum.getPrecursor().getCharge());

    }

    @Test
    public void testRead2() throws IOException {

        String mgf =
                "SEARCH=MIS\n" +
                        "REPTYPE=Peptide\n" +
                        "BEGIN IONS\n" +
                        "PEPMASS=748.4\n" +
                        "CHARGE=2+\n" +
                        "TITLE=File: 4-way iTRAQ Jun07 100mM.wiff, Sample: 4-way iTRAQ Jun07 100mM (sample number 1), Elution: 7.98 min, Period: 1, Cycle(s): 439 (Experiment 2) (Charge not auto determined)\n" +
                        "101.0709 0.009\n" +
                        "114.1181 0.0048\n" +
                        "116.1069 0.0481\n" +
                        "130.0473 0.0102\n" +
                        "145.1212 0.0107\n" +
                        "168.0689 0.0058\n" +
                        "174.9078 0.0059\n" +
                        "175.12 0.0472\n" +
                        "225.1246 0.0402\n" +
                        "228.1703 0.0135\n" +
                        "244.0746 0.0139\n" +
                        "245.1898 0.014\n" +
                        "268.1433 0.0073\n" +
                        "278.672 0.0074\n" +
                        "279.2078 0.0074\n" +
                        "284.0837 0.0225\n" +
                        "289.8129 0.0076\n" +
                        "297.1182 0.0154\n" +
                        "314.1779 0.0158\n" +
                        "318.6803 0.0398\n" +
                        "319.1736 0.008\n" +
                        "326.7412 0.0081\n" +
                        "327.2085 0.0725\n" +
                        "340.2162 0.0329\n" +
                        "340.7424 0.0082\n" +
                        "341.1538 0.0165\n" +
                        "371.1784 0.0258\n" +
                        "384.7445 0.0175\n" +
                        "387.266 0.0175\n" +
                        "391.2222 0.0176\n" +
                        "399.1148 0.0267\n" +
                        "400.2283 0.0267\n" +
                        "405.7387 0.0269\n" +
                        "409.2198 0.018\n" +
                        "414.7371 0.0182\n" +
                        "430.1416 0.0185\n" +
                        "448.739 0.0189\n" +
                        "456.2939 0.019\n" +
                        "459.2973 0.0095\n" +
                        "462.253 0.0383\n" +
                        "479.298 0.0488\n" +
                        "479.8152 0.0098\n" +
                        "491.8272 0.0099\n" +
                        "497.2974 0.0497\n" +
                        "519.3082 0.0203\n" +
                        "521.9111 0.0102\n" +
                        "527.7811 0.0819\n" +
                        "527.9039 0.0102\n" +
                        "528.3033 0.041\n" +
                        "539.3919 0.0103\n" +
                        "547.0567 0.0104\n" +
                        "547.4424 0.0104\n" +
                        "593.3775 0.0326\n" +
                        "611.4782 0.022\n" +
                        "613.3419 0.0221\n" +
                        "636.3652 0.1012\n" +
                        "658.7029 0.0114\n" +
                        "662.9184 0.0344\n" +
                        "666.4913 0.023\n" +
                        "677.8711 0.0348\n" +
                        "699.6578 0.0236\n" +
                        "705.4335 0.0237\n" +
                        "714.3856 0.0357\n" +
                        "714.6834 0.0119\n" +
                        "739.2164 0.0242\n" +
                        "742.1877 0.0121\n" +
                        "745.3596 0.0487\n" +
                        "745.6151 0.0487\n" +
                        "748.3919 0.0975\n" +
                        "748.6358 0.0732\n" +
                        "749.0138 0.0366\n" +
                        "749.2211 0.0122\n" +
                        "749.3919 0.0488\n" +
                        "749.8189 0.0122\n" +
                        "772.2903 0.0248\n" +
                        "782.8647 0.0125\n" +
                        "794.3899 0.0251\n" +
                        "799.3083 0.063\n" +
                        "799.9887 0.0504\n" +
                        "800.1652 0.0126\n" +
                        "800.3291 0.1639\n" +
                        "800.846 0.0252\n" +
                        "804.6589 0.0126\n" +
                        "804.9497 0.0885\n" +
                        "812.8962 0.0127\n" +
                        "843.053 0.0259\n" +
                        "861.8397 0.0262\n" +
                        "866.4638 0.0131\n" +
                        "874.2728 0.0132\n" +
                        "895.228 0.0533\n" +
                        "895.7481 0.0267\n" +
                        "899.4595 0.0401\n" +
                        "923.8794 0.0271\n" +
                        "998.552 0.0282\n" +
                        "END IONS";

        MgfReader reader = new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        Assert.assertTrue("Expecting a spectra", reader.hasNext());
        checkSpectra(reader.next(), new ScanNumber[0], 2, 94, new RetentionTime[]{new RetentionTimeDiscrete(478.8, TimeUnit.SECOND)}, 2.64039, 748.4, 2);
        Assert.assertFalse("No more spectra to read", reader.hasNext());
    }

    @Test
    public void testRead3() throws IOException {

        String mgf = "SEARCH=MIS\n" +
                "REPTYPE=Peptide\n" +
                "BEGIN IONS\n" +
                "PEPMASS=555.239734369881\n" +
                "CHARGE=2+\n" +
                "TITLE=File: 80xdil 25 peptides.wiff, Sample: 80xdil 25 peptides (sample number 1), Elution: 8 min, Period: 1, Cycle(s): 426 (Experiment 2)\n" +
                "553.9055 0.021\n" +
                "554.4411 0.0735\n" +
                "554.6932 0.021\n" +
                "554.9244 0.0525\n" +
                "555.1872 0.904\n" +
                "555.3764 0.0105\n" +
                "555.4395 0.021\n" +
                "555.6813 0.4312\n" +
                "555.8286 0.0526\n" +
                "556.1757 0.1368\n" +
                "556.2704 0.0421\n" +
                "935.4138 0.0273\n" +
                "END IONS\n" +
                "";
        MgfReader parser = new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);


        Assert.assertTrue(parser.hasNext());
        checkSpectra(parser.next(), new ScanNumber[0], 2, 12, new RetentionTime[]{new RetentionTimeDiscrete(480, TimeUnit.SECOND)}, 1.7935, 555.239734369881, 2);
        Assert.assertFalse(parser.hasNext());
    }

    @Test
    public void testExtension() throws Exception {

        StringReader reader = new StringReader("");
        assertEquals("mgf", new MgfReader(reader, new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE).getExtension());
    }

    @Test
    public void testMessySpectrum() throws Exception {

        StringReader reader = new StringReader("INSTRUMENT=Violin\n" +
                "\n" +
                "800.42261\t520.85944\n" +
                "801.40369\t393.4707\n" +
                "\n" +
                "BEGIN IONS\n" +
                "TITLE=p1_1137697_A1_51.pkl (query 1)\n" +
                "CHARGE=1+\n" +
                "PEPMASS=861.082763671875\n" +
                "17.224962 14.605842\n" +
                "\n" +
                "\n" +
                "    \n" +
                "22.851137 14.845939 abc\n" +
                "22.991457 810.3001\n" +
                "\n" +
                "END IONS\n" +
                "\n" +
                "BEGIN IONS\n" +
                "TITLE=p1_1137697_A1_51.pkl (query 2)\n" +
                "CHARGE=1+\n" +
                "PEPMASS=877.054321289063\n" +
                "22.862453 12.180872\n" +
                "38.82884     13.694568\t\t\n" +
                "\n" +
                "END IONS\n" +
                "\n");

        MgfReader mgfReader = new MgfReader(reader, new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.FLOAT);

        List<MsnSpectrum> spectra = new ArrayList<MsnSpectrum>();

        while (mgfReader.hasNext()) {

            spectra.add(mgfReader.next());
        }

        Assert.assertEquals(3, spectra.size());

        checkSpectra(spectra.get(0), new ScanNumber[0], 1, 2, new RetentionTime[0], 914.3301391601562, 0, 0);
        checkPeak(800.42261, 520.85944, 0, spectra.get(0));
        checkPeak(801.40369, 393.4707, 1, spectra.get(0));

        checkSpectra(spectra.get(1), new ScanNumber[0], 2, 3, new RetentionTime[0], 839.7518901824951, 861.082763671875, 1);
        checkPeak(17.224962, 14.605842, 0, spectra.get(1));
        checkPeak(22.851137, 14.845939, 1, spectra.get(1));
        checkPeak(22.991457, 810.3001, 2, spectra.get(1));

        checkSpectra(spectra.get(2), new ScanNumber[0], 2, 2, new RetentionTime[0], 25.875439643859863, 877.054321289063, 1);
        checkPeak(22.862453, 12.180872, 0, spectra.get(2));
        checkPeak(38.82884, 13.694568, 1, spectra.get(2));
    }

    @Test
    public void testDefaultCharge() throws Exception {

        StringReader mgf = new StringReader(
                "COM=Project: blabla\n" +
                        "CHARGE=2+, 3+ AND 4+\n" +
                        "INSTRUMENT=Violin\n" +
                        "\n" +
                        "800.42261\t520.85944\n" +
                        "801.40369\t393.4707\n" +
                        "804.4314\t317.92062\n" +
                        "806.37402\t1502.3396\n" +
                        "809.42621\t258.33871\n" +
                        "810.42065\t268.10699\n" +
                        "813.4176\t289.59726\n" +
                        "817.41772\t329.18951\n" +
                        "822.42627\t323.10419\n" +
                        "823.42096\t260.3602\n" +
                        "\n" +
                        "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.240 6955 1\n" +
                        "197.725 96 0\n" +
                        "198.053 38 0\n" +
                        "END IONS\n"
        );
        MgfReader reader = new MgfReader(mgf, new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        MsnSpectrum spectrum = reader.next();
        assertEquals(2, spectrum.getPrecursor().getCharge());

        spectrum = reader.next();
        assertEquals(1, spectrum.getPrecursor().getCharge());
    }

    @Test
    public void testScan() throws Exception {

        MgfReader reader = new MgfReader(new StringReader(""), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        MsnSpectrum spectrum = new MsnSpectrum();
        reader.parseScanTag("1278,1280-1284,1290-1294,1298", spectrum);

        List<ScanNumber> scanNumbers = spectrum.getScanNumbers();
        assertEquals(4, scanNumbers.size());
    }

    private void checkSpectra(MsnSpectrum spectrum, ScanNumber[] scanNumbers, int msLevel, int peakCount, RetentionTime[] retentionTime, double totalIonCurrent, double precursorMz, int charge) {

        final ScanNumberList acutalScanNumbers = spectrum.getScanNumbers();
        assertEquals("Scan number size", scanNumbers.length, acutalScanNumbers.size());
        for (int i = 0; i < scanNumbers.length; i++) {

            assertEquals(scanNumbers[i], acutalScanNumbers.get(i));
        }

        assertEquals("Ms level", msLevel, spectrum.getMsLevel());
        final RetentionTimeList actualRetentionTimes = spectrum.getRetentionTimes();
        assertEquals("Retention time size", retentionTime.length, actualRetentionTimes.size());
        for (int i = 0; i < retentionTime.length; i++) {

            assertEquals(retentionTime[i], actualRetentionTimes.get(i));
        }

        assertEquals("Peak count", peakCount, spectrum.size());
        assertEquals("Total ion current", totalIonCurrent, spectrum.getTotalIonCurrent(), 0.00001);
        assertEquals("Precursor mz", precursorMz, spectrum.getPrecursor().getMz(), 0.00001);
        assertEquals("Charge", charge, spectrum.getPrecursor().getCharge());
    }

    private void checkPeak(double expectedMz, double expectedIntensity, int index, PeakList peakList) {

        assertEquals("Mz", expectedMz, peakList.getMz(index), 0.00001);
        assertEquals("Intensity", expectedIntensity, peakList.getIntensity(index), 0.00001);
    }

    @Test
    public void testReadUnsortedPeaks() throws Exception {

        String mgf =
                "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.240 6955 1\n" +
                        "CHARGE=1+\n" +
                        "197.725 96 0\n" +
                        "198.053 38 0\n" +
                        "199.141 34 0\n" +
                        "715.513 48 0\n" +
                        "711.524 29 0\n" +
                        "738.915 1.86E2 0\n" +
                        "739.374 81 1\n" +
                        "743.954 37 0\n" +
                        "744.386 88 0\n" +
                        "7.51116E2 90 0\n" +
                        "768.535 462 0\n" +
                        "771.675 169 1\n" +
                        "772.725 70 0\n" +
                        "END IONS";

        MgfReader reader =
                new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);
        reader.acceptUnsortedSpectra();

        MsnSpectrum spectrum = reader.next();

        reader.close();

        assertEquals(1, spectrum.getPrecursor().getCharge());
        assertEquals(786.24, spectrum.getPrecursor().getMz(), 0.00001);
        assertEquals(6955.0, spectrum.getPrecursor().getIntensity(), 0.00001);
        assertEquals("Cmpd 11, +MSn(786.25) 16.0 min", spectrum.getComment());

        checkPeak(197.725, 96, 0, spectrum);
        checkPeak(198.053, 38, 1, spectrum);
        checkPeak(199.141, 34, 2, spectrum);
        checkPeak(711.524, 29, 3, spectrum);
        checkPeak(715.513, 48, 4, spectrum);
        checkPeak(738.915, 1.86E2, 5, spectrum);
        checkPeak(739.374, 81, 6, spectrum);
        checkPeak(743.954, 37, 7, spectrum);
        checkPeak(744.386, 88, 8, spectrum);
        checkPeak(7.51116E2, 90, 9, spectrum);
        checkPeak(768.535, 462, 10, spectrum);
        checkPeak(771.675, 169, 11, spectrum);
        checkPeak(772.725, 70, 12, spectrum);
    }


    @Test(expected = IOException.class)
    public void testReadUnsortedPeaksNotAccepted() throws IOException {

        String mgf =
                "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.240 6955 1\n" +
                        "CHARGE=1+\n" +
                        "197.725 96 0\n" +
                        "198.053 38 0\n" +
                        "199.141 34 0\n" +
                        "715.513 48 0\n" +
                        "711.524 29 0\n" +
                        "738.915 1.86E2 0\n" +
                        "739.374 81 1\n" +
                        "743.954 37 0\n" +
                        "744.386 88 0\n" +
                        "7.51116E2 90 0\n" +
                        "768.535 462 0\n" +
                        "771.675 169 1\n" +
                        "772.725 70 0\n" +
                        "END IONS";

        MgfReader reader =
                new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        reader.next();
    }

    @Test //review does this test any production code?
    public void testOverridingParseUnknownTag() throws IOException {

        String mgf =
                "BEGIN IONS\n" +
                        "TITLE=HUP_18_rt1800.0-2100.0_mz822.0-823.0_withRT0.7305,7326.2\n" +
                        "PEPMASS=822.000\t946967.268\n" +
                        "CHARGE=2+\n" +
                        "SCANS=7305,7326\n" +
                        "QUANTIPEAKS=[7305=>202337.29, 7326=>744629.98]\n" +
                        "198.053 38 0\n" +
                        "199.141 34 0\n" +
                        "711.524 29 0\n" +
                        "715.513 48 0\n" +
                        "738.915 1.86E2 0\n" +
                        "739.374 81 1\n" +
                        "743.954 37 0\n" +
                        "744.386 88 0\n" +
                        "7.51116E2 90 0\n" +
                        "768.535 462 0\n" +
                        "771.675 169 1\n" +
                        "772.725 70 0\n" +
                        "END IONS";

        ParseUnknownTagOverridingReader reader =
                new ParseUnknownTagOverridingReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        reader.next();

        Map<Integer, Double> map = reader.getQuantiByScan();

        Assert.assertEquals(2, map.size());
        Assert.assertTrue(map.containsKey(7305));
        Assert.assertEquals(202337.29, map.get(7305), 0.01);
        Assert.assertTrue(map.containsKey(7326));
        Assert.assertEquals(744629.98, map.get(7326), 0.01);
    }

    @Test
    public void testReadEmptyPeaks2() throws Exception {

        String mgf =
                "BEGIN IONS\n" +
                        "TITLE=Normoxia-9old-preview_mode_1.1039.1039.4\n" +
                        "SCANS=1039\n" +
                        "RTINSECONDS=806.6521\n" +
                        "PEPMASS=1585.275512695313 7151.875\n" +
                        "CHARGE=4+\n" +
                        "END IONS\n";

        MgfReader reader = new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        int count = 0;
        while (reader.hasNext()) {

            MsnSpectrum sp = reader.next();

            Assert.assertEquals(0, sp.size());

            count++;
        }
        reader.close();

        Assert.assertEquals(1, count);
    }

    @Test
    public void testReadEmptyPeaks() throws Exception {

        String mgf =
                "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.240 6955 1\n" +
                        "CHARGE=1+\n" +
                        "END IONS\n" +
                        "BEGIN IONS\n" +
                        "TITLE=HUP_18_rt1800.0-2100.0_mz822.0-823.0_withRT0.7305,7326.2\n" +
                        "PEPMASS=822.000\t946967.268\n" +
                        "CHARGE=2+\n" +
                        "SCANS=7305,7326\n" +
                        "QUANTIPEAKS=[7305=>202337.29, 7326=>744629.98]\n" +
                        "198.053 38 0\n" +
                        "199.141 34 0\n" +
                        "711.524 29 0\n" +
                        "715.513 48 0\n" +
                        "738.915 1.86E2 0\n" +
                        "739.374 81 1\n" +
                        "743.954 37 0\n" +
                        "744.386 88 0\n" +
                        "7.51116E2 90 0\n" +
                        "768.535 462 0\n" +
                        "771.675 169 1\n" +
                        "772.725 70 0\n" +
                        "END IONS";

        MgfReader reader = new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);
        List<Integer> expectedSize = Lists.newArrayList(0, 12);

        int count = 0;
        while (reader.hasNext()) {

            MsnSpectrum sp = reader.next();

            Assert.assertEquals(expectedSize.get(count).intValue(), sp.size());

            count++;
        }
        reader.close();

        Assert.assertEquals(2, count);
    }

    @Test(expected = IOException.class)
    public void testReadBadlyFormattedEntry() throws Exception {

        String mgf =
                "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.240 6955 1\n" +
                        "CHARGE=1+\n" +
                        "END IONS\n" +

                        "BEGIN IONS\n" +
                        "TITLE=HUP_18_rt1800.0-2100.0_mz822.0-823.0_withRT0.7305,7326.2\n" +
                        "PEPMASS=822.000\t946967.268\n" +
                        "CHARGE=2+\n" +
                        "SCANS=7305,7326\n" +
                        "QUANTIPEAKS=[7305=>202337.29, 7326=>744629.98]\n" +
                        "198.053 38 0\n" +
                        "199.141 34 0\n" +
                        "711.524 29 0\n" +
                        "715.513 48 0\n" +
                        "738.915 1.86E2 0\n" +
                        "739.374 81 1\n" +
                        "743.954 37 0\n" +
                        "744.386 88 0\n" +
                        "7.51116E2 90 0\n" +
                        "768.535 462 0\n" +
                        "771.675 169 1\n" +
                        "772.725 70 0\n" +

                        "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.240 6955 1\n" +
                        "CHARGE=1+\n" +
                        "END IONS\n";

        MgfReader reader = new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        while (reader.hasNext()) {

            MsnSpectrum sp = reader.next();

            Assert.assertEquals(0, sp.size());
        }
        reader.close();
    }

    @Test
    public void testReadBadlyFormattedEntry2() throws Exception {

        String mgf =
                "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.240 6955 1\n" +
                        "CHARGE=1+\n" +
                        "END IONS\n" +

                        "BEGIN IONS\n" +
                        "TITLE=HUP_18_rt1800.0-2100.0_mz822.0-823.0_withRT0.7305,7326.2\n" +
                        "PEPMASS=822.000\t946967.268\n" +
                        "CHARGE=2+\n" +
                        "SCANS=7305,7326\n" +
                        "QUANTIPEAKS=[7305=>202337.29, 7326=>744629.98]\n" +
                        "198.053 38 0\n" +
                        "199.141 34 0\n" +
                        "711.524 29 0\n" +
                        "715.513 48 0\n" +
                        "738.915 1.86E2 0\n" +
                        "739.374 81 1\n" +
                        "743.954 37 0\n" +
                        "744.386 88 0\n" +
                        "7.51116E2 90 0\n" +
                        "768.535 462 0\n" +
                        "771.675 169 1\n" +
                        "772.725 70 0\n" +

                        "BEGIN IONS\n" +
                        "TITLE=Cmpd 11, +MSn(786.25) 16.0 min\n" +
                        "PEPMASS=786.240 6955 1\n" +
                        "CHARGE=1+\n" +
                        "END IONS\n";

        MgfReader reader = new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        int count = 0;
        while (reader.hasNext()) {

            try {
                MsnSpectrum sp = reader.next();
                Assert.assertEquals(0, sp.size());

                count++;
            } catch (IOException e) {

                Assert.assertEquals("mandatory 'END IONS' tag is missing!: cannot parse entry, context=[source=software://www.expasy.ch/test, line=24]",
                        e.getMessage());
            }
        }
        reader.close();

        Assert.assertEquals(2, count);
    }

    @Test
    public void testReadWholeNumberRT() throws Exception {

        String mgf =
                "BEGIN IONS\n" +
                        "TITLE=Normoxia-9old-preview_mode_1.1039.1039.4\n" +
                        "SCANS=1039\n" +
                        "RTINSECONDS=1\n" +
                        "PEPMASS=1585.275512695313 7151.875\n" +
                        "CHARGE=4+\n" +
                        "END IONS\n";

        MgfReader reader = new MgfReader(new StringReader(mgf), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE);

        int count = 0;
        while (reader.hasNext()) {

            MsnSpectrum sp = reader.next();

            Assert.assertFalse(sp.getRetentionTimes().isEmpty());

            count++;
        }
        reader.close();

        Assert.assertEquals(1, count);
    }

    private static class ParseUnknownTagOverridingReader extends MgfReader {

        private Map<Integer, Double> quanti;

        public ParseUnknownTagOverridingReader(Reader reader, URI spectraSource, PeakList.Precision precision) throws IOException {

            super(reader, spectraSource, precision);

            quanti = Maps.newHashMap();
        }

        @Override
        protected boolean parseUnknownTag(String tag, String value, MsnSpectrum spectrum) {

            if (tag.startsWith("QUANTIPEAKS"))

                return parseQuantiPeaks(value);
            else

                return super.parseUnknownTag(tag, value, spectrum);
        }

        public Map<Integer, Double> getQuantiByScan() {

            return Collections.unmodifiableMap(quanti);
        }

        protected boolean parseQuantiPeaks(String value) {

            if (value.charAt(0) == '[' && value.charAt(value.length() - 1) == ']') {

                Map<String, String> stringMap = Splitter.on(",").trimResults().omitEmptyStrings().withKeyValueSeparator("=>").split(value.subSequence(1, value.length() - 1));

                for (String scan : stringMap.keySet()) {

                    quanti.put(Integer.parseInt(scan), Double.parseDouble(stringMap.get(scan)));
                }

                return true;
            }

            return false;
        }
    }

}
