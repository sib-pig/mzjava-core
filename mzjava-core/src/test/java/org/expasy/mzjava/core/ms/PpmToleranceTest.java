/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PpmToleranceTest {

    @Test
    public void testWithinTolerance() throws Exception {

        PpmTolerance tolerance = new PpmTolerance(50);

        Assert.assertEquals(false, tolerance.withinTolerance(80, 80 - 0.0041));
        Assert.assertEquals(true, tolerance.withinTolerance(80, 80 - 0.004));
        Assert.assertEquals(true, tolerance.withinTolerance(80, 80 - 0.0039));
        Assert.assertEquals(true, tolerance.withinTolerance(80, 80));
        Assert.assertEquals(true, tolerance.withinTolerance(80, 80 + 0.0039));
        Assert.assertEquals(true, tolerance.withinTolerance(80, 80 + 0.004));
        Assert.assertEquals(false, tolerance.withinTolerance(80, 80 + 0.0041));
    }

    @Test
    public void testCheck() throws Exception {

        PpmTolerance tolerance = new PpmTolerance(50);

        Assert.assertEquals(Tolerance.Location.SMALLER, tolerance.check(80, 80 - 0.0040001));
        Assert.assertEquals(Tolerance.Location.WITHIN, tolerance.check(80, 80 - 0.0039));
        Assert.assertEquals(Tolerance.Location.WITHIN, tolerance.check(80, 80));
        Assert.assertEquals(Tolerance.Location.WITHIN, tolerance.check(80, 80 + 0.0039));
        Assert.assertEquals(Tolerance.Location.LARGER, tolerance.check(80, 80 + 0.0040001));
    }

    @Test
    public void testGetMin() throws Exception {

        PpmTolerance tolerance = new PpmTolerance(50);

        Assert.assertEquals(80-0.004, tolerance.getMin(80), 0.00001);
    }

    @Test
    public void testGetMax() throws Exception {

        PpmTolerance tolerance = new PpmTolerance(50);

        Assert.assertEquals(80+0.004, tolerance.getMax(80), 0.00001);
    }

    @Test
    public void test1() throws Exception {

        double actualMr = 1257.6581;
        double expectedMr = 1257.6594;

        Tolerance tolerance = new PpmTolerance(1.035);

        Assert.assertEquals(true, tolerance.withinTolerance(expectedMr, actualMr));
    }
}
