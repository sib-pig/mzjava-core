/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.io.ms.spectrum;

import org.expasy.mzjava.core.ms.spectrum.RetentionTimeList;
import org.expasy.mzjava.core.ms.spectrum.Spectrum;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;

import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class AbstractReaderTest<A extends PeakAnnotation, S extends Spectrum<A>> {

    protected void checkRetentionTime(RetentionTimeList retentionTimeList, double... expectedTimes) {

        Assert.assertEquals(expectedTimes.length, retentionTimeList.size());
        for (int i = 0; i < expectedTimes.length; i++) {

            Assert.assertEquals(expectedTimes[i], retentionTimeList.get(i).getTime(), 0.000001);
        }
    }

    protected void checkPeak(double expectedMz, double expectedIntensity, int index, S spectrum, double delta, PeakAnnotation... expectedAnnotations) {

        Assert.assertEquals("Checking mz at " + index, expectedMz, spectrum.getMz(index), delta);
        Assert.assertEquals("Checking intensity at " + index, expectedIntensity, spectrum.getIntensity(index), delta);

        List<A> actualAnnotations = spectrum.getAnnotations(index);

        Assert.assertEquals("Checking annotations at " + index, expectedAnnotations.length, actualAnnotations.size());
        for (int i = 0; i < expectedAnnotations.length; i++) {

            PeakAnnotation expectedAnnotation = expectedAnnotations[i];
            PeakAnnotation actualAnnotation = actualAnnotations.get(i);

            Assert.assertEquals(expectedAnnotation, actualAnnotation);
        }
    }

    protected void checkPeak(double expectedMz, double expectedIntensity, int index, S spectrum, double delta) {

        Assert.assertEquals("Checking mz at " + index, expectedMz, spectrum.getMz(index), delta);
        Assert.assertEquals("Checking intensity at " + index, expectedIntensity, spectrum.getIntensity(index), delta);

        List<A> actualAnnotations = spectrum.getAnnotations(index);
        Assert.assertEquals(0, actualAnnotations.size());
    }
}
