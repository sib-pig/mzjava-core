package org.expasy.mzjava.core.ms.cluster;

import com.google.common.base.Optional;
import com.google.common.collect.Sets;
import org.expasy.mzjava.utils.function.Procedure;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.HashSet;
import java.util.UUID;

import static org.mockito.Mockito.mock;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@SuppressWarnings("unchecked")
public class DenseSimilarityGraphTest {

    /**
     * Graph:
     * <pre>
     *     A1-A2--B1--B2  C1
     *     |\ /|\ | /
     *     |/ \| \|/
     *     A3-A4  B3
     * </pre>
     *
     * @throws Exception
     */
    @Test
    public void test() throws Exception {

        UUID nodeA1 = UUID.fromString("a1000000-0000-0000-0000-000000000000");
        UUID nodeA2 = UUID.fromString("a2000000-0000-0000-0000-000000000000");
        UUID nodeA3 = UUID.fromString("a3000000-0000-0000-0000-000000000000");
        UUID nodeA4 = UUID.fromString("a4000000-0000-0000-0000-000000000000");

        UUID nodeB1 = UUID.fromString("b1000000-0000-0000-0000-000000000000");
        UUID nodeB2 = UUID.fromString("b2000000-0000-0000-0000-000000000000");
        UUID nodeB3 = UUID.fromString("b3000000-0000-0000-0000-000000000000");

        UUID nodeC1 = UUID.fromString("c1000000-0000-0000-0000-000000000000");

        DenseSimilarityGraph.Builder<UUID> builder = new DenseSimilarityGraph.Builder<>();
        SimEdge<UUID> edge1 = builder.add(nodeA1, nodeA2, 0.9);
        SimEdge<UUID> edge2 = builder.add(nodeA1, nodeA3, 0.9);
        SimEdge<UUID> edge3 = builder.add(nodeA1, nodeA4, 0.9);
        SimEdge<UUID> edge4 = builder.add(nodeA2, nodeA3, 0.9);
        SimEdge<UUID> edge5 = builder.add(nodeA2, nodeA4, 0.9);
        SimEdge<UUID> edge6 = builder.add(nodeA3, nodeA4, 0.9);

        SimEdge<UUID> edge7 = builder.add(nodeB1, nodeB2, 0.9);
        SimEdge<UUID> edge8 = builder.add(nodeB1, nodeB3, 0.9);
        SimEdge<UUID> edge9 = builder.add(nodeB2, nodeB3, 0.9);

        SimEdge<UUID> edge10 = builder.add(nodeA2, nodeB1, 0.8);
        SimEdge<UUID> edge11 = builder.add(nodeA2, nodeB3, 0.8);

        builder.add(nodeC1);

        Assert.assertEquals(11, builder.edgeCount());

        DenseSimilarityGraph<UUID> graph = builder.build();

        Assert.assertEquals(8, graph.getVertexCount());

        Assert.assertEquals(
                Sets.newHashSet(nodeB1, nodeB2, nodeA2),
                Sets.newHashSet(graph.getNeighbors(nodeB3)));

        Assert.assertEquals(
                new HashSet(),
                Sets.newHashSet(graph.getNeighbors(nodeC1)));

        Assert.assertEquals(
                Sets.newHashSet(nodeA1, nodeA2, nodeA3, nodeA4, nodeB1, nodeB2, nodeB3, nodeC1),
                Sets.newHashSet(graph.getVertices()));

        Assert.assertEquals(
                Sets.newHashSet(edge1, edge2, edge3, edge4, edge5, edge6, edge7, edge8, edge9, edge10, edge11),
                Sets.newHashSet(graph.getEdges()));

        Assert.assertEquals(edge5, graph.findEdge(nodeA2, nodeA4).get());
        Assert.assertEquals(edge5, graph.findEdge(nodeA4, nodeA2).get());
        Assert.assertEquals(Optional.<SimEdge<UUID>>absent(), graph.findEdge(nodeA4, nodeC1));

        Assert.assertEquals(0, graph.degree(nodeC1));
        Assert.assertEquals(5, graph.degree(nodeA2));
    }

    /**
     * Graph:
     * <pre>
     *     A1   B1   C1
     * </pre>
     *
     * @throws Exception
     */
    @Test
    public void testDisconnected() throws Exception {

        UUID nodeA1 = UUID.fromString("a1000000-0000-0000-0000-000000000000");
        UUID nodeB1 = UUID.fromString("b1000000-0000-0000-0000-000000000000");
        UUID nodeC1 = UUID.fromString("c1000000-0000-0000-0000-000000000000");

        DenseSimilarityGraph.Builder<UUID> builder = new DenseSimilarityGraph.Builder<>();

        builder.add(nodeA1);
        builder.add(nodeB1);
        builder.add(nodeC1);

        Assert.assertEquals(0, builder.edgeCount());

        DenseSimilarityGraph<UUID> graph = builder.build();

        Assert.assertEquals(3, graph.getVertexCount());

        Assert.assertEquals(0, graph.degree(nodeA1));
        Assert.assertEquals(0, graph.degree(nodeB1));
        Assert.assertEquals(0, graph.degree(nodeC1));
    }

    @Test
    public void testReuseBuilder() throws Exception {

        DenseSimilarityGraph.Builder<String> builder = DenseSimilarityGraph.builder();
        Assert.assertEquals(true, builder.isReusable());
        builder.add("A1");

        DenseSimilarityGraph<String> graph = builder.build();
        Assert.assertEquals(Sets.newHashSet("A1"), graph.getVertices());

        builder.add("A2");
        graph = builder.build();
        Assert.assertEquals(Sets.newHashSet("A2"), graph.getVertices());
    }

    @Test
    public void testBuilder() throws Exception {

        DenseSimilarityGraph.Builder<String> builder = new DenseSimilarityGraph.Builder<>();
        builder.add("A1");
        final SimEdge<String> edge = new SimEdge<>("A1", "A2", 0.8);
        builder.add(edge);

        DenseSimilarityGraph<String> graph = builder.build();
        Assert.assertEquals(Sets.newHashSet("A1", "A2"), graph.getVertices());
        Assert.assertEquals(Sets.newHashSet(edge), Sets.newHashSet(graph.getEdges()));
    }

    /**
     * Tests that an exception is thrown when getting neighbours for a node that is not part of the graph.
     */
    @Test(expected = IllegalStateException.class)
    public void testGetNeighborsWithMissingNode() throws Exception {

        UUID node1 = UUID.fromString("a1000000-0000-0000-0000-000000000000");
        UUID node2 = UUID.fromString("b1000000-0000-0000-0000-000000000000");

        DenseSimilarityGraph.Builder<UUID> builder = new DenseSimilarityGraph.Builder<>();
        builder.add(node1);

        DenseSimilarityGraph<UUID> graph = builder.build();

        graph.getNeighbors(node2);
    }

    @Test
    public void testContains() throws Exception {

        UUID node1 = UUID.fromString("a1000000-0000-0000-0000-000000000000");
        UUID node2 = UUID.fromString("b1000000-0000-0000-0000-000000000000");

        DenseSimilarityGraph.Builder<UUID> builder = new DenseSimilarityGraph.Builder<>();
        builder.add(node1);

        DenseSimilarityGraph<UUID> graph = builder.build();

        Assert.assertEquals(true, graph.containsVertex(node1));

        Assert.assertEquals(false, graph.containsVertex(node2));
    }

    /**
     * Tests that an exception is thrown when finding an edge when vertex1 is not in the graph.
     */
    @Test(expected = IllegalStateException.class)
    public void testFindEdgeWithMissingVertex1() throws Exception {

        UUID node1 = UUID.fromString("a1000000-0000-0000-0000-000000000000");
        UUID node2 = UUID.fromString("b1000000-0000-0000-0000-000000000000");

        DenseSimilarityGraph.Builder<UUID> builder = new DenseSimilarityGraph.Builder<>();
        builder.add(node1);

        DenseSimilarityGraph<UUID> graph = builder.build();

        graph.findEdge(node2, node1);
    }

    /**
     * Tests that an exception is thrown when finding an edge when vertex2 is not in the graph.
     */
    @Test(expected = IllegalStateException.class)
    public void testFindEdgeWithMissingVertex2() throws Exception {

        UUID node1 = UUID.fromString("a1000000-0000-0000-0000-000000000000");
        UUID node2 = UUID.fromString("b1000000-0000-0000-0000-000000000000");

        DenseSimilarityGraph.Builder<UUID> builder = new DenseSimilarityGraph.Builder<>();
        builder.add(node1);

        DenseSimilarityGraph<UUID> graph = builder.build();

        graph.findEdge(node1, node2);
    }

    /**
     * Tests that an exception is thrown when degree is called using anode that is not part of the graph
     */
    @Test(expected = IllegalStateException.class)
    public void testDegreeWithMissingVertex1() throws Exception {

        UUID node1 = UUID.fromString("a1000000-0000-0000-0000-000000000000");
        UUID node2 = UUID.fromString("b1000000-0000-0000-0000-000000000000");

        DenseSimilarityGraph.Builder<UUID> builder = new DenseSimilarityGraph.Builder<>();
        builder.add(node1);

        DenseSimilarityGraph<UUID> graph = builder.build();

        graph.degree(node2);
    }

    /**
     * Graph:
     * <pre>
     *     A1-A2--B1--B2  C1
     *     |\ /|\ | /
     *     |/ \| \|/
     *     A3-A4  B3
     * </pre>
     *
     * @throws Exception
     */
    @Test
    public void testGetEdgesOf() throws Exception {

        UUID nodeA1 = UUID.fromString("a1000000-0000-0000-0000-000000000000");
        UUID nodeA2 = UUID.fromString("a2000000-0000-0000-0000-000000000000");
        UUID nodeA3 = UUID.fromString("a3000000-0000-0000-0000-000000000000");
        UUID nodeA4 = UUID.fromString("a4000000-0000-0000-0000-000000000000");

        UUID nodeB1 = UUID.fromString("b1000000-0000-0000-0000-000000000000");
        UUID nodeB2 = UUID.fromString("b2000000-0000-0000-0000-000000000000");
        UUID nodeB3 = UUID.fromString("b3000000-0000-0000-0000-000000000000");

        UUID nodeC1 = UUID.fromString("c1000000-0000-0000-0000-000000000000");

        DenseSimilarityGraph.Builder<UUID> builder = new DenseSimilarityGraph.Builder<>();
        SimEdge<UUID> edgeA1_A2 = builder.add(nodeA1, nodeA2, 0.9);
        SimEdge<UUID> edgeA1_A3 = builder.add(nodeA1, nodeA3, 0.9);
        SimEdge<UUID> edgeA1_A4 = builder.add(nodeA1, nodeA4, 0.9);
        SimEdge<UUID> edgeA2_A3 = builder.add(nodeA2, nodeA3, 0.9);
        SimEdge<UUID> edgeA2_A4 = builder.add(nodeA2, nodeA4, 0.9);
        SimEdge<UUID> edgeA3_A4 = builder.add(nodeA3, nodeA4, 0.9);

        SimEdge<UUID> edgeB1_B2 = builder.add(nodeB1, nodeB2, 0.9);
        SimEdge<UUID> edgeB1_B3 = builder.add(nodeB1, nodeB3, 0.9);
        SimEdge<UUID> edgeB2_B3 = builder.add(nodeB2, nodeB3, 0.9);

        SimEdge<UUID> edgeA2_B1 = builder.add(nodeA2, nodeB1, 0.8);
        SimEdge<UUID> edgeA2_B3 = builder.add(nodeA2, nodeB3, 0.8);

        builder.add(nodeC1);

        Assert.assertEquals(11, builder.edgeCount());

        DenseSimilarityGraph<UUID> graph = builder.build();

        Assert.assertEquals(8, graph.getVertexCount());

        Assert.assertEquals(
                Sets.newHashSet(edgeA1_A2, edgeA2_A3, edgeA2_A4, edgeA2_B1, edgeA2_B3),
                Sets.newHashSet(graph.getEdges(nodeA2)));

        Assert.assertEquals(
                Sets.<SimEdge<UUID>>newHashSet(),
                Sets.newHashSet(graph.getEdges(nodeC1)));

        Assert.assertEquals(
                Sets.newHashSet(edgeA1_A2, edgeA1_A3, edgeA1_A4),
                Sets.newHashSet(graph.getEdges(nodeA1)));

        Assert.assertEquals(
                Sets.newHashSet(edgeA2_B1, edgeB1_B2, edgeB1_B3),
                Sets.newHashSet(graph.getEdges(nodeB1)));

        Assert.assertEquals(
                Sets.newHashSet(edgeA1_A3, edgeA2_A3, edgeA3_A4),
                Sets.newHashSet(graph.getEdges(nodeA3)));

        Assert.assertEquals(
                Sets.newHashSet(edgeB1_B2, edgeB2_B3),
                Sets.newHashSet(graph.getEdges(nodeB2)));
    }

    @Test(expected = IllegalStateException.class)
    public void testGetEdgesOfMissing() throws Exception {

        DenseSimilarityGraph.Builder<String> builder = new DenseSimilarityGraph.Builder<>();
        builder.add("A1");
        final SimEdge<String> edge = new SimEdge<>("A1", "A2", 0.8);
        builder.add(edge);

        DenseSimilarityGraph<String> graph = builder.build();
        graph.getEdges("B3");
    }

    /**
     * Graph:
     * <pre>
     *     A1-A2--B1--B2  C1
     *     |\ /|\ | /
     *     |/ \| \|/
     *     A3-A4  B3
     * </pre>
     *
     * @throws Exception
     */
    @Test
    public void testForEachNode() throws Exception {

        UUID nodeA1 = UUID.fromString("a1000000-0000-0000-0000-000000000000");
        UUID nodeA2 = UUID.fromString("a2000000-0000-0000-0000-000000000000");
        UUID nodeA3 = UUID.fromString("a3000000-0000-0000-0000-000000000000");
        UUID nodeA4 = UUID.fromString("a4000000-0000-0000-0000-000000000000");

        UUID nodeB1 = UUID.fromString("b1000000-0000-0000-0000-000000000000");
        UUID nodeB2 = UUID.fromString("b2000000-0000-0000-0000-000000000000");
        UUID nodeB3 = UUID.fromString("b3000000-0000-0000-0000-000000000000");

        UUID nodeC1 = UUID.fromString("c1000000-0000-0000-0000-000000000000");

        DenseSimilarityGraph.Builder<UUID> builder = new DenseSimilarityGraph.Builder<>();
        builder.add(nodeA1, nodeA2, 0.9);
        builder.add(nodeA1, nodeA3, 0.9);
        builder.add(nodeA1, nodeA4, 0.9);
        builder.add(nodeA2, nodeA3, 0.9);
        builder.add(nodeA2, nodeA4, 0.9);
        builder.add(nodeA3, nodeA4, 0.9);

        builder.add(nodeB1, nodeB2, 0.9);
        builder.add(nodeB1, nodeB3, 0.9);
        builder.add(nodeB2, nodeB3, 0.9);

        builder.add(nodeA2, nodeB1, 0.8);
        builder.add(nodeA2, nodeB3, 0.8);

        builder.add(nodeC1);

        Assert.assertEquals(11, builder.edgeCount());

        DenseSimilarityGraph<UUID> graph = builder.build();

        Assert.assertEquals(8, graph.getVertexCount());

        Procedure<UUID> procedure = mock(Procedure.class);
        graph.forEachVertex(procedure);
        Mockito.verify(procedure).execute(nodeA1);
        Mockito.verify(procedure).execute(nodeA2);
        Mockito.verify(procedure).execute(nodeA3);
        Mockito.verify(procedure).execute(nodeA4);
        Mockito.verify(procedure).execute(nodeB1);
        Mockito.verify(procedure).execute(nodeB2);
        Mockito.verify(procedure).execute(nodeB3);
        Mockito.verify(procedure).execute(nodeC1);
        Mockito.verifyNoMoreInteractions(procedure);
    }

    /**
     * Graph:
     * <pre>
     *     A1-A2--B1--B2  C1
     *     |\ /|\ | /
     *     |/ \| \|/
     *     A3-A4  B3
     * </pre>
     *
     * @throws Exception
     */
    @Test
    public void testForEachEdge() throws Exception {

        UUID nodeA1 = UUID.fromString("a1000000-0000-0000-0000-000000000000");
        UUID nodeA2 = UUID.fromString("a2000000-0000-0000-0000-000000000000");
        UUID nodeA3 = UUID.fromString("a3000000-0000-0000-0000-000000000000");
        UUID nodeA4 = UUID.fromString("a4000000-0000-0000-0000-000000000000");

        UUID nodeB1 = UUID.fromString("b1000000-0000-0000-0000-000000000000");
        UUID nodeB2 = UUID.fromString("b2000000-0000-0000-0000-000000000000");
        UUID nodeB3 = UUID.fromString("b3000000-0000-0000-0000-000000000000");

        UUID nodeC1 = UUID.fromString("c1000000-0000-0000-0000-000000000000");

        DenseSimilarityGraph.Builder<UUID> builder = new DenseSimilarityGraph.Builder<>();
        SimEdge<UUID> edgeA1_A2 = builder.add(nodeA1, nodeA2, 0.9);
        SimEdge<UUID> edgeA1_A3 = builder.add(nodeA1, nodeA3, 0.9);
        SimEdge<UUID> edgeA1_A4 = builder.add(nodeA1, nodeA4, 0.9);
        SimEdge<UUID> edgeA2_A3 = builder.add(nodeA2, nodeA3, 0.9);
        SimEdge<UUID> edgeA2_A4 = builder.add(nodeA2, nodeA4, 0.9);
        SimEdge<UUID> edgeA3_A4 = builder.add(nodeA3, nodeA4, 0.9);

        SimEdge<UUID> edgeB1_B2 = builder.add(nodeB1, nodeB2, 0.9);
        SimEdge<UUID> edgeB1_B3 = builder.add(nodeB1, nodeB3, 0.9);
        SimEdge<UUID> edgeB2_B3 = builder.add(nodeB2, nodeB3, 0.9);

        SimEdge<UUID> edgeA2_B1 = builder.add(nodeA2, nodeB1, 0.8);
        SimEdge<UUID> edgeA2_B3 = builder.add(nodeA2, nodeB3, 0.8);

        builder.add(nodeC1);

        Assert.assertEquals(11, builder.edgeCount());

        DenseSimilarityGraph<UUID> graph = builder.build();

        Assert.assertEquals(8, graph.getVertexCount());

        Procedure<SimEdge<UUID>> procedure = mock(Procedure.class);
        graph.forEachEdge(procedure);
        Mockito.verify(procedure).execute(edgeA1_A2);
        Mockito.verify(procedure).execute(edgeA1_A3);
        Mockito.verify(procedure).execute(edgeA1_A4);
        Mockito.verify(procedure).execute(edgeA2_A3);
        Mockito.verify(procedure).execute(edgeA2_A4);
        Mockito.verify(procedure).execute(edgeA2_B1);
        Mockito.verify(procedure).execute(edgeA2_B3);
        Mockito.verify(procedure).execute(edgeA3_A4);
        Mockito.verify(procedure).execute(edgeB1_B2);
        Mockito.verify(procedure).execute(edgeB1_B3);
        Mockito.verify(procedure).execute(edgeB2_B3);
        Mockito.verifyNoMoreInteractions(procedure);
    }

    @Test(expected = IllegalStateException.class)
    public void testForEachEdgeOfMissing() throws Exception {

        DenseSimilarityGraph.Builder<String> builder = new DenseSimilarityGraph.Builder<>();
        builder.add("A1");
        final SimEdge<String> edge = new SimEdge<>("A1", "A2", 0.8);
        builder.add(edge);

        DenseSimilarityGraph<String> graph = builder.build();
        graph.forEachEdge("B3", mock(Procedure.class));
    }

    /**
     * Graph:
     * <pre>
     *     A1-A2--B1--B2  C1
     *     |\ /|\ | /
     *     |/ \| \|/
     *     A3-A4  B3
     * </pre>
     *
     * @throws Exception
     */
    @Test
    public void testForEachNeighbour() throws Exception {

        UUID nodeA1 = UUID.fromString("a1000000-0000-0000-0000-000000000000");
        UUID nodeA2 = UUID.fromString("a2000000-0000-0000-0000-000000000000");
        UUID nodeA3 = UUID.fromString("a3000000-0000-0000-0000-000000000000");
        UUID nodeA4 = UUID.fromString("a4000000-0000-0000-0000-000000000000");

        UUID nodeB1 = UUID.fromString("b1000000-0000-0000-0000-000000000000");
        UUID nodeB2 = UUID.fromString("b2000000-0000-0000-0000-000000000000");
        UUID nodeB3 = UUID.fromString("b3000000-0000-0000-0000-000000000000");

        UUID nodeC1 = UUID.fromString("c1000000-0000-0000-0000-000000000000");

        DenseSimilarityGraph.Builder<UUID> builder = new DenseSimilarityGraph.Builder<>();
        builder.add(nodeA1, nodeA2, 0.9);
        builder.add(nodeA1, nodeA3, 0.9);
        builder.add(nodeA1, nodeA4, 0.9);
        builder.add(nodeA2, nodeA3, 0.9);
        builder.add(nodeA2, nodeA4, 0.9);
        builder.add(nodeA3, nodeA4, 0.9);

        builder.add(nodeB1, nodeB2, 0.9);
        builder.add(nodeB1, nodeB3, 0.9);
        builder.add(nodeB2, nodeB3, 0.9);

        builder.add(nodeA2, nodeB1, 0.8);
        builder.add(nodeA2, nodeB3, 0.8);

        builder.add(nodeC1);

        Assert.assertEquals(11, builder.edgeCount());

        DenseSimilarityGraph<UUID> graph = builder.build();

        Assert.assertEquals(8, graph.getVertexCount());

        Procedure<UUID> procedure = mock(Procedure.class);
        graph.forEachNeighbour(nodeA1, procedure);
        Mockito.verify(procedure).execute(nodeA2);
        Mockito.verify(procedure).execute(nodeA3);
        Mockito.verify(procedure).execute(nodeA4);
        Mockito.verifyNoMoreInteractions(procedure);

        Mockito.reset(procedure);
        graph.forEachNeighbour(nodeA2, procedure);
        Mockito.verify(procedure).execute(nodeA1);
        Mockito.verify(procedure).execute(nodeA3);
        Mockito.verify(procedure).execute(nodeA4);
        Mockito.verify(procedure).execute(nodeB1);
        Mockito.verify(procedure).execute(nodeB3);
        Mockito.verifyNoMoreInteractions(procedure);

        Mockito.reset(procedure);
        graph.forEachNeighbour(nodeC1, procedure);
        Mockito.verifyNoMoreInteractions(procedure);
    }

    @Test(expected = IllegalStateException.class)
    public void testForEachNeighbourOfMissing() throws Exception {

        DenseSimilarityGraph.Builder<String> builder = new DenseSimilarityGraph.Builder<>();
        builder.add("A1");
        final SimEdge<String> edge = new SimEdge<>("A1", "A2", 0.8);
        builder.add(edge);

        DenseSimilarityGraph<String> graph = builder.build();
        graph.forEachNeighbour("B3", mock(Procedure.class));
    }

    /**
     * Graph:
     * <pre>
     *     A1-A2--B1--B2  C1
     *     |\ /|\ | /
     *     |/ \| \|/
     *     A3-A4  B3
     * </pre>
     *
     * @throws Exception
     */
    @Test
    public void testForEachEdgeOf() throws Exception {

        UUID nodeA1 = UUID.fromString("a1000000-0000-0000-0000-000000000000");
        UUID nodeA2 = UUID.fromString("a2000000-0000-0000-0000-000000000000");
        UUID nodeA3 = UUID.fromString("a3000000-0000-0000-0000-000000000000");
        UUID nodeA4 = UUID.fromString("a4000000-0000-0000-0000-000000000000");

        UUID nodeB1 = UUID.fromString("b1000000-0000-0000-0000-000000000000");
        UUID nodeB2 = UUID.fromString("b2000000-0000-0000-0000-000000000000");
        UUID nodeB3 = UUID.fromString("b3000000-0000-0000-0000-000000000000");

        UUID nodeC1 = UUID.fromString("c1000000-0000-0000-0000-000000000000");

        DenseSimilarityGraph.Builder<UUID> builder = new DenseSimilarityGraph.Builder<>();
        SimEdge<UUID> edgeA1_A2 = builder.add(nodeA1, nodeA2, 0.9);
        SimEdge<UUID> edgeA1_A3 = builder.add(nodeA1, nodeA3, 0.9);
        SimEdge<UUID> edgeA1_A4 = builder.add(nodeA1, nodeA4, 0.9);
        SimEdge<UUID> edgeA2_A3 = builder.add(nodeA2, nodeA3, 0.9);
        SimEdge<UUID> edgeA2_A4 = builder.add(nodeA2, nodeA4, 0.9);
        SimEdge<UUID> edgeA3_A4 = builder.add(nodeA3, nodeA4, 0.9);

        SimEdge<UUID> edgeB1_B2 = builder.add(nodeB1, nodeB2, 0.9);
        SimEdge<UUID> edgeB1_B3 = builder.add(nodeB1, nodeB3, 0.9);
        SimEdge<UUID> edgeB2_B3 = builder.add(nodeB2, nodeB3, 0.9);

        SimEdge<UUID> edgeA2_B1 = builder.add(nodeA2, nodeB1, 0.8);
        SimEdge<UUID> edgeA2_B3 = builder.add(nodeA2, nodeB3, 0.8);

        builder.add(nodeC1);

        Assert.assertEquals(11, builder.edgeCount());

        DenseSimilarityGraph<UUID> graph = builder.build();

        Assert.assertEquals(8, graph.getVertexCount());

        Procedure<SimEdge<UUID>> procedure = mock(Procedure.class);
        graph.forEachEdge(nodeA1, procedure);
        Mockito.verify(procedure).execute(edgeA1_A2);
        Mockito.verify(procedure).execute(edgeA1_A3);
        Mockito.verify(procedure).execute(edgeA1_A4);
        Mockito.verifyNoMoreInteractions(procedure);

        Mockito.reset(procedure);
        graph.forEachEdge(nodeA2, procedure);
        Mockito.verify(procedure).execute(edgeA1_A2);
        Mockito.verify(procedure).execute(edgeA2_A3);
        Mockito.verify(procedure).execute(edgeA2_A4);
        Mockito.verify(procedure).execute(edgeA2_B1);
        Mockito.verify(procedure).execute(edgeA2_B3);
        Mockito.verifyNoMoreInteractions(procedure);

        Mockito.reset(procedure);
        graph.forEachEdge(nodeA3, procedure);
        Mockito.verify(procedure).execute(edgeA1_A3);
        Mockito.verify(procedure).execute(edgeA2_A3);
        Mockito.verify(procedure).execute(edgeA3_A4);
        Mockito.verifyNoMoreInteractions(procedure);

        Mockito.reset(procedure);
        graph.forEachEdge(nodeB1, procedure);
        Mockito.verify(procedure).execute(edgeA2_B1);
        Mockito.verify(procedure).execute(edgeB1_B2);
        Mockito.verify(procedure).execute(edgeB1_B3);
        Mockito.verifyNoMoreInteractions(procedure);

        Mockito.reset(procedure);
        graph.forEachEdge(nodeB3, procedure);
        Mockito.verify(procedure).execute(edgeA2_B3);
        Mockito.verify(procedure).execute(edgeB1_B3);
        Mockito.verify(procedure).execute(edgeB2_B3);
        Mockito.verifyNoMoreInteractions(procedure);

        Mockito.reset(procedure);
        graph.forEachEdge(nodeC1, procedure);
        Mockito.verifyNoMoreInteractions(procedure);
    }

    /**
     * Graph:
     * <pre>
     *     A1-A2--B1--B2  C1
     *     |\ /|\ | /
     *     |/ \| \|/
     *     A3-A4  B3
     * </pre>
     *
     * @throws Exception
     */
    @Test
    public void testEdgeCount() throws Exception {

        String nodeA1 = "a1000000-0000-0000-0000-000000000000";
        String nodeA2 = "a2000000-0000-0000-0000-000000000000";
        String nodeA3 = "a3000000-0000-0000-0000-000000000000";
        String nodeA4 = "a4000000-0000-0000-0000-000000000000";

        String nodeB1 = "b1000000-0000-0000-0000-000000000000";
        String nodeB2 = "b2000000-0000-0000-0000-000000000000";
        String nodeB3 = "b3000000-0000-0000-0000-000000000000";

        String nodeC1 = "c1000000-0000-0000-0000-000000000000";

        DenseSimilarityGraph.Builder<String> builder = new DenseSimilarityGraph.Builder<>();
        builder.add(nodeA1, nodeA2, 0.9);
        builder.add(nodeA1, nodeA3, 0.9);
        builder.add(nodeA1, nodeA4, 0.9);
        builder.add(nodeA2, nodeA3, 0.9);
        builder.add(nodeA2, nodeA4, 0.9);
        builder.add(nodeA3, nodeA4, 0.9);

        builder.add(nodeB1, nodeB2, 0.9);
        builder.add(nodeB1, nodeB3, 0.9);
        builder.add(nodeB2, nodeB3, 0.9);

        builder.add(nodeA2, nodeB1, 0.8);
        builder.add(nodeA2, nodeB3, 0.8);

        builder.add(nodeC1);

        final DenseSimilarityGraph<String> graph = builder.build();
        Assert.assertEquals(11, graph.getEdgeCount());
    }
}
