package org.expasy.mzjava.core.ms.library;

import com.google.common.collect.Sets;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.utils.function.Procedure;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultSpectrumLibraryTest {

    /**
     * Library         365    478-479    567-567
     * |     |  /         \  |
     * Query   200    365    468            570     600
     */
    @Test
    public void testIterator() throws Exception {

        final PeakList pl365 = mockSpectrum(365, 2);
        final PeakList pl478 = mockSpectrum(478, 2);
        final PeakList pl479 = mockSpectrum(479, 2);
        final PeakList pl567_a = mockSpectrum(567, 2);
        final PeakList pl567_b = mockSpectrum(567, 2);

        SpectrumLibrary<PeakList> lib = new DefaultSpectrumLibrary<>(new AbsoluteTolerance(20),
                Sets.newHashSet(
                        pl365,
                        pl478,
                        pl479,
                        pl567_a,
                        pl567_b
                )
        );

        final Set<PeakList> found = new HashSet<>();
        final Procedure<PeakList> procedure = new Procedure<PeakList>() {
            @Override
            public void execute(PeakList peakList) {

                found.add(peakList);
            }
        };

        lib.forEach(new Peak(200, 564, 2), procedure);
        Assert.assertEquals(0, found.size());

        found.clear();
        lib.forEach(new Peak(365, 564, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl365), found);
        found.clear();

        found.clear();
        lib.forEach(new Peak(468, 564, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl478, pl479), found);
        found.clear();

        found.clear();
        lib.forEach(new Peak(570, 564, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl567_a, pl567_b), found);
        found.clear();

        found.clear();
        lib.forEach(new Peak(600, 564, 2), procedure);
        Assert.assertEquals(0, found.size());
        found.clear();
    }

    @Test
    public void testMultipleCharges() throws Exception {

        final PeakList pl1365 = mockSpectrum(1365, 1);
        final PeakList pl1478 = mockSpectrum(1478, 1);
        final PeakList pl479 = mockSpectrum(479, 2);
        final PeakList pl567_a = mockSpectrum(567, 2);
        final PeakList pl567_b = mockSpectrum(567, 2);

        SpectrumLibrary<PeakList> lib = new DefaultSpectrumLibrary<>(new AbsoluteTolerance(20),
                Sets.newHashSet(
                        pl1365,
                        pl1478,
                        pl479,
                        pl567_a,
                        pl567_b
                )
        );

        final Set<PeakList> found = new HashSet<>();
        final Procedure<PeakList> procedure = new Procedure<PeakList>() {
            @Override
            public void execute(PeakList peakList) {

                found.add(peakList);
            }
        };

        lib.forEach(new Peak(1361, 564, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(pl1365), found);

        found.clear();
        lib.forEach(new Peak(470, 564, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl479), found);

        found.clear();
        lib.forEach(new Peak(576, 564, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl567_a, pl567_b), found);
    }

    @Test
    public void testMultipleCharges2() throws Exception {

        final PeakList pl1365 = mockSpectrum(1365, 1);
        final PeakList pl1478 = mockSpectrum(1478, 1);
        final PeakList pl479 = mockSpectrum(479, 2);
        final PeakList pl567_a = mockSpectrum(567, 2);
        final PeakList pl567_b = mockSpectrum(567, 2);

        final SpectrumLibrary<PeakList> lib = new DefaultSpectrumLibrary<>(new AbsoluteTolerance(20),
                Sets.newHashSet(
                        pl1365,
                        pl1478,
                        pl479,
                        pl567_a,
                        pl567_b
                )
        );

        final Set<PeakList> found = new HashSet<>();
        final Procedure<PeakList> procedure = new Procedure<PeakList>() {
            @Override
            public void execute(PeakList peakList) {

                found.add(peakList);
            }
        };

        lib.forEach(new Peak(1361, 564, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(pl1365), found);

        found.clear();
        lib.forEach(new Peak(1473, 564, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(pl1478), found);

        found.clear();
        lib.forEach(new Peak(470, 564, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl479), found);

        found.clear();
        lib.forEach(new Peak(576, 564, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl567_a, pl567_b), found);
    }

    @Test
    public void testIterator3() throws Exception {

        final SpectrumLibrary<PeakList> library = new DefaultSpectrumLibrary<>(new AbsoluteTolerance(0.2),
                Sets.newHashSet(
                        mockSpectrum(392.22721917570004, 2),
                        mockSpectrum(392.2705640467, 2)
                )
        );

        final Set<PeakList> found = new HashSet<>();
        final Procedure<PeakList> procedure = new Procedure<PeakList>() {
            @Override
            public void execute(PeakList peakList) {

                found.add(peakList);
            }
        };

        library.forEach(new Peak(391.285003662109, 1.0, 2), procedure);
        Assert.assertEquals(0, found.size());

        found.clear();
        library.forEach(new Peak(391.7109375, 1.0, 2), procedure);
        Assert.assertEquals(0, found.size());
    }

    /**
     * The spectra within tolerance. * mean that there is an edge to all 4 nodes
     * <p/>
     * A = 1875.5, B = 1875.8, C = 1876.2, D = 1876.5
     * a = 1875.5, B = 1875.8, C = 1876.2, D = 1876.5
     * <p/>
     * A--B--C--D
     * |* |* |* |
     * a--b--c--d
     */
    @Test
    public void testTraverseOfEnd() throws Exception {

        final PeakList pl1875_5 = mockSpectrum(1875.5, 2);
        final PeakList pl1875_8 = mockSpectrum(1875.8, 2);
        final PeakList pl1876_2 = mockSpectrum(1876.2, 2);
        final PeakList pl1876_5 = mockSpectrum(1876.5, 2);
        SpectrumLibrary<PeakList> library = new DefaultSpectrumLibrary<>(new AbsoluteTolerance(0.5),
                Sets.newHashSet(
                        pl1875_5,
                        pl1875_8,
                        pl1876_2,
                        pl1876_5
                )
        );

        final Set<PeakList> found = new HashSet<>();
        final Procedure<PeakList> procedure = new Procedure<PeakList>() {
            @Override
            public void execute(PeakList peakList) {

                found.add(peakList);
            }
        };

        library.forEach(new Peak(1875.5, 12, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl1875_5, pl1875_8), found);

        found.clear();
        library.forEach(new Peak(1875.8, 12, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl1875_5, pl1875_8, pl1876_2), found);

        found.clear();
        library.forEach(new Peak(1876.2, 12, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl1875_8, pl1876_2, pl1876_5), found);

        found.clear();
        library.forEach(new Peak(1876.5, 12, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl1876_2, pl1876_5), found);
    }

    @Test
    public void test() throws Exception {

        final PeakList pl1422_635 = mockSpectrum(1422.635, 1);
        final PeakList pl1422_654 = mockSpectrum(1422.654, 1);
        final PeakList pl698_271 = mockSpectrum(698.271, 2);
        final PeakList pl698_272 = mockSpectrum(698.272, 2);
        SpectrumLibrary<PeakList> library = new DefaultSpectrumLibrary<>(new AbsoluteTolerance(0.4),
                Sets.newHashSet(
                        pl1422_635,
                        pl1422_654,
                        pl698_271,
                        pl698_272
                )
        );

        final Set<PeakList> found = new HashSet<>();
        final Procedure<PeakList> procedure = new Procedure<PeakList>() {
            @Override
            public void execute(PeakList peakList) {

                found.add(peakList);
            }
        };

        library.forEach(new Peak(1422.634, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(pl1422_635, pl1422_654), found);

        found.clear();
        library.forEach(new Peak(1422.654, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(pl1422_635, pl1422_654), found);

        found.clear();
        library.forEach(new Peak(698.271, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(pl698_271, pl698_272), found);
    }

    @Test
    public void testTest2() throws Exception {

        PeakList pl860_289 = mockSpectrum(860.289, 1);
        PeakList pl860_634 = mockSpectrum(860.634, 1);
        PeakList pl860_639 = mockSpectrum(860.639, 1);
        PeakList pl1007_358 = mockSpectrum(1007.358, 1);
        PeakList pl1007_369 = mockSpectrum(1007.369, 1);
        PeakList pl1007_769 = mockSpectrum(1007.769, 1);
        PeakList pl1007_791 = mockSpectrum(1007.791, 1);
        PeakList pl1007_806 = mockSpectrum(1007.806, 1);
        PeakList pl1007_807 = mockSpectrum(1007.807, 1);
        PeakList pl1007_809 = mockSpectrum(1007.809, 1);
        PeakList pl1007_825 = mockSpectrum(1007.825, 1);
        PeakList pl1007_833 = mockSpectrum(1007.833, 1);
        PeakList pl1007_838 = mockSpectrum(1007.838, 1);
        PeakList pl1007_842 = mockSpectrum(1007.842, 1);
        PeakList pl1007_844 = mockSpectrum(1007.844, 1);
        PeakList pl1007_857 = mockSpectrum(1007.857, 1);
        PeakList pl1235_521_a = mockSpectrum(1235.521, 1);
        PeakList pl1235_521_b = mockSpectrum(1235.521, 1);
        PeakList pl1235_532_a = mockSpectrum(1235.532, 1);
        PeakList pl1235_532_b = mockSpectrum(1235.532, 1);
        PeakList pl1235_536 = mockSpectrum(1235.536, 1);
        PeakList pl1235_543_a = mockSpectrum(1235.543, 1);
        PeakList pl1235_543_b = mockSpectrum(1235.543, 1);
        PeakList pl1235_545 = mockSpectrum(1235.545, 1);
        PeakList pl1235_553 = mockSpectrum(1235.553, 1);
        PeakList pl1235_557 = mockSpectrum(1235.557, 1);
        PeakList pl1235_558 = mockSpectrum(1235.558, 1);
        PeakList pl1260_551 = mockSpectrum(1260.551, 1);
        PeakList pl1260_56 = mockSpectrum(1260.56, 1);
        PeakList pl1397_569 = mockSpectrum(1397.569, 1);
        PeakList pl1397_582 = mockSpectrum(1397.582, 1);
        PeakList pl1397_596 = mockSpectrum(1397.596, 1);
        PeakList pl1397_603 = mockSpectrum(1397.603, 1);
        PeakList pl1397_608 = mockSpectrum(1397.608, 1);
        PeakList pl1397_62 = mockSpectrum(1397.62, 1);
        PeakList pl1397_626 = mockSpectrum(1397.626, 1);
        PeakList pl1397_63 = mockSpectrum(1397.63, 1);
        PeakList pl1422_622 = mockSpectrum(1422.622, 1);
        PeakList pl1422_635 = mockSpectrum(1422.635, 1);
        PeakList pl1422_654 = mockSpectrum(1422.654, 1);
        PeakList pl698_271 = mockSpectrum(698.271, 2);
        PeakList pl698_272 = mockSpectrum(698.272, 2);
        PeakList pl698_273 = mockSpectrum(698.273, 2);
        PeakList pl698_279 = mockSpectrum(698.279, 2);
        PeakList pl698_281 = mockSpectrum(698.281, 2);
        PeakList pl698_286 = mockSpectrum(698.286, 2);
        PeakList pl698_287 = mockSpectrum(698.287, 2);
        PeakList pl710_771 = mockSpectrum(710.771, 2);
        PeakList pl710_774 = mockSpectrum(710.774, 2);
        PeakList pl710_791 = mockSpectrum(710.791, 2);
        PeakList pl710_804 = mockSpectrum(710.804, 2);
        PeakList pl710_807 = mockSpectrum(710.807, 2);
        PeakList pl731_297 = mockSpectrum(731.297, 2);
        PeakList pl731_317 = mockSpectrum(731.317, 2);
        PeakList pl731_326 = mockSpectrum(731.326, 2);
        PeakList pl731_333 = mockSpectrum(731.333, 2);
        PeakList pl778_821 = mockSpectrum(778.821, 2);
        PeakList pl779_286 = mockSpectrum(779.286, 2);
        PeakList pl779_295 = mockSpectrum(779.295, 2);
        PeakList pl779_3 = mockSpectrum(779.3, 2);
        PeakList pl779_305 = mockSpectrum(779.305, 2);
        PeakList pl779_312 = mockSpectrum(779.312, 2);
        PeakList pl779_314 = mockSpectrum(779.314, 2);
        PeakList pl779_316 = mockSpectrum(779.316, 2);
        PeakList pl779_318 = mockSpectrum(779.318, 2);
        PeakList pl779_319_a = mockSpectrum(779.319, 2);
        PeakList pl779_319_b = mockSpectrum(779.319, 2);
        PeakList pl779_319_c = mockSpectrum(779.319, 2);
        PeakList pl779_321 = mockSpectrum(779.321, 2);
        PeakList pl791_812 = mockSpectrum(791.812, 2);
        PeakList pl791_821_a = mockSpectrum(791.821, 2);
        PeakList pl791_821_b = mockSpectrum(791.821, 2);
        PeakList pl791_831 = mockSpectrum(791.831, 2);
        PeakList pl791_832 = mockSpectrum(791.832, 2);
        PeakList pl791_847 = mockSpectrum(791.847, 2);

        SpectrumLibrary<PeakList> library = new DefaultSpectrumLibrary<>(new AbsoluteTolerance(0.2),
                Sets.newHashSet(
                        pl860_289,
                        pl860_634,
                        pl860_639,
                        pl1007_358,
                        pl1007_369,
                        pl1007_769,
                        pl1007_791,
                        pl1007_806,
                        pl1007_807,
                        pl1007_809,
                        pl1007_825,
                        pl1007_833,
                        pl1007_838,
                        pl1007_842,
                        pl1007_844,
                        pl1007_857,
                        pl1235_521_a,
                        pl1235_521_b,
                        pl1235_532_a,
                        pl1235_532_b,
                        pl1235_536,
                        pl1235_543_a,
                        pl1235_543_b,
                        pl1235_545,
                        pl1235_553,
                        pl1235_557,
                        pl1235_558,
                        pl1260_551,
                        pl1260_56,
                        pl1397_569,
                        pl1397_582,
                        pl1397_596,
                        pl1397_603,
                        pl1397_608,
                        pl1397_62,
                        pl1397_626,
                        pl1397_63,
                        pl1422_622,
                        pl1422_635,
                        pl1422_654,
                        pl698_271,
                        pl698_272,
                        pl698_273,
                        pl698_279,
                        pl698_281,
                        pl698_286,
                        pl698_287,
                        pl710_771,
                        pl710_774,
                        pl710_791,
                        pl710_804,
                        pl710_807,
                        pl731_297,
                        pl731_317,
                        pl731_326,
                        pl731_333,
                        pl778_821,
                        pl779_286,
                        pl779_295,
                        pl779_3,
                        pl779_305,
                        pl779_312,
                        pl779_314,
                        pl779_316,
                        pl779_318,
                        pl779_319_a,
                        pl779_319_b,
                        pl779_319_c,
                        pl779_321,
                        pl791_812,
                        pl791_821_a,
                        pl791_821_b,
                        pl791_831,
                        pl791_832,
                        pl791_847
                )
        );

        final Set<PeakList> found = new LinkedHashSet<>();
        final Procedure<PeakList> procedure = new Procedure<PeakList>() {
            @Override
            public void execute(PeakList peakList) {

                found.add(peakList);
            }
        };

        found.clear();
        library.forEach(new Peak(860.289, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(pl860_289), found);

        found.clear();
        library.forEach(new Peak(860.634, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl860_634,
                pl860_639), found);

        found.clear();
        library.forEach(new Peak(860.639, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl860_634,
                pl860_639), found);

        found.clear();
        library.forEach(new Peak(1007.358, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1007_358,
                pl1007_369), found);

        found.clear();
        library.forEach(new Peak(1007.369, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1007_358,
                pl1007_369), found);

        found.clear();
        library.forEach(new Peak(1007.769, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1007_769,
                pl1007_791,
                pl1007_806,
                pl1007_807,
                pl1007_809,
                pl1007_825,
                pl1007_833,
                pl1007_838,
                pl1007_842,
                pl1007_844,
                pl1007_857), found);

        found.clear();
        library.forEach(new Peak(1007.791, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1007_769,
                pl1007_791,
                pl1007_806,
                pl1007_807,
                pl1007_809,
                pl1007_825,
                pl1007_833,
                pl1007_838,
                pl1007_842,
                pl1007_844,
                pl1007_857), found);

        found.clear();
        library.forEach(new Peak(1007.806, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1007_769,
                pl1007_791,
                pl1007_806,
                pl1007_807,
                pl1007_809,
                pl1007_825,
                pl1007_833,
                pl1007_838,
                pl1007_842,
                pl1007_844,
                pl1007_857), found);

        found.clear();
        library.forEach(new Peak(1007.807, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1007_769,
                pl1007_791,
                pl1007_806,
                pl1007_807,
                pl1007_809,
                pl1007_825,
                pl1007_833,
                pl1007_838,
                pl1007_842,
                pl1007_844,
                pl1007_857), found);

        found.clear();
        library.forEach(new Peak(1007.809, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1007_769,
                pl1007_791,
                pl1007_806,
                pl1007_807,
                pl1007_809,
                pl1007_825,
                pl1007_833,
                pl1007_838,
                pl1007_842,
                pl1007_844,
                pl1007_857), found);

        found.clear();
        library.forEach(new Peak(1007.825, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1007_769,
                pl1007_791,
                pl1007_806,
                pl1007_807,
                pl1007_809,
                pl1007_825,
                pl1007_833,
                pl1007_838,
                pl1007_842,
                pl1007_844,
                pl1007_857), found);

        found.clear();
        library.forEach(new Peak(1007.833, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1007_769,
                pl1007_791,
                pl1007_806,
                pl1007_807,
                pl1007_809,
                pl1007_825,
                pl1007_833,
                pl1007_838,
                pl1007_842,
                pl1007_844,
                pl1007_857), found);

        found.clear();
        library.forEach(new Peak(1007.838, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1007_769,
                pl1007_791,
                pl1007_806,
                pl1007_807,
                pl1007_809,
                pl1007_825,
                pl1007_833,
                pl1007_838,
                pl1007_842,
                pl1007_844,
                pl1007_857), found);

        found.clear();
        library.forEach(new Peak(1007.842, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1007_769,
                pl1007_791,
                pl1007_806,
                pl1007_807,
                pl1007_809,
                pl1007_825,
                pl1007_833,
                pl1007_838,
                pl1007_842,
                pl1007_844,
                pl1007_857), found);

        found.clear();
        library.forEach(new Peak(1007.844, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1007_769,
                pl1007_791,
                pl1007_806,
                pl1007_807,
                pl1007_809,
                pl1007_825,
                pl1007_833,
                pl1007_838,
                pl1007_842,
                pl1007_844,
                pl1007_857), found);

        found.clear();
        library.forEach(new Peak(1007.857, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1007_769,
                pl1007_791,
                pl1007_806,
                pl1007_807,
                pl1007_809,
                pl1007_825,
                pl1007_833,
                pl1007_838,
                pl1007_842,
                pl1007_844,
                pl1007_857), found);

        found.clear();
        library.forEach(new Peak(1235.521, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1235_521_a,
                pl1235_521_b,
                pl1235_532_a,
                pl1235_532_b,
                pl1235_536,
                pl1235_543_a,
                pl1235_543_b,
                pl1235_545,
                pl1235_553,
                pl1235_557,
                pl1235_558), found);

        found.clear();
        library.forEach(new Peak(1235.521, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1235_521_a,
                pl1235_521_b,
                pl1235_532_a,
                pl1235_532_b,
                pl1235_536,
                pl1235_543_a,
                pl1235_543_b,
                pl1235_545,
                pl1235_553,
                pl1235_557,
                pl1235_558), found);

        found.clear();
        library.forEach(new Peak(1235.532, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1235_521_a,
                pl1235_521_b,
                pl1235_532_a,
                pl1235_532_b,
                pl1235_536,
                pl1235_543_a,
                pl1235_543_b,
                pl1235_545,
                pl1235_553,
                pl1235_557,
                pl1235_558), found);

        found.clear();
        library.forEach(new Peak(1235.532, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1235_521_a,
                pl1235_521_b,
                pl1235_532_a,
                pl1235_532_b,
                pl1235_536,
                pl1235_543_a,
                pl1235_543_b,
                pl1235_545,
                pl1235_553,
                pl1235_557,
                pl1235_558), found);

        found.clear();
        library.forEach(new Peak(1235.536, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1235_521_a,
                pl1235_521_b,
                pl1235_532_a,
                pl1235_532_b,
                pl1235_536,
                pl1235_543_a,
                pl1235_543_b,
                pl1235_545,
                pl1235_553,
                pl1235_557,
                pl1235_558), found);

        found.clear();
        library.forEach(new Peak(1235.543, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1235_521_a,
                pl1235_521_b,
                pl1235_532_a,
                pl1235_532_b,
                pl1235_536,
                pl1235_543_a,
                pl1235_543_b,
                pl1235_545,
                pl1235_553,
                pl1235_557,
                pl1235_558), found);

        found.clear();
        library.forEach(new Peak(1235.543, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1235_521_a,
                pl1235_521_b,
                pl1235_532_a,
                pl1235_532_b,
                pl1235_536,
                pl1235_543_a,
                pl1235_543_b,
                pl1235_545,
                pl1235_553,
                pl1235_557,
                pl1235_558), found);

        found.clear();
        library.forEach(new Peak(1235.545, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1235_521_a,
                pl1235_521_b,
                pl1235_532_a,
                pl1235_532_b,
                pl1235_536,
                pl1235_543_a,
                pl1235_543_b,
                pl1235_545,
                pl1235_553,
                pl1235_557,
                pl1235_558), found);

        found.clear();
        library.forEach(new Peak(1235.553, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1235_521_a,
                pl1235_521_b,
                pl1235_532_a,
                pl1235_532_b,
                pl1235_536,
                pl1235_543_a,
                pl1235_543_b,
                pl1235_545,
                pl1235_553,
                pl1235_557,
                pl1235_558), found);

        found.clear();
        library.forEach(new Peak(1235.557, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1235_521_a,
                pl1235_521_b,
                pl1235_532_a,
                pl1235_532_b,
                pl1235_536,
                pl1235_543_a,
                pl1235_543_b,
                pl1235_545,
                pl1235_553,
                pl1235_557,
                pl1235_558), found);

        found.clear();
        library.forEach(new Peak(1235.558, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1235_521_a,
                pl1235_521_b,
                pl1235_532_a,
                pl1235_532_b,
                pl1235_536,
                pl1235_543_a,
                pl1235_543_b,
                pl1235_545,
                pl1235_553,
                pl1235_557,
                pl1235_558), found);

        found.clear();
        library.forEach(new Peak(1260.551, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1260_551,
                pl1260_56), found);

        found.clear();
        library.forEach(new Peak(1260.56, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1260_551,
                pl1260_56), found);

        found.clear();
        library.forEach(new Peak(1397.569, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1397_569,
                pl1397_582,
                pl1397_596,
                pl1397_603,
                pl1397_608,
                pl1397_62,
                pl1397_626,
                pl1397_63), found);

        found.clear();
        library.forEach(new Peak(1397.582, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1397_569,
                pl1397_582,
                pl1397_596,
                pl1397_603,
                pl1397_608,
                pl1397_62,
                pl1397_626,
                pl1397_63), found);

        found.clear();
        library.forEach(new Peak(1397.596, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1397_569,
                pl1397_582,
                pl1397_596,
                pl1397_603,
                pl1397_608,
                pl1397_62,
                pl1397_626,
                pl1397_63), found);

        found.clear();
        library.forEach(new Peak(1397.603, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1397_569,
                pl1397_582,
                pl1397_596,
                pl1397_603,
                pl1397_608,
                pl1397_62,
                pl1397_626,
                pl1397_63), found);

        found.clear();
        library.forEach(new Peak(1397.608, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1397_569,
                pl1397_582,
                pl1397_596,
                pl1397_603,
                pl1397_608,
                pl1397_62,
                pl1397_626,
                pl1397_63), found);

        found.clear();
        library.forEach(new Peak(1397.62, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1397_569,
                pl1397_582,
                pl1397_596,
                pl1397_603,
                pl1397_608,
                pl1397_62,
                pl1397_626,
                pl1397_63), found);

        found.clear();
        library.forEach(new Peak(1397.626, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1397_569,
                pl1397_582,
                pl1397_596,
                pl1397_603,
                pl1397_608,
                pl1397_62,
                pl1397_626,
                pl1397_63), found);

        found.clear();
        library.forEach(new Peak(1397.63, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1397_569,
                pl1397_582,
                pl1397_596,
                pl1397_603,
                pl1397_608,
                pl1397_62,
                pl1397_626,
                pl1397_63), found);

        found.clear();
        library.forEach(new Peak(1422.622, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1422_622,
                pl1422_635,
                pl1422_654), found);

        found.clear();
        library.forEach(new Peak(1422.635, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1422_622,
                pl1422_635,
                pl1422_654), found);

        found.clear();
        library.forEach(new Peak(1422.654, 1, 1), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl1422_622,
                pl1422_635,
                pl1422_654), found);

        found.clear();
        library.forEach(new Peak(698.271, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl698_271,
                pl698_272,
                pl698_273,
                pl698_279,
                pl698_281,
                pl698_286,
                pl698_287), found);

        found.clear();
        library.forEach(new Peak(698.272, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl698_271,
                pl698_272,
                pl698_273,
                pl698_279,
                pl698_281,
                pl698_286,
                pl698_287), found);

        found.clear();
        library.forEach(new Peak(698.273, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl698_271,
                pl698_272,
                pl698_273,
                pl698_279,
                pl698_281,
                pl698_286,
                pl698_287), found);

        found.clear();
        library.forEach(new Peak(698.279, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl698_271,
                pl698_272,
                pl698_273,
                pl698_279,
                pl698_281,
                pl698_286,
                pl698_287), found);

        found.clear();
        library.forEach(new Peak(698.281, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl698_271,
                pl698_272,
                pl698_273,
                pl698_279,
                pl698_281,
                pl698_286,
                pl698_287), found);

        found.clear();
        library.forEach(new Peak(698.286, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl698_271,
                pl698_272,
                pl698_273,
                pl698_279,
                pl698_281,
                pl698_286,
                pl698_287), found);

        found.clear();
        library.forEach(new Peak(698.287, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl698_271,
                pl698_272,
                pl698_273,
                pl698_279,
                pl698_281,
                pl698_286,
                pl698_287), found);

        found.clear();
        library.forEach(new Peak(710.771, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl710_771,
                pl710_774,
                pl710_791,
                pl710_804,
                pl710_807), found);

        found.clear();
        library.forEach(new Peak(710.774, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl710_771,
                pl710_774,
                pl710_791,
                pl710_804,
                pl710_807), found);

        found.clear();
        library.forEach(new Peak(710.791, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl710_771,
                pl710_774,
                pl710_791,
                pl710_804,
                pl710_807), found);

        found.clear();
        library.forEach(new Peak(710.804, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl710_771,
                pl710_774,
                pl710_791,
                pl710_804,
                pl710_807), found);

        found.clear();
        library.forEach(new Peak(710.807, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl710_771,
                pl710_774,
                pl710_791,
                pl710_804,
                pl710_807), found);

        found.clear();
        library.forEach(new Peak(731.297, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl731_297,
                pl731_317,
                pl731_326,
                pl731_333), found);

        found.clear();
        library.forEach(new Peak(731.317, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl731_297,
                pl731_317,
                pl731_326,
                pl731_333), found);

        found.clear();
        library.forEach(new Peak(731.326, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl731_297,
                pl731_317,
                pl731_326,
                pl731_333), found);

        found.clear();
        library.forEach(new Peak(731.333, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl731_297,
                pl731_317,
                pl731_326,
                pl731_333), found);

        found.clear();
        library.forEach(new Peak(778.821, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl778_821), found);

        found.clear();
        library.forEach(new Peak(779.286, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl779_286,
                pl779_295,
                pl779_3,
                pl779_305,
                pl779_312,
                pl779_314,
                pl779_316,
                pl779_318,
                pl779_319_a,
                pl779_319_b,
                pl779_319_c,
                pl779_321), found);

        found.clear();
        library.forEach(new Peak(779.295, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl779_286,
                pl779_295,
                pl779_3,
                pl779_305,
                pl779_312,
                pl779_314,
                pl779_316,
                pl779_318,
                pl779_319_a,
                pl779_319_b,
                pl779_319_c,
                pl779_321), found);

        found.clear();
        library.forEach(new Peak(779.3, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl779_286,
                pl779_295,
                pl779_3,
                pl779_305,
                pl779_312,
                pl779_314,
                pl779_316,
                pl779_318,
                pl779_319_a,
                pl779_319_b,
                pl779_319_c,
                pl779_321), found);

        found.clear();
        library.forEach(new Peak(779.305, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl779_286,
                pl779_295,
                pl779_3,
                pl779_305,
                pl779_312,
                pl779_314,
                pl779_316,
                pl779_318,
                pl779_319_a,
                pl779_319_b,
                pl779_319_c,
                pl779_321), found);

        found.clear();
        library.forEach(new Peak(779.312, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl779_286,
                pl779_295,
                pl779_3,
                pl779_305,
                pl779_312,
                pl779_314,
                pl779_316,
                pl779_318,
                pl779_319_a,
                pl779_319_b,
                pl779_319_c,
                pl779_321), found);

        found.clear();
        library.forEach(new Peak(779.314, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl779_286,
                pl779_295,
                pl779_3,
                pl779_305,
                pl779_312,
                pl779_314,
                pl779_316,
                pl779_318,
                pl779_319_a,
                pl779_319_b,
                pl779_319_c,
                pl779_321), found);

        found.clear();
        library.forEach(new Peak(779.316, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl779_286,
                pl779_295,
                pl779_3,
                pl779_305,
                pl779_312,
                pl779_314,
                pl779_316,
                pl779_318,
                pl779_319_a,
                pl779_319_b,
                pl779_319_c,
                pl779_321), found);

        found.clear();
        library.forEach(new Peak(779.318, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl779_286,
                pl779_295,
                pl779_3,
                pl779_305,
                pl779_312,
                pl779_314,
                pl779_316,
                pl779_318,
                pl779_319_a,
                pl779_319_b,
                pl779_319_c,
                pl779_321), found);

        found.clear();
        library.forEach(new Peak(779.319, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl779_286,
                pl779_295,
                pl779_3,
                pl779_305,
                pl779_312,
                pl779_314,
                pl779_316,
                pl779_318,
                pl779_319_a,
                pl779_319_b,
                pl779_319_c,
                pl779_321), found);

        found.clear();
        library.forEach(new Peak(779.319, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl779_286,
                pl779_295,
                pl779_3,
                pl779_305,
                pl779_312,
                pl779_314,
                pl779_316,
                pl779_318,
                pl779_319_a,
                pl779_319_b,
                pl779_319_c,
                pl779_321), found);

        found.clear();
        library.forEach(new Peak(779.319, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl779_286,
                pl779_295,
                pl779_3,
                pl779_305,
                pl779_312,
                pl779_314,
                pl779_316,
                pl779_318,
                pl779_319_a,
                pl779_319_b,
                pl779_319_c,
                pl779_321), found);

        found.clear();
        library.forEach(new Peak(779.321, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl779_286,
                pl779_295,
                pl779_3,
                pl779_305,
                pl779_312,
                pl779_314,
                pl779_316,
                pl779_318,
                pl779_319_a,
                pl779_319_b,
                pl779_319_c,
                pl779_321), found);

        found.clear();
        library.forEach(new Peak(791.812, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl791_812,
                pl791_821_a,
                pl791_821_b,
                pl791_831,
                pl791_832,
                pl791_847), found);

        found.clear();
        library.forEach(new Peak(791.821, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl791_812,
                pl791_821_a,
                pl791_821_b,
                pl791_831,
                pl791_832,
                pl791_847), found);

        found.clear();
        library.forEach(new Peak(791.821, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl791_812,
                pl791_821_a,
                pl791_821_b,
                pl791_831,
                pl791_832,
                pl791_847), found);

        found.clear();
        library.forEach(new Peak(791.831, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl791_812,
                pl791_821_a,
                pl791_821_b,
                pl791_831,
                pl791_832,
                pl791_847), found);

        found.clear();
        library.forEach(new Peak(791.832, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl791_812,
                pl791_821_a,
                pl791_821_b,
                pl791_831,
                pl791_832,
                pl791_847), found);

        found.clear();
        library.forEach(new Peak(791.847, 1, 2), procedure);
        Assert.assertEquals(Sets.newHashSet(
                pl791_812,
                pl791_821_a,
                pl791_821_b,
                pl791_831,
                pl791_832,
                pl791_847), found);
    }

    private PeakList mockSpectrum(double mz, int charge) {

        PeakList peakList = mock(PeakList.class);
        when(peakList.getPrecursor()).thenReturn(new Peak(mz, 10, charge));
        when(peakList.toString()).thenReturn(mz + " " + charge + "+");

        return peakList;
    }
}