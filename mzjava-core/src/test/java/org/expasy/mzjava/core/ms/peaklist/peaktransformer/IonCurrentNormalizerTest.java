/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.peaklist.peaktransformer;

import org.expasy.mzjava.core.ms.peaklist.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class IonCurrentNormalizerTest {

    @Test
    public void test() throws Exception {

        IonCurrentNormalizer<PeakAnnotation> normalizer = new IonCurrentNormalizer<>();
        PeakList<PeakAnnotation> peakList = new DoublePeakList<>();
        PeakCollectorSink<PeakAnnotation> sink = new PeakCollectorSink<>();
        sink.setPeakList(peakList);
        normalizer.setSink(sink);

        normalizer.start(3);
        List<PeakAnnotation> empty = Collections.emptyList();
        normalizer.processPeak(1, 10, empty);
        normalizer.processPeak(2, 100, empty);
        normalizer.processPeak(3, 25, empty);
        normalizer.end();

        double delta = 0.0000001;
        double sum = 10+100+25.0;

        Assert.assertEquals(3, peakList.size());

        Assert.assertEquals(1, peakList.getMz(0), delta);
        Assert.assertEquals(10/sum, peakList.getIntensity(0), delta);

        Assert.assertEquals(2, peakList.getMz(1), delta);
        Assert.assertEquals(100/sum, peakList.getIntensity(1), delta);

        Assert.assertEquals(3, peakList.getMz(2), delta);
        Assert.assertEquals(25/sum, peakList.getIntensity(2), delta);
    }

    @Test
    public void testWithZero() throws Exception {

        IonCurrentNormalizer<PeakAnnotation> normalizer = new IonCurrentNormalizer<>();
        PeakList<PeakAnnotation> peakList = new DoublePeakList<>();
        PeakCollectorSink<PeakAnnotation> sink = new PeakCollectorSink<>();
        sink.setPeakList(peakList);
        normalizer.setSink(sink);

        normalizer.start(3);
        List<PeakAnnotation> empty = Collections.emptyList();
        normalizer.processPeak(1, 10, empty);
        normalizer.processPeak(2, 100, empty);
        normalizer.processPeak(3, 0, empty);
        normalizer.end();

        double delta = 0.0000001;
        double sum = 10+100;

        Assert.assertEquals(3, peakList.size());

        Assert.assertEquals(1, peakList.getMz(0), delta);
        Assert.assertEquals(10/sum, peakList.getIntensity(0), delta);

        Assert.assertEquals(2, peakList.getMz(1), delta);
        Assert.assertEquals(100/sum, peakList.getIntensity(1), delta);

        Assert.assertEquals(3, peakList.getMz(2), delta);
        Assert.assertEquals(0/sum, peakList.getIntensity(2), delta);
    }

    @Test
    public void testAllZero() throws Exception {

        IonCurrentNormalizer<PeakAnnotation> normalizer = new IonCurrentNormalizer<>();
        PeakList<PeakAnnotation> peakList = new DoublePeakList<>();
        PeakCollectorSink<PeakAnnotation> sink = new PeakCollectorSink<>();
        sink.setPeakList(peakList);
        normalizer.setSink(sink);

        normalizer.start(3);
        List<PeakAnnotation> empty = Collections.emptyList();
        normalizer.processPeak(1, 0, empty);
        normalizer.processPeak(2, 0, empty);
        normalizer.processPeak(3, 0, empty);
        normalizer.end();

        double delta = 0.0000001;
        double sum = 0;

        Assert.assertEquals(3, peakList.size());

        Assert.assertEquals(1, peakList.getMz(0), delta);
        Assert.assertEquals(0/sum, peakList.getIntensity(0), delta);

        Assert.assertEquals(2, peakList.getMz(1), delta);
        Assert.assertEquals(0/sum, peakList.getIntensity(1), delta);

        Assert.assertEquals(3, peakList.getMz(2), delta);
        Assert.assertEquals(0/sum, peakList.getIntensity(2), delta);
    }

    @Test
    public void testEmpty() throws Exception {

        IonCurrentNormalizer<PeakAnnotation> normalizer = new IonCurrentNormalizer<>();
        PeakList<PeakAnnotation> peakList = new DoublePeakList<>();
        PeakCollectorSink<PeakAnnotation> sink = new PeakCollectorSink<>();
        sink.setPeakList(peakList);
        normalizer.setSink(sink);

        normalizer.start(0);
        normalizer.end();

        Assert.assertEquals(0, peakList.size());
    }
}
