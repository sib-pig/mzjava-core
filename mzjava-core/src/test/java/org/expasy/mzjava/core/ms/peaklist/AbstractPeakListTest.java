/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.peaklist;

import com.google.common.base.Function;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.IdentityPeakProcessor;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.spectrum.MockPeakAnnotation;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class AbstractPeakListTest {

    @Test
    public void testCopyConstructor() throws Exception {

        DoubleConstantPeakList<MockPeakAnnotation> peakList = new  DoubleConstantPeakList<MockPeakAnnotation>(6);
        peakList.getPrecursor().setMzAndCharge(348.892354, 12);
        peakList.getPrecursor().setIntensity(500);

        peakList.add(10.0,10.0);
        peakList.add(20.0,20.0);
        peakList.add(30.0,30.0);
        peakList.add(40.0,40.0);
        peakList.add(50.0,50.0,new MockPeakAnnotation(IonType.y, 10, 2));
        peakList.add(60.0,60.0,new MockPeakAnnotation(IonType.y, 10, 3));

        DoubleConstantPeakList<MockPeakAnnotation> copy = peakList.copy(new IdentityPeakProcessor<MockPeakAnnotation>());

        Peak precursorCopy = copy.getPrecursor();

        Assert.assertEquals(348.892354, precursorCopy.getMz(), 0.000001);
        Assert.assertEquals(500, precursorCopy.getIntensity(), 0.000001);
        Assert.assertEquals(12, precursorCopy.getCharge());

        Assert.assertTrue(copy.hasAnnotations());
        Assert.assertArrayEquals(new int[]{4, 5}, copy.getAnnotationIndexes());

        MockPeakAnnotation annotation = copy.getFirstAnnotation(4).get();
        Assert.assertEquals(IonType.y, annotation.getIonType());
        Assert.assertEquals(10, annotation.getCharge());
        Assert.assertEquals(2, annotation.getId());

        annotation = copy.getFirstAnnotation(5).get();
        Assert.assertEquals(IonType.y, annotation.getIonType());
        Assert.assertEquals(10, annotation.getCharge());
        Assert.assertEquals(3, annotation.getId());
    }

    @Test
    public void testShiftAnnotations() throws Exception {

        MockPeakList peakList = new MockPeakList(4);

        peakList.addAnnotations(1, Arrays.asList(new MockPeakAnnotation(IonType.y, 2, 11)));
        peakList.addAnnotations(2, Arrays.asList(new MockPeakAnnotation(IonType.y, 2, 11)));
        peakList.addAnnotations(3, Arrays.asList(new MockPeakAnnotation(IonType.y, 2, 11)));

        Assert.assertArrayEquals(new int[]{1, 2, 3}, peakList.getAnnotationIndexes());

        peakList.shiftAnnotations(2);
        Assert.assertArrayEquals(new int[]{1, 3, 4}, peakList.getAnnotationIndexes());
    }

    @Test
    public void testGetId() throws Exception {

        PeakList peakList = new MockPeakList(0);

        UUID id = peakList.getId();
        Assert.assertEquals(id, peakList.getId());

        UUID id2 = UUID.randomUUID();
        peakList.setId(id2);
        Assert.assertEquals(id2, peakList.getId());
    }

    @Test(expected = IllegalStateException.class)
    public void testExceptionOnAddSorted() throws Exception {

        MockPeakList peakList = new MockPeakList(0);

        peakList.addSorted(new double[]{1, 2}, new double[]{10});
    }

    @Test
    public void testWrapperPeakCursor() throws Exception {

        PeakCursor cursor = mock(PeakCursor.class);
        //noinspection unchecked
        Function<List, List> listListFunction = mock(Function.class);
        //noinspection unchecked
        AbstractPeakList.WrapperPeakCursor wrapper = new AbstractPeakList.WrapperPeakCursor(cursor, listListFunction);

        Mockito.reset(cursor);
        wrapper.size();
        verify(cursor).size();

        Mockito.reset(cursor);
        wrapper.isEmpty();
        verify(cursor).isEmpty();

        Mockito.reset(cursor);
        wrapper.resetCursor();
        verify(cursor).resetCursor();

        Mockito.reset(cursor);
        wrapper.next();
        verify(cursor).next();

        Mockito.reset(cursor);
        wrapper.previous();
        verify(cursor).previous();

        Mockito.reset(cursor);
        wrapper.next(1234.98);
        verify(cursor).next(1234.98);

        Mockito.reset(cursor);
        wrapper.currIntensity();
        verify(cursor).currIntensity();

        Mockito.reset(cursor);
        wrapper.currAnnotations();
        verify(cursor).currAnnotations();
        verify(listListFunction).apply(anyList());

        Mockito.reset(cursor);
        wrapper.currMz();
        verify(cursor).currMz();

        Mockito.reset(cursor);
        wrapper.canPeek(8);
        verify(cursor).canPeek(8);

        Mockito.reset(cursor);
        wrapper.peekIntensity(4);
        verify(cursor).peekIntensity(4);

        Mockito.reset(cursor);
        wrapper.peekMz(6);
        verify(cursor).peekMz(6);

        Mockito.reset(cursor);
        wrapper.lastMz();
        verify(cursor).lastMz();

        Mockito.reset(cursor);
        wrapper.lastIntensity();
        verify(cursor).lastIntensity();

        Mockito.reset(cursor);
        wrapper.getMz(2);
        verify(cursor).getMz(2);

        Mockito.reset(cursor);
        wrapper.getIntensity(7);
        verify(cursor).getIntensity(7);

        Mockito.reset(cursor);
        wrapper.moveToClosest(4321.123);
        verify(cursor).moveToClosest(4321.123);

        Mockito.reset(cursor);
        wrapper.moveBefore(1234.321);
        verify(cursor).moveBefore(1234.321);

        Mockito.reset(cursor);
        wrapper.movePast(5678.123);
        verify(cursor).movePast(5678.123);

        Mockito.reset(cursor);
        wrapper.getClosestIndex(3521.734);
        verify(cursor).getClosestIndex(3521.734);
    }

    private static class MockPeakList extends AbstractPeakList<MockPeakAnnotation> {

        private MockPeakList(int size) {

            this.size = size;
        }

        private MockPeakList(AbstractPeakList<MockPeakAnnotation> src) {

            super(src);
        }

        @Override
        protected int doInsert(double mz, double intensity) {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        protected PeakSink<MockPeakAnnotation> newMergeSink() {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public int doAppend(double mz, double intensity) {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public double getMz(int index) {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public double getIntensity(int index) {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public void setIntensityAt(double intensity, int index) {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public int getMostIntenseIndex(double minMz, double maxMz) {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public int getMostIntenseIndex() {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public double[] getMzs(double[] dest) {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public double[] getMzs(double[] dest, int destPos) {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public double[] getMzs(int srcPos, double[] dest, int destPos, int length) {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public double[] getIntensities(double[] dest) {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public double[] getIntensities(double[] dest, int destPos) {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public double[] getIntensities(int srcPos, double[] dest, int destPos, int length) {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public void trimToSize() {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public void ensureCapacity(int minCapacity) {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public Precision getPrecision() {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public MockPeakList copy(PeakProcessor<MockPeakAnnotation, MockPeakAnnotation> peakProcessor) {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public MockPeakList copy(PeakProcessorChain<MockPeakAnnotation> peakProcessorChain) {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public double calcVectorLength() {

            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public int indexOf(int fromIndex, int toIndex, double mzKey) {

            throw new UnsupportedOperationException("Mock class does not support");
        }
    }
}
