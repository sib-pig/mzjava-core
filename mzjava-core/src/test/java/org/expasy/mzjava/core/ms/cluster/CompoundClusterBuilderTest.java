package org.expasy.mzjava.core.ms.cluster;

import org.junit.Test;

import java.util.Set;

import static org.mockito.Mockito.*;

public class CompoundClusterBuilderTest {

    @SuppressWarnings("unchecked")
    @Test
    public void testCluster() throws Exception {

        SimilarityGraph graph = mock(SimilarityGraph.class);

        ClusterBuilder builder1 = mock(ClusterBuilder.class);
        ClusterBuilder builder2 = mock(ClusterBuilder.class);

        Set clusters1 = mock(Set.class);
        when(builder1.cluster(graph)).thenReturn(clusters1);

        CompoundClusterBuilder compoundClusterBuilder = new CompoundClusterBuilder(builder1, builder2);
        compoundClusterBuilder.cluster(graph);

        verify(builder1).cluster(graph);
        verify(builder2).cluster(graph, clusters1);
        verifyNoMoreInteractions(builder1);
        verifyNoMoreInteractions(builder2);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testCluster1() throws Exception {

        SimilarityGraph graph = mock(SimilarityGraph.class);

        ClusterBuilder builder1 = mock(ClusterBuilder.class);
        ClusterBuilder builder2 = mock(ClusterBuilder.class);

        Set startingClusters = mock(Set.class);
        Set clustersBuilder1 = mock(Set.class);

        when(builder1.cluster(graph, startingClusters)).thenReturn(clustersBuilder1);

        CompoundClusterBuilder compoundClusterBuilder = new CompoundClusterBuilder(builder1, builder2);
        compoundClusterBuilder.cluster(graph, startingClusters);

        verify(builder1).cluster(graph, startingClusters);
        verify(builder2).cluster(graph, clustersBuilder1);
        verifyNoMoreInteractions(builder1);
        verifyNoMoreInteractions(builder2);
    }
}