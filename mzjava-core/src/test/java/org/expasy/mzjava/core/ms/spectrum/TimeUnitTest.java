/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrum;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @version 1
 */
public class TimeUnitTest {

    @Test
    public void testConvert() throws Exception {

        Assert.assertEquals(1, TimeUnit.SECOND.convert(1, TimeUnit.SECOND), 0.000001);
        Assert.assertEquals(60, TimeUnit.MINUTE.convert(1, TimeUnit.SECOND), 0.000001);
        Assert.assertEquals(60*60, TimeUnit.HOUR.convert(1, TimeUnit.SECOND), 0.000001);

        Assert.assertEquals(1, TimeUnit.SECOND.convert(60, TimeUnit.MINUTE), 0.000001);
        Assert.assertEquals(1, TimeUnit.MINUTE.convert(1, TimeUnit.MINUTE), 0.000001);
        Assert.assertEquals(3600, TimeUnit.HOUR.convert(60, TimeUnit.MINUTE), 0.000001);

        Assert.assertEquals(0.016666666666666666, TimeUnit.SECOND.convert(60, TimeUnit.HOUR), 0.000001);
        Assert.assertEquals(1, TimeUnit.MINUTE.convert(60, TimeUnit.HOUR), 0.000001);
        Assert.assertEquals(1, TimeUnit.HOUR.convert(1, TimeUnit.HOUR), 0.000001);
    }
}
