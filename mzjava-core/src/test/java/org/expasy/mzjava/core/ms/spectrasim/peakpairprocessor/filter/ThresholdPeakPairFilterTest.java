/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.filter;

import gnu.trove.list.array.TDoubleArrayList;
import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.PeakPairSink;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class ThresholdPeakPairFilterTest {

    @Test
    public void test() throws Exception {

        ThresholdPeakPairFilter<PeakAnnotation, PeakAnnotation> filter = new ThresholdPeakPairFilter<PeakAnnotation, PeakAnnotation>(12);

        MockSink sink = new MockSink();
        List<PeakAnnotation> empty = Collections.emptyList();

        filter.setSink(sink);

        filter.begin(null, null);

        filter.processPeakPair(1, 3, 4, empty, empty);
        filter.processPeakPair(2, 3, 12, empty, empty);
        filter.processPeakPair(3, 13, 2, empty, empty);
        filter.processPeakPair(4, 13, 18, empty, empty);

        filter.end();

        Assert.assertArrayEquals(new double[]{2, 3, 4}, sink.getCentroids().toArray(), 0.0000000001);
        Assert.assertArrayEquals(new double[]{3, 13, 13}, sink.getIntensitiesX().toArray(), 0.0000000001);
        Assert.assertArrayEquals(new double[]{12, 2, 18}, sink.getIntensitiesY().toArray(), 0.0000000001);
    }

    private static class MockSink implements PeakPairSink<PeakAnnotation, PeakAnnotation> {

        private TDoubleArrayList centroids = new TDoubleArrayList();
        private TDoubleArrayList xIntensities = new TDoubleArrayList();
        private TDoubleArrayList yIntensities = new TDoubleArrayList();
        private List<List<? extends PeakAnnotation>> xAnnotationList = new ArrayList<List<? extends PeakAnnotation>>();
        private List<List<? extends PeakAnnotation>> yAnnotationList = new ArrayList<List<? extends PeakAnnotation>>();

        @Override
        public void begin(PeakList<PeakAnnotation> xPeakList, PeakList<PeakAnnotation> yPeakList) {

        }

        @Override
        public void processPeakPair(double centroid, double xIntensity, double yIntensity, List<PeakAnnotation> xAnnotations, List<PeakAnnotation> yAnnotations) {

            centroids.add(centroid);
            xIntensities.add(xIntensity);
            yIntensities.add(yIntensity);
            xAnnotationList.add(xAnnotations);
            yAnnotationList.add(yAnnotations);
        }

        @Override
        public void end() {

        }

        public TDoubleArrayList getCentroids() {

            return centroids;
        }

        public TDoubleArrayList getIntensitiesX() {

            return xIntensities;
        }

        public TDoubleArrayList getIntensitiesY() {

            return yIntensities;
        }

        public List<List<? extends PeakAnnotation>> getAnnotationListX() {

            return xAnnotationList;
        }

        public List<List<? extends PeakAnnotation>> getAnnotationListY() {

            return yAnnotationList;
        }
    }
}
