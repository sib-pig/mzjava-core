package org.expasy.mzjava.core.ms.peaklist;

import org.expasy.mzjava.core.ms.peaklist.Polarity;
import org.junit.Assert;
import org.junit.Test;

public class PolarityTest {

    @Test
    public void testToInteger() throws Exception {

        Assert.assertEquals(1, Polarity.toInteger(Polarity.POSITIVE, -1));
        Assert.assertEquals(1, Polarity.toInteger(Polarity.POSITIVE, 1));
        Assert.assertEquals(0, Polarity.toInteger(Polarity.POSITIVE, 0));
        Assert.assertEquals(0, Polarity.toInteger(Polarity.POSITIVE, -0));

        Assert.assertEquals(-0, Polarity.toInteger(Polarity.NEGATIVE, 0));
        Assert.assertEquals(-0, Polarity.toInteger(Polarity.NEGATIVE, -0));
        Assert.assertEquals(-1, Polarity.toInteger(Polarity.NEGATIVE, 1));
        Assert.assertEquals(-1, Polarity.toInteger(Polarity.NEGATIVE, -1));

        Assert.assertEquals(-1, Polarity.toInteger(Polarity.UNKNOWN, -1));
        Assert.assertEquals(1, Polarity.toInteger(Polarity.UNKNOWN, 1));
    }

    @Test
    public void testGetPolarity() throws Exception {

        Assert.assertEquals(Polarity.POSITIVE, Polarity.getPolarity(1));
        Assert.assertEquals(Polarity.NEGATIVE, Polarity.getPolarity(-1));
        Assert.assertEquals(Polarity.UNKNOWN, Polarity.getPolarity(0));
    }
}