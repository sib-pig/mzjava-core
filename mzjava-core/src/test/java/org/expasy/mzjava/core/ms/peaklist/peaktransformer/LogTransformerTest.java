package org.expasy.mzjava.core.ms.peaklist.peaktransformer;

import org.expasy.mzjava.core.ms.peaklist.MockPeakSink;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessor;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

public class LogTransformerTest {

	@Test
	public void testLogE() throws Exception {

		double[] mzs = new double[]{408.05784111354126, 514.6757533311302, 584.0217507817738};
		double[] intensities = new double[]{8.0, 12.0, 0.0001};

		LogTransformer<PeakAnnotation> processor = new LogTransformer<PeakAnnotation>();
		MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();
		processor.setSink(sink);

		runProcessor(processor, mzs, intensities);

		double[] actualMz = sink.getMzList();
		double[] actualIntensities = sink.getIntensityList();

		Assert.assertEquals(mzs.length, actualMz.length);

		Assert.assertEquals(2.1972245773362196, actualIntensities[0], 0.000000000001);
		Assert.assertEquals(2.5649493574615367, actualIntensities[1], 0.000000000001);
		Assert.assertEquals(9.999500033329732E-5, actualIntensities[2], 0.000000000001);

		Assert.assertEquals(1, sink.getStartCalled());
		Assert.assertEquals(1, sink.getEndCalled());
	}

	@Test
	public void testProcessPeakUndefLogE() throws Exception {

		LogTransformer<PeakAnnotation> processor = new LogTransformer<PeakAnnotation>();
		MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();
		processor.setSink(sink);

		processor.processPeak(2, -10, null);
		double[] actualIntensities = sink.getIntensityList();
		Assert.assertEquals(0.0, actualIntensities[0], 0.00000001);
	}

	private void runProcessor(PeakProcessor<PeakAnnotation, PeakAnnotation> processor, double[] keys, double[] values) {

		processor.start(keys.length);

		for (int i = 0; i < keys.length; i++) {

			List<PeakAnnotation> objects = Collections.emptyList();
			processor.processPeak(keys[i], values[i], objects);
		}

		processor.end();
	}
}
