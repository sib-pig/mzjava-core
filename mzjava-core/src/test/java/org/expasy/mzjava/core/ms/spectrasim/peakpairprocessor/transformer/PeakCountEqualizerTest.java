/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.transformer;

import gnu.trove.list.array.TDoubleArrayList;
import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.PeakPairSink;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PeakCountEqualizerTest {

    @Test
    public void test() throws Exception {

        MockSink sink = new MockSink();

        PeakCountEqualizer<PeakAnnotation, PeakAnnotation> equalizer = new PeakCountEqualizer<PeakAnnotation, PeakAnnotation>();
        equalizer.setSink(sink);

        equalizer.begin(null, null);

        List<PeakAnnotation> empty = Collections.emptyList();

        equalizer.processPeakPair(1, 1, 5, empty, empty);
        equalizer.processPeakPair(2, 2, 0, empty, empty);
        equalizer.processPeakPair(3, 3, 0, empty, empty);
        equalizer.processPeakPair(4, 4, 4, empty, empty);
        equalizer.processPeakPair(5, 5, 0, empty, empty);

        equalizer.end();

        Assert.assertEquals("Begin was not called", true, sink.beginCalled);
        Assert.assertEquals("End was not called", true, sink.endCalled);

        checkArrayList(sink.getCentroids(), 1, 4, 5);
        checkArrayList(sink.getxValues(), 0, 4, 5);
        checkArrayList(sink.getyValues(), 5, 4, 0);
    }

    private void checkArrayList(TDoubleArrayList arrayList, double... expected) {

        Assert.assertEquals(expected.length, arrayList.size());

        for(int i = 0; i < expected.length; i++) {

            Assert.assertEquals(expected[i], arrayList.get(i), 0.0000001);
        }
    }

    private static class MockSink implements PeakPairSink<PeakAnnotation, PeakAnnotation> {

        private TDoubleArrayList xValues = new TDoubleArrayList();
        private TDoubleArrayList yValues = new TDoubleArrayList();
        private TDoubleArrayList centroids = new TDoubleArrayList();

        boolean beginCalled = false;
        boolean endCalled = false;

        @Override
        public void begin(PeakList<PeakAnnotation> xPeakList, PeakList<PeakAnnotation> yPeakList) {

            beginCalled = true;
        }

        @Override
        public void processPeakPair(double centroid, double x, double y, List<PeakAnnotation> xAnnotations, List<PeakAnnotation> yAnnotations) {

            xValues.add(x);
            yValues.add(y);
            centroids.add(centroid);
        }

        public void end() {

            endCalled = true;
        }

        public TDoubleArrayList getxValues() {

            return xValues;
        }

        public TDoubleArrayList getyValues() {

            return yValues;
        }

        public TDoubleArrayList getCentroids() {

            return centroids;
        }
    }
}
