package org.expasy.mzjava.core.mol;

import org.apache.commons.math3.complex.Complex;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class LowResIsotopicDistributionCalculatorTest {

    @Test
    public void testLoading() {
        LowResIsotopicDistributionCalculator isotopicDistributionCalculator = new LowResIsotopicDistributionCalculator();

        isotopicDistributionCalculator = new LowResIsotopicDistributionCalculator(16);
        Assert.assertEquals(16,isotopicDistributionCalculator.maxNrIsotopes);

        isotopicDistributionCalculator = new LowResIsotopicDistributionCalculator(53);
        Assert.assertEquals(64,isotopicDistributionCalculator.maxNrIsotopes);
    }

    @Test
    public void testBasicCalculation() {
        LowResIsotopicDistributionCalculator isotopicDistributionCalculator = new LowResIsotopicDistributionCalculator(16);

        double[] isoDist = isotopicDistributionCalculator.calcIsotopicDistribution(Composition.parseComposition("C"));

        Assert.assertArrayEquals(new double[]{0.9890,0.011,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0},isoDist,0.001);

        isoDist = isotopicDistributionCalculator.calcIsotopicDistribution(Composition.parseComposition("C2"));

        Assert.assertArrayEquals(new double[]{0.989*0.989,2*0.989*0.011,0.011*0.011,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0},isoDist,0.001);

        isoDist = isotopicDistributionCalculator.calcIsotopicDistribution(Composition.parseComposition("C3"));

        Assert.assertArrayEquals(new double[]{0.989*0.989*0.989,3*0.989*0.011,3*0.989*0.011*0.011,0.011*0.011*0.011,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0},isoDist,0.001);

        isoDist = isotopicDistributionCalculator.calcIsotopicDistribution(Composition.parseComposition("O"));

        Assert.assertArrayEquals(new double[]{0.99762, 0.00038, 0.002, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}, isoDist, 0.001);

        isoDist = isotopicDistributionCalculator.calcIsotopicDistribution(Composition.parseComposition("CO"));

        Assert.assertArrayEquals(new double[]{0.989 * 0.99762, 0.989 * 0.00038 + 0.99762 * 0.011, 0.9890 * 0.002 + 0.011 * 0.00038, 0.011 * 0.002, 0.002 * 0.002, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}, isoDist, 0.001);
    }

    @Test
    public void testCalculationCompostion() {

        LowResIsotopicDistributionCalculator isotopicDistributionCalculator = new LowResIsotopicDistributionCalculator(16);

        double[] isoDist = isotopicDistributionCalculator.calcIsotopicDistribution(Composition.parseComposition("C11H10N2O"));

        Assert.assertArrayEquals(new double[]{0.87556,0.115201,0.008726347,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0},isoDist,0.01);

        isoDist = isotopicDistributionCalculator.calcIsotopicDistribution(Composition.parseComposition("C40H72N12O13S2"));

        Assert.assertArrayEquals(new double[]{0.5323,0.2771,0.1317,0.0432,0.0120,0.0028,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0},isoDist,0.01);
    }

    @Test
    public void testCalculationMass() {

        LowResIsotopicDistributionCalculator isotopicDistributionCalculator = new LowResIsotopicDistributionCalculator(16);

        double mass = Composition.parseComposition("C40H72N12O13S2").getMolecularMass();

        double[] isoDist1 = isotopicDistributionCalculator.getIsotopicDistribution(Composition.parseComposition("C40H72N12O13S2"));
        double[] isoDist2 = isotopicDistributionCalculator.getIsotopicDistribution(mass,Composition.parseComposition("C40H72N12O13S2"));

        Assert.assertArrayEquals(isoDist1,isoDist2,0.01);

        isoDist1 = isotopicDistributionCalculator.getIsotopicDistribution(Composition.parseComposition("C20H36N6O6S1"));
        isoDist2 = isotopicDistributionCalculator.getIsotopicDistribution(mass/2,Composition.parseComposition("C40H72N12O13S2"));

        Assert.assertArrayEquals(isoDist1,isoDist2,0.01);
    }

    @Test
    public void testComplexPolar() {
        Complex c = new Complex(2.0,3.0);

        LowResIsotopicDistributionCalculator lowResIsotopicDistributionCalculator = new LowResIsotopicDistributionCalculator(16);
        LowResIsotopicDistributionCalculator.ComplexPolar cp = lowResIsotopicDistributionCalculator.new ComplexPolar(c);
        Complex c2 = cp.castToComplex();

        Assert.assertEquals(c.getReal(),c2.getReal(),0.000001);
        Assert.assertEquals(c.getImaginary(),c2.getImaginary(),0.000001);

        cp = lowResIsotopicDistributionCalculator.new ComplexPolar(0.0,1.0);

        Assert.assertEquals(1.0,cp.castToComplex().getReal(),0.0000001);
        Assert.assertEquals(0.0,cp.castToComplex().getImaginary(),0.0000001);

        cp.multiply(cp);

        Assert.assertEquals(1.0,cp.castToComplex().getReal(),0.0000001);
        Assert.assertEquals(0.0,cp.castToComplex().getImaginary(), 0.0000001);

        Complex c1 = new Complex(2.0,3.0);
        Complex c3 = new Complex(3.0,4.0);

        LowResIsotopicDistributionCalculator.ComplexPolar cp1 = lowResIsotopicDistributionCalculator.new ComplexPolar(c1);
        LowResIsotopicDistributionCalculator.ComplexPolar cp2 = lowResIsotopicDistributionCalculator.new ComplexPolar(c3);

        cp1.multiply(cp2);
        c1 = c1.multiply(c3);
        Assert.assertEquals(c1.getReal(), cp1.castToComplex().getReal(), 0.0000001);
        Assert.assertEquals(c1.getImaginary(),cp1.castToComplex().getImaginary(), 0.0000001);

        c = new Complex(2.0,3.0);
        c1 = c.pow(1/7.0);
        cp1 = lowResIsotopicDistributionCalculator.new ComplexPolar(c1);
        cp1.pow(7);

        Assert.assertEquals(c.getReal(), cp1.castToComplex().getReal(), 0.0000001);
        Assert.assertEquals(c.getImaginary(), cp1.castToComplex().getImaginary(), 0.0000001);
    }



}
