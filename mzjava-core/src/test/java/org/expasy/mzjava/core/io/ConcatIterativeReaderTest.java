/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.io;

import org.expasy.mzjava.core.io.ConcatIterativeReader;
import org.expasy.mzjava.core.io.IterativeReader;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class ConcatIterativeReaderTest {

    @Test
    public void test() throws Exception {

        MockReader reader1 = new MockReader("1, 1", "1, 2", "1, 3");

        MockReader reader2 = new MockReader("2, 1", "2, 2");

        @SuppressWarnings("unchecked") //Generic array creation
                ConcatIterativeReader<String> concatReader = new ConcatIterativeReader<String>(reader1, reader2);

        List<String> actual = new ArrayList<String>();
        while(concatReader.hasNext()) {

            actual.add(concatReader.next());
        }
        concatReader.close();

        Assert.assertEquals(Arrays.asList("1, 1", "1, 2", "1, 3", "2, 1", "2, 2"), actual);
        Assert.assertEquals(true, reader1.isClosed());
        Assert.assertEquals(true, reader2.isClosed());
    }

    @Test
    public void testEmpty() throws Exception {

        @SuppressWarnings("unchecked") //Generic array creation
        ConcatIterativeReader<String> concatReader = new ConcatIterativeReader<String>();

        Assert.assertEquals(false, concatReader.hasNext());
        concatReader.close();
    }

    @Test(expected = NoSuchElementException.class)
    public void testNextOnEmpty() throws Exception {

        @SuppressWarnings("unchecked") //Generic array creation
        ConcatIterativeReader<String> concatReader = new ConcatIterativeReader<String>();

        concatReader.next();
    }

    private class MockReader implements IterativeReader<String> {

        private final Iterator<String> iterator;
        private boolean closed = false;

        public MockReader(String... elements) {

            iterator = Arrays.asList(elements).iterator();
        }

        @Override
        public boolean hasNext() {

            return iterator.hasNext();
        }

        @Override
        public String next() throws IOException {

            return iterator.next();
        }

        @Override
        public void close() throws IOException {

            closed = true;
        }

        public boolean isClosed() {

            return closed;
        }
    }
}
