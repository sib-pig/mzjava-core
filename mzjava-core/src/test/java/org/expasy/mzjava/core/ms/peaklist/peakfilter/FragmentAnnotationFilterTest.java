package org.expasy.mzjava.core.ms.peaklist.peakfilter;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import org.expasy.mzjava.core.ms.peaklist.MockPeakSink;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;

/**
 * @author fnikitin
 * Date: 6/11/13
 */
public class FragmentAnnotationFilterTest {

    @Test
    public void test() {

        @SuppressWarnings("unchecked")
        Predicate<PeakAnnotation> mockPredicate = Mockito.mock(Predicate.class);
        Mockito.when(mockPredicate.apply(Matchers.<PeakAnnotation>any())).thenReturn(true);

        FragmentAnnotationFilter<PeakAnnotation> filterFragment = new FragmentAnnotationFilter<PeakAnnotation>(mockPredicate);

        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();

        filterFragment.setSink(sink);

        List<PeakAnnotation> emptyAnnotations = Collections.emptyList();
        List<PeakAnnotation> mockAnnotations = Lists.newArrayList(Mockito.mock(PeakAnnotation.class));

        filterFragment.start(7);
        filterFragment.processPeak(1, 150, mockAnnotations);
        filterFragment.processPeak(2, 10, mockAnnotations);
        filterFragment.processPeak(3, 1, emptyAnnotations);
        filterFragment.processPeak(4, 100, emptyAnnotations);
        filterFragment.processPeak(5, 200, emptyAnnotations);
        filterFragment.processPeak(6, 5, emptyAnnotations);
        filterFragment.processPeak(7, 1000, mockAnnotations);
        filterFragment.end();

        Assert.assertArrayEquals(new double[]{1, 2, 7}, sink.getMzList(), 0.00001);
        Assert.assertArrayEquals(new double[]{150, 10, 1000}, sink.getIntensityList(), 0.000001);

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getEndCalled());
    }
}
