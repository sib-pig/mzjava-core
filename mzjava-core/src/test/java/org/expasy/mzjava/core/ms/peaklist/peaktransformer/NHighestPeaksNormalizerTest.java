/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.peaklist.peaktransformer;

import org.expasy.mzjava.core.ms.peaklist.MockPeakSink;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class NHighestPeaksNormalizerTest {

    @Test
    public void test() {

        NHighestPeaksNormalizer norm = new NHighestPeaksNormalizer(5, 100);
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();
        norm.setSink(sink);

        List<PeakAnnotation> annotations = Collections.emptyList();

        norm.start(15);

        norm.processPeak(1, 493, annotations);
        norm.processPeak(2, 229, annotations);
        norm.processPeak(3, 138, annotations);
        norm.processPeak(4, 704, annotations);
        norm.processPeak(5, 751, annotations);
        norm.processPeak(6, 18, annotations);
        norm.processPeak(7, 874, annotations);
        norm.processPeak(8, 435, annotations);
        norm.processPeak(9, 446, annotations);
        norm.processPeak(10, 222, annotations);
        norm.processPeak(11, 375, annotations);
        norm.processPeak(12, 88, annotations);
        norm.processPeak(13, 459, annotations);
        norm.processPeak(14, 327, annotations);
        norm.processPeak(15, 222, annotations);
        norm.end();

        Assert.assertArrayEquals(new double[]{1, 2, 3, 4, 5,6 ,7 ,8 ,9, 10, 11, 12, 13, 14, 15}, sink.getMzList(), 0.000001);
        Assert.assertArrayEquals(new double[]{15.02591, 6.97958, 4.20603, 21.45687, 22.88936, 0.54861, 26.63822, 13.25815, 13.59342, 6.76623, 11.42944, 2.68211, 13.98964, 9.96647, 6.76623}, sink.getIntensityList(), 0.00001);

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getEndCalled());
    }
}
