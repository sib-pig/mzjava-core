package org.expasy.mzjava.core.mol;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.special.Gamma;
import org.apache.commons.math3.util.FastMath;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class FullResIsotopicDistributionCalculatorTest {
    @Test
    public void testLoading() {
        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();
    }

    @Test
    public void testMerging() {
        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();

        FullResIsotopicDistributionCalculator.Peak[] peaks = new FullResIsotopicDistributionCalculator.Peak[6];
        peaks[0] = new FullResIsotopicDistributionCalculator.Peak(1.0,0.3);
        peaks[1] = new FullResIsotopicDistributionCalculator.Peak(1.1,0.3);
        peaks[2] = new FullResIsotopicDistributionCalculator.Peak(2.0,0.5);
        peaks[3] = new FullResIsotopicDistributionCalculator.Peak(3.0,0.5);
        peaks[4] = new FullResIsotopicDistributionCalculator.Peak(3.0,0.5);
        peaks[5] = new FullResIsotopicDistributionCalculator.Peak(3.02,0.5);

        isotopicDistributionCalculator.minProb = 0.01;
        isotopicDistributionCalculator.massTol = 0.2;
        isotopicDistributionCalculator.maxNrIsotopes = 10;

        peaks = isotopicDistributionCalculator.mergePeaks(peaks) ;

        Assert.assertEquals(3,peaks.length);
        Assert.assertEquals(2.1/2,peaks[0].mz,0.0000001);
        Assert.assertEquals(0.3+0.3,peaks[0].prob,0.0000001);
        Assert.assertEquals(2.0,peaks[1].mz,0.0000001);
        Assert.assertEquals(0.5,peaks[1].prob,0.0000001);
        Assert.assertEquals(9.02/3,peaks[2].mz,0.0000001);
        Assert.assertEquals(0.5+0.5+0.5,peaks[2].prob,0.0000001);

        peaks = new FullResIsotopicDistributionCalculator.Peak[6];
        peaks[0] = new FullResIsotopicDistributionCalculator.Peak(1.0,0.3);
        peaks[1] = new FullResIsotopicDistributionCalculator.Peak(1.1,0.3);
        peaks[2] = new FullResIsotopicDistributionCalculator.Peak(2.0,0.5);
        peaks[3] = new FullResIsotopicDistributionCalculator.Peak(3.0,0.5);
        peaks[4] = new FullResIsotopicDistributionCalculator.Peak(3.0,0.5);
        peaks[5] = new FullResIsotopicDistributionCalculator.Peak(3.02,0.5);
        peaks[5] = new FullResIsotopicDistributionCalculator.Peak(4.02,0.5);

        peaks = isotopicDistributionCalculator.mergePeaks(peaks) ;

        Assert.assertEquals(4,peaks.length);
        Assert.assertEquals(2.1/2,peaks[0].mz,0.0000001);
        Assert.assertEquals(0.3+0.3,peaks[0].prob,0.0000001);
        Assert.assertEquals(2.0,peaks[1].mz,0.0000001);
        Assert.assertEquals(0.5,peaks[1].prob,0.0000001);
        Assert.assertEquals(3.0,peaks[2].mz,0.0000001);
        Assert.assertEquals(0.5+0.5,peaks[2].prob,0.0000001);
        Assert.assertEquals(4.02,peaks[3].mz,0.0000001);
        Assert.assertEquals(0.5,peaks[3].prob,0.0000001);

    }

    @Test
    public void testDicardPeaks() {
        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();

        FullResIsotopicDistributionCalculator.Peak[] peaks = new FullResIsotopicDistributionCalculator.Peak[6];
        peaks[0] = new FullResIsotopicDistributionCalculator.Peak(1.0, 0.3);
        peaks[1] = new FullResIsotopicDistributionCalculator.Peak(1.5, 0.001);
        peaks[2] = new FullResIsotopicDistributionCalculator.Peak(2.0, 0.5);
        peaks[3] = new FullResIsotopicDistributionCalculator.Peak(3.0, 0.001);
        peaks[4] = new FullResIsotopicDistributionCalculator.Peak(3.5, 0.001);
        peaks[5] = new FullResIsotopicDistributionCalculator.Peak(4.02, 0.5);

        isotopicDistributionCalculator.minProb = 0.01;
        isotopicDistributionCalculator.massTol = 0.2;
        isotopicDistributionCalculator.maxNrIsotopes = 10;

        peaks = isotopicDistributionCalculator.discardSmallPeaks(peaks);

        Assert.assertEquals(3, peaks.length);
        Assert.assertEquals(1.0, peaks[0].mz, 0.0000001);
        Assert.assertEquals(0.3, peaks[0].prob, 0.0000001);
        Assert.assertEquals(2.0, peaks[1].mz, 0.0000001);
        Assert.assertEquals(0.5, peaks[1].prob, 0.0000001);
        Assert.assertEquals(4.02, peaks[2].mz, 0.0000001);
        Assert.assertEquals(0.5, peaks[2].prob, 0.0000001);


        peaks = new FullResIsotopicDistributionCalculator.Peak[6];
        peaks[0] = new FullResIsotopicDistributionCalculator.Peak(1.0, 0.001);
        peaks[1] = new FullResIsotopicDistributionCalculator.Peak(1.5, 0.001);
        peaks[2] = new FullResIsotopicDistributionCalculator.Peak(2.0, 0.5);
        peaks[3] = new FullResIsotopicDistributionCalculator.Peak(3.0, 0.001);
        peaks[4] = new FullResIsotopicDistributionCalculator.Peak(3.5, 0.001);
        peaks[5] = new FullResIsotopicDistributionCalculator.Peak(4.02, 0.001);

        peaks = isotopicDistributionCalculator.discardSmallPeaks(peaks);

        Assert.assertEquals(1, peaks.length);
        Assert.assertEquals(2.0, peaks[0].mz, 0.0000001);
        Assert.assertEquals(0.5, peaks[0].prob, 0.0000001);
    }

    @Test
    public void testFold() {
        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();

        FullResIsotopicDistributionCalculator.Peak[] peaks1 = new FullResIsotopicDistributionCalculator.Peak[5];
        peaks1[0] = new FullResIsotopicDistributionCalculator.Peak(1.0, 1.0);
        peaks1[1] = new FullResIsotopicDistributionCalculator.Peak(2.0, 0.1);
        peaks1[2] = new FullResIsotopicDistributionCalculator.Peak(3.0, 0.5);
        peaks1[3] = new FullResIsotopicDistributionCalculator.Peak(4.0, 0.001);
        peaks1[4] = new FullResIsotopicDistributionCalculator.Peak(5.0, 0.001);

        FullResIsotopicDistributionCalculator.Peak[] peaks2 = new FullResIsotopicDistributionCalculator.Peak[3];
        peaks2[0] = new FullResIsotopicDistributionCalculator.Peak(1.0, 1.0);
        peaks2[1] = new FullResIsotopicDistributionCalculator.Peak(2.0, 0.7);
        peaks2[2] = new FullResIsotopicDistributionCalculator.Peak(3.0, 0.1);

        // 2.0 3.0 4.0                       1.0 0.7  0.1
        //     3.0 4.0 5.0                       0.1  0.07 0.01
        //         4.0 5.0 6.0                        0.5  0.35  0.05
        //             5.0 6.0 7.0                         0.001 0.0007 0.0001
        //                 6.0 7.0 8.0                           0.001  0.0007 0.0001
        // -------------------------------------------------------------------------
        // 2.0 3.0 4.0 5.0 6.0 7.0 8.0       1.0 0.8  0.67 0.361 0.0517 0.0008 0.0001

        isotopicDistributionCalculator.minProb = 0.01;
        isotopicDistributionCalculator.massTol = 0.2;
        isotopicDistributionCalculator.maxNrIsotopes = 10;

        peaks1 = isotopicDistributionCalculator.fold(peaks1,peaks2);

        Assert.assertEquals(7, peaks1.length);
        Assert.assertEquals(2.0, peaks1[0].mz, 0.0000001);
        Assert.assertEquals(1.0, peaks1[0].prob, 0.0000001);
        Assert.assertEquals(3.0, peaks1[1].mz, 0.0000001);
        Assert.assertEquals(0.8, peaks1[1].prob, 0.0000001);
        Assert.assertEquals(4.0, peaks1[2].mz, 0.0000001);
        Assert.assertEquals(0.67, peaks1[2].prob, 0.0000001);
        Assert.assertEquals(5.0, peaks1[3].mz, 0.0000001);
        Assert.assertEquals(0.361, peaks1[3].prob, 0.0000001);
        Assert.assertEquals(6.0, peaks1[4].mz, 0.0000001);
        Assert.assertEquals(0.0517, peaks1[4].prob, 0.0000001);
        Assert.assertEquals(7.0, peaks1[5].mz, 0.0000001);
        Assert.assertEquals(0.0008, peaks1[5].prob, 0.0000001);
        Assert.assertEquals(8.0, peaks1[6].mz, 0.0000001);
        Assert.assertEquals(0.0001, peaks1[6].prob, 0.0000001);
    }

    @Test
    public void testBinomProbs() {
        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();

        isotopicDistributionCalculator.minProb = 0.01;
        isotopicDistributionCalculator.massTol = 0.2;
        isotopicDistributionCalculator.maxNrIsotopes = 10;

        double[] binom = isotopicDistributionCalculator.calcBinomialProbs(10,0.2,10);

        BinomialDistribution bd = new BinomialDistribution(10, 0.2);
        for (int i=0;i<10;i++) {
            Assert.assertEquals(bd.probability(i),binom[i],0.0001);
        }

    }

    @Test
    public void testBinomProbsSpeed() {
        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();
        isotopicDistributionCalculator.minProb = 0.01;
        isotopicDistributionCalculator.massTol = 0.2;
        isotopicDistributionCalculator.maxNrIsotopes = 10;


        int nrTrials = 1000;
        long before = System.currentTimeMillis();
        for (int i=0;i<nrTrials;i++) {
            int n = i%100;
            BinomialDistribution bd = new BinomialDistribution(n, 0.2);
            for (int j=0;j<n;j++) {
                bd.probability(j);
            }
        }
        long after = System.currentTimeMillis();

        System.out.println("BinomialDistribution: "+(after-before));


        before = System.currentTimeMillis();
        for (int i=0;i<nrTrials;i++) {
            int n = i%100;
            for (int j=0;j<n;j++) {
                isotopicDistributionCalculator.calcBinomialProbs(n, 0.2,n);
            }
        }
        after = System.currentTimeMillis();

        System.out.println("Own: "+(after-before));
    }

    @Test
    public void testSimpeIsoDist() {

        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();
        PeakList<PeakAnnotation> peaks = isotopicDistributionCalculator.getIsotopicDistribution(Composition.parseComposition("C100"), 1,10,0.001,0.00001);


        LowResIsotopicDistributionCalculator lowResIsotopicDistributionCalculator = new LowResIsotopicDistributionCalculator(10);
        double[] isoDist = lowResIsotopicDistributionCalculator.getIsotopicDistribution(Composition.parseComposition("C100"));

        for (int i=0;i<peaks.size();i++) {
            Assert.assertEquals(peaks.getIntensity(i),isoDist[i],0.01);
        }
    }

    @Test
    public void testSimpeIsoDist3() {

        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();

        Composition comp = Composition.parseComposition("Ca");
        PeakList<PeakAnnotation> peaks = isotopicDistributionCalculator.getIsotopicDistribution(comp, 0,10,0.000001,0.00001);

//CALCIUM    Ca(40) 39.9625911 96.941 Ca(42) 41.9586183 0.647 Ca(43) 42.9587668 0.135 Ca(44) 43.9554811 2.086 Ca(46) 45.953693 0.004 Ca(48) 47.952534 0.187

        Assert.assertEquals(6,peaks.size());
        Assert.assertEquals(comp.getMolecularMass(),peaks.getMz(0),0.00000001);
        Assert.assertEquals(0.96941,peaks.getIntensity(0),0.00000001);
        Assert.assertEquals(41.9586183,peaks.getMz(1),0.00000001);
        Assert.assertEquals(0.00647,peaks.getIntensity(1),0.00000001);
        Assert.assertEquals(42.9587668,peaks.getMz(2),0.00000001);
        Assert.assertEquals(0.00135,peaks.getIntensity(2),0.00000001);
        Assert.assertEquals(43.9554811,peaks.getMz(3),0.00000001);
        Assert.assertEquals(0.02086,peaks.getIntensity(3),0.00000001);
        Assert.assertEquals(45.953693,peaks.getMz(4),0.00000001);
        Assert.assertEquals(0.00004,peaks.getIntensity(4),0.00000001);
        Assert.assertEquals(47.952534,peaks.getMz(5),0.00000001);
        Assert.assertEquals(0.00187,peaks.getIntensity(5),0.00000001);

        double sum = 0.0;
        for (int i=0;i<6;i++) sum += peaks.getIntensity(i);
        Assert.assertEquals(1.0,sum,0.000001);

        isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();
        peaks = isotopicDistributionCalculator.getIsotopicDistribution(Composition.parseComposition("Ca5"), 0, 25, 0.0000001, 0.000001);

        FullResIsotopicDistributionCalculator.Peak[] nominalPeaks = readCa5IsoDist();

        int i = 0;
        int j = 0;
        int nrMatch = 0;
        double massTol = 0.01;
        double probTol = 2.0;

        while (i<peaks.size() && j<nominalPeaks.length) {
            double m = peaks.getMz(i);

            while (nominalPeaks[j].mz<m-massTol&& j<nominalPeaks.length-1) j++;
            if (nominalPeaks[j].mz-m<massTol && Math.abs(nominalPeaks[j].prob-peaks.getIntensity(i)/peaks.getIntensity(0)*100)<probTol) {
                nrMatch++;
            }
            i++;
        }

        Assert.assertTrue(1.0 *nrMatch/ peaks.size()>0.7);
        Assert.assertTrue(1.0 * nrMatch / nominalPeaks.length > 0.7);
    }


    @Test
    public void testSimpeIsoDist2() {

        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();

        Composition comp = Composition.parseComposition("P");
        PeakList<PeakAnnotation> peaks = isotopicDistributionCalculator.getIsotopicDistribution(comp, 0,10,0.001,0.00001);

        Assert.assertEquals(1,peaks.size());
        Assert.assertEquals(comp.getMolecularMass(),peaks.getMz(0),0.00000001);
        Assert.assertEquals(1.0,peaks.getIntensity(0),0.00000001);


        comp = Composition.parseComposition("P100");
        peaks = isotopicDistributionCalculator.getIsotopicDistribution(comp, 0, 10, 0.001, 0.00001);

        Assert.assertEquals(1,peaks.size());
        Assert.assertEquals(comp.getMolecularMass(),peaks.getMz(0),0.00000001);
        Assert.assertEquals(1.0,peaks.getIntensity(0),0.00000001);
    }

    @Test
    public void testSimpleIsoDistSpeed() {

        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();

        LowResIsotopicDistributionCalculator lowResIsotopicDistributionCalculator = new LowResIsotopicDistributionCalculator(16);

        int nrTrials = 1000;
        long before = System.currentTimeMillis();
        for (int i=0;i<nrTrials;i++) {
            isotopicDistributionCalculator.getIsotopicDistribution(Composition.parseComposition("C100"), 1, 10, 0.001, 0.00001);
        }
        long after = System.currentTimeMillis();

        System.out.println("FullResIsotopicDistributionCalculator: "+(after-before));


        before = System.currentTimeMillis();
        for (int i=0;i<nrTrials;i++) {
            lowResIsotopicDistributionCalculator.getIsotopicDistribution(Composition.parseComposition("C100"));
        }
        after = System.currentTimeMillis();

        System.out.println("LowResIsotopicDistributionCalculator: " + (after - before));
    }

    @Test
    public void testCombinations() {

        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();
        isotopicDistributionCalculator.minProb = 0.001;
        isotopicDistributionCalculator.massTol = 0.00001;
        isotopicDistributionCalculator.maxNrIsotopes = 10;


        double[] isoMasses = new double[]{15.994914622,16.9991315,17.9991604};
        double[] isoProbs = new double[]{Math.log(0.99762),Math.log(0.00038),Math.log(0.002)};
        int[] nrNeutrons = new int[]{0,1,2};

        int[] isoCnts = new int[isoMasses.length];
        isotopicDistributionCalculator.peakListIdx = 0;
        isotopicDistributionCalculator.logFactorialN = isotopicDistributionCalculator.logfactorial(3);

        isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();
        isotopicDistributionCalculator.minProb = 0.0;
        isotopicDistributionCalculator.massTol = 0.0;
        isotopicDistributionCalculator.maxNrIsotopes = 10;
        isotopicDistributionCalculator.isotopicPeakBuffer = new FullResIsotopicDistributionCalculator.Peak[100];
        isotopicDistributionCalculator.calcCombinations(isoMasses, isoProbs, nrNeutrons, 0, 3, isoCnts);

        FullResIsotopicDistributionCalculator.Peak[] isoPeaks = Arrays.copyOfRange(isotopicDistributionCalculator.isotopicPeakBuffer, 0, isotopicDistributionCalculator.peakListIdx);
        Arrays.sort(isoPeaks, isotopicDistributionCalculator.peakComparator);

        Assert.assertEquals(10, isoPeaks.length);
        Assert.assertEquals(47.984743866,isoPeaks[0].mz,0.00001);
        Assert.assertEquals(48.988960743,isoPeaks[1].mz,0.00001);
        Assert.assertEquals(49.988989644,isoPeaks[2].mz,0.00001);
        Assert.assertEquals(49.993177622,isoPeaks[3].mz,0.00001);
        Assert.assertEquals(50.993206522,isoPeaks[4].mz,0.00001);
        Assert.assertEquals(50.9973945,isoPeaks[5].mz,0.00001);
        Assert.assertEquals(51.993235422,isoPeaks[6].mz,0.00001);
        Assert.assertEquals(51.9974234,isoPeaks[7].mz,0.00001);
        Assert.assertEquals(52.997452300,isoPeaks[8].mz,0.00001);
        Assert.assertEquals(53.9974812, isoPeaks[9].mz, 0.00001);

    }

    @Test
    public void testMultiNomialProb() {

        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();

        double[] isoProbs = new double[]{Math.log(0.99),Math.log(0.0095),Math.log(0.0005)};
        int[] cnts = new int[] {97,2,1};

        isotopicDistributionCalculator.logFactorialN = isotopicDistributionCalculator.logfactorial(100);
        Assert.assertEquals(0.008257762,Math.exp(isotopicDistributionCalculator.calcMultinomialLogProb(2, cnts, isoProbs)),0.000001);

    }

    @Test
    public void testFactorial() {

        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();

        int nrTrials = 10000;
        for (int i=0;i<nrTrials;i++) {
            Assert.assertEquals(Gamma.logGamma(i + 1), isotopicDistributionCalculator.logfactorial(i), 0.000001);
        }
    }

    @Test
    public void testFactorialSpeed() {

        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();


        int nrTrials = 10000;
        long before = System.currentTimeMillis();
        for (int i=0;i<nrTrials;i++) {
             isotopicDistributionCalculator.logfactorial(i%20+1);
        }
        long after = System.currentTimeMillis();

        System.out.println("FullResIsotopicDistributionCalculator: "+(after-before));


        before = System.currentTimeMillis();
        for (int i=0;i<nrTrials;i++) {
            Gamma.logGamma(i%20+1);
        }
        after = System.currentTimeMillis();

        System.out.println("Gamma: "+(after-before));

    }

    @Test
    public void testMultinomialDist() {

        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();
        isotopicDistributionCalculator.minProb = 0.001;
        isotopicDistributionCalculator.massTol = 0.00001;
        isotopicDistributionCalculator.maxNrIsotopes = 10;

        isotopicDistributionCalculator.logFactorialN = isotopicDistributionCalculator.logfactorial(100);

        FullResIsotopicDistributionCalculator.Peak[] isoPeaksMultin = isotopicDistributionCalculator.calcMultinomialIsoDist(AtomicSymbol.C.ordinal(),100);

        FullResIsotopicDistributionCalculator.Peak[] isoPeaksBinom = isotopicDistributionCalculator.calcBinomialIsoDist(AtomicSymbol.C.ordinal(), 100);


        for (int i=0;i<isoPeaksMultin.length;i++) {
            Assert.assertEquals(isoPeaksBinom[i].mz,isoPeaksMultin[i].mz,0.001);
            Assert.assertEquals(isoPeaksBinom[i].prob, isoPeaksMultin[i].prob, 0.001);
        }

    }


    @Test
    public void testMultiIsoDist() {

        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();
        PeakList<PeakAnnotation> peaks = isotopicDistributionCalculator.getIsotopicDistribution(Composition.parseComposition("O100"), 1, 10, 0.00001, 0.1);

        LowResIsotopicDistributionCalculator lowResIsotopicDistributionCalculator = new LowResIsotopicDistributionCalculator(10);
        double[] isoDist = lowResIsotopicDistributionCalculator.getIsotopicDistribution(Composition.parseComposition("O100"));

        for (int i=0;i<peaks.size();i++) {
            Assert.assertEquals(peaks.getIntensity(i),isoDist[i],0.01);
        }
    }



    @Test
    public void testPeptideIsoDist() {

        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();
        PeakList<PeakAnnotation> peaks = isotopicDistributionCalculator.getIsotopicDistribution(Composition.parseComposition("C40H72N12O13S2"), 0, 10, 0.0013, 0.2);

        LowResIsotopicDistributionCalculator lowResIsotopicDistributionCalculator = new LowResIsotopicDistributionCalculator(16);
        double[] isoDist = lowResIsotopicDistributionCalculator.getIsotopicDistribution(Composition.parseComposition("C40H72N12O13S2"));

        for (int i=0;i<peaks.size();i++) {
            Assert.assertEquals(peaks.getIntensity(i),isoDist[i],0.01);
            Assert.assertTrue(peaks.getIntensity(i) >= 0.0013);
        }

        // Nominal values taken from http://www.sisweb.com/mstools/isotope.htm
        // Differences are due to different isotopic data used in the two programs
        peaks = isotopicDistributionCalculator.getIsotopicDistribution(Composition.parseComposition("C40H72N12O13S2"), 0, 10, 0.01, 0.0000001);

        double massTol = 0.01;
        Assert.assertEquals(992.47839,peaks.getMz(0),massTol);
        Assert.assertEquals(993.47542,peaks.getMz(1),massTol);
        Assert.assertEquals(993.48175,peaks.getMz(2),massTol);
        Assert.assertEquals(994.47419,peaks.getMz(3),massTol);
        Assert.assertEquals(994.47878,peaks.getMz(4),massTol);
        Assert.assertEquals(994.48262,peaks.getMz(5),massTol);
        Assert.assertEquals(994.4851,peaks.getMz(6),massTol);
        Assert.assertEquals(995.47755,peaks.getMz(7),massTol);

        double probTol = 2.0;
        Assert.assertEquals(100,peaks.getIntensity(0)/peaks.getIntensity(0)*100,probTol);
        Assert.assertEquals(4,peaks.getIntensity(1)/peaks.getIntensity(0)*100,probTol);
        Assert.assertEquals(43,peaks.getIntensity(2)/peaks.getIntensity(0)*100,probTol);
        Assert.assertEquals(9,peaks.getIntensity(3)/peaks.getIntensity(0)*100,probTol);
        Assert.assertEquals(2,peaks.getIntensity(4)/peaks.getIntensity(0)*100,probTol);
        Assert.assertEquals(2,peaks.getIntensity(5)/peaks.getIntensity(0)*100,probTol);
        Assert.assertEquals(9,peaks.getIntensity(6)/peaks.getIntensity(0)*100,probTol);
        Assert.assertEquals(4,peaks.getIntensity(7)/peaks.getIntensity(0)*100,probTol);


        isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();
        peaks = isotopicDistributionCalculator.getIsotopicDistribution(Composition.parseComposition("C40H72N12O13S2"), 0, 10, 0.0000001, 0.000001);

        FullResIsotopicDistributionCalculator.Peak[] nominalPeaks = readPeptideIsoDist();

        int i = 0;
        int j = 0;
        int nrMatch = 0;
        while (i<peaks.size() && j<nominalPeaks.length) {
            double m = peaks.getMz(i);

            while (nominalPeaks[j].mz<m-massTol&& j<nominalPeaks.length-1) j++;
            if (nominalPeaks[j].mz-m<massTol && Math.abs(nominalPeaks[j].prob-peaks.getIntensity(i)/peaks.getIntensity(0)*100)<probTol) {
                nrMatch++;
            }
            i++;
        }

        Assert.assertTrue(1.0 *nrMatch/ peaks.size()>0.7);
        Assert.assertTrue(1.0 *nrMatch/ nominalPeaks.length>0.7);
    }

    @Test
    public void testPeptideIsoDist2() {
        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();
        PeakList<PeakAnnotation> peaks = isotopicDistributionCalculator.getIsotopicDistribution(Composition.parseComposition("C21H37N7O9"), 0, 10, 0.01, 0.0000001);

        Assert.assertEquals(5,peaks.size());
    }

    @Test
    public void testPeptideIsoDist3() {
        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();
        PeakList<PeakAnnotation> peaks = isotopicDistributionCalculator.getIsotopicDistribution(Composition.parseComposition("Sn"), 0, 20, 0.000001, 0.0000001);

//TIN        Sn(112) 111.904821 0.97 Sn(114) 113.902782 0.65 Sn(115) 114.903346 0.34 Sn(116) 115.901744 14.53 Sn(117) 116.902954 7.68 Sn(118) 117.901606 24.23 Sn(119) 118.903309 8.59
//           Sn(120) 119.902197 32.59 Sn(122) 121.903440 4.63 Sn(124) 123.905275 5.79

        Assert.assertEquals(10,peaks.size());
        Assert.assertEquals(111.904821,peaks.getMz(0),0.00001);
        Assert.assertEquals(113.902782,peaks.getMz(1),0.00001);
        Assert.assertEquals(114.903346,peaks.getMz(2),0.00001);
        Assert.assertEquals(115.901744,peaks.getMz(3),0.00001);
        Assert.assertEquals(116.902954,peaks.getMz(4),0.00001);
        Assert.assertEquals(117.901606,peaks.getMz(5),0.00001);
        Assert.assertEquals(118.903309,peaks.getMz(6),0.00001);
        Assert.assertEquals(119.902197,peaks.getMz(7),0.00001);
        Assert.assertEquals(121.903440,peaks.getMz(8),0.00001);
        Assert.assertEquals(123.905275,peaks.getMz(9),0.00001);

        Assert.assertEquals(0.97/100,peaks.getIntensity(0),0.00001);
        Assert.assertEquals(0.65/100,peaks.getIntensity(1),0.00001);
        Assert.assertEquals(0.34/100,peaks.getIntensity(2),0.00001);
        Assert.assertEquals(14.53/100,peaks.getIntensity(3),0.00001);
        Assert.assertEquals(7.68/100,peaks.getIntensity(4),0.00001);
        Assert.assertEquals(24.23/100,peaks.getIntensity(5),0.00001);
        Assert.assertEquals(8.59/100,peaks.getIntensity(6),0.00001);
        Assert.assertEquals(32.59/100,peaks.getIntensity(7),0.00001);
        Assert.assertEquals(4.63/100,peaks.getIntensity(8),0.00001);
        Assert.assertEquals(5.79/100,peaks.getIntensity(9),0.00001);
    }

    @Test
    public void testPeptideIsoDist4() {
        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();
        PeakList<PeakAnnotation> peaks = isotopicDistributionCalculator.getIsotopicDistribution(Composition.parseComposition("Sn100"), 0, 20, 0.000001, 0.0000001);

        Assert.assertEquals(0,peaks.size());
    }

    @Test
    public void testPeptideIsoDistSpeed() {

        FullResIsotopicDistributionCalculator isotopicDistributionCalculator = new FullResIsotopicDistributionCalculator();

        LowResIsotopicDistributionCalculator lowResIsotopicDistributionCalculator = new LowResIsotopicDistributionCalculator(4);

        Composition comp = Composition.parseComposition("C40H72N12O13S2");

        int nrTrials = 1000;
        long before = System.currentTimeMillis();
        for (int i=0;i<nrTrials;i++) {
            isotopicDistributionCalculator.getIsotopicDistribution(comp, 1, 10, 0.01, 0.1);
        }
        long after = System.currentTimeMillis();

        System.out.println("FullResIsotopicDistributionCalculator: "+(after-before));


        before = System.currentTimeMillis();
        for (int i=0;i<nrTrials;i++) {
            lowResIsotopicDistributionCalculator.getIsotopicDistribution(comp);
        }
        after = System.currentTimeMillis();

        System.out.println("LowResIsotopicDistributionCalculator: " + (after - before));
    }

    @Test
    public void testPowerSpeed() {

        int nrTrials = 100000;
        long before = System.currentTimeMillis();
        for (int i=0;i<nrTrials;i++) {
            FastMath.pow(0.9999, i % 30);
        }
        long after = System.currentTimeMillis();

        System.out.println("Math.pow: "+(after-before));


        before = System.currentTimeMillis();
        for (int i=0;i<nrTrials;i++) {
            double tmp = 1.0;
            for (int j=0;j<i%30;j++) tmp *= 0.9999;
        }
        after = System.currentTimeMillis();

        System.out.println("Direct: "+(after-before));
    }

    protected FullResIsotopicDistributionCalculator.Peak[] readCa5IsoDist() {
        String str = "199.81295 100\n" +
                "201.80898 3.3522\n" +
                "202.80913 0.722\n" +
                "203.80501 0.045\n" +
                "203.80585 10.7581\n" +
                "204.80516 0.0194\n" +
                "205.80104 0.0003\n" +
                "205.80188 0.2885\n" +
                "205.80405 0.0206\n" +
                "205.80531 0.0021\n" +
                "206.80119 0.0002\n" +
                "206.80203 0.0621\n" +
                "207.79791 0.0029\n" +
                "207.79875 0.4629\n" +
                "207.80008 0.0006\n" +
                "207.80289 0.9799\n" +
                "208.79806 0.0012\n" +
                "208.80023 0.0001\n" +
                "209.79478 0.0093\n" +
                "209.79695 0.0018\n" +
                "209.79821 0.0001\n" +
                "209.79892 0.0263\n" +
                "210.79493 0.002\n" +
                "210.79907 0.0057\n" +
                "211.79081 0.0001\n" +
                "211.79165 0.01\n" +
                "211.79495 0.0003\n" +
                "211.79579 0.0843\n" +
                "212.7951 0.0001\n" +
                "213.78768 0.0001\n" +
                "213.78985 0.0001\n" +
                "213.79182 0.0017\n" +
                "213.79399 0.0002\n" +
                "214.79197 0.0004\n" +
                "215.78455 0.0001\n" +
                "215.78869 0.0027\n" +
                "215.79283 0.0038\n" +
                "217.78886 0.0001\n" +
                "219.78573 0.0002\n";

        String[] lines = str.split("\\n");
        FullResIsotopicDistributionCalculator.Peak[] peaks = new FullResIsotopicDistributionCalculator.Peak[lines.length];

        for (int i=0;i<lines.length;i++) {
            String[] fields = lines[i].split("\\s");
            peaks[i] = new FullResIsotopicDistributionCalculator.Peak(Double.parseDouble(fields[0]),Double.parseDouble(fields[1]));
        }

        return peaks;
    }

    protected FullResIsotopicDistributionCalculator.Peak[] readPeptideIsoDist() {
        String str = "992.47839 100\n" +
                "993.47542 4.3356\n" +
                "993.47778 1.5786\n" +
                "993.48175 43.2629\n" +
                "993.4826 0.5213\n" +
                "993.48467 1.1521\n" +
                "994.47246 0.0862\n" +
                "994.47419 8.8613\n" +
                "994.47481 0.0684\n" +
                "994.47717 0.0062\n" +
                "994.47878 1.8757\n" +
                "994.47963 0.0226\n" +
                "994.48114 0.683\n" +
                "994.4817 0.05\n" +
                "994.48199 0.0082\n" +
                "994.48262 2.4058\n" +
                "994.48263 0.2005\n" +
                "994.48406 0.0182\n" +
                "994.4851 9.1244\n" +
                "994.48596 0.2255\n" +
                "994.48681 0.0013\n" +
                "994.48803 0.4984\n" +
                "994.48888 0.006\n" +
                "994.49094 0.0065\n" +
                "995.46949 0.001\n" +
                "995.47122 0.3842\n" +
                "995.47185 0.0014\n" +
                "995.47358 0.0699\n" +
                "995.4742 0.0003\n" +
                "995.47582 0.0373\n" +
                "995.47667 0.0004\n" +
                "995.47755 3.8337\n" +
                "995.47817 0.0296\n" +
                "995.4784 0.0462\n" +
                "995.47874 0.001\n" +
                "995.47902 0.0004\n" +
                "995.47965 0.1043\n" +
                "995.47966 0.0087\n" +
                "995.48047 0.1021\n" +
                "995.48053 0.0027\n" +
                "995.48109 0.0008\n" +
                "995.48201 0.038\n" +
                "995.48202 0.0032\n" +
                "995.48213 0.3956\n" +
                "995.48299 0.0098\n" +
                "995.48345 0.0001\n" +
                "995.48384 0.0001\n" +
                "995.48449 0.144\n" +
                "995.48506 0.0216\n" +
                "995.48535 0.0036\n" +
                "995.48591 0.0003\n" +
                "995.48598 1.0408\n" +
                "995.48599 0.0867\n" +
                "995.48683 0.0106\n" +
                "995.48684 0.0019\n" +
                "995.48742 0.0079\n" +
                "995.48797 0.0003\n" +
                "995.48827 0.0001\n" +
                "995.48846 1.25\n" +
                "995.4889 0.0277\n" +
                "995.48891 0.0023\n" +
                "995.48931 0.0476\n" +
                "995.49017 0.0005\n" +
                "995.49033 0.0001\n" +
                "995.49138 0.1051\n" +
                "995.49224 0.0026\n" +
                "995.4943 0.0028\n" +
                "996.46826 0.0076\n" +
                "996.46999 0.1963\n" +
                "996.47061 0.003\n" +
                "996.47285 0.0004\n" +
                "996.4734 0.0421\n" +
                "996.47458 0.1662\n" +
                "996.47521 0.0006\n" +
                "996.47543 0.002\n" +
                "996.47669 0.0021\n" +
                "996.4767 0.0002\n" +
                "996.47694 0.0303\n" +
                "996.4775 0.0044\n" +
                "996.47756 0.0001\n" +
                "996.47779 0.0004\n" +
                "996.47842 0.2132\n" +
                "996.47843 0.0178\n" +
                "996.47904 0.0016\n" +
                "996.47905 0.0001\n" +
                "996.47917 0.0079\n" +
                "996.47986 0.0008\n" +
                "996.48003 0.0002\n" +
                "996.4809 0.8085\n" +
                "996.4814 0.0001\n" +
                "996.48152 0.0062\n" +
                "996.48176 0.02\n" +
                "996.4821 0.0004\n" +
                "996.48238 0.0002\n" +
                "996.48261 0.0001\n" +
                "996.48301 0.0451\n" +
                "996.48302 0.0038\n" +
                "996.48383 0.0442\n" +
                "996.48386 0.0005\n" +
                "996.48387 0.0001\n" +
                "996.48388 0.0006\n" +
                "996.48445 0.0003\n" +
                "996.48468 0.0005\n" +
                "996.48537 0.0164\n" +
                "996.48538 0.0014\n" +
                "996.48549 0.0542\n" +
                "996.48593 0.0012\n" +
                "996.48594 0.0001\n" +
                "996.48622 0.0002\n" +
                "996.48634 0.0021\n" +
                "996.48674 0.0006\n" +
                "996.48687 0.0314\n" +
                "996.48785 0.0197\n" +
                "996.48829 0.0004\n" +
                "996.48841 0.0046\n" +
                "996.4887 0.0008\n" +
                "996.48927 0.0001\n" +
                "996.48933 0.2195\n" +
                "996.48934 0.0183\n" +
                "996.49019 0.0046\n" +
                "996.4902 0.0008\n" +
                "996.49077 0.0017\n" +
                "996.49133 0.0001\n" +
                "996.49181 0.1251\n" +
                "996.49226 0.012\n" +
                "996.49227 0.001\n" +
                "996.49267 0.0065\n" +
                "996.49311 0.0001\n" +
                "996.49352 0.0001\n" +
                "996.49474 0.0144\n" +
                "996.49517 0.0002\n" +
                "996.49559 0.0005\n" +
                "996.49765 0.0006\n" +
                "997.46529 0.0001\n" +
                "997.46702 0.0085\n" +
                "997.46765 0.0001\n" +
                "997.47043 0.0018\n" +
                "997.47162 0.0033\n" +
                "997.47279 0.0003\n" +
                "997.47335 0.0849\n" +
                "997.47397 0.0013\n" +
                "997.4742 0.001\n" +
                "997.47454 0.0001\n" +
                "997.47545 0.0092\n" +
                "997.47546 0.0008\n" +
                "997.4762 0.0001\n" +
                "997.47627 0.0023\n" +
                "997.47676 0.0182\n" +
                "997.47761 0.0002\n" +
                "997.47781 0.0017\n" +
                "997.47782 0.0001\n" +
                "997.47793 0.0351\n" +
                "997.47856 0.0001\n" +
                "997.47879 0.0009\n" +
                "997.47968 0.0005\n" +
                "997.48005 0.0009\n" +
                "997.48006 0.0001\n" +
                "997.48029 0.0064\n" +
                "997.48086 0.0019\n" +
                "997.48115 0.0002\n" +
                "997.48178 0.0922\n" +
                "997.48179 0.0077\n" +
                "997.4824 0.0007\n" +
                "997.48241 0.0001\n" +
                "997.48253 0.0011\n" +
                "997.48263 0.0009\n" +
                "997.48264 0.0002\n" +
                "997.48322 0.0003\n" +
                "997.4839 0.0014\n" +
                "997.48426 0.1108\n" +
                "997.4847 0.0025\n" +
                "997.48471 0.0002\n" +
                "997.48476 0.0001\n" +
                "997.48488 0.0009\n" +
                "997.48511 0.0042\n" +
                "997.48545 0.0001\n" +
                "997.48626 0.0005\n" +
                "997.48636 0.0095\n" +
                "997.48637 0.0008\n" +
                "997.48718 0.0093\n" +
                "997.48722 0.0002\n" +
                "997.48724 0.0001\n" +
                "997.4878 0.0001\n" +
                "997.48804 0.0002\n" +
                "997.48872 0.0035\n" +
                "997.48873 0.0003\n" +
                "997.48884 0.0054\n" +
                "997.48929 0.0005\n" +
                "997.48958 0.0001\n" +
                "997.4897 0.0003\n" +
                "997.4901 0.0003\n" +
                "997.49023 0.0136\n" +
                "997.49108 0.0001\n" +
                "997.4912 0.002\n" +
                "997.49165 0.0002\n" +
                "997.49177 0.0006\n" +
                "997.49206 0.0001\n" +
                "997.49269 0.0301\n" +
                "997.4927 0.0025\n" +
                "997.49315 0.0004\n" +
                "997.49354 0.001\n" +
                "997.49355 0.0002\n" +
                "997.49413 0.0002\n" +
                "997.49517 0.0097\n" +
                "997.49561 0.0025\n" +
                "997.49562 0.0002\n" +
                "997.49602 0.0007\n" +
                "997.49647 0.0001\n" +
                "997.49809 0.0014\n" +
                "997.49853 0.0001\n" +
                "997.49895 0.0001\n" +
                "997.50101 0.0001\n" +
                "998.46406 0.0002\n" +
                "998.4692 0.0019\n" +
                "998.47038 0.0037\n" +
                "998.47249 0.0002\n" +
                "998.4733 0.0001\n" +
                "998.47379 0.0008\n" +
                "998.47422 0.0047\n" +
                "998.47423 0.0004\n" +
                "998.47484 0.0001\n" +
                "998.47497 0.0007\n" +
                "998.47615 0.0001\n" +
                "998.4767 0.0179\n" +
                "998.47732 0.0003\n" +
                "998.47756 0.0004\n" +
                "998.47763 0.001\n" +
                "998.47764 0.0001\n" +
                "998.47881 0.004\n" +
                "998.47882 0.0003\n" +
                "998.47963 0.001\n" +
                "998.48011 0.0038\n" +
                "998.48097 0.0001\n" +
                "998.48117 0.0007\n" +
                "998.48118 0.0001\n" +
                "998.48129 0.0048\n" +
                "998.48173 0.0001\n" +
                "998.48214 0.0002\n" +
                "998.48267 0.0028\n" +
                "998.48304 0.0002\n" +
                "998.4834 0.0002\n" +
                "998.48365 0.0009\n" +
                "998.48421 0.0004\n" +
                "998.48513 0.0195\n" +
                "998.48514 0.0016\n" +
                "998.48575 0.0002\n" +
                "998.48588 0.0001\n" +
                "998.48599 0.0004\n" +
                "998.486 0.0001\n" +
                "998.48657 0.0001\n" +
                "998.48726 0.0006\n" +
                "998.48761 0.0111\n" +
                "998.48806 0.0011\n" +
                "998.48807 0.0001\n" +
                "998.48823 0.0001\n" +
                "998.48847 0.0006\n" +
                "998.48962 0.0002\n" +
                "998.48972 0.0013\n" +
                "998.48973 0.0001\n" +
                "998.49054 0.0013\n" +
                "998.49111 0.0002\n" +
                "998.49208 0.0005\n" +
                "998.4922 0.0004\n" +
                "998.49264 0.0001\n" +
                "998.49345 0.0001\n" +
                "998.49358 0.0029\n" +
                "998.49444 0.0001\n" +
                "998.49456 0.0002\n" +
                "998.49512 0.0001\n" +
                "998.49604 0.003\n" +
                "998.49605 0.0003\n" +
                "998.49651 0.0002\n" +
                "998.4969 0.0001\n" +
                "998.49852 0.0006\n" +
                "998.49897 0.0003\n" +
                "998.49938 0.0001\n" +
                "998.50145 0.0001\n" +
                "999.46623 0.0001\n" +
                "999.46742 0.0001\n" +
                "999.47125 0.0002\n" +
                "999.47256 0.0008\n" +
                "999.47373 0.0008\n" +
                "999.47585 0.0001\n" +
                "999.47714 0.0002\n" +
                "999.47758 0.002\n" +
                "999.47759 0.0002\n" +
                "999.47833 0.0001\n" +
                "999.4797 0.0001\n" +
                "999.48006 0.0025\n" +
                "999.4805 0.0001\n" +
                "999.48091 0.0001\n" +
                "999.48099 0.0004\n" +
                "999.48216 0.0008\n" +
                "999.48217 0.0001\n" +
                "999.48298 0.0002\n" +
                "999.48347 0.0005\n" +
                "999.48452 0.0002\n" +
                "999.48464 0.0005\n" +
                "999.48603 0.0012\n" +
                "999.487 0.0001\n" +
                "999.48757 0.0001\n" +
                "999.48849 0.0027\n" +
                "999.4885 0.0002\n" +
                "999.48934 0.0001\n" +
                "999.49061 0.0001\n" +
                "999.49097 0.0009\n" +
                "999.49141 0.0002\n" +
                "999.49182 0.0001\n" +
                "999.49307 0.0001\n" +
                "999.49389 0.0001\n" +
                "999.49447 0.0001\n" +
                "999.49694 0.0004\n" +
                "999.4994 0.0002\n" +
                "1000.47461 0.0001\n" +
                "1000.47591 0.0002\n" +
                "1000.47709 0.0001\n" +
                "1000.47847 0.0001\n" +
                "1000.48093 0.0004\n" +
                "1000.48306 0.0001\n" +
                "1000.48341 0.0002\n" +
                "1000.48434 0.0001\n" +
                "1000.48552 0.0001\n" +
                "1000.48682 0.0001\n" +
                "1000.48938 0.0003\n" +
                "1000.49184 0.0003\n" +
                "1000.49432 0.0001\n" +
                "1001.48429 0.0001\n";

        String[] lines = str.split("\\n");
        FullResIsotopicDistributionCalculator.Peak[] peaks = new FullResIsotopicDistributionCalculator.Peak[lines.length];

        for (int i=0;i<lines.length;i++) {
            String[] fields = lines[i].split("\\s");
            peaks[i] = new FullResIsotopicDistributionCalculator.Peak(Double.parseDouble(fields[0]),Double.parseDouble(fields[1]));
        }

        return peaks;
    }
}
