package org.expasy.mzjava.core.mol;

// # $RECIPE$ $NUMBER$ - Fetching Atoms and Molecules #
//
// ## $PROBLEM$ ##
// You want to create Atom and Molecule.
//
// ## $SOLUTION$ ##
//
// [Flyweight](http://www.oodesign.com/flyweight-pattern.html) objects *Atom*s are accessible through the [singleton](http://www.oodesign.com/singleton-pattern.html) class *PeriodicTable*.
// Molecules have to be built from the *Composition* class.

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class CreatingMoleculeRecipe {

// Getting main isotope atom given an *AtomicSymbol*
    @Test
    public void getAtomFromSymbol() {

        //<SNIP>
        PeriodicTable periodicTable = PeriodicTable.getInstance();

        Atom hydrogen = periodicTable.getAtom(AtomicSymbol.H);

        Assert.assertEquals(1, hydrogen.getMassNumber());
        //</SNIP>
    }

// Getting specific isotope given an *AtomicSymbol*
    @Test
    public void getAtom2() {

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        //<SNIP>
        Atom deuterium = periodicTable.getAtom(AtomicSymbol.H, 2);

        Assert.assertEquals(2, deuterium.getMassNumber());
        //</SNIP>
    }

// Getting *Atom* from a String
    @Test
    public void getAtomFromString2() {

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        //<SNIP>
        Atom hydrogen = periodicTable.getAtom(AtomicSymbol.valueOf("H"), 2);

        Assert.assertEquals(2, hydrogen.getMassNumber());
        //</SNIP>
    }

// *PeriodicTable* also provides static *Atom*s and masses that are the most used
    @Test
    public void getAtomFromPeriodicTable() {

        //<SNIP>
        Atom atom = PeriodicTable.C;
        Assert.assertEquals(PeriodicTable.C_MASS, atom.getMass(), 0.0001);
        //</SNIP>
    }

// Accessors from *Atom*s
    @Test
    public void otherAccessors() {

        Atom atom = PeriodicTable.C;

        //<SNIP>
        AtomicSymbol symbol = atom.getSymbol();
        Assert.assertEquals(AtomicSymbol.C, symbol);

        double abundance = atom.getAbundance();
        Assert.assertEquals(0.989, abundance, 0.001);

        double mass = atom.getMass();
        Assert.assertEquals(12, mass, 0.0001);
        //</SNIP>
    }

// Lastly *PeriodicTable* provides a method that fetch the list of isotopes from an *AtomicSymbol*
    @Test
    public void getAtoms() {

        //<SNIP>
        PeriodicTable periodicTable = PeriodicTable.getInstance();

        List<Atom> isotopes = periodicTable.getAtoms(AtomicSymbol.C);

        Assert.assertEquals(Arrays.asList(PeriodicTable.C, PeriodicTable.C13, periodicTable.getAtom(AtomicSymbol.valueOf("C"), 14)), isotopes);
        //</SNIP>
    }

// Creating *Composition* given atoms and an overall optional charge
    @Test
    public void newCompositionMass() {

        //<SNIP>
        Composition sulfateComp = new Composition.Builder(AtomicSymbol.S).add(AtomicSymbol.O, 4).charge(-2).build();

        // accessors
        Set<Atom> atoms = sulfateComp.getAtoms();
        Assert.assertTrue(atoms.contains(PeriodicTable.S));
        Assert.assertTrue(atoms.contains(PeriodicTable.O));

        Assert.assertEquals(4, sulfateComp.getCount(PeriodicTable.O));
        Assert.assertEquals(0, sulfateComp.getCount(PeriodicTable.C));

        double exactMass = sulfateComp.getMolecularMass();
        Assert.assertEquals(95.9528, exactMass, 0.0001);

        double avgMass = MassCalculator.calculateAverageMass(sulfateComp);
        Assert.assertEquals(96.0627, avgMass, 0.0001);
        //</SNIP>
    }

// Creating *Composition* from a string
    @Test
    public void newCompositionMass2() {

        //<SNIP>
        Composition sulfateComp = Composition.parseComposition("SO4(2-)");
        //</SNIP>

        double exactMass = sulfateComp.getMolecularMass();
        Assert.assertEquals(95.9528, exactMass, 0.0001);
    }

// ## $DISCUSSION$ ##
// *Composition* parses chemical formula expressions.
// An expression is composed of many atoms, each atom is represented by
// <ol>
// <li>a symbol</li>
// <li>an optional neutron number</li>
// <li>and an optional quantifier (can be negative)</li>
// </ol>

// An overall charge can be defined at the end of expression
// For example SO4(2-) or H[2]3O(+) are valid expressions.

// ## $RELATED$ ##

// See @org.expasy.mzjava.core.mol.AtomicCompositionParser@ to learn more about the @org.expasy.mzjava.core.mol.Composition@ format
}