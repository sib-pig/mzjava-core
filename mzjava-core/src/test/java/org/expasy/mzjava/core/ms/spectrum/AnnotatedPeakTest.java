/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrum;

import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class AnnotatedPeakTest {

    @Test
    public void testNoArgConstructor() throws Exception {

        AnnotatedPeak<MockPeakAnnotation> peak = new AnnotatedPeak<MockPeakAnnotation>();

        Assert.assertEquals(0.0, peak.getMz(), 0.00001);
        Assert.assertEquals(0.0, peak.getIntensity(), 0.00001);
        Assert.assertEquals(0, peak.getCharge());
        List<MockPeakAnnotation> annotations = peak.getAnnotations();
        Assert.assertEquals(true, annotations.isEmpty());
        Assert.assertEquals(0, peak.getAnnotationCount());
        Assert.assertEquals(false, peak.hasAnnotations());
    }

    @Test
    public void testArgConstructor() throws Exception {

        AnnotatedPeak<MockPeakAnnotation> peak = new AnnotatedPeak<MockPeakAnnotation>(3561.98, 56, 3,
                new MockPeakAnnotation(IonType.y, 2, 4),
                new MockPeakAnnotation(IonType.b, 2, 6));

        Assert.assertEquals(3561.98, peak.getMz(), 0.001);
        Assert.assertEquals(56, peak.getIntensity(), 0.0000001);
        Assert.assertEquals(3, peak.getCharge());

        Assert.assertEquals(true, peak.hasAnnotations());
        Assert.assertEquals(2, peak.getAnnotationCount());
        Assert.assertEquals(new MockPeakAnnotation(IonType.y, 2, 4),
                peak.getAnnotation(0));
        Assert.assertEquals(new MockPeakAnnotation(IonType.b, 2, 6),
                peak.getAnnotation(1));

        List<MockPeakAnnotation> annotations = peak.getAnnotations();
        Assert.assertEquals(2, annotations.size());
        Assert.assertEquals(new MockPeakAnnotation(IonType.y, 2,4),
                annotations.get(0));
        Assert.assertEquals(new MockPeakAnnotation(IonType.b, 2, 6),
                annotations.get(1));
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testAddException() throws Exception {

        AnnotatedPeak<MockPeakAnnotation> peak = new AnnotatedPeak<MockPeakAnnotation>(3561.98, 56, 3,
                new MockPeakAnnotation(IonType.y, 2, 4),
                new MockPeakAnnotation(IonType.b, 2, 6));

        peak.getAnnotations().add(new MockPeakAnnotation(IonType.z, 45, 7));
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testRemoveException() throws Exception {

        AnnotatedPeak<MockPeakAnnotation> peak = new AnnotatedPeak<MockPeakAnnotation>(3561.98, 56, 3,
                new MockPeakAnnotation(IonType.y, 2, 4),
                new MockPeakAnnotation(IonType.b, 2, 6));

        peak.getAnnotations().remove(new MockPeakAnnotation(IonType.z, 45, 7));
    }

    @Test
    public void testCopyConstructor() throws Exception {

        AnnotatedPeak<MockPeakAnnotation> peak = new AnnotatedPeak<MockPeakAnnotation>(3561.98, 56, 3,
                new MockPeakAnnotation(IonType.y, 2, 4),
                new MockPeakAnnotation(IonType.b, 2, 6));

        AnnotatedPeak<MockPeakAnnotation> peakCopy = new AnnotatedPeak<MockPeakAnnotation>(peak);

        Assert.assertEquals(3561.98, peakCopy.getMz(), 0.001);
        Assert.assertEquals(56, peakCopy.getIntensity(), 0.0000001);
        Assert.assertEquals(3, peakCopy.getCharge());

        Assert.assertEquals(true, peakCopy.hasAnnotations());
        Assert.assertEquals(2, peakCopy.getAnnotationCount());
        Assert.assertEquals(new MockPeakAnnotation(IonType.y, 2, 4),
                peakCopy.getAnnotation(0));
        Assert.assertEquals(new MockPeakAnnotation(IonType.b, 2, 6),
                peakCopy.getAnnotation(1));

        List<MockPeakAnnotation> annotations = peakCopy.getAnnotations();
        Assert.assertEquals(2, annotations.size());
        Assert.assertEquals(new MockPeakAnnotation(IonType.y, 2, 4),
                annotations.get(0));
        Assert.assertEquals(new MockPeakAnnotation(IonType.b, 2, 6),
                annotations.get(1));

        //Check that annotations were copied
        for(int i = 0; i < peak.getAnnotationCount(); i++) {

            Assert.assertNotSame(peak.getAnnotation(i), peakCopy.getAnnotation(i));
        }
    }

    @Test
    public void testAdd() throws Exception {

        AnnotatedPeak<MockPeakAnnotation> peak = new AnnotatedPeak<MockPeakAnnotation>();

        Assert.assertEquals(0, peak.getAnnotationCount());
        peak.add(new MockPeakAnnotation(IonType.i, 1, 1));

        Assert.assertEquals(1, peak.getAnnotationCount());
        Assert.assertEquals(new MockPeakAnnotation(IonType.i, 1, 1), peak.getAnnotation(0));
    }

    @Test
    public void testRemoveByObject() throws Exception {

        MockPeakAnnotation aIon = new MockPeakAnnotation(IonType.a, 1, 2);
        AnnotatedPeak<MockPeakAnnotation> peak = new AnnotatedPeak<MockPeakAnnotation>(576.3, 12, 2,
                new MockPeakAnnotation(IonType.i, 1, 1),
                aIon,
                new MockPeakAnnotation(IonType.x, 1, 1)
                );

        Assert.assertEquals(3, peak.getAnnotationCount());

        boolean removed = peak.remove(aIon);
        Assert.assertEquals(true, removed);
        Assert.assertEquals(2, peak.getAnnotationCount());
        Assert.assertEquals(new MockPeakAnnotation(IonType.i, 1, 1), peak.getAnnotation(0));
        Assert.assertEquals(new MockPeakAnnotation(IonType.x, 1, 1), peak.getAnnotation(1));
    }

    @Test
    public void testRemoveByIndex() throws Exception {

        MockPeakAnnotation aIon = new MockPeakAnnotation(IonType.a, 1, 2);
        AnnotatedPeak<MockPeakAnnotation> peak = new AnnotatedPeak<MockPeakAnnotation>(576.3, 12, 2,
                new MockPeakAnnotation(IonType.i, 1, 1),
                aIon,
                new MockPeakAnnotation(IonType.x, 1, 2)
        );

        Assert.assertEquals(3, peak.getAnnotationCount());

        MockPeakAnnotation removed = peak.remove(1);
        Assert.assertSame(aIon, removed);
        Assert.assertEquals(2, peak.getAnnotationCount());
        Assert.assertEquals(new MockPeakAnnotation(IonType.i, 1, 1), peak.getAnnotation(0));
        Assert.assertEquals(new MockPeakAnnotation(IonType.x, 1, 2), peak.getAnnotation(1));
    }

    @Test
    public void testSortAnnotations() throws Exception {

        AnnotatedPeak<MockPeakAnnotation> peak = new AnnotatedPeak<MockPeakAnnotation>(3561.98, 56, 3,
                new MockPeakAnnotation(IonType.y, 2, 4),
                new MockPeakAnnotation(IonType.b, 2, 6));

        Assert.assertEquals(new MockPeakAnnotation(IonType.y, 2, 4),
                peak.getAnnotation(0));
        Assert.assertEquals(new MockPeakAnnotation(IonType.b, 2, 6),
                peak.getAnnotation(1));

        peak.sortAnnotations(new Comparator<MockPeakAnnotation>() {
            @Override
            public int compare(MockPeakAnnotation o1, MockPeakAnnotation o2) {

                return o1.getIonType().toString().compareTo(o2.getIonType().toString());
            }
        });
        Assert.assertEquals(new MockPeakAnnotation(IonType.b, 2, 6),
                peak.getAnnotation(0));
        Assert.assertEquals(new MockPeakAnnotation(IonType.y, 2, 4),
                peak.getAnnotation(1));

    }

    @Test
    public void testCopy() throws Exception {

        AnnotatedPeak<MockPeakAnnotation> peak = new AnnotatedPeak<MockPeakAnnotation>(3561.98, 56, 3,
                new MockPeakAnnotation(IonType.y, 2, 4),
                new MockPeakAnnotation(IonType.b, 2, 6));

        AnnotatedPeak<MockPeakAnnotation> peakCopy = peak.copy();

        Assert.assertEquals(3561.98, peakCopy.getMz(), 0.001);
        Assert.assertEquals(56, peakCopy.getIntensity(), 0.0000001);
        Assert.assertEquals(3, peakCopy.getCharge());

        Assert.assertEquals(true, peakCopy.hasAnnotations());
        Assert.assertEquals(2, peakCopy.getAnnotationCount());
        Assert.assertEquals(new MockPeakAnnotation(IonType.y, 2, 4),
                peakCopy.getAnnotation(0));
        Assert.assertEquals(new MockPeakAnnotation(IonType.b, 2, 6),
                peakCopy.getAnnotation(1));

        List<MockPeakAnnotation> annotations = peakCopy.getAnnotations();
        Assert.assertEquals(2, annotations.size());
        Assert.assertEquals(new MockPeakAnnotation(IonType.y, 2, 4),
                annotations.get(0));
        Assert.assertEquals(new MockPeakAnnotation(IonType.b, 2, 6),
                annotations.get(1));

        //Check that annotations were copied
        for(int i = 0; i < peak.getAnnotationCount(); i++) {

            Assert.assertNotSame(peak.getAnnotation(i), peakCopy.getAnnotation(i));
        }
    }
}
