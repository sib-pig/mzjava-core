package org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.transformer;

import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.MockPeakPairSink;
import org.junit.Assert;
import org.junit.Test;
import java.util.Collections;
import java.util.List;

import static java.lang.Math.log;

/**
 * @author Aivett Bilbao
 * @version 1.0
 */
public class PeakPairIntensityLogETransformerTest {

    @Test
    public void testProcessPeakPair() throws Exception {

        PeakPairIntensityLogETransformer<PeakAnnotation, PeakAnnotation> processor = new PeakPairIntensityLogETransformer<PeakAnnotation, PeakAnnotation>();

        MockPeakPairSink<PeakAnnotation> sink = new MockPeakPairSink<PeakAnnotation>();
        processor.setSink(sink);
        List<PeakAnnotation> empty = Collections.emptyList();

        processor.begin(null, null);

        processor.processPeakPair(1.1, 4, 14, empty, empty);
        processor.processPeakPair(1.2, 44, 3, empty, empty);
        processor.processPeakPair(1.3, 12, 8, empty, empty);
        processor.processPeakPair(1.4, 77, 2, empty, empty);

        processor.end();

        Assert.assertArrayEquals(new double[]{1.1, 1.2, 1.3, 1.4}, sink.getCentroids(), 0.00000001);
        Assert.assertArrayEquals(new double[]{log(4 + 1), log(44 + 1), log(12 + 1), log(77 + 1)}, sink.getXvalues(), 0.00000001);
        Assert.assertArrayEquals(new double[]{log(14 + 1), log(3 + 1), log(8 + 1), log(2 + 1)}, sink.getYvalues(), 0.00000001);

        Assert.assertTrue(sink.wasBeginCalled());
        Assert.assertTrue(sink.wasEndCalled());
    }

    @Test
    public void testProcessPeakPairUndefLogE() throws Exception {

        PeakPairIntensityLogETransformer<PeakAnnotation, PeakAnnotation> processor = new PeakPairIntensityLogETransformer<PeakAnnotation, PeakAnnotation>();
        MockPeakPairSink<PeakAnnotation> sink = new MockPeakPairSink<PeakAnnotation>();
        processor.setSink(sink);
        List<PeakAnnotation> empty = Collections.emptyList();

        processor.begin(null, null);

        processor.processPeakPair(1.1, -10, Math.E, empty, empty);
        processor.processPeakPair(50.1, -10, 0, empty, empty);

        Assert.assertArrayEquals(new double[]{0, 0}, sink.getXvalues(), 0.00000001);
        Assert.assertArrayEquals(new double[]{log(Math.E + 1), log(1)}, sink.getYvalues(), 0.00000001);
    }
}