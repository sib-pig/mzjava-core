/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.io.ms.spectrum;

import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.RetentionTime;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeList;
import org.expasy.mzjava.core.ms.spectrum.*;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Tests that all the spectra in mgf_test2.mgf are read correctly
 *
 * @author Oliver Horlacher
 * @version 1.0
 */
public class MgfReaderTest3 {


    private final List<MsnSpectrum> spectra = new ArrayList<MsnSpectrum>();

    @Before
    public void setUp() throws IOException {

        MgfReader reader = new MgfReader(new File(getClass().getResource("mgf_test2.mgf").getFile()), PeakList.Precision.FLOAT);

//        reader.addTitleParser(new TitleParser1());
//        reader.addTitleParser(new TitleParser2());
//        reader.addTitleParser(new TitleParser3());
//        reader.addTitleParser(new TitleParser4());

        while (reader.hasNext()) {

            spectra.add(reader.next());
        }
    }

    @Test
    public void test0() throws Exception {

        MsnSpectrum spectrum = spectra.get(0);

        assertEquals("2008_12_11_ALS_31_LY01_J20.1001.1001.3.dta", spectrum.getComment());
        checkScanNumbers(spectrum.getScanNumbers(), 1001);
        checkRetentionTimes(spectrum.getRetentionTimes());
        assertArrayEquals(new int[]{3}, spectrum.getPrecursor().getChargeList());
        assertEquals(420.876707333333, spectrum.getPrecursor().getMz(), 0.0000000001);

        double delta = 0.000001;
        assertEquals(116.4783935546875, spectrum.getMz(0), delta);
        assertEquals(828.9000244140625, spectrum.getIntensity(0), delta);
        assertEquals(120.76241302490234, spectrum.getMz(1), delta);
        assertEquals(869.7999877929688, spectrum.getIntensity(1), delta);
    }

    @Test
    public void test1() throws Exception {

        MsnSpectrum spectrum = spectra.get(1);

        assertEquals("scan number 799", spectrum.getComment());
        checkScanNumbers(spectrum.getScanNumbers(), 799);
        checkRetentionTimes(spectrum.getRetentionTimes());
        assertArrayEquals(new int[]{2}, spectrum.getPrecursor().getChargeList());
        assertEquals(749.7925, spectrum.getPrecursor().getMz(), 0.00001);

        double delta = 0.000001;
        assertEquals(268.1630859375, spectrum.getMz(0), delta);
        assertEquals(847.0, spectrum.getIntensity(0), delta);
        assertEquals(280.7532958984375, spectrum.getMz(1), delta);
        assertEquals(785.0, spectrum.getIntensity(1), delta);
    }

    @Test
    public void test2() throws Exception {

        MsnSpectrum spectrum = spectra.get(2);

        assertEquals("ADLMLYVSK/2", spectrum.getComment());
        checkScanNumbers(spectrum.getScanNumbers());
        checkRetentionTimes(spectrum.getRetentionTimes());
        assertArrayEquals(new int[]{2}, spectrum.getPrecursor().getChargeList());
        assertEquals(1040.563, spectrum.getPrecursor().getMz(), 0.00001);

        double delta = 0.000001;
        assertEquals(158.8000030517578, spectrum.getMz(0), delta);
        assertEquals(206.0, spectrum.getIntensity(0), delta);
        assertEquals(186.8000030517578, spectrum.getMz(1), delta);
        assertEquals(800.0, spectrum.getIntensity(1), delta);
        assertEquals(201.1999969482422, spectrum.getMz(2), delta);
        assertEquals(160.0, spectrum.getIntensity(2), delta);
    }

    @Test
    public void test3() throws Exception {

        MsnSpectrum spectrum = spectra.get(3);

        assertEquals("B06-1151_p.00478.00478.2", spectrum.getComment());
        checkScanNumbers(spectrum.getScanNumbers(), 478);
        checkRetentionTimes(spectrum.getRetentionTimes());
        assertArrayEquals(new int[]{2}, spectrum.getPrecursor().getChargeList());
        assertEquals(476.23272705078125, spectrum.getPrecursor().getMz(), 0.00001);

        double delta = 0.000001;
        assertEquals(135.03199768066406, spectrum.getMz(0), delta);
        assertEquals(1.3760000467300415, spectrum.getIntensity(0), delta);
        assertEquals(146.04200744628906, spectrum.getMz(1), delta);
        assertEquals(5.997000217437744, spectrum.getIntensity(1), delta);
    }

    @Test
    public void test4() throws Exception {

        MsnSpectrum spectrum = spectra.get(4);

        assertEquals("1: Scan 5", spectrum.getComment());
        checkScanNumbers(spectrum.getScanNumbers(), 5);
        checkRetentionTimes(spectrum.getRetentionTimes(), 305.748);
        assertArrayEquals(new int[]{2}, spectrum.getPrecursor().getChargeList());
        assertEquals(455.33357, spectrum.getPrecursor().getMz(), 0.00001);
        assertEquals(23793.477, spectrum.getPrecursor().getIntensity(), 0.00001);

        double delta = 0.000001;
        assertEquals(137.13076782226562, spectrum.getMz(0), delta);
        assertEquals(95.81723022460938, spectrum.getIntensity(0), delta);
        assertEquals(140.9105682373047, spectrum.getMz(1), delta);
        assertEquals(184.6490936279297, spectrum.getIntensity(1), delta);
    }

    @Test
    public void test5() throws Exception {

        MsnSpectrum spectrum = spectra.get(5);

        assertEquals("57: Sum of 2 scans in range 245", spectrum.getComment());
        checkScanNumbers(spectrum.getScanNumbers(), 245, 248);
        checkRetentionTimes(spectrum.getRetentionTimes(), 587.6048, 590.7181);
        assertArrayEquals(new int[]{2}, spectrum.getPrecursor().getChargeList());
        assertEquals(401.72495, spectrum.getPrecursor().getMz(), 0.00001);
        assertEquals(468515.31, spectrum.getPrecursor().getIntensity(), 0.01);

        double delta = 0.000001;
        assertEquals(128.8025360107422, spectrum.getMz(0), delta);
        assertEquals(51434.48046875, spectrum.getIntensity(0), delta);
        assertEquals(129.63063049316406, spectrum.getMz(1), delta);
        assertEquals(157186.9375, spectrum.getIntensity(1), delta);
    }

    @Test
    public void test6() throws Exception {

        MsnSpectrum spectrum = spectra.get(6);

        assertEquals("57: Sum of 2 scans in range 245", spectrum.getComment());
        checkScanNumbers(spectrum.getScanNumbers(), new int[]{245}, new int[]{248});
        checkRetentionTimes(spectrum.getRetentionTimes(), 587.6048, 590.7181);
        assertArrayEquals(new int[]{2}, spectrum.getPrecursor().getChargeList());
        assertEquals(401.72495, spectrum.getPrecursor().getMz(), 0.00001);
        assertEquals(468515.31, spectrum.getPrecursor().getIntensity(), 0.01);

        double delta = 0.000001;
        assertEquals(128.8025360107422, spectrum.getMz(0), delta);
        assertEquals(51434.48046875, spectrum.getIntensity(0), delta);
        assertEquals(129.63063049316406, spectrum.getMz(1), delta);
        assertEquals(157186.9375, spectrum.getIntensity(1), delta);
    }

    @Test
    public void test7() throws Exception {

        MsnSpectrum spectrum = spectra.get(7);

        assertEquals("22927: Scan rt=4669.74 from file [2]", spectrum.getComment());
        checkScanNumbers(spectrum.getScanNumbers(), 48055);
        checkRetentionTimes(spectrum.getRetentionTimes(), 4669.736);
        assertArrayEquals(new int[]{2}, spectrum.getPrecursor().getChargeList());
        assertEquals(797.36086, spectrum.getPrecursor().getMz(), 0.00001);
        assertEquals(89994.258, spectrum.getPrecursor().getIntensity(), 0.001);

        double delta = 0.000001;
        assertEquals(227.05462646484375, spectrum.getMz(0), delta);
        assertEquals(199.5477294921875, spectrum.getIntensity(0), delta);
        assertEquals(242.21568298339844, spectrum.getMz(1), delta);
        assertEquals(120.42233276367188, spectrum.getIntensity(1), delta);
    }

    @Test
    public void test8() throws Exception {

        MsnSpectrum spectrum = spectra.get(8);

        assertEquals("953: Scan 2453", spectrum.getComment());
        checkScanNumbers(spectrum.getScanNumbers(), 2453);
        checkRetentionTimes(spectrum.getRetentionTimes(), 2602.8316);
        assertArrayEquals(new int[]{2, 3}, spectrum.getPrecursor().getChargeList());
        assertEquals(565.88, spectrum.getPrecursor().getMz(), 0.00001);
        assertEquals(19193.301, spectrum.getPrecursor().getIntensity(), 0.001);

        double delta = 0.000001;
        assertEquals(173.23782348632812, spectrum.getMz(0), delta);
        assertEquals(480.6231384277344, spectrum.getIntensity(0), delta);
        assertEquals(181.7628631591797, spectrum.getMz(1), delta);
        assertEquals(107.39108276367188, spectrum.getIntensity(1), delta);
    }

    @Test
    public void test9() throws Exception {

        MsnSpectrum spectrum = spectra.get(9);

        assertEquals("File:A12-07003.mzXML Scans:2 RT:0.0160min Charge:3+ Fragmentation:cid", spectrum.getComment());
        checkScanNumbers(spectrum.getScanNumbers(), 2);
        checkRetentionTimes(spectrum.getRetentionTimes(), 0.9616);
        assertArrayEquals(new int[]{3}, spectrum.getPrecursor().getChargeList());
        assertEquals(505.8808, spectrum.getPrecursor().getMz(), 0.00001);

        double delta = 0.000001;
        assertEquals(144.18130493164062, spectrum.getMz(0), delta);
        assertEquals(5.699999809265137, spectrum.getIntensity(0), delta);
        assertEquals(150.16830444335938, spectrum.getMz(1), delta);
        assertEquals(18.850000381469727, spectrum.getIntensity(1), delta);
    }

    @Test
    public void test10() throws Exception {

        MsnSpectrum spectrum = spectra.get(10);

        assertEquals("20120209_02_NGlycoFASP.00173.00173.2", spectrum.getComment());
        checkScanNumbers(spectrum.getScanNumbers(), 173);
        checkRetentionTimes(spectrum.getRetentionTimes(), 150.10);
        assertArrayEquals(new int[]{2}, spectrum.getPrecursor().getChargeList());
        assertEquals(455.198792, spectrum.getPrecursor().getMz(), 0.00001);
        assertEquals(1897.9000, spectrum.getPrecursor().getIntensity(), 0.0001);

        double delta = 0.000001;
        assertEquals(129.2469024658203, spectrum.getMz(0), delta);
        assertEquals(9.0, spectrum.getIntensity(0), delta);
        assertEquals(129.960205078125, spectrum.getMz(1), delta);
        assertEquals(11.0, spectrum.getIntensity(1), delta);
    }

    @Test
    public void test11() throws Exception {

        MsnSpectrum spectrum = spectra.get(11);

        assertEquals("msmsid:F000005,quan:0.000,start:0.021,end:0.021,survey:S000004,parent:489.540000,parent_area:18.1315,Qstart:0.021,Qend:0.021,AnalTime:111,Activation:cid", spectrum.getComment());
        checkScanNumbers(spectrum.getScanNumbers(), 4);
        checkRetentionTimes(spectrum.getRetentionTimes(), 0.021);
        assertArrayEquals(new int[]{2}, spectrum.getPrecursor().getChargeList());
        assertEquals(489.4510562, spectrum.getPrecursor().getMz(), 0.00001);
        assertEquals(18.1315, spectrum.getPrecursor().getIntensity(), 0.0001);

        double delta = 0.000001;
        assertEquals(156.12789916992188, spectrum.getMz(0), delta);
        assertEquals(2.139400005340576, spectrum.getIntensity(0), delta);
        assertEquals(181.93634033203125, spectrum.getMz(1), delta);
        assertEquals(1.2020000219345093, spectrum.getIntensity(1), delta);
    }

    @Test
    public void test12() throws Exception {

        MsnSpectrum spectrum = spectra.get(12);

        assertEquals("File1023 Spectrum1 scans: 1178", spectrum.getComment());
        checkScanNumbers(spectrum.getScanNumbers(), 1178);
        checkRetentionTimes(spectrum.getRetentionTimes(), 391);
        assertArrayEquals(new int[]{2}, spectrum.getPrecursor().getChargeList());
        assertEquals(461.76498, spectrum.getPrecursor().getMz(), 0.00001);
        assertEquals(941599.25000, spectrum.getPrecursor().getIntensity(), 0.00001);

        double delta = 0.000001;
        assertEquals(70.06523132324219, spectrum.getMz(0), delta);
        assertEquals(21397.0, spectrum.getIntensity(0), delta);
        assertEquals(73.0469970703125, spectrum.getMz(1), delta);
        assertEquals(2051.6201171875, spectrum.getIntensity(1), delta);
    }

    private void checkScanNumbers(ScanNumberList actualScanNumberList, int... expectedScanNumbers) {

        assertEquals(expectedScanNumbers.length, actualScanNumberList.size());

        for (int i = 0; i < expectedScanNumbers.length; i++) {

            int expectedScanNumber = expectedScanNumbers[i];
            ScanNumber scanNumber = actualScanNumberList.get(i);

            assertEquals(expectedScanNumber, scanNumber.getMinScanNumber());
            assertEquals(expectedScanNumber, scanNumber.getMaxScanNumber());
        }
    }

    private void checkScanNumbers(ScanNumberList actualScanNumberList, int[] expectedMinScanNumbers, int[] expectedMaxScanNumbers) {

        assertEquals(expectedMinScanNumbers.length, actualScanNumberList.size());
        assertEquals(expectedMaxScanNumbers.length, actualScanNumberList.size());

        for (int i = 0; i < expectedMinScanNumbers.length; i++) {

            ScanNumber scanNumber = actualScanNumberList.get(i);

            assertEquals(expectedMinScanNumbers[i], scanNumber.getMinScanNumber());
            assertEquals(expectedMaxScanNumbers[i], scanNumber.getMaxScanNumber());
        }
    }

    private void checkRetentionTimes(RetentionTimeList actualRetentionTimes, double... expectedRetentionTimes) {

        assertEquals(expectedRetentionTimes.length, actualRetentionTimes.size());

        for (int i = 0; i < expectedRetentionTimes.length; i++) {

            double expectedRetentionTime = expectedRetentionTimes[i];
            RetentionTime actualRetentionTime = actualRetentionTimes.get(i);

            assertEquals(expectedRetentionTime, actualRetentionTime.getMinRetentionTime(), 0.00000001);
            assertEquals(expectedRetentionTime, actualRetentionTime.getMaxRetentionTime(), 0.00000001);
        }
    }
}
