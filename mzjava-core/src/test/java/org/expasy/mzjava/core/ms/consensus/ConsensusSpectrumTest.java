package org.expasy.mzjava.core.ms.consensus;

import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.AbstractMergePeakFilter;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.IdentityPeakProcessor;
import org.expasy.mzjava.core.ms.spectrum.LibPeakAnnotation;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class ConsensusSpectrumTest {

    @Test
    public void testConsensusSpectrumCreation() {
        Set<UUID> memberIds = new HashSet<>();
        memberIds.add(UUID.randomUUID());
        memberIds.add(UUID.randomUUID());
        memberIds.add(UUID.randomUUID());

        ConsensusSpectrum<LibPeakAnnotation> consensus = new ConsensusSpectrum<>(100, PeakList.Precision.DOUBLE,memberIds);
        consensus.add(100.0, 10.0, new LibPeakAnnotation(1,1.0,1.0));
        Assert.assertEquals(10, consensus.getTotalIonCurrent(), 0.0);
        consensus.add(100.0, 10.0, new LibPeakAnnotation(1,1.0,1.0));
        Assert.assertEquals(20, consensus.getTotalIonCurrent(), 0.0);
        consensus.add(200.0, 12.0, new LibPeakAnnotation(2,2.0,2.0));
        Assert.assertEquals(32, consensus.getTotalIonCurrent(), 0.0);
        consensus.add(300.0, 13.0, new LibPeakAnnotation(3,3.0,3.0));
        Assert.assertEquals(45, consensus.getTotalIonCurrent(), 0.0);

        consensus.setPrecursor(new Peak(500.0,10.0,2));
        consensus.setPrecursorStats(100.0,1.0);

        consensus.setName("test");

        ConsensusSpectrum<LibPeakAnnotation> copy = consensus.copy(new IdentityPeakProcessor<LibPeakAnnotation>());

        Assert.assertTrue(consensus.equals(copy));

        Set<UUID> memberIds2 = new HashSet<>();
        memberIds.add(UUID.randomUUID());
        memberIds.add(UUID.randomUUID());

        ConsensusSpectrum<LibPeakAnnotation> consensus2 = new ConsensusSpectrum<>(100, PeakList.Precision.DOUBLE,memberIds2);
        consensus.add(100.0, 10.0, new LibPeakAnnotation(1,1.0,1.0));
        consensus.add(100.0, 10.0, new LibPeakAnnotation(1,1.0,1.0));
        consensus.add(200.0, 12.0, new LibPeakAnnotation(2,2.0,2.0));
        consensus.add(300.0, 13.0, new LibPeakAnnotation(3,3.0,3.0));

        consensus.setPrecursor(new Peak(500.0,10.0,2));
        consensus.setPrecursorStats(100.0,1.0);
        consensus.setName("test");

        Assert.assertFalse(consensus.equals(consensus2));

        ConsensusSpectrum<LibPeakAnnotation> consensus3 = new ConsensusSpectrum<>(100, PeakList.Precision.DOUBLE,memberIds);
        consensus.add(100.0, 10.0, new LibPeakAnnotation(1,1.0,1.0));
        consensus.add(100.0, 10.0, new LibPeakAnnotation(1,1.0,1.0));
        consensus.add(200.0, 12.0, new LibPeakAnnotation(2,2.0,2.0));
        consensus.add(300.0, 13.0, new LibPeakAnnotation(3,3.0,3.0));

        // different precursor charge
        consensus.setPrecursor(new Peak(500.0,10.0,3));
        consensus.setPrecursorStats(100.0,1.0);
        consensus.setName("test");

        Assert.assertFalse(consensus.equals(consensus3));

        ConsensusSpectrum<LibPeakAnnotation> consensus4 = new ConsensusSpectrum<>(100, PeakList.Precision.DOUBLE,memberIds);
        consensus.add(100.0, 10.0, new LibPeakAnnotation(1,1.0,1.0));
        consensus.add(100.0, 10.0, new LibPeakAnnotation(1,1.0,1.0));
        consensus.add(200.0, 12.0, new LibPeakAnnotation(2,2.0,2.0));
        consensus.add(300.0, 13.0, new LibPeakAnnotation(3,3.0,3.0));

        // different precursor mz
        consensus.setPrecursor(new Peak(501.0,10.0,2));
        consensus.setPrecursorStats(100.0,1.0);
        consensus.setName("test");

        Assert.assertFalse(consensus.equals(consensus4));

        ConsensusSpectrum<LibPeakAnnotation> consensus5 = new ConsensusSpectrum<>(100, PeakList.Precision.DOUBLE,memberIds);
        consensus.add(100.0, 10.0, new LibPeakAnnotation(1,1.0,1.0));
        consensus.add(100.0, 10.0, new LibPeakAnnotation(1,1.0,1.0));
        consensus.add(201.0, 12.0, new LibPeakAnnotation(2,2.0,2.0));
        consensus.add(300.0, 14.0, new LibPeakAnnotation(3,3.0,3.0));

        // different precursor mz
        consensus.setPrecursor(new Peak(500.0,10.0,2));
        consensus.setPrecursorStats(100.0,1.0);
        consensus.setName("test");

        Assert.assertFalse(consensus.equals(consensus5));

    }

    @Test (expected = UnsupportedOperationException.class)
    public void memberIdsImmutableTest() {
        Set<UUID> memberIds = new HashSet<>();
        memberIds.add(UUID.randomUUID());
        memberIds.add(UUID.randomUUID());
        memberIds.add(UUID.randomUUID());

        ConsensusSpectrum<LibPeakAnnotation> consensus = new ConsensusSpectrum<>(100, PeakList.Precision.DOUBLE,memberIds);

        consensus.getMemberIds().add(UUID.randomUUID());
    }

    @Test
    public void testConsensusBuilder1()
    {
        Set<MsnSpectrum> spectra = new HashSet<>();

        MsnSpectrum spectrum = new MsnSpectrum();
        spectrum.add(100.0, 10.0);
        spectrum.add(200.0, 12.0);
        spectrum.add(300.0, 13.0);
        spectrum.setPrecursor(new Peak(1000.0,10.0,1));
        spectra.add(spectrum);

        MsnSpectrum spectrum2 = new MsnSpectrum();
        spectrum2.add(100.0, 10.0);
        spectrum2.add(200.0, 12.0);
        spectrum2.add(500.0, 13.0);
        spectrum2.setPrecursor(new Peak(1001.0, 11.0, 1));
        spectra.add(spectrum2);

        MsnSpectrum spectrum3 = new MsnSpectrum();
        spectrum3.add(100.0, 10.0);
        spectrum3.add(200.0, 12.0);
        spectrum3.add(500.0, 13.0);
        spectrum3.add(600.0, 13.0);
        spectrum3.setPrecursor(new Peak(1002.0, 12.0, 1));
        spectra.add(spectrum3);

        ConsensusSpectrum consensusSpectrum =
                ConsensusSpectrum.Builder.getBuilder().
                        spectra(spectra).setPeakFilterParams(0.2, 2).
                        fragMzTolerance(0.2).
                        intensityCombMethod(AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                        build();

        Assert.assertEquals(consensusSpectrum.size(),3);

        consensusSpectrum =
                ConsensusSpectrum.Builder.getBuilder().
                        spectra(spectra).setPeakFilterParams(0.0, 1).
                        fragMzTolerance(0.2).
                        intensityCombMethod(AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                        build();

        Assert.assertEquals(consensusSpectrum.size(),5);

        Assert.assertEquals(consensusSpectrum.getMemberIds().size(),3);
        Assert.assertTrue(consensusSpectrum.getMemberIds().contains(spectrum.getId()));
        Assert.assertTrue(consensusSpectrum.getMemberIds().contains(spectrum2.getId()));
        Assert.assertTrue(consensusSpectrum.getMemberIds().contains(spectrum3.getId()));

        Assert.assertEquals(consensusSpectrum.getPrecursor().getMz(),consensusSpectrum.getPrecursorMzMean(),0.0001);
        Assert.assertEquals(consensusSpectrum.getPrecursor().getMz(),1001.0,0.0001);
        Assert.assertEquals(consensusSpectrum.getPrecursor().getIntensity(),11.0,0.0001);
        double mean = consensusSpectrum.getPrecursorMzMean();
        double std = (1000.0-mean)*(1000.0-mean)+(1001.0-mean)*(1001.0-mean)+(1002.0-mean)*(1002.0-mean);
        std = Math.sqrt(std/2);
        Assert.assertEquals(consensusSpectrum.getPrecursorMzStdev(),std,0.0001);

    }

    @Test
    public void testConsensusSpectrumEquals()
    {
        Set<MsnSpectrum> spectra1 = new HashSet<>();

        MsnSpectrum spectrum1 = new MsnSpectrum();
        spectrum1.add(100.0, 10.0);
        spectrum1.add(200.0, 12.0);
        spectrum1.add(300.0, 13.0);
        spectrum1.setPrecursor(new Peak(1000.0,10.0,1));
        spectra1.add(spectrum1);

        MsnSpectrum spectrum2 = new MsnSpectrum();
        spectrum2.add(100.0, 10.0);
        spectrum2.add(200.0, 12.0);
        spectrum2.add(500.0, 13.0);
        spectrum2.setPrecursor(new Peak(1001.0, 11.0, 1));
        spectra1.add(spectrum2);

        Set<MsnSpectrum> spectra2 = new HashSet<>();

        MsnSpectrum spectrum3 = new MsnSpectrum();
        spectrum3.add(100.0, 10.0);
        spectrum3.add(200.0, 12.0);
        spectrum3.add(300.0, 13.0);
        spectrum3.setPrecursor(new Peak(1000.0,10.0,1));
        spectra2.add(spectrum3);

        MsnSpectrum spectrum4 = new MsnSpectrum();
        spectrum4.add(100.0, 10.0);
        spectrum4.add(200.0, 12.0);
        spectrum4.add(500.0, 13.0);
        spectrum4.setPrecursor(new Peak(1001.0, 11.0, 1));
        spectra2.add(spectrum4);


        ConsensusSpectrum consensusSpectrum1 =
                ConsensusSpectrum.Builder.getBuilder().
                        spectra(spectra1).setPeakFilterParams(0.2, 2).
                        fragMzTolerance(0.2).
                        intensityCombMethod(AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                        build();

        ConsensusSpectrum consensusSpectrum2 =
                ConsensusSpectrum.Builder.getBuilder().
                        spectra(spectra2).setPeakFilterParams(0.2, 2).
                        fragMzTolerance(0.2).
                        intensityCombMethod(AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                        build();

        Assert.assertTrue(consensusSpectrum1.equals(consensusSpectrum2));

        consensusSpectrum2 =
                ConsensusSpectrum.Builder.getBuilder().
                        spectra(spectra2).setPeakFilterParams(0.0, 1).
                        fragMzTolerance(0.2).
                        intensityCombMethod(AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                        build();

        Assert.assertFalse(consensusSpectrum1.equals(consensusSpectrum2));
    }

    @Test
    public void testConsensusSpectrumHash()
    {
        Set<MsnSpectrum> spectra1 = new HashSet<>();

        MsnSpectrum spectrum1 = new MsnSpectrum();
        spectrum1.add(100.0, 10.0);
        spectrum1.add(200.0, 12.0);
        spectrum1.add(300.0, 13.0);
        spectrum1.setPrecursor(new Peak(1000.0,10.0,1));
        spectra1.add(spectrum1);

        MsnSpectrum spectrum2 = new MsnSpectrum();
        spectrum2.add(100.0, 10.0);
        spectrum2.add(200.0, 12.0);
        spectrum2.add(500.0, 13.0);
        spectrum2.setPrecursor(new Peak(1001.0, 11.0, 1));
        spectra1.add(spectrum2);

        Set<MsnSpectrum> spectra2 = new HashSet<>();

        MsnSpectrum spectrum3 = new MsnSpectrum();
        spectrum3.add(100.0, 10.0);
        spectrum3.add(200.0, 12.0);
        spectrum3.add(300.0, 13.0);
        spectrum3.setPrecursor(new Peak(1000.0,10.0,1));
        spectra2.add(spectrum3);

        MsnSpectrum spectrum4 = new MsnSpectrum();
        spectrum4.add(100.0, 10.0);
        spectrum4.add(200.0, 12.0);
        spectrum4.add(500.0, 13.0);
        spectrum4.setPrecursor(new Peak(1001.0, 11.0, 1));
        spectra2.add(spectrum4);


        ConsensusSpectrum consensusSpectrum1 =
                ConsensusSpectrum.Builder.getBuilder().
                        spectra(spectra1).setPeakFilterParams(0.2, 2).
                        fragMzTolerance(0.2).
                        intensityCombMethod(AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                        build();

        ConsensusSpectrum consensusSpectrum2 =
                ConsensusSpectrum.Builder.getBuilder().
                        spectra(spectra2).setPeakFilterParams(0.2, 2).
                        fragMzTolerance(0.2).
                        intensityCombMethod(AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                        build();

        Assert.assertEquals(consensusSpectrum1.hashCode(),consensusSpectrum2.hashCode());

        consensusSpectrum2 =
                ConsensusSpectrum.Builder.getBuilder().
                        spectra(spectra2).setPeakFilterParams(0.0, 1).
                        fragMzTolerance(0.2).
                        intensityCombMethod(AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                        build();

        Assert.assertNotEquals(consensusSpectrum1.hashCode(),consensusSpectrum2.hashCode());
    }
}
