package org.expasy.mzjava.core.io;

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IterativeReadersTest {

    @Test
    public void testIterativeReader() throws Exception {

        IterativeReader<String> reader = IterativeReaders.fromCollection(Lists.newArrayList("A1", "A2", "B1"));

        Assert.assertEquals(Lists.newArrayList("A1", "A2", "B1"), IterativeReaders.toArrayList(reader));
    }

    @Test
    public void testIterativeReader2() throws Exception {

        IterativeReader<String> reader = IterativeReaders.fromIterator(Lists.newArrayList("A1", "A2", "B1").iterator());

        Assert.assertEquals(Lists.newArrayList("A1", "A2", "B1"), IterativeReaders.toArrayList(reader));
    }

    @Test
    public void testToArrayList() throws Exception {

        //noinspection unchecked
        IterativeReader<String> reader = mock(IterativeReader.class);
        when(reader.hasNext()).thenReturn(true, true, true, false);
        when(reader.next()).thenReturn("A1", "A2", "B1");

        List<String> list = IterativeReaders.toArrayList(reader);

        Assert.assertEquals(Lists.newArrayList("A1", "A2", "B1"), list);
    }

    @Test(expected = IllegalStateException.class)
    public void testToArrayListExceptionOnNext() throws Exception {

        //noinspection unchecked
        IterativeReader<String> reader = mock(IterativeReader.class);
        when(reader.hasNext()).thenReturn(true, true, true, false);
        when(reader.next()).thenThrow(new IOException());

        IterativeReaders.toArrayList(reader);
    }

    @Test(expected = IllegalStateException.class)
    public void testToArrayListExceptionOnClose() throws Exception {

        //noinspection unchecked
        IterativeReader<String> reader = mock(IterativeReader.class);
        when(reader.hasNext()).thenReturn(true, true, true, false);
        when(reader.next()).thenReturn("A1", "A2", "B1");
        Mockito.doThrow(new IOException()).when(reader).close();

        IterativeReaders.toArrayList(reader);
    }
}