/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.peaklist.peakfilter;

import org.expasy.mzjava.core.ms.peaklist.MockPeakSink;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessor;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class ThresholdFilterTest {

    @Test
    public void test() throws Exception {

        double[] mzs = new double[]{1009.0023719667987, 1080.6453215726483, 1143.6371799568678, 1313.3446681570776, 1497.9970067622116, 1573.7126261762332, 1589.3719155794226, 1647.788997704897, 1659.2531083438853, 1674.3943350407728, 1808.801080376506, 1856.343197234145, 1862.2359488629222, 1882.9627022854636, 1895.1735998300373, 2089.4074737867454, 2136.024062438867, 2184.3728777275037, 2206.722055021579, 2293.9203234875304};
        double[] intensities = new double[]{7.0, 15.0, 36.0, 22.0, 12.0, 30.0, 7.0, 9.0, 14.0, 12.0, 20.0, 6.0, 2.0, 4.0, 1.0, 24.0, 4.0, 14.0, 24.0, 10.0};

        ThresholdFilter<PeakAnnotation> filter = new ThresholdFilter<PeakAnnotation>(10);
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();
        filter.setSink(sink);

        runProcessor(filter, mzs, intensities);
        
        double [] actualMz = sink.getMzList();
        double [] actualIntensities = sink.getIntensityList();

        Assert.assertEquals(12, actualMz.length);

        Assert.assertEquals(1080.6453215726483, actualMz[0], 0.000000000001);
        Assert.assertEquals(15.0, actualIntensities[0], 0.000000000001);
        Assert.assertEquals(1143.6371799568678, actualMz[1], 0.000000000001);
        Assert.assertEquals(36.0, actualIntensities[1], 0.000000000001);
        Assert.assertEquals(1313.3446681570776, actualMz[2], 0.000000000001);
        Assert.assertEquals(22.0, actualIntensities[2], 0.000000000001);
        Assert.assertEquals(1497.9970067622116, actualMz[3], 0.000000000001);
        Assert.assertEquals(12.0, actualIntensities[3], 0.000000000001);
        Assert.assertEquals(1573.7126261762332, actualMz[4], 0.000000000001);
        Assert.assertEquals(30.0, actualIntensities[4], 0.000000000001);
        Assert.assertEquals(1659.2531083438853, actualMz[5], 0.000000000001);
        Assert.assertEquals(14.0, actualIntensities[5], 0.000000000001);
        Assert.assertEquals(1674.3943350407728, actualMz[6], 0.000000000001);
        Assert.assertEquals(12.0, actualIntensities[6], 0.000000000001);
        Assert.assertEquals(1808.801080376506, actualMz[7], 0.000000000001);
        Assert.assertEquals(20.0, actualIntensities[7], 0.000000000001);
        Assert.assertEquals(2089.4074737867454, actualMz[8], 0.000000000001);
        Assert.assertEquals(24.0, actualIntensities[8], 0.000000000001);
        Assert.assertEquals(2184.3728777275037, actualMz[9], 0.000000000001);
        Assert.assertEquals(14.0, actualIntensities[9], 0.000000000001);
        Assert.assertEquals(2206.722055021579, actualMz[10], 0.000000000001);
        Assert.assertEquals(24.0, actualIntensities[10], 0.000000000001);
        Assert.assertEquals(2293.9203234875304, actualMz[11], 0.000000000001);
        Assert.assertEquals(10.0, actualIntensities[11], 0.000000000001);

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getEndCalled());
    }

    @Test
    public void test2() throws Exception {

        double[] mzs = new double[]{1009.0023719667987, 1080.6453215726483, 1143.6371799568678, 1313.3446681570776, 1497.9970067622116, 1573.7126261762332, 1589.3719155794226, 1647.788997704897, 1659.2531083438853, 1674.3943350407728, 1808.801080376506, 1856.343197234145, 1862.2359488629222, 1882.9627022854636, 1895.1735998300373, 2089.4074737867454, 2136.024062438867, 2184.3728777275037, 2206.722055021579, 2293.9203234875304};
        double[] intensities = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

        ThresholdFilter<PeakAnnotation> filter = new ThresholdFilter<PeakAnnotation>(0);
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();
        filter.setSink(sink);

        runProcessor(filter, mzs, intensities);

        double [] actualMz = sink.getMzList();

        Assert.assertEquals(20, actualMz.length);

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getEndCalled());
    }

    @Test
    public void test3() throws Exception {

        double[] mzs = new double[]{1009.0023719667987, 1080.6453215726483, 1143.6371799568678, 1313.3446681570776, 1497.9970067622116, 1573.7126261762332, 1589.3719155794226, 1647.788997704897, 1659.2531083438853, 1674.3943350407728, 1808.801080376506, 1856.343197234145, 1862.2359488629222, 1882.9627022854636, 1895.1735998300373, 2089.4074737867454, 2136.024062438867, 2184.3728777275037, 2206.722055021579, 2293.9203234875304};
        double[] intensities = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

        ThresholdFilter<PeakAnnotation> filter = new ThresholdFilter<PeakAnnotation>(0, ThresholdFilter.Inequality.GREATER);
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();
        filter.setSink(sink);

        runProcessor(filter, mzs, intensities);

        double [] actualMz = sink.getMzList();
        double [] actualIntensities = sink.getIntensityList();

        Assert.assertEquals(1, actualMz.length);

        Assert.assertEquals(1009.0023719667987, actualMz[0], 0.000000000001);
        Assert.assertEquals(1.0, actualIntensities[0], 0.000000000001);

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getEndCalled());
    }

    private void runProcessor(PeakProcessor<PeakAnnotation, PeakAnnotation> processor, double[] keys, double[] values) {

        processor.start(keys.length);

        for (int i = 0; i < keys.length; i++) {

            List<PeakAnnotation> objects = Collections.emptyList();
            processor.processPeak(keys[i], values[i], objects);
        }

        processor.end();
    }
}
