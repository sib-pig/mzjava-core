package org.expasy.mzjava.core.ms.peaklist.peaktransformer;

import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Markus
 * @version 1.0
 */
public class AddFlankingPeaksTransformerTest {

    double[] masses =
            new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
    double[] intensities =
            new double[]{50, 0, 0, 0, 50, 50, 0, 0, 0, 50, 0, 0, 0, 0, 50, 0, 0, 0, 0, 50};

    @Test
    public void testFlankingPeaksTransformer() {

        AddFlankingPeaksTransformer<PeakAnnotation> transformer = new AddFlankingPeaksTransformer<PeakAnnotation>(0.5, 30.0);
        DoublePeakList<PeakAnnotation> expPeakList = new DoublePeakList<PeakAnnotation>();
        expPeakList.addSorted(masses, intensities, masses.length);

        expPeakList.apply(transformer);

        double[] newIntensities =
                new double[]{50, 25, 0, 25, 50, 50, 25, 0, 25, 50, 25, 0, 0, 25, 50, 25, 0, 0, 25, 50};

        double[] flankedInt = expPeakList.getIntensities(null);

        Assert.assertArrayEquals(newIntensities, flankedInt, 0.0001);
    }

}
