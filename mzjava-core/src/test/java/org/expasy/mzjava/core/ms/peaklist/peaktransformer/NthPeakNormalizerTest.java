/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.peaklist.peaktransformer;

import org.expasy.mzjava.core.ms.peaklist.MockPeakSink;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class NthPeakNormalizerTest {

    @Test
    public void test1() {

        NthPeakNormalizer<PeakAnnotation> norm = new NthPeakNormalizer<PeakAnnotation>(1);
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();
        norm.setSink(sink);

        List<PeakAnnotation> annotations = Collections.emptyList();

        norm.start(6);

        norm.processPeak(1, 5, annotations);
        norm.processPeak(2, 10, annotations);
        norm.processPeak(3, 2, annotations);
        norm.processPeak(4, 8, annotations);
        norm.processPeak(5, 6, annotations);
        norm.processPeak(6, 4, annotations);

        norm.end();

        Assert.assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, sink.getMzList(), 0.000001);
        Assert.assertArrayEquals(new double[]{5/10.0, 1.0, 2/10.0, 8/10.0, 6/10.0, 4/10.0}, sink.getIntensityList(), 0.000001);

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getEndCalled());
    }

    @Test
    public void test2() {

        NthPeakNormalizer<PeakAnnotation> norm = new NthPeakNormalizer<PeakAnnotation>(2);
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();
        norm.setSink(sink);

        List<PeakAnnotation> annotations = Collections.emptyList();

        norm.start(6);

        norm.processPeak(1, 5, annotations);
        norm.processPeak(2, 10, annotations);
        norm.processPeak(3, 2, annotations);
        norm.processPeak(4, 8, annotations);
        norm.processPeak(5, 6, annotations);
        norm.processPeak(6, 4, annotations);

        norm.end();

        Assert.assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, sink.getMzList(), 0.000001);
        Assert.assertArrayEquals(new double[]{0.625, 1.25, 0.25, 1.00, 0.75, 0.50}, sink.getIntensityList(), 0.000001);

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getEndCalled());
    }
}
