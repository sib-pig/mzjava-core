/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.transformer;

import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.MockPeakPairSink;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PeakPairIntensitySqrtTransformerTest {

    @Test
    public void test() throws Exception {

        PeakPairIntensitySqrtTransformer<PeakAnnotation, PeakAnnotation> processor = new PeakPairIntensitySqrtTransformer<PeakAnnotation, PeakAnnotation>();

        MockPeakPairSink<PeakAnnotation> sink = new MockPeakPairSink<PeakAnnotation>();
        processor.setSink(sink);
        List<PeakAnnotation> empty = Collections.emptyList();

        processor.begin(null, null);

        processor.processPeakPair(1.1, Math.pow(4, 2), Math.pow(14, 2), empty, empty);
        processor.processPeakPair(1.2, Math.pow(44, 2), Math.pow(3, 2), empty, empty);
        processor.processPeakPair(1.3, Math.pow(12, 2), Math.pow(8, 2), empty, empty);
        processor.processPeakPair(1.4, Math.pow(77, 2), Math.pow(2, 2), empty, empty);

        processor.end();

        Assert.assertArrayEquals(new double[]{1.1, 1.2, 1.3, 1.4}, sink.getCentroids(), 0.00000001);
        Assert.assertArrayEquals(new double[]{4, 44, 12, 77}, sink.getXvalues(), 0.00000001);
        Assert.assertArrayEquals(new double[]{14, 3, 8, 2}, sink.getYvalues(), 0.00000001);

        Assert.assertTrue(sink.wasBeginCalled());
        Assert.assertTrue(sink.wasEndCalled());
    }

	@Test
	public void testProcessPeakPairUndefSqrt() throws Exception {

		PeakPairIntensitySqrtTransformer<PeakAnnotation, PeakAnnotation> processor = new PeakPairIntensitySqrtTransformer<PeakAnnotation, PeakAnnotation>();

		MockPeakPairSink<PeakAnnotation> sink = new MockPeakPairSink<PeakAnnotation>();
		processor.setSink(sink);

		List<PeakAnnotation> empty = Collections.emptyList();

		processor.begin(null, null);

		processor.processPeakPair(1.1, -10, 10, empty, empty);

		Assert.assertEquals(0.0, sink.getXvalues()[0], 0.00000001);
	}
}
