package org.expasy.mzjava.core.ms.peaklist.peaktransformer;

import gnu.trove.list.array.TDoubleArrayList;
import org.apache.commons.math3.stat.ranking.NaturalRanking;
import org.apache.commons.math3.stat.ranking.TiesStrategy;
import org.expasy.mzjava.utils.PrimitiveArrayUtils;
import org.expasy.mzjava.utils.ArrayIndexComparator;
import org.expasy.mzjava.utils.ArrayIndexSorter;
import org.expasy.mzjava.core.ms.peaklist.MockPeakSink;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessor;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * @author Markus Muller
 * @version 1.0
 */
public class RankNormalizerTest {

    @Test
    public void test() throws Exception {

        double[] mzs = new double[]{587.7174933594041, 987.9064324137777, 1303.3698821516343, 1358.1108032884222, 1483.8264131267663, 2037.2556613941845, 2118.2197265634236, 2283.2362175811922, 2318.0625328860915, 2399.419164587697};
        double[] intensities = new double[]{9.0, 26.0, 7.0, 46.0, 10.0, 22.0, 4.0, 28.0, 4.0, 13.0};
        //0.1        0.2
        double[] expectedIntensities = new double[]{0.4, 0.8, 0.3, 1.0, 0.5, 0.7, 0.15, 0.9, 0.15, 0.6};

        RankNormalizer<PeakAnnotation> rankNormalizer = new RankNormalizer<PeakAnnotation>();
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();
        rankNormalizer.setSink(sink);

        runProcessor(rankNormalizer, mzs, intensities);

        Assert.assertArrayEquals(expectedIntensities, sink.getIntensityList(), 0.00000001);

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getEndCalled());
    }

    @Test
    public void test2() throws Exception {

        double[] mzs = new double[]{587.7174933594041, 987.9064324137777, 1303.3698821516343, 1358.1108032884222, 1483.8264131267663, 2037.2556613941845, 2118.2197265634236, 2283.2362175811922, 2318.0625328860915, 2399.419164587697};
        double[] intensities = new double[]{9.0, 26.0, 7.0, 46.0, 10.0, 22.0, 4.0, 28.0, 4.0, 13.0};

        double[] expectedIntensities = new double[]{0.4, 0.8, 0.3, 1.0, 0.5, 0.7, 0.1, 0.9, 0.2, 0.6};

        RankNormalizer<PeakAnnotation> rankNormalizer = new RankNormalizer<PeakAnnotation>(TiesStrategy.SEQUENTIAL);
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();
        rankNormalizer.setSink(sink);

        runProcessor(rankNormalizer, mzs, intensities);

        Assert.assertArrayEquals(expectedIntensities, sink.getIntensityList(), 0.00000001);

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getEndCalled());
    }

    private void runProcessor(PeakProcessor<PeakAnnotation, PeakAnnotation> processor, double[] mzs, double[] intensities) {

        processor.start(mzs.length);

        for (int i = 0; i < mzs.length; i++) {

            List<PeakAnnotation> objects = Collections.emptyList();
            processor.processPeak(mzs[i], intensities[i], objects);
        }

        processor.end();
    }

    public static void main(String[] args) {

        new RankNormalizerTest().benchmark();
    }

    public void benchmark() {

        for (int i = 0; i < 20; i++) {

            run();
        }
    }

    private void run() {

        Random random = new Random();

        int size = 10000;
        double[][] dataTable = new double[size][];
        double[][] oldJplDataTable = new double[size][];
        TDoubleArrayList[] arrayLists = new TDoubleArrayList[size];

        for (int i = 0; i < dataTable.length; i++) {

            dataTable[i] = new double[random.nextInt(200) + 1];
            oldJplDataTable[i] = new double[dataTable[i].length];

            arrayLists[i] = new TDoubleArrayList(dataTable[i].length);

            for (int j = 0; j < dataTable[i].length; j++) {

                double v = random.nextDouble();
                dataTable[i][j] = v;
                oldJplDataTable[i][j] = v;
                arrayLists[i].add(v);
            }
        }

        TiesStrategy tiesStrategy = TiesStrategy.SEQUENTIAL;
        NaturalRanking naturalRanking = new NaturalRanking(tiesStrategy);
        RankNormalizer rankTransform = new RankNormalizer(tiesStrategy);

        long start = System.currentTimeMillis();
        for (int i = 0; i < dataTable.length; i++) {

            dataTable[i] = naturalRanking.rank(dataTable[i]);
        }
        long apacheTime = System.currentTimeMillis() - start;

        start = System.currentTimeMillis();
        for (int i = 0; i < arrayLists.length; i++) {

            arrayLists[i] = rankTransform.rank(arrayLists[i]);
        }
        long jplTime = System.currentTimeMillis() - start;

        start = System.currentTimeMillis();
        for (int i = 0; i < oldJplDataTable.length; i++) {

            oldJplDataTable[i] = rank(oldJplDataTable[i]);
        }
        long oldJplTime = System.currentTimeMillis() - start;

        System.out.println("apacheTime " + apacheTime + "ms");                      //sout
        System.out.println("jplTime    " + jplTime + "ms");                      //sout
        System.out.println("oldJplTime " + oldJplTime + "ms");                      //sout

//        for (int i = 0; i < arrayLists.length; i++) {
//
//            TDoubleArrayList arrayList = arrayLists[i];
//            double[] data = dataTable[i];
//            double[] oldJpl = oldJplDataTable[i];
//
//            Assert.assertArrayEquals(data, arrayList.toArray(), 0.00000001);
//            Assert.assertArrayEquals(data, oldJpl, 0.00000001);
//        }
        Assert.fail("Test has no asserts");
    }

    private double[] rank(double[] intensities) {

        Double[] array = PrimitiveArrayUtils.toDoubles(intensities, null);

        ArrayIndexSorter sorter = new ArrayIndexSorter();

        Integer[] idx = sorter.sort(new DoublesIndexComparator(array));

        int nrOfRankedPeaks = intensities.length;

        // assign relative ranks
        for (int i = 0; i < intensities.length; i++) {
            if (i >= intensities.length - nrOfRankedPeaks) {
                intensities[idx[i]] = (i + 1.0);// / intensities.length;
            } else {
                intensities[idx[i]] = 1.0;// / intensities.length;
            }
        }

        return intensities;
    }

    private static class DoublesIndexComparator extends ArrayIndexComparator<Double> {

        public DoublesIndexComparator(Double[] doubles) {

            super(doubles);
        }

        @Override
        public int compare(Integer i1, Integer i2) {

            return array[i1].compareTo(array[i2]);
        }

    }
}
