package org.expasy.mzjava.core.ms.cluster;

import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collection;
import java.util.Set;
import java.util.UUID;

/**
 * @author Markus Muller
 * @author Oliver Horlacher
 * @version 0.0
 */
public class GlobalThresholdClusterBuilderTest {
    @Test
    public void testGlobalThresholdClustering() {

        String nodeA1 = "nodeA1";
        String nodeA2 = "nodeA2";
        String nodeA3 = "nodeA3";
        String nodeA4 = "nodeA4";

        String nodeB1 = "nodeB1";
        String nodeB2 = "nodeB2";
        String nodeB3 = "nodeB3";

        String nodeC1 = "nodeC1";

        DenseSimilarityGraph.Builder<String> builder = new DenseSimilarityGraph.Builder<>();

        builder.add(nodeA1, nodeA2, 0.9);
        builder.add(nodeA1, nodeA3, 0.9);
        builder.add(nodeA1, nodeA4, 0.9);
        builder.add(nodeA2, nodeA3, 0.9);
        builder.add(nodeA2, nodeA4, 0.9);
        builder.add(nodeA3, nodeA4, 0.9);

        builder.add(nodeB1, nodeB2, 0.7);
        builder.add(nodeB1, nodeB3, 0.7);
        builder.add(nodeB2, nodeB3, 0.7);

        builder.add(nodeA2, nodeB1, 0.5);
        builder.add(nodeA2, nodeB3, 0.5);

        builder.add(nodeC1);

        DenseSimilarityGraph<String> simGraph = builder.build();

        GlobalThresholdClusterBuilder<String> clusterer = new GlobalThresholdClusterBuilder<>(0.6);
        Collection<Set<String>> clusters = clusterer.cluster(simGraph);

        Assert.assertEquals(clusters.size(), 3);
        for (Set<String> cluster : clusters) {

            if (cluster.contains(nodeA1)) {
                Assert.assertEquals(cluster.size(), 4);
                Assert.assertTrue(cluster.contains(nodeA2));
                Assert.assertTrue(cluster.contains(nodeA3));
                Assert.assertTrue(cluster.contains(nodeA4));
            }

            if (cluster.contains(nodeB1)) {
                Assert.assertEquals(cluster.size(), 3);
                Assert.assertTrue(cluster.contains(nodeB2));
                Assert.assertTrue(cluster.contains(nodeB3));
            }

            if (cluster.contains(nodeC1)) {
                Assert.assertEquals(cluster.size(), 1);
            }
        }
    }

    @Test
    public void testGlobalThresholdClustering2() {

        String nodeA1 = "nodeA1";
        String nodeA2 = "nodeA2";
        String nodeA3 = "nodeA3";
        String nodeA4 = "nodeA4";

        String nodeB1 = "nodeB1";
        String nodeB2 = "nodeB2";
        String nodeB3 = "nodeB3";

        String nodeC1 = "nodeC1";
        String nodeC2 = "nodeC2";

        DenseSimilarityGraph.Builder<String> builder = new DenseSimilarityGraph.Builder<>();

        builder.add(nodeA1, nodeA2, 0.5);
        builder.add(nodeA1, nodeA3, 0.5);
        builder.add(nodeA1, nodeA4, 0.5);
        builder.add(nodeA2, nodeA3, 0.9);
        builder.add(nodeA2, nodeA4, 0.9);
        builder.add(nodeA3, nodeA4, 0.9);

        builder.add(nodeB1, nodeB2, 0.7);
        builder.add(nodeB1, nodeB3, 0.7);
        builder.add(nodeB2, nodeB3, 0.7);

        builder.add(nodeA2, nodeB1, 0.5);
        builder.add(nodeA2, nodeB3, 0.5);

        builder.add(nodeC1, nodeC2, 0.5);

        DenseSimilarityGraph<String> simGraph = builder.build();

        GlobalThresholdClusterBuilder<String> clusterer = new GlobalThresholdClusterBuilder<>(0.6);
        Collection<Set<String>> clusters = clusterer.cluster(simGraph);

        Assert.assertEquals(clusters.size(), 5);
        for (Set<String> cluster : clusters) {

            if (cluster.contains(nodeA1)) {
                Assert.assertEquals(cluster.size(), 1);
            }

            if (cluster.contains(nodeA2)) {
                Assert.assertEquals(cluster.size(), 3);
                Assert.assertTrue(cluster.contains(nodeA3));
                Assert.assertTrue(cluster.contains(nodeA4));
            }

            if (cluster.contains(nodeB1)) {
                Assert.assertEquals(cluster.size(), 3);
                Assert.assertTrue(cluster.contains(nodeB2));
                Assert.assertTrue(cluster.contains(nodeB3));
            }

            if (cluster.contains(nodeC1)) {
                Assert.assertEquals(cluster.size(), 1);
            }

            if (cluster.contains(nodeC2)) {
                Assert.assertEquals(cluster.size(), 1);
            }
        }
    }

    /**
     * Graph:
     * <pre>
     *     A1-A2--B1--B2
     *     |\ /|\ | /
     *     |/ \| \|/
     *     A3-A4  B3
     * </pre>
     *
     * @throws Exception
     */
    @Test
    public void test() throws Exception {

        UUID nodeA1 = UUID.fromString("a1000000-0000-0000-0000-000000000000");
        UUID nodeA2 = UUID.fromString("a2000000-0000-0000-0000-000000000000");
        UUID nodeA3 = UUID.fromString("a3000000-0000-0000-0000-000000000000");
        UUID nodeA4 = UUID.fromString("a4000000-0000-0000-0000-000000000000");

        UUID nodeB1 = UUID.fromString("b1000000-0000-0000-0000-000000000000");
        UUID nodeB2 = UUID.fromString("b2000000-0000-0000-0000-000000000000");
        UUID nodeB3 = UUID.fromString("b3000000-0000-0000-0000-000000000000");

        DenseSimilarityGraph.Builder<UUID> builder = new DenseSimilarityGraph.Builder<>();
        builder.add(nodeA1, nodeA2, 0.9);
        builder.add(nodeA1, nodeA3, 0.9);
        builder.add(nodeA1, nodeA4, 0.9);
        builder.add(nodeA2, nodeA3, 0.9);
        builder.add(nodeA2, nodeA4, 0.9);
        builder.add(nodeA3, nodeA4, 0.9);

        builder.add(nodeB1, nodeB2, 0.9);
        builder.add(nodeB1, nodeB3, 0.9);
        builder.add(nodeB2, nodeB3, 0.9);

        builder.add(nodeA2, nodeB1, 0.8);
        builder.add(nodeA2, nodeB3, 0.8);

        ClusterBuilder<UUID> clusterBuilder = new GlobalThresholdClusterBuilder<>(0.81);

        Collection<Set<UUID>> results = clusterBuilder.cluster(builder.build());

        Assert.assertEquals(2, results.size());

        for (Set<UUID> cluster : results) {

            if (cluster.size() == 4) {

                Assert.assertEquals(Sets.newHashSet(nodeA1, nodeA2, nodeA3, nodeA4), cluster);
            } else {

                Assert.assertEquals(Sets.newHashSet(nodeB1, nodeB2, nodeB3), cluster);
            }
        }
    }

    /**
     * <pre>
     *          A1
     *          |
     *         0.1
     *          |
     *  B1-0.90-B2
     *    \     /
     *   0.9  0.9
     *     \  /
     *      B3
     * </pre>
     *
     * @throws Exception
     */
    @Test
    public void testSplitSmall() throws Exception {

        UUID nodeA1 = UUID.fromString("a1000000-0000-0000-0000-000000000000");

        UUID nodeB1 = UUID.fromString("b1000000-0000-0000-0000-000000000000");
        UUID nodeB2 = UUID.fromString("b2000000-0000-0000-0000-000000000000");
        UUID nodeB3 = UUID.fromString("b3000000-0000-0000-0000-000000000000");

        DenseSimilarityGraph.Builder<UUID> builder = new DenseSimilarityGraph.Builder<>();
        builder.add(nodeA1, nodeB1, 0.1);

        builder.add(nodeB1, nodeB2, 0.9);
        builder.add(nodeB1, nodeB3, 0.9);
        builder.add(nodeB2, nodeB3, 0.9);

        ClusterBuilder<UUID> clusterBuilder = new GlobalThresholdClusterBuilder<>(0.2);

        Collection<Set<UUID>> results = clusterBuilder.cluster(builder.build());

        Assert.assertEquals(2, results.size());
    }
}

