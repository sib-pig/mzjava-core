/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrum;

import org.expasy.mzjava.core.ms.peaklist.BasePeakListTest;
import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.IdentityPeakProcessor;
import org.expasy.mzjava.utils.URIBuilder;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MsnSpectrumTest extends BasePeakListTest {

    public MsnSpectrumTest() {

        super(PeakList.Precision.DOUBLE);
    }

    private double[] getMzField(DoublePeakList peakList) throws NoSuchFieldException, IllegalAccessException {

        Field field = peakList.getClass().getDeclaredField("mzList");
        field.setAccessible(true);
        return (double[]) field.get(peakList);
    }

    @Override
    protected void checkPeakList(double[] expectedMzs, double[] expectedIntensities, PeakList<? extends PeakAnnotation> spectrum) throws NoSuchFieldException, IllegalAccessException {

        DoublePeakList peakList = getPeakList(spectrum);

        peakList.trimToSize();
        double[] mzs = getMzField(peakList);
        Assert.assertEquals(expectedMzs.length, mzs.length);
        for (int i = 0; i < mzs.length; i++) {

            Assert.assertEquals(expectedMzs[i], mzs[i], 0.0001);
        }

        double[] intensities = getIntensityField(peakList);
        Assert.assertEquals(expectedIntensities.length, intensities.length);
        for (int i = 0; i < intensities.length; i++) {

            Assert.assertEquals(expectedIntensities[i], intensities[i], 0.0001);
        }
    }

    private double[] getIntensityField(DoublePeakList peakList) throws NoSuchFieldException, IllegalAccessException {

        Field field = peakList.getClass().getDeclaredField("intensityList");
        field.setAccessible(true);
        return (double[]) field.get(peakList);
    }

    private MsnSpectrum newPeakList(int initialCapacity) {

        return new MsnSpectrum(initialCapacity, PeakList.Precision.DOUBLE);
    }

    @Override
    protected <A extends PeakAnnotation> int getIntensityArrayLength(PeakList<A> spectrum) throws NoSuchFieldException, IllegalAccessException {

        return getMzField(getPeakList(spectrum)).length;
    }

    @Override
    protected <A extends PeakAnnotation> int getMzArrayLength(PeakList<A> spectrum) throws NoSuchFieldException, IllegalAccessException {

        return getIntensityField(getPeakList(spectrum)).length;
    }

    private DoublePeakList getPeakList(PeakList spectrum) throws NoSuchFieldException, IllegalAccessException {

        Field field = Spectrum.class.getDeclaredField("peakList");
        field.setAccessible(true);
        return (DoublePeakList)field.get(spectrum);
    }

    protected MsnSpectrum newPeakList() {

        return newPeakList(0);
    }

    @Override
    @Test
    public void testEquals() throws Exception {

        runTestEquals(newPeakList(), newPeakList());
    }

    @Override
    @Test
    public void testHashCode() throws Exception {

        runTestHashCode(newPeakList(), newPeakList());
    }

    @Override
    @Test
    public void testGetIntensitiesArr() throws Exception {

        runTestGetIntensitiesArr(newPeakList());
    }

    @Override
    @Test
    public void testGetIntensities() throws Exception {

        MsnSpectrum peakList = newPeakList();

        runTestGetIntensities(peakList);
    }

    @Override
    @Test
    public void testGetIntensities2() throws Exception {

        runTestGetIntensities2(newPeakList());
    }

    @Override
    @Test
    public void testGetIntensities3() throws Exception {

        runTestGetIntensities3(newPeakList());
    }

    @Override
    @Test
    public void testGetIntensities4() throws Exception {

        runTestGetIntensities4(newPeakList());
    }

    @Override
    @Test
    public void testGetIntensities5() throws Exception {

        runTestGetIntensities5(newPeakList());
    }

    @Override
    @Test
    public void testGetMzs1() throws Exception {

        runTestGetMzs1(newPeakList());
    }

    @Override
    @Test
    public void testGetMzs2() throws Exception {

        runTestGetMzs2(newPeakList());
    }

    @Override
    @Test
    public void testGetMzs3() throws Exception {

        runTestGetMzs3(newPeakList());
    }

    @Override
    @Test
    public void testGetIntensityAt() throws Exception {

        MsnSpectrum peakList = newPeakList();

        runTestGetIntensityAt(peakList);
    }

    @Test
    public void testAddSame() {

        runTestAddSame(newPeakList());
    }

    @Override
    @Test
    public void testEnsureCapacity() throws NoSuchFieldException, IllegalAccessException {

        runTestEnsureCapacity(newPeakList());
    }

    @Override
    @Test
    public void testSetLoadFactor() throws NoSuchFieldException, IllegalAccessException {

        //Only for PeakLists
    }

    @Override
    @Test
    public void testGetMostIntenseIndex() {

        runTestGetMostIntenseIndex(newPeakList(15));
    }

    @Override
    @Test
    public void testGetMostIntenseIndex2() {

        runTestGetMostIntenseIndex2(newPeakList(0));
    }

    @Override
    @Test
    public void testGetMostIntenseIndex3() {

        runTestGetMostIntenseIndex3(newPeakList(0));
    }

    @Override
    @Test
    public void testGetMostIntenseIndex4() {

        runTestGetMostIntenseIndex4(newPeakList(0));
    }

    @Override
    @Test
    public void testGetMostIntenseIndex5() {

        runTestGetMostIntenseIndex5(newPeakList(0));
    }

    @Override
    @Test
    public void testTotalIonCurrent() {

        runTestTotalIonCurrent(newPeakList());
    }

    @Override
    @Test
    public void testCopyMzs() {

        runTestCopyMzs(newPeakList(0));
    }

    @Override
    @Test
    public void testSetIntensity() {

        int initialCapacity = 0;
        runTestSetIntensity(newPeakList(initialCapacity));
    }

    @Override
    @Test
    public void testValueOf() {

        runTestValueOf(precision);
    }

    @Override
    @Test
    public void testValueOfWithIntensities() {

        runTestValueOfWithIntensities(precision);
    }

    @Override
    @Test
    public void testAddPeakWithAnnotation() throws Exception {

        // only for PeakList
    }

    @Override
    @Test
    public void testGetMostIntenseIndexWholeSpectra() throws Exception {

        runGetMostIntenseIndexWholeSpectra(newPeakList());
    }

    @Override
    @Test(expected = IndexOutOfBoundsException.class)
    public void testEmptyPeakListGetMz() throws Exception {

        MsnSpectrum peakList = newPeakList();
        runTestEmptyPeakListGetMz(peakList);
    }

    @Override
    @Test
    public void testEmptyPeakListGetMzs() throws Exception {

        runTestEmptyPeakListGetMzs(newPeakList());
    }

    @Override
    @Test(expected = IndexOutOfBoundsException.class)
    public void testEmptyPeakListGetIntensityAt() throws Exception {

        runTestEmptyPeakListGetIntensityAt(newPeakList());
    }

    @Override
    @Test
    public void testEmptyPeakListGetIntensities() throws Exception {

        runTestEmptyPeakListGetIntensities(newPeakList());
    }

    @Override
    @Test
    public void testClear() throws Exception {

        MsnSpectrum peakList = newPeakList();
        runTestClear(peakList);
    }

    @Override
    @Test
    public void testPrecision() throws Exception {

        MsnSpectrum peakList = newPeakList();
        PeakList.Precision precision = this.precision;
        runTestPrecision(peakList, precision);
    }

    @Test
    public void testBulkAdd() throws Exception {

        runTestBulkAdd(newPeakList());
    }

    @Override
    @Test
    public void testBulkAdd2() throws Exception {

        runTestBulkAdd2(newPeakList());
    }

    @Override
    @Test
    public void testBulkAdd3() throws Exception {

        runTestBulkAdd3(newPeakList());
    }

    @Override
    @Test
    public void testBulkAddException() throws Exception {

        runTestAddException(newPeakList());
    }

    @Override
    @Test
    public void testBulkAddException2() throws Exception {

        runTestBulkAddException2(newPeakList());
    }

    @Override
    @Test
    public void testBulkAddException3() throws Exception {

        runTestBulkAddException3(newPeakList());
    }

    @Override
    @Test
    public void testBulkAddException4() throws Exception {

        runTestBulkAddException4(newPeakList());
    }

    @Test
    public void testInsert() throws Exception {

        runTestInsert(newPeakList());
    }

    @Test
    public void testMerge() throws Exception {

        runTestMerge(newPeakList());
    }

    @Test
    public void testMerge2() {

        runTestMerge2(newPeakList());
    }

    @Test
    public void testMerge3() {

        runTestMerge3(newPeakList());
    }

    @Test
    public void testMerge4() {

        runTestMerge4(newPeakList());
    }

    @Test
    public void testMerge5() throws Exception {

        //Only for PeakList
    }

    @Test
    public void testMerge6() throws Exception {

        //Only for PeakList
    }

    @Override
    public void testAddPeaks() throws Exception {

        //Only for PeakList
    }

    @Test
    public void testAddDuplicateWithAnnotation() throws Exception {

        runTestAddDuplicateWithAnnotation(newPeakList(), PeakAnnotation.class);
    }

    @Test
    public void testDoInsert() throws Exception {

        //Only for PeakList
    }

    @Override
    public void testIndexOf() throws Exception {

        runTestIndexOf(newPeakList());
    }

    @Override
    public void testGetClosestIndex() {

        runTestGetClosestIndex(newPeakList());
    }

    @Test
    public void testAddUnsortedPeaks() throws Exception {

        //Only for PeakList
    }

    @Test
    public void testGetSpectrumSource() throws Exception {

        MsnSpectrum spectrum = new MsnSpectrum();

        Assert.assertEquals(URIBuilder.UNDEFINED_URI, spectrum.getSpectrumSource());

        spectrum.setSpectrumSource(new URIBuilder("org.expasy", "test").build());
        Assert.assertEquals(new URIBuilder("org.expasy", "test").build(), spectrum.getSpectrumSource());
    }

    @Test
    public void testComment() throws Exception {

        MsnSpectrum spectrum = new MsnSpectrum();

        Assert.assertEquals("", spectrum.getComment());

        spectrum.setComment("Some comment");
        Assert.assertEquals("Some comment", spectrum.getComment());
    }

    @Test
    public void testFragMethod() throws Exception {

        MsnSpectrum spectrum = new MsnSpectrum();

        Assert.assertEquals("", spectrum.getFragMethod());

        spectrum.setFragMethod("magic");
        Assert.assertEquals("magic", spectrum.getFragMethod());
    }

    @Test
    public void testParentScanNumber() throws Exception {

        MsnSpectrum spectrum = new MsnSpectrum();

        Assert.assertEquals(new ScanNumberDiscrete(-1), spectrum.getParentScanNumber());

        ScanNumberInterval parentScanNumber = new ScanNumberInterval(3, 8);
        spectrum.setParentScanNumber(parentScanNumber);
        Assert.assertEquals(parentScanNumber, spectrum.getParentScanNumber());
    }

    @Test
    public void testScanNumbers() throws Exception {

        MsnSpectrum spectrum = new MsnSpectrum();

        Assert.assertEquals(0, spectrum.getScanNumbers().size());

        spectrum.addScanNumber(2);
        Assert.assertEquals(Collections.singletonList(new ScanNumberDiscrete(2)), spectrum.getScanNumbers());

        spectrum.addScanNumber(new ScanNumberInterval(5, 7));
        Assert.assertEquals(Arrays.asList(new ScanNumberDiscrete(2), new ScanNumberInterval(5, 7)),
                spectrum.getScanNumbers());

        spectrum.addScanNumbers(new ScanNumberList(8, 9, 10));
        Assert.assertEquals(Arrays.asList(
                new ScanNumberDiscrete(2),
                new ScanNumberInterval(5, 7),
                new ScanNumberDiscrete(8),
                new ScanNumberDiscrete(9),
                new ScanNumberDiscrete(10)),
                spectrum.getScanNumbers());
    }

    @Test
    public void testRetentionTime() throws Exception {

        MsnSpectrum spectrum = new MsnSpectrum();

        Assert.assertEquals(0, spectrum.getRetentionTimes().size());

        spectrum.addRetentionTime(new RetentionTimeDiscrete(50, TimeUnit.SECOND));
        Assert.assertEquals(Collections.singletonList(new RetentionTimeDiscrete(50, TimeUnit.SECOND)), spectrum.getRetentionTimes());

        spectrum.addRetentionTimes(new RetentionTimeList(
                new RetentionTimeDiscrete(90, TimeUnit.SECOND),
                new RetentionTimeDiscrete(150, TimeUnit.SECOND)
        ));
        Assert.assertEquals(Arrays.asList(
                new RetentionTimeDiscrete(50, TimeUnit.SECOND),
                new RetentionTimeDiscrete(90, TimeUnit.SECOND),
                new RetentionTimeDiscrete(150, TimeUnit.SECOND)
        ), spectrum.getRetentionTimes());
    }

    @Test
    public void testSpectrumIndex() throws Exception {

        MsnSpectrum spectrum = new MsnSpectrum(0, 1, PeakList.Precision.DOUBLE_CONSTANT);

        Assert.assertEquals(-1, spectrum.getSpectrumIndex());

        spectrum.setSpectrumIndex(5);
        Assert.assertEquals(5, spectrum.getSpectrumIndex());
    }

    @Test
    public void testMsnSpectrumCopyConstructor() {

        MsnSpectrum src = new MsnSpectrum();
        src.setComment("comment");
        src.setFragMethod("frag method");
        src.setParentScanNumber(new ScanNumberDiscrete(0));
        src.setSpectrumIndex(1);
        src.setSpectrumSource(URI.create("file://source.mgf"));
        src.addScanNumber(1);
        src.addRetentionTime(new RetentionTimeDiscrete(1, TimeUnit.SECOND));

        MsnSpectrum copy = src.copy(new IdentityPeakProcessor<>());

        Assert.assertEquals("comment", copy.getComment());
        Assert.assertEquals("frag method", copy.getFragMethod());
        Assert.assertEquals(new ScanNumberDiscrete(0), copy.getParentScanNumber());
        Assert.assertEquals(1, copy.getSpectrumIndex());
        Assert.assertEquals(URI.create("file://source.mgf"), copy.getSpectrumSource());
        Assert.assertEquals(new ScanNumberDiscrete(1), copy.getScanNumbers().getFirst());
        Assert.assertEquals(1, src.getRetentionTimes().getFirst().getTime(), 0.1);
    }
}
