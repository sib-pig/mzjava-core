package org.expasy.mzjava.core.ms.cluster;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class FMeasureTest {

    @Test
    public void testAllIntersect() {

        Set<String> expectedCluster = Sets.newHashSet("A1", "A2", "A3", "A4");
        Set<String> actualCluster = Sets.newHashSet("A1", "A2", "A3", "A4");

        int actualOverlap = FMeasure.intersect(expectedCluster, actualCluster);

        Assert.assertEquals(4, actualOverlap, 0);

    }

    @Test
    public void testCalcTotalMembers() {

        //noinspection unchecked
        Collection<HashSet<String>> expectedClusters = Lists.newArrayList(

                Sets.newHashSet("A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10"),
                Sets.newHashSet("B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10", "B11", "B12"),
                Sets.newHashSet("C1", "C2", "C3", "C4", "C5")
        );

        int totalEvent = FMeasure.calcTotalMembers(expectedClusters);

        Assert.assertEquals(27, totalEvent, 0);

    }

    @Test
    public void testIntersect() {

        Set<String> expectedCluster = Sets.newHashSet("A1", "A2", "A3", "A4");
        Set<String> calculatedCluster = Sets.newHashSet("B1", "A2", "B3", "A1");

        int actualOverlap = FMeasure.intersect(expectedCluster, calculatedCluster);

        Assert.assertEquals(2, actualOverlap, 0.000000000001);
    }

    @Test
    public void testDifferentSizeIntersect() {

        Set<String> expectedCluster = Sets.newHashSet("A1", "A2", "A3", "A4");
        Set<String> actualCluster = Sets.newHashSet("B1", "A4");

        int actualOverlap = FMeasure.intersect(expectedCluster, actualCluster);

        Assert.assertEquals(1, actualOverlap, 0);
    }

    /**
     * Test splitting a cluster
     */
    @Test
    public void testCalClusterFMeasure() {

        //noinspection unchecked
        Collection<Set<String>> actualClusters = Lists.<Set<String>>newArrayList(
                Sets.newHashSet("A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12", "A13", "A14"),
                Sets.newHashSet("A15", "A16", "A17", "A18", "A19", "A20")
        );

        //noinspection unchecked
        Collection<Set<String>> expectedClusters = Collections.<Set<String>>singleton(
                Sets.newHashSet("A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12", "A13", "A14", "A15", "A16", "A17", "A18", "A19", "A20")
        );

        double actualF1 = FMeasure.calcFMeasure(expectedClusters, actualClusters);
        Assert.assertEquals(0.8235294117647058, actualF1, 0.000000000001);
    }

    /**
     * Test exactly the same clusters
     */
    @Test
    public void testCalFMeasure2() {

        Collection<Set<String>> expectedClusters = new HashSet<>();
        expectedClusters.add(Sets.newHashSet("A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12", "A13", "A14"));

        Collection<Set<String>> actualClusters = new HashSet<>();
        actualClusters.add(Sets.newHashSet("A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12", "A13", "A14"));

        double actualF1 = FMeasure.calcFMeasure(expectedClusters, actualClusters);
        Assert.assertEquals((double) 1, actualF1, 0.000000000001);

    }

    /**
     * Test merging clusters
     */
    @Test
    public void testCalFMeasure3() {

        //noinspection unchecked
        Collection<Set<String>> expectedClusters = Lists.<Set<String>>newArrayList(
                Sets.newHashSet("A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12", "A13", "A14", "A15", "A16", "A17", "A18", "A19", "A20"),
                Sets.newHashSet("B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10")

        );

        Collection<Set<String>> actualClusters = new HashSet<>();
        actualClusters.add(Sets.newHashSet("A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12", "A13", "A14", "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10"));

        double actualF1 = FMeasure.calcFMeasure(expectedClusters, actualClusters);
        double expectedF1 = 0.6203208556149733;

        Assert.assertEquals(expectedF1, actualF1, 0.000000000001);

    }

    /**
     * Test one event in wrong cluster
     */
    @Test
    public void testCalFMeasure4() {

        //noinspection unchecked
        Collection<Set<String>> expectedClusters = Lists.<Set<String>>newArrayList(
                Sets.newHashSet("A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12", "A13", "A14"),
                Sets.newHashSet("B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10")
        );

        //noinspection unchecked
        Collection<Set<String>> actualClusters = Lists.<Set<String>>newArrayList(
                Sets.newHashSet("A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12", "A13", "A14"),
                Sets.newHashSet("A1", "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10")
        );

        double actualF1 = FMeasure.calcFMeasure(expectedClusters, actualClusters);
        double expectedF1 = 0.9585537918871252;

        Assert.assertEquals(expectedF1, actualF1, 0.000000000001);

    }

    @Test
    public void testCalFMeasure5() {

        //noinspection unchecked
        Collection<Set<String>> expectedClusters = Lists.<Set<String>>newArrayList(
                Sets.newHashSet("A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12", "A13", "A14"),
                Sets.newHashSet("B1")
        );

        Collection<Set<String>> actualCluster = new HashSet<>();
        actualCluster.add(Sets.newHashSet("B1", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12", "A13", "A14"));

        double actualF1 = FMeasure.calcFMeasure(expectedClusters, actualCluster);

        Assert.assertEquals(0.9094827586206897, actualF1, 0.000000000001);

    }

    @Test
    public void testCalFMeasure6() {


        //noinspection unchecked
        Collection<Set<String>> expectedClusters = Lists.<Set<String>>newArrayList(
                Sets.newHashSet("A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12", "A13", "A14", "A15", "A16", "A17", "A18", "A19", "A20", "A21", "A22", "A23", "A24", "A25", "A26", "A27", "A28", "A29", "A30", "A31", "A32", "A33", "A34", "A35", "A36", "A37", "A38", "A39", "A40", "A41", "A42", "A43", "A44", "A45", "A46", "A47"),
                Sets.newHashSet("B1")
        );

        Collection<Set<String>> actualClusters = new HashSet<>();
        actualClusters.add(Sets.newHashSet("B1", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12", "A13", "A14", "A15", "A16", "A17", "A18", "A19", "A20", "A21", "A22", "A23", "A24", "A25", "A26", "A27", "A28", "A29", "A30", "A31", "A32", "A33", "A34", "A35", "A36", "A37", "A38", "A39", "A40", "A41", "A42", "A43", "A44", "A45", "A46", "A47"));

        double actualF1 = FMeasure.calcFMeasure(expectedClusters, actualClusters);

        Assert.assertEquals(0.9697099892588615, actualF1, 0.000000000001);

    }

    @Test
    public void testCalFMeasure8() {

        //noinspection unchecked
        Collection<Set<String>> expectedClusters = Lists.<Set<String>>newArrayList(
                Sets.newHashSet("A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10"),
                Sets.newHashSet("B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10", "B11", "B12"),
                Sets.newHashSet("C1", "C2", "C3", "C4", "C5")
        );

        //noinspection unchecked
        Collection<Set<String>> actualClusters = Lists.<Set<String>>newArrayList(
                Sets.newHashSet("A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10"),
                Sets.newHashSet("B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10", "B11", "B12", "C1", "C2", "C3", "C4", "C5")
        );

        double calculatedFMeasure = FMeasure.calcFMeasure(expectedClusters, actualClusters);

        Assert.assertEquals(0.822361546499477, calculatedFMeasure, 0.000000000001);
    }

    @Test
    public void testCalFMeasure9() {

        //noinspection unchecked
        Collection<Set<String>> expectedClusters = Lists.<Set<String>>newArrayList(
                Sets.newHashSet("A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10"),
                Sets.newHashSet("B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10", "B11", "B12"),
                Sets.newHashSet("C1", "C2", "C3", "C4", "C5")
        );

        //noinspection unchecked
        Collection<Set<String>> actualClusters = Lists.<Set<String>>newArrayList(
                Sets.newHashSet("A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10"),
                Sets.newHashSet("B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10", "B11", "B12"),
                Sets.newHashSet("C1", "C2", "C3", "C4", "C5"),
                Sets.newHashSet("A1")
        );

        double calculatedFMeasure = FMeasure.calcFMeasure(expectedClusters, actualClusters);

        Assert.assertEquals(0.9805068226120860, calculatedFMeasure, 0.000000000001);
    }
}