/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor;

import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.map.hash.TIntObjectHashMap;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.spectrum.MockPeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class DelayedPeakPairProcessorTest {

    @Test
    public void testAnnotations() throws Exception {

        @SuppressWarnings("unchecked") //Mockito can't mock generics
        PeakPairSink<MockPeakAnnotation, MockPeakAnnotation> sink = Mockito.mock(PeakPairSink.class);

        List<MockPeakAnnotation> xAnnotations1 = Arrays.asList(
                new MockPeakAnnotation(IonType.y, 2, 5),
                new MockPeakAnnotation(IonType.b, 4, 2)
                );

        List<MockPeakAnnotation> yAnnotations1 = Arrays.asList(
                new MockPeakAnnotation(IonType.y, 2, 5),
                new MockPeakAnnotation(IonType.b, 3, 4)
        );

        List<MockPeakAnnotation> xAnnotations2 = Arrays.asList(
                new MockPeakAnnotation(IonType.y, 2, 7),
                new MockPeakAnnotation(IonType.y, 2, 8)
        );

        List<MockPeakAnnotation> yAnnotations2 = Arrays.asList(
                new MockPeakAnnotation(IonType.y, 2, 8),
                new MockPeakAnnotation(IonType.b, 2, 4)
        );

        MockDelayedPeakPairProcessor processor = new MockDelayedPeakPairProcessor();
        processor.setSink(sink);

        processor.begin(null, null);

        processor.processPeakPair(12, 1, 2, xAnnotations1, yAnnotations1);
        processor.processPeakPair(15, 3, 4, xAnnotations2, yAnnotations2);

        processor.end();

        TIntObjectHashMap<List<MockPeakAnnotation>> xAnnotationMap = processor.xAnnotationMap;
        TIntObjectHashMap<List<MockPeakAnnotation>> yAnnotationMap = processor.yAnnotationMap;

        Assert.assertEquals(2, xAnnotationMap.size());
        Assert.assertEquals(2, yAnnotationMap.size());

        Assert.assertEquals(xAnnotations1, xAnnotationMap.get(0));
        Assert.assertEquals(yAnnotations1, yAnnotationMap.get(0));

        Assert.assertEquals(xAnnotations2, xAnnotationMap.get(1));
        Assert.assertEquals(yAnnotations2, yAnnotationMap.get(1));

        //Mockito can't mock generics
        //noinspection unchecked
        Mockito.verify(sink).begin(Mockito.any(PeakList.class), Mockito.any(PeakList.class));
        Mockito.verify(sink).end();
    }

    @Test
    public void testGetAnnotations() throws Exception {

        final MockPeakAnnotation annotation1 = new MockPeakAnnotation(IonType.y, 2, 3);
        final MockPeakAnnotation annotation2 = new MockPeakAnnotation(IonType.z, 2, 4);

        TIntObjectHashMap<List<MockPeakAnnotation>> yAnnotationMap = new TIntObjectHashMap<List<MockPeakAnnotation>>();
        yAnnotationMap.put(12, Arrays.asList(annotation1, annotation2));

        DelayedPeakPairProcessor<MockPeakAnnotation, MockPeakAnnotation> processor = new MockDelayedPeakPairProcessor();

        List<? extends MockPeakAnnotation> annotations = processor.getAnnotations(12, yAnnotationMap);

        Assert.assertEquals(2, annotations.size());
        Assert.assertEquals(annotation1, annotations.get(0));
        Assert.assertEquals(annotation2, annotations.get(1));
    }

    private static class MockDelayedPeakPairProcessor extends DelayedPeakPairProcessor<MockPeakAnnotation, MockPeakAnnotation> {

        private TIntObjectHashMap<List<MockPeakAnnotation>> xAnnotationMap;
        private TIntObjectHashMap<List<MockPeakAnnotation>> yAnnotationMap;

        @Override
        protected void process(TDoubleArrayList centroids, TDoubleArrayList vectorX, TDoubleArrayList vectorY, TIntObjectHashMap<List<MockPeakAnnotation>> xAnnotationMap, TIntObjectHashMap<List<MockPeakAnnotation>> yAnnotationMap) {

            this.xAnnotationMap = xAnnotationMap;
            this.yAnnotationMap = yAnnotationMap;
        }
    }
}
