/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.peaklist.peakfilter;

import org.expasy.mzjava.core.ms.peaklist.MockPeakSink;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class NPeaksFilterTest {

    @Test
    public void test() {

        NPeaksFilter<PeakAnnotation> filter = new NPeaksFilter<PeakAnnotation>(3);

        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();

        filter.setSink(sink);

        List<PeakAnnotation> annotations = Collections.emptyList();

        filter.start(7);
        filter.processPeak(1, 150, annotations);
        filter.processPeak(2, 10, annotations);
        filter.processPeak(3, 1, annotations);
        filter.processPeak(4, 100, annotations);
        filter.processPeak(5, 200, annotations);
        filter.processPeak(6, 5, annotations);
        filter.processPeak(7, 1000, annotations);
        filter.end();

        Assert.assertArrayEquals(new double[]{1, 5, 7}, sink.getMzList(), 0.00001);
        Assert.assertArrayEquals(new double[]{150, 200, 1000}, sink.getIntensityList(), 0.000001);

        Assert.assertEquals(1, sink.getStartCalled());
        Assert.assertEquals(1, sink.getEndCalled());
    }

    @Test
    public void test2() {

        NPeaksFilter<PeakAnnotation> filter = new NPeaksFilter<PeakAnnotation>(3);

        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();

        filter.setSink(sink);

        List<PeakAnnotation> annotations = Collections.emptyList();

        filter.start(4);
        filter.processPeak(1, 4, annotations);
        filter.processPeak(2, 7, annotations);
        filter.processPeak(3, 10, annotations);
        filter.processPeak(4, 5, annotations);
        filter.end();

        Assert.assertArrayEquals(new double[]{2, 3, 4}, sink.getMzList(), 0.00001);
        Assert.assertArrayEquals(new double[]{7, 10, 5}, sink.getIntensityList(), 0.00001);
    }

    @Test
    public void test3() {

        NPeaksFilter<PeakAnnotation> filter = new NPeaksFilter<PeakAnnotation>(3);

        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();

        filter.setSink(sink);

        List<PeakAnnotation> annotations = Collections.emptyList();

        filter.start(7);
        filter.processPeak(1, 150, annotations);
        filter.processPeak(2, 10, annotations);
        filter.processPeak(3, 1, annotations);
        filter.processPeak(4, 100, annotations);
        filter.processPeak(5, 100, annotations);
        filter.processPeak(6, 5, annotations);
        filter.processPeak(7, 1000, annotations);
        filter.end();

        Assert.assertArrayEquals(new double[]{1, 4, 7}, sink.getMzList(), 0.000001);
        Assert.assertArrayEquals(new double[]{150, 100, 1000}, sink.getIntensityList(), 0.000001);
    }
}
