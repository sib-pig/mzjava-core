/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.peaklist.peakfilter;

import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.IdentityPeakProcessor;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.when;

/**
 * @author mmueller
 * @version 1.0
 */
public class BinnedSpectrumFilterTest {

    private double[] masses =
            new double[] {1, 2, 3, 4.6,  5, 6, 7, 8, 9, 10, 10.3, 11.5, 13, 14, 15.7, 16};
    private double[] intensities =
            new double[] {2, 4, 6,   8, 10, 1, 3, 5, 8,  9,    6,    8, 10,  1,    3,  5};


    @Test
    public void testOneFilter1() {

        DoublePeakList<PeakAnnotation> expPeakList = new DoublePeakList<>();
        expPeakList.addSorted(masses, intensities, masses.length);
        PeakList<PeakAnnotation> newPL = expPeakList.copy(new IdentityPeakProcessor<>());
        BinnedSpectrumFilter<PeakAnnotation> binnedSpectrumFilter = new BinnedSpectrumFilter<>(0, 20.0, 2.0, BinnedSpectrumFilter.IntensityMode.SUM);
        newPL.apply(binnedSpectrumFilter);
        final double[] ms = newPL.getMzs(null);
        final double[] intens = newPL.getIntensities(null);
        Assert.assertTrue(Arrays.equals(new double[]{1.0, 3.0, 5.0, 7.0, 9.0, 11.0, 13.0, 15.0, 17.0, 19.0}, ms));
        Assert.assertTrue(Arrays.equals(new double[]{2.0,10.0,18.0, 4.0,13.0, 23.0, 10.0,  4.0,  5.0,  0.0}, intens));
    }

    @Test
    public void testOneFilterHighest() {

        DoublePeakList<PeakAnnotation> expPeakList = new DoublePeakList<>();
        expPeakList.addSorted(masses, intensities, masses.length);
        PeakList<PeakAnnotation> newPL = expPeakList.copy(new IdentityPeakProcessor<>());
        BinnedSpectrumFilter<PeakAnnotation> binnedSpectrumFilter = new BinnedSpectrumFilter<>(0, 20.0, 2.0, BinnedSpectrumFilter.IntensityMode.HIGHEST);
        newPL.apply(binnedSpectrumFilter);
        final double[] ms = newPL.getMzs(null);
        final double[] intens = newPL.getIntensities(null);
        Assert.assertArrayEquals(new double[]{1.0, 3.0, 5.0, 7.0, 9.0, 11.0, 13.0, 15.0, 17.0, 19.0}, ms, 0.0000001);
        Assert.assertArrayEquals(new double[]{2.0, 6.0, 10.0, 3.0, 8.0, 9.0, 10.0, 3.0, 5.0, 0.0}, intens, 0.0000001);
    }

    @Test
    public void testOneFilterAverage() {

        DoublePeakList<PeakAnnotation> expPeakList = new DoublePeakList<>();
        expPeakList.addSorted(masses, intensities, masses.length);
        PeakList<PeakAnnotation> newPL = expPeakList.copy(new IdentityPeakProcessor<>());
        BinnedSpectrumFilter<PeakAnnotation> binnedSpectrumFilter = new BinnedSpectrumFilter<>(0, 20.0, 2.0, BinnedSpectrumFilter.IntensityMode.AVG);
        newPL.apply(binnedSpectrumFilter);
        final double[] ms = newPL.getMzs(null);
        final double[] intens = newPL.getIntensities(null);
        Assert.assertArrayEquals(new double[]{1.0, 3.0, 5.0, 7.0, 9.0, 11.0, 13.0, 15.0, 17.0, 19.0}, ms, 0.0000001);
        Assert.assertArrayEquals(new double[]{2.0, 10.0, 18.0, 4.0, 13.0, 23.0, 10.0, 4.0, 5.0, 0.0}, intens, 0.0000001);
    }

    @Test
    public void annotationTest() {

        PeakList<PeakAnnotation> peakList = new DoublePeakList<>();
        peakList.add(21.520550658199998, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(30.525833001199995, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(35.5180079692, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(35.5362007222, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(42.033825208699994, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(44.5232903122, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(49.5154652802, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(56.54709975920001, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(58.520747623199995, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(60.04438989469999, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(65.06037430970001, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(70.0287398307, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(70.0651253367, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(79.5498394132, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(88.0393045167, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(88.0631139637, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(93.03147948469999, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(98.0236544527, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(102.03676182769999, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(112.08692341070002, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(116.0342191387, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(128.0762213387, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(129.11347251170002, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(136.5894958892, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(141.55786141020002, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(150.56314375320002, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(158.09240271870001, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(171.1002277507, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(171.5922355432, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(175.11895181970002, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(180.1055100937, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(185.05568286169998, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(203.0662475477, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(228.6136992662, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(229.1057070587, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(237.6189816092, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(255.1451665697, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(272.1717156707, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(282.1084467127, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(300.1190113987, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(341.19317939369995, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(342.17719497869996, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(359.20374407969996, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(456.22012242470004, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(457.20413800970005, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(474.23068711070005, 1.0, Collections.singletonList(mockPeakAnnotation()));

        DoublePeakList<PeakAnnotation> expPeakList = new DoublePeakList<>();
        int annotCnt = 0;
        for (int i = 0; i < peakList.size(); i++ ) {
            expPeakList.add(peakList.getMz(i), peakList.getIntensity(i), peakList.getAnnotations(i));
            annotCnt += peakList.getAnnotations(i).size();
        }
        PeakList<PeakAnnotation> newPL = expPeakList.copy(new IdentityPeakProcessor<>());
        BinnedSpectrumFilter<PeakAnnotation> binnedSpectrumFilter = new BinnedSpectrumFilter<>(0.0, 500.0, 10.0, BinnedSpectrumFilter.IntensityMode.SUM);
        newPL.apply(binnedSpectrumFilter);

        int[] indexes = newPL.getAnnotationIndexes();
        int annotCntNew = 0;
        for (int indexe : indexes) {
            annotCntNew += newPL.getAnnotations(indexe).size();
        }

        Assert.assertEquals(annotCnt,annotCntNew);
    }

    private PeakAnnotation mockPeakAnnotation() {

        PeakAnnotation peakAnnotation = Mockito.mock(PeakAnnotation.class);
        when(peakAnnotation.copy()).thenReturn(peakAnnotation);
        return peakAnnotation;
    }
}

