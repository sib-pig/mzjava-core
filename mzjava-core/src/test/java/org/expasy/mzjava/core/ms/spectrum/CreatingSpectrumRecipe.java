package org.expasy.mzjava.core.ms.spectrum;

import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.junit.Assert;
import org.junit.Test;

import java.net.URI;

// # $RECIPE$ $NUMBER$ - Creating Spectrum #
//
// ## $PROBLEM$ ##
// You want to make instance of *Spectrum*.
//
// ## $SOLUTION$ ##
//
// *Spectrum* is a *PeakList*. It is an abstract class that wraps a *PeakList*.
// Actually, 3 implementations are provided.
// <ol>
//   <li>*MsnSpectrum* represents spectrum scan</li>
//   <li>*PeptideSpectrum* represents peptide spectrum match (PSM)</li>
//   <li>*LibrarySpectrum* represents spectrum spectrum match (SSM) for building spectral library</li>
// </ol>
public class CreatingSpectrumRecipe {

// A *Spectrum* is a *PeakList* with experimental spectrum information
    @Test
    public void creatingMsnSpectrum() {
        //<SNIP>
        MsnSpectrum spectrum = new MsnSpectrum(PeakList.Precision.DOUBLE);
        spectrum.setSpectrumSource(URI.create("file:/spectra.mgf"));
        spectrum.setComment("my first spectrum");
        spectrum.addRetentionTimes(new RetentionTimeList(new RetentionTimeDiscrete(0.57, TimeUnit.SECOND)));
        //</SNIP>

        Assert.assertNotNull(spectrum);
    }

// ## $DISCUSSION$ ##
//
//
// ## $RELATED$ ##

// $CreatingPeakListRecipe$ describes how to add and access peaks.
//
// $CreatingMsReaderRecipe$ show you how to use our MS readers.
//
// $CreatingPeptideSpectrumRecipe$ explains how to generate theoretical peptide spectrum.
}