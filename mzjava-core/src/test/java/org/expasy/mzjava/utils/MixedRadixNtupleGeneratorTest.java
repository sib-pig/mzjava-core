package org.expasy.mzjava.utils;

import com.google.common.base.Predicate;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author fnikitin
 * Date: 11/1/13
 */
public class MixedRadixNtupleGeneratorTest {

    @Test
    public void testGenerateSameRadixTuples() {

        MixedRadixNtupleGenerator.NtupleContainer container = new MixedRadixNtupleGenerator.NtupleContainer();

        MixedRadixNtupleGenerator factory = new MixedRadixNtupleGenerator(container);

        factory.generate(3, 2);

        List<int[]> ntuples = container.getNtuples();

        Assert.assertEquals((int) Math.pow(2, 3), ntuples.size());

        Assert.assertArrayEquals(new int[] {0, 0, 0}, ntuples.get(0));
        Assert.assertArrayEquals(new int[] {0, 0, 1}, ntuples.get(1));
        Assert.assertArrayEquals(new int[] {0, 1, 0}, ntuples.get(2));
        Assert.assertArrayEquals(new int[] {0, 1, 1}, ntuples.get(3));
        Assert.assertArrayEquals(new int[] {1, 0, 0}, ntuples.get(4));
        Assert.assertArrayEquals(new int[] {1, 0, 1}, ntuples.get(5));
        Assert.assertArrayEquals(new int[] {1, 1, 0}, ntuples.get(6));
        Assert.assertArrayEquals(new int[] {1, 1, 1}, ntuples.get(7));
    }

    @Test
    public void testGenerateSameRadixTuplesWithPredicate1() {

        MixedRadixNtupleGenerator.NtupleContainer container = newContainer(1);

        MixedRadixNtupleGenerator generator = new MixedRadixNtupleGenerator(container);

        generator.generate(3, 2);

        List<int[]> ntuples = container.getNtuples();

        Assert.assertEquals(4, ntuples.size());

        Assert.assertArrayEquals(new int[] {0, 0, 0}, ntuples.get(0));
        Assert.assertArrayEquals(new int[] {0, 0, 1}, ntuples.get(1));
        Assert.assertArrayEquals(new int[] {0, 1, 0}, ntuples.get(2));
        Assert.assertArrayEquals(new int[] {1, 0, 0}, ntuples.get(3));
    }

    @Test
    public void testGenerateSameRadixTuplesWithLimit2() {

        MixedRadixNtupleGenerator.NtupleContainer container = newContainer(2);

        MixedRadixNtupleGenerator generator = new MixedRadixNtupleGenerator(container);

        generator.generate(3, 2);

        List<int[]> ntuples = container.getNtuples();

        Assert.assertEquals(7, ntuples.size());

        Assert.assertArrayEquals(new int[] {0, 0, 0}, ntuples.get(0));
        Assert.assertArrayEquals(new int[] {0, 0, 1}, ntuples.get(1));
        Assert.assertArrayEquals(new int[] {0, 1, 0}, ntuples.get(2));
        Assert.assertArrayEquals(new int[] {0, 1, 1}, ntuples.get(3));
        Assert.assertArrayEquals(new int[] {1, 0, 0}, ntuples.get(4));
        Assert.assertArrayEquals(new int[] {1, 0, 1}, ntuples.get(5));
        Assert.assertArrayEquals(new int[] {1, 1, 0}, ntuples.get(6));
    }

    @Test
    public void testGenerateMixedRadixTuples() {

        MixedRadixNtupleGenerator.NtupleContainer container = new MixedRadixNtupleGenerator.NtupleContainer();

        MixedRadixNtupleGenerator generator = new MixedRadixNtupleGenerator(container);

        generator.generate(new int[]{3, 2, 2});

        List<int[]> ntuples = container.getNtuples();

        Assert.assertEquals(12, ntuples.size());

        Assert.assertArrayEquals(new int[] {0, 0, 0}, ntuples.get(0));
        Assert.assertArrayEquals(new int[] {0, 0, 1}, ntuples.get(1));
        Assert.assertArrayEquals(new int[] {0, 1, 0}, ntuples.get(2));
        Assert.assertArrayEquals(new int[] {0, 1, 1}, ntuples.get(3));
        Assert.assertArrayEquals(new int[] {1, 0, 0}, ntuples.get(4));
        Assert.assertArrayEquals(new int[] {1, 0, 1}, ntuples.get(5));
        Assert.assertArrayEquals(new int[] {1, 1, 0}, ntuples.get(6));
        Assert.assertArrayEquals(new int[] {1, 1, 1}, ntuples.get(7));
        Assert.assertArrayEquals(new int[] {2, 0, 0}, ntuples.get(8));
        Assert.assertArrayEquals(new int[] {2, 0, 1}, ntuples.get(9));
        Assert.assertArrayEquals(new int[] {2, 1, 0}, ntuples.get(10));
        Assert.assertArrayEquals(new int[] {2, 1, 1}, ntuples.get(11));
    }

    @Test
    public void testGenerateMixedRadixTuplesWithPredicate1() {

        int[] radices = new int[] {3, 2, 2};

        MixedRadixNtupleGenerator.NtupleContainer container = newContainer(1);
        MixedRadixNtupleGenerator generator = new MixedRadixNtupleGenerator(container);

        generator.generate(radices);

        List<int[]> ntuples = container.getNtuples();

        Assert.assertEquals(5, ntuples.size());

        Assert.assertArrayEquals(new int[] {0, 0, 0}, ntuples.get(0));
        Assert.assertArrayEquals(new int[] {0, 0, 1}, ntuples.get(1));
        Assert.assertArrayEquals(new int[] {0, 1, 0}, ntuples.get(2));
        Assert.assertArrayEquals(new int[] {1, 0, 0}, ntuples.get(3));
        Assert.assertArrayEquals(new int[] {2, 0, 0}, ntuples.get(4));
    }

    @Test
    public void testGenerateMixedRadixTuplesWithPredicate2() {

        int[] radices = new int[] {3, 2, 2};

        MixedRadixNtupleGenerator.NtupleContainer container = newContainer(2);
        MixedRadixNtupleGenerator generator = new MixedRadixNtupleGenerator(container);

        generator.generate(radices);

        List<int[]> ntuples = container.getNtuples();

        Assert.assertEquals(10, ntuples.size());

        Assert.assertArrayEquals(new int[] {0, 0, 0}, ntuples.get(0));
        Assert.assertArrayEquals(new int[] {0, 0, 1}, ntuples.get(1));
        Assert.assertArrayEquals(new int[] {0, 1, 0}, ntuples.get(2));
        Assert.assertArrayEquals(new int[] {0, 1, 1}, ntuples.get(3));
        Assert.assertArrayEquals(new int[] {1, 0, 0}, ntuples.get(4));
        Assert.assertArrayEquals(new int[] {1, 0, 1}, ntuples.get(5));
        Assert.assertArrayEquals(new int[] {1, 1, 0}, ntuples.get(6));
        Assert.assertArrayEquals(new int[] {2, 0, 0}, ntuples.get(7));
        Assert.assertArrayEquals(new int[] {2, 0, 1}, ntuples.get(8));
        Assert.assertArrayEquals(new int[] {2, 1, 0}, ntuples.get(9));
    }

    @Test
    public void testTuplesCount() {

        int[] radices = new int[] {3, 2, 2};

        Assert.assertEquals(12, MixedRadixNtupleGenerator.countNtuples(radices));
    }

    private static MixedRadixNtupleGenerator.NtupleContainer newContainer(final int upper) {

        MixedRadixNtupleGenerator.NtupleContainer container =

                new MixedRadixNtupleGenerator.NtupleContainer(new Predicate<int[]>() {
                    @Override
                    public boolean apply(int[] ints) {

                        return countNonZeroElements(ints) <= upper;
                    }
                });

        return container;
    }

    private static int countNonZeroElements(int[] tuple) {

        checkNotNull(tuple);

        int count = 0;
        for (int element : tuple) {

            if (element > 0)
                count++;
        }
        return count;
    }
}
