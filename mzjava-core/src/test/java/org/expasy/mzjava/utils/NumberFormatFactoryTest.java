package org.expasy.mzjava.utils;


import org.junit.Assert;
import org.junit.Test;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;


public class NumberFormatFactoryTest {
	
	@Test
	public void testNewLocaleInstance() {
		NumberFormat df = DecimalFormat.getInstance(Locale.FRANCE);
		
		Assert.assertEquals("1,329", df.format(1.328984366538442783));
	}
	
	@Test
	public void testNewLocaleInstanceWithPattern() {
		DecimalFormatSymbols frSymbols =
		    new DecimalFormatSymbols(Locale.FRANCE);
		
		DecimalFormat df = new DecimalFormat("0.0000", frSymbols);
		
		Assert.assertEquals("1,3290", df.format(1.328984366538442783));
	}
	
	@Test
	public void testDecimalFormatFactory() {
		NumberFormat df = NumberFormatFactory.getInstance();
		
		Assert.assertEquals("1.33", df.format(1.328984366538442783));
	}
	
	@Test
	public void testDecimalFormatFactoryWithPrecision() {
		NumberFormat df = NumberFormatFactory.valueOf(4);
		
		Assert.assertEquals("1.3290", df.format(1.328984366538442783));
	}
	
	@Test
	public void testDecimalFractionalPart() {
		NumberFormat df = NumberFormatFactory.valueOf(3);
		Assert.assertEquals("0.034", df.format(0.0342345678));
	}
	
	@Test
	public void testDecimalIntegerPart() {
		NumberFormat df = NumberFormatFactory.valueOf(4, 0);
		
		Assert.assertEquals("1329", df.format(1328.984366538442783));
	}
	
	@Test
	public void testDecimalIntegerAndFractionalPart() {
		NumberFormat df = NumberFormatFactory.valueOf(4, 4);
		
		Assert.assertEquals("1328.9844", df.format(1328.984366538442783));
	}
	
	@Test
	public void testDecimal() {
		NumberFormat dec = NumberFormatFactory.valueOf(2);
		System.out.println(dec.format(0.001));
	}

	@Test
	public void testInteger() {
		NumberFormat nf = NumberFormatFactory.INT_PRECISION;

		Assert.assertEquals("13", nf.format(12.984366538442783));
	}

	@Test
	public void testInteger2() {
		NumberFormat nf = NumberFormatFactory.valueOf(1, 0);

		Assert.assertEquals("13", nf.format(12.984366538442783));
	}

	@Test
	public void testInteger3() {
		NumberFormat nf = NumberFormatFactory.valueOf(4, 0);

		Assert.assertEquals("0013", nf.format(12.984366538442783));
	}

	@Test
	public void testInteger4() {
		NumberFormat nf = NumberFormatFactory.INT_PRECISION;

		Assert.assertEquals("12984367", nf.format(12984366.538442783));
	}
}
