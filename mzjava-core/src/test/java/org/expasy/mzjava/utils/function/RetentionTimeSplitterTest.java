package org.expasy.mzjava.utils.function;

import org.expasy.mzjava.core.ms.spectrum.*;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

public class RetentionTimeSplitterTest {

    @Test
    public void testSplit() throws Exception {

        final MsnSpectrum spectrum1 = mockSpectrum(1);
        final MsnSpectrum spectrum2 = mockSpectrum(1.4);

        final MsnSpectrum spectrum3 = mockSpectrum(1.9);
        final MsnSpectrum spectrum4 = mockSpectrum(2.0);
        final MsnSpectrum spectrum5 = mockSpectrum(2.49999);

        //noinspection unchecked
        List<MsnSpectrum> spectra = Arrays.asList(
                spectrum1,
                spectrum2,
                spectrum3,
                spectrum4,
                spectrum5
        );

        RetentionTimeSplitter spliterator = new RetentionTimeSplitter(new RetentionTimeDiscrete(0.5, TimeUnit.SECOND));

        List<List<MsnSpectrum>> splits = spliterator.split(spectra);
        Assert.assertEquals(2, splits.size());
        //noinspection unchecked
        Assert.assertEquals(Arrays.asList(spectrum1, spectrum2), splits.get(0));
        //noinspection unchecked
        Assert.assertEquals(Arrays.asList(spectrum3, spectrum4, spectrum5), splits.get(1));
    }

    @Test(expected = IllegalStateException.class)
    public void testSplitUnsorted() throws Exception {

        final MsnSpectrum spectrum1 = mockSpectrum(6);
        final MsnSpectrum spectrum2 = mockSpectrum(1.4);

        //noinspection unchecked
        List<MsnSpectrum> spectra = Arrays.asList(
                spectrum1,
                spectrum2
        );

        RetentionTimeSplitter spliterator = new RetentionTimeSplitter(new RetentionTimeDiscrete(0.5, TimeUnit.SECOND));

        spliterator.split(spectra);
    }

    @Test
    public void testSplitEmpty() throws Exception {

        //noinspection unchecked
        List<MsnSpectrum> spectra = new ArrayList<MsnSpectrum>();

        RetentionTimeSplitter spliterator = new RetentionTimeSplitter(new RetentionTimeDiscrete(0.5, TimeUnit.SECOND));

        List<List<MsnSpectrum>> splits = spliterator.split(spectra);
        Assert.assertEquals(1, splits.size());
        Assert.assertSame(spectra, splits.get(0));
    }

    private MsnSpectrum mockSpectrum(double retentionTime) {

        MsnSpectrum spectrum = Mockito.mock(MsnSpectrum.class);
        when(spectrum.getRetentionTimes()).thenReturn(new RetentionTimeList(new RetentionTimeDiscrete(retentionTime, TimeUnit.SECOND)));
        //noinspection unchecked
        return spectrum;
    }
}