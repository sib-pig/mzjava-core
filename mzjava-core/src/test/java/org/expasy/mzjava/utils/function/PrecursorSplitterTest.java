package org.expasy.mzjava.utils.function;

import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.FloatConstantPeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PrecursorSplitterTest {

    @Test
    public void testSplit() throws Exception {

        PeakList pl1 = newPeakList(100, 1);
        PeakList pl2 = newPeakList(100.2999, 1);

        PeakList pl3 = newPeakList(100.59991, 1);

        PeakList pl4 = newPeakList(100, 2);
        PeakList pl5 = newPeakList(100.2999, 2);

        PeakList pl6 = newPeakList(100.59991, 2);

        ArrayList<PeakList> input = new ArrayList<PeakList>();
        input.add(pl1);
        input.add(pl2);
        input.add(pl3);
        input.add(pl4);
        input.add(pl5);
        input.add(pl6);

        PrecursorSplitter spliterator = new PrecursorSplitter(new AbsoluteTolerance(0.3));

        List<List<PeakList>> splits = spliterator.split(input);
        Assert.assertEquals(4, splits.size());

        Assert.assertEquals(Arrays.asList(pl1, pl2), splits.get(0));
        Assert.assertEquals(Arrays.asList(pl3), splits.get(1));
        Assert.assertEquals(Arrays.asList(pl4, pl5), splits.get(2));
        Assert.assertEquals(Arrays.asList(pl6), splits.get(3));
    }

    @Test
    public void testSplitEmpty() throws Exception {

        ArrayList<PeakList> input = new ArrayList<PeakList>();

        PrecursorSplitter spliterator = new PrecursorSplitter(new AbsoluteTolerance(0.3));

        List<List<PeakList>> splits = spliterator.split(input);
        Assert.assertEquals(1, splits.size());
        Assert.assertSame(input, splits.get(0));
    }

    @Test(expected = IllegalStateException.class)
    public void testSplitUnsorted() throws Exception {

        PeakList pl1 = newPeakList(100.2999, 1);
        PeakList pl2 = newPeakList(100, 1);

        ArrayList<PeakList> input = new ArrayList<PeakList>();
        input.add(pl1);
        input.add(pl2);

        PrecursorSplitter spliterator = new PrecursorSplitter(new AbsoluteTolerance(0.3));

        spliterator.split(input);
    }

    private PeakList newPeakList(double mz, int charge) {

        FloatConstantPeakList peakList = new FloatConstantPeakList(1);
        peakList.getPrecursor().setMzAndCharge(mz, charge);
        return peakList;
    }
}