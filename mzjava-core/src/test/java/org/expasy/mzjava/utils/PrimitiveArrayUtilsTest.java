package org.expasy.mzjava.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author fnikitin
 * Date: 8/15/12
 */
public class PrimitiveArrayUtilsTest {

    @Test
    public void testGrowFloats() throws Exception {

        float[] floats = new float[5];

        PrimitiveArrayUtils.grow(floats, 5);

        Assert.assertEquals(5, floats.length);
    }

    @Test
    public void testGrowFloats2() throws Exception {

        float[] floats = new float[5];

        floats = PrimitiveArrayUtils.grow(floats, 5);

        Assert.assertEquals(10, floats.length);
    }

    @Test
    public void testGrowDoubles() throws Exception {

        double[] doubles = new double[5];

        PrimitiveArrayUtils.grow(doubles, 5);

        Assert.assertEquals(5, doubles.length);
    }

    @Test
    public void testGrowDoubles2() throws Exception {

        double[] doubles = new double[5];

        doubles = PrimitiveArrayUtils.grow(doubles, 5);

        Assert.assertEquals(10, doubles.length);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testGrowFloatsPositiveLoadingFactor() throws Exception {

        PrimitiveArrayUtils.grow(new float[0], -1);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testGrowDoublesPositiveLoadingFactor() throws Exception {

        PrimitiveArrayUtils.grow(new double[0], -1);
    }

    @Test (expected = NullPointerException.class)
    public void testGrowFloatsNotNull() throws Exception {

        PrimitiveArrayUtils.grow((float[])null, 0);
    }

    @Test (expected = NullPointerException.class)
    public void testGrowDoublesNotNull() throws Exception {

        PrimitiveArrayUtils.grow((double[])null, 0);
    }

    @Test
    public void testToDoubles() {

        double[] doubles = new double[]{9, 8, 7, 6, 5, 4, 3, 2, 1};

        Double[] array = PrimitiveArrayUtils.toDoubles(doubles, null);

        Assert.assertArrayEquals(new Double[]{9., 8., 7., 6., 5., 4., 3., 2., 1.}, array);
    }
}
