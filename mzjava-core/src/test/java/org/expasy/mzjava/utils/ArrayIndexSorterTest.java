package org.expasy.mzjava.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author fnikitin
 * Date: 3/4/14
 */
public class ArrayIndexSorterTest {

    @Test
    public void testSortIndexUp() throws Exception {

        Integer[] ints = new Integer[]{9, 8, 7, 6, 5, 4, 3, 2, 1};

        ArrayIndexSorter sorter = new ArrayIndexSorter();

        Integer[] indices = sorter.sort(new IntsIndexComparator(ints));

        Assert.assertArrayEquals(new Integer[]{8, 7, 6, 5, 4, 3, 2, 1, 0}, indices);
    }

    @Test
    public void testSortIndexUpReused() throws Exception {

        Integer[] ints = new Integer[]{9, 8, 7, 6, 5, 4, 3, 2, 1};
        IntsIndexComparator comparator = new IntsIndexComparator(ints);

        ArrayIndexSorter sorter = new ArrayIndexSorter();

        Integer[] indices = sorter.sort(comparator);

        Assert.assertArrayEquals(new Integer[]{8, 7, 6, 5, 4, 3, 2, 1, 0}, indices);

        comparator.setArray(1, 4, 3, 2);

        indices = sorter.sort(comparator);

        Assert.assertArrayEquals(new Integer[]{0, 3, 2, 1}, indices);
    }

    private static class IntsIndexComparator extends ArrayIndexComparator<Integer> {

        public IntsIndexComparator(Integer[] ints) {

            super(ints);
        }

        @Override
        public int compare(Integer i1, Integer i2) {

            return array[i1].compareTo(array[i2]);
        }

    }
}
