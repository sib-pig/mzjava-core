package org.expasy.mzjava.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author markus
 *         Date: 13.06.12
 *         Time: 18:40
 */
public class IntervalListTest {

    IntervalList il;

    @Test
    public void test() {

        il = new IntervalList();

        il.addInterval(400.0, 800);
        il.addInterval(900.0, 1000.0);
        il.addInterval(1168.39, 1210.209);
        il.addInterval(1168.39, 1210.209);
        il.addInterval(1188.39, 1210.209);
        il.addInterval(950.0, 750.0);
        il.addInterval(750.0, 750.0);


        Assert.assertTrue(il.contains(500.0));
        Assert.assertTrue(il.contains(800.0));
        Assert.assertTrue(il.contains(820.0));
        Assert.assertTrue(il.contains(855.0));
        Assert.assertTrue(il.contains(750.0));
        Assert.assertTrue(il.contains(400.00001));
        Assert.assertTrue(il.contains(1179.5));
        Assert.assertFalse(il.contains(399.99999));
        Assert.assertFalse(il.contains(1168.389));
        Assert.assertFalse(il.contains(10000.0));
        Assert.assertFalse(il.contains(-100.0));
    }

    @Test
    public void test2() {

        il = new IntervalList();

        il.addInterval(500.0, 800);
        il.addInterval(-100.0, 100000.0);

        Assert.assertTrue(il.contains(500.0));
        Assert.assertTrue(il.contains(820.0));
        Assert.assertTrue(il.contains(10900.00001));
        Assert.assertTrue(il.contains(1179.5));
        Assert.assertFalse(il.contains(-10000.0));
    }

    @Test
    public void test3() {

        il = new IntervalList();

        for (int i = 0; i < 1000; i++) {
            double r1 = Math.random() * 2000;
            double r2 = Math.random() * 2000;
            il.addInterval(r1, r2);
        }

        il.contains(500.0);
        il.contains(820.0);
        il.contains(10900.00001);
        il.contains(1179.5);
        il.contains(-10000.0);
    }

    public void runBenchmark() {

        class Interval {

            public Interval(double lBound, double rBound) {

                setBounds(lBound, rBound);
            }

            private double lBound;
            private double rBound;

            public boolean contains(double value) {

                return (value >= lBound && value <= rBound);
            }

            public void setBounds(double lBound, double rBound) {

                if (lBound <= rBound) {
                    this.lBound = lBound;
                    this.rBound = rBound;
                } else {
                    this.lBound = rBound;
                    this.rBound = lBound;
                }
            }

            double getLowerBound() {

                return lBound;
            }

            double getUpperBound() {

                return rBound;
            }
        }

        il = new IntervalList();

        int nrIntervals = 1000;
        double dv = 3500.0 / nrIntervals;
        double[] lBounds = new double[nrIntervals];
        double[] uBounds = new double[nrIntervals];
        Interval[] intervals = new Interval[nrIntervals];
        for (int i = 0; i < nrIntervals; i++) {
            il.addInterval(i * dv, i * dv + 10.0);
            lBounds[i] = i * dv;
            uBounds[i] = i * dv + 10.0;
            intervals[i] = new Interval(i * dv, i * dv + 10.0);
        }

        int N = 100000;
        dv = 3900.0 / N;
        long t0 = System.currentTimeMillis();

        for (int i = 0; i < N; i++) {
            double value = i * dv;
            il.contains(value);
        }
        long t1 = System.currentTimeMillis();
        long t = t1 - t0;
        System.out.println("Elapsed time with IntervalList is: " + t + " ms");


        t0 = System.currentTimeMillis();
        for (int i = 0; i < N; i++) {
            double value = i * dv;
            boolean ok;
            for (int k = 0; k < nrIntervals; k++) {
                ok = (value >= lBounds[k] && value <= uBounds[k]);
            }
        }
        t1 = System.currentTimeMillis();
        t = t1 - t0;
        System.out.println("Elapsed time without IntervalList is: " + t + " ms");

        t0 = System.currentTimeMillis();
        for (int i = 0; i < N; i++) {
            double value = i * dv;
            boolean ok;
            for (int k = 0; k < nrIntervals; k++) {
                ok = intervals[k].contains(value);
            }
        }
        t1 = System.currentTimeMillis();
        t = t1 - t0;
        System.out.println("Elapsed time with Interval[] is: " + t + " ms");
    }

    public static void main(String[] args) {

        new IntervalListTest().runBenchmark();
    }
}

