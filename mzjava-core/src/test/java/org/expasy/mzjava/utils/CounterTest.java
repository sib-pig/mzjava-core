package org.expasy.mzjava.utils;

import org.junit.Assert;
import org.junit.Test;

public class CounterTest {

    @Test
    public void testReset() throws Exception {

        Counter counter = new Counter(25);
        counter.reset();
        Assert.assertEquals(0, counter.getCount());
    }

    @Test
    public void testEquals() throws Exception {

        Counter same1 = new Counter(25);
        Counter same2 = new Counter(25);
        Counter different = new Counter(5);

        Assert.assertEquals(true, same1.equals(same2));
        Assert.assertEquals(true, same2.equals(same1));
        Assert.assertEquals(false, different.equals(same1));
    }

    @Test
    public void testHashCode() throws Exception {

        Counter same1 = new Counter(25);
        Counter same2 = new Counter(25);
        Counter different = new Counter(5);

        Assert.assertEquals(same1.hashCode(), same2.hashCode());
        Assert.assertNotEquals(different.hashCode(), same1.hashCode());
    }

    @Test
    public void testIncrement() throws Exception {

        Counter counter = new Counter();
        Assert.assertEquals(0, counter.getCount());

        counter.increment();
        Assert.assertEquals(1, counter.getCount());

        counter.increment();
        Assert.assertEquals(2, counter.getCount());
    }

    @Test
    public void testIncrement1() throws Exception {

        Counter counter = new Counter();
        Assert.assertEquals(0, counter.getCount());

        counter.increment(2);
        Assert.assertEquals(2, counter.getCount());

        counter.increment(4);
        Assert.assertEquals(6, counter.getCount());

        counter.increment(-4);
        Assert.assertEquals(2, counter.getCount());
    }

    @Test
    public void testDecrement() throws Exception {

        Counter counter = new Counter();
        Assert.assertEquals(0, counter.getCount());

        counter.decrement();
        Assert.assertEquals(-1, counter.getCount());

        counter.decrement();
        Assert.assertEquals(-2, counter.getCount());
    }

    @Test
    public void testDecrement1() throws Exception {

        Counter counter = new Counter();
        Assert.assertEquals(0, counter.getCount());

        counter.decrement(2);
        Assert.assertEquals(-2, counter.getCount());

        counter.decrement(4);
        Assert.assertEquals(-6, counter.getCount());

        counter.decrement(-4);
        Assert.assertEquals(-2, counter.getCount());
    }

    @Test
    public void testToString() throws Exception {

        Assert.assertEquals("1", new Counter(1).toString());
        Assert.assertEquals("-1", new Counter(-1).toString());
    }
}