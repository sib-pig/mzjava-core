package org.expasy.mzjava.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * @version sqrt -1
 * @author fnikitin
 * Date: 10/8/12
 */
public class URIBuilderTest {

	@Test
	public void testSimpleURI() {

		URIBuilder builder = new URIBuilder("www.expasy.ch", "liberator");

		Assert.assertEquals("software://www.expasy.ch/liberator", builder.build().toString());
	}

	@Test
	public void testWithVersion() {

		URIBuilder builder = new URIBuilder("www.expasy.ch", "liberator");
		builder.version("1.1");

		Assert.assertEquals("software://www.expasy.ch/liberator:v1.1", builder.build().toString());
	}

	@Test
	public void testWithAll() {

		URIBuilder builder = new URIBuilder("www.expasy.ch", "liberator");
		builder.version("1.1");
		builder.arg(0, "file.pepxml");
		builder.param("precision", "2");
		builder.param("frags", "cz");

		Assert.assertEquals("software://www.expasy.ch/liberator:v1.1?arg0=file.pepxml&precision=2&frags=cz",
				builder.build().toString());
	}
}
