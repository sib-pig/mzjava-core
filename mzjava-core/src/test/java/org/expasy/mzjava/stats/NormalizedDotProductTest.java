package org.expasy.mzjava.stats;

import gnu.trove.list.array.TDoubleArrayList;
import org.junit.Assert;
import org.junit.Test;

public class NormalizedDotProductTest {

    @Test
    public void testCosim() throws Exception {

        Assert.assertEquals(0.0,
                NormalizedDotProduct.cosim(
                        new double[]{1, 0},
                        new double[]{0, 1}
                ), 0.0000001);

        Assert.assertEquals(1.0,
                NormalizedDotProduct.cosim(
                        new double[]{1, 0},
                        new double[]{1, 0}
                ), 0.0000001);

        Assert.assertEquals(1.0,
                NormalizedDotProduct.cosim(
                        new double[]{1, 1},
                        new double[]{0.5, 0.5}
                ), 0.0000001);

        Assert.assertEquals(Double.NaN,
                NormalizedDotProduct.cosim(
                        new double[]{},
                        new double[]{}
                ), 0.0000001);

        Assert.assertEquals(0.5,
                NormalizedDotProduct.cosim(
                        new double[]{1, 1, 1, 1},
                        new double[]{0, 1, 0, 0}
                ), 0.0000001);
    }

    @Test
    public void testCosim1() throws Exception {

        Assert.assertEquals(0.0,
                NormalizedDotProduct.cosim(
                        new TDoubleArrayList(new double[]{1, 0}),
                        new TDoubleArrayList(new double[]{0, 1})
                ), 0.0000001);

        Assert.assertEquals(1.0,
                NormalizedDotProduct.cosim(
                        new TDoubleArrayList(new double[]{1, 0}),
                        new TDoubleArrayList(new double[]{1, 0})
                ), 0.0000001);

        Assert.assertEquals(1.0,
                NormalizedDotProduct.cosim(
                        new TDoubleArrayList(new double[]{1, 1}),
                        new TDoubleArrayList(new double[]{0.5, 0.5})
                ), 0.0000001);

        Assert.assertEquals(Double.NaN,
                NormalizedDotProduct.cosim(
                        new TDoubleArrayList(new double[]{}),
                        new TDoubleArrayList(new double[]{})
                ), 0.0000001);

        Assert.assertEquals(0.5,
                NormalizedDotProduct.cosim(
                        new TDoubleArrayList(new double[]{1, 1, 1, 1}),
                        new TDoubleArrayList(new double[]{0, 1, 0, 0})
                ), 0.0000001);
    }
}