package org.expasy.mzjava.stats;

import gnu.trove.list.TDoubleList;
import gnu.trove.list.array.TDoubleArrayList;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PearsonsCorrelationCoeffTest {

    @Test
    public void testArrayCalcR() throws Exception {

        double[] arr1 = new double[]{8.0, 11.0,  7.0,  6.0, 21.0, 11.0, 2.0, 15.0, 14.0, 23.0,  4.0,  7.0, 18.0, 1.0, 26.0, 3.0, 24.0, 6.0, 12.0};
        double[] arr2 = new double[]{9.0, 21.0, 20.0,  1.0, 23.0, 9.0,  1.0,  7.0, 26.0, 12.0, 16.0,  9.0, 5.0,  7.0, 2.0,  9.0, 9.0,  5.0, 28.0};

        double sim = PearsonsCorrelationCoeff.calcR(arr1, arr2);

        Assert.assertEquals(0.11801410773112014, sim, 0.000000001);
    }

    @Test
    public void testTListCalcR() throws Exception {

        TDoubleList list1 = new TDoubleArrayList(new double[]{8.0, 11.0,  7.0,  6.0, 21.0, 11.0, 2.0, 15.0, 14.0, 23.0,  4.0,  7.0, 18.0, 1.0, 26.0, 3.0, 24.0, 6.0, 12.0});
        TDoubleList list2 = new TDoubleArrayList(new double[]{9.0, 21.0, 20.0,  1.0, 23.0, 9.0,  1.0,  7.0, 26.0, 12.0, 16.0,  9.0, 5.0,  7.0, 2.0,  9.0, 9.0,  5.0, 28.0});

        double sim = PearsonsCorrelationCoeff.calcR(list1, list2);

        Assert.assertEquals(0.11801410773112014, sim, 0.000000001);
    }
}
