/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.stats;

import gnu.trove.list.array.TDoubleArrayList;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class FrequencyTableTest {

    @Test
    public void testToString() throws Exception {

        FrequencyTable table = new FrequencyTable(0.5, "one");

        table.add(0.0);
        table.add(0.1);
        table.add(0.49999999);
        table.add(0.5);
        table.add(9.99999999);
        table.add(10.0);

        String expected = "0.0\t3\n" +
                "0.5\t1\n" +
                "9.5\t1\n" +
                "10.0\t1\n";

        String actual = table.toString();
        Assert.assertEquals(expected, actual);

        Assert.assertEquals(0, table.getMinBinId());
        Assert.assertEquals(20, table.getMaxBinId());
    }

    @Test
    public void testToStringNormalized() throws Exception {

        FrequencyTable table = new FrequencyTable(0.5);

        table.add(0.0);
        table.add(0.1);
        table.add(0.49999999);
        table.add(0.5);
        table.add(9.99999999);
        table.add(10.0);

        String expected = "0.0\t0.5\n" +
                "0.5\t0.16666666666666666\n" +
                "9.5\t0.16666666666666666\n" +
                "10.0\t0.16666666666666666\n";

        String actual = table.toString(true);
        Assert.assertEquals(expected, actual);
    }


    @Test
    public void testToTable() throws Exception {

        FrequencyTable table = new FrequencyTable(0.5, "one");

        table.add(0.0);
        table.add(0.1);
        table.add(0.49999999);
        table.add(0.5);
        table.add(9.99999999);
        table.add(10.0);

        TDoubleArrayList[] data = FrequencyTable.toTable(table);

        Assert.assertEquals(4, data.length);
        checkRow(data[0], 0.0, 3);
        checkRow(data[1], 0.5, 1);
        checkRow(data[2], 9.5, 1);
        checkRow(data[3], 10.0, 1);
    }

    @Test(expected = IllegalStateException.class)
    public void testToTableDiffBinSize() throws Exception {

        FrequencyTable table1 = new FrequencyTable(0.5, "one");
        FrequencyTable table2 = new FrequencyTable(1, "two");

        FrequencyTable.toTable(table1, table2);
    }

    private void checkRow(TDoubleArrayList actual, double... expected) {

        Assert.assertEquals(expected.length, actual.size());
        for(int i = 0; i < expected.length; i++) {

            Assert.assertEquals("index " + i, expected[i], actual.get(i), 0.00000001);
        }
    }

    @Test
    public void testStaticToString() throws Exception {

        FrequencyTable table1 = new FrequencyTable(1, "Table 1");
        table1.add(1);
        table1.add(2);
        table1.add(2);
        table1.add(3);
        table1.add(3);
        table1.add(3);

        FrequencyTable table2 = new FrequencyTable(1, "Table 1");
        table2.add(11);
        table2.add(12);
        table2.add(12);
        table2.add(13);
        table2.add(13);
        table2.add(13);

        Assert.assertEquals("count\tTable 1\tTable 1\n" +
                "1.5\t0.16666666666666666\t\n" +
                "2.5\t0.3333333333333333\t\n" +
                "3.5\t0.5\t\n" +
                "11.5\t0.0\t0.16666666666666666\n" +
                "12.5\t0.0\t0.3333333333333333\n" +
                "13.5\t0.0\t0.5\n" +
                "N\t6\t6",
                FrequencyTable.toString("count", table1, table2));
    }

    @Test(expected = IllegalStateException.class)
    public void testStaticToStringDiffBinSize() throws Exception {

        FrequencyTable table1 = new FrequencyTable(1, "Table 1");
        FrequencyTable table2 = new FrequencyTable(2, "Table 1");

        //noinspection ResultOfMethodCallIgnored
        FrequencyTable.toString("count", table1, table2);
    }


    @Test
    public void testStaticToStringNotNormalized() throws Exception {

        FrequencyTable table1 = new FrequencyTable(1, "Table 1");
        table1.add(1);
        table1.add(2);
        table1.add(2);
        table1.add(3);
        table1.add(3);
        table1.add(3);

        FrequencyTable table2 = new FrequencyTable(1, "Table 1");
        table2.add(11);
        table2.add(12);
        table2.add(12);
        table2.add(13);
        table2.add(13);
        table2.add(13);

        Assert.assertEquals("count\tTable 1\tTable 1\n" +
                        "1.5\t1\t0\n" +
                        "2.5\t2\t0\n" +
                        "3.5\t3\t0\n" +
                        "11.5\t0\t1\n" +
                        "12.5\t0\t2\n" +
                        "13.5\t0\t3",
                FrequencyTable.toStringNotNormalize("count", table1, table2));
    }

    @Test(expected = IllegalStateException.class)
    public void testStaticToStringNotNormalizedDiffBinSize() throws Exception {

        FrequencyTable table1 = new FrequencyTable(1, "Table 1");
        FrequencyTable table2 = new FrequencyTable(2, "Table 1");

        //noinspection ResultOfMethodCallIgnored
        FrequencyTable.toStringNotNormalize("count", table1, table2);
    }

    @Test
    public void testAddAll() throws Exception {

        FrequencyTable table1 = new FrequencyTable(1, "Table 1");
        table1.add(1);
        table1.add(2);
        table1.add(2);
        table1.add(3);
        table1.add(3);
        table1.add(3);

        FrequencyTable table2 = new FrequencyTable(1, "Table 1");
        table2.add(1);
        table2.add(2);
        table2.add(3);
        table2.add(3);
        table2.add(12);
        table2.add(13);
        table2.add(13);

        table1.addAll(table2);

        Assert.assertEquals(2, table1.getFrequency(table1.getBinId(1)));
        Assert.assertEquals(3, table1.getFrequency(table1.getBinId(2)));
        Assert.assertEquals(5, table1.getFrequency(table1.getBinId(3)));
        Assert.assertEquals(1, table1.getFrequency(table1.getBinId(12)));
        Assert.assertEquals(2, table1.getFrequency(table1.getBinId(13)));

        Assert.assertEquals(13, table1.getTotal());
    }

    @Test
    public void testGetBinCenter() throws Exception {

        FrequencyTable table = new FrequencyTable(1);

        Assert.assertEquals(-25.5, table.getCenter(-25), 0.0000001);
        Assert.assertEquals(-25, table.getBinId(-25.5));

        Assert.assertEquals(25.5, table.getCenter(25), 0.0000001);
        Assert.assertEquals(25, table.getBinId(25.5));
    }
}
