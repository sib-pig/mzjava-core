package org.expasy.mzjava.stats.statscores;

import cern.jet.random.Normal;
import cern.jet.random.Uniform;
import cern.jet.random.engine.MersenneTwister;
import gnu.trove.list.TDoubleList;
import gnu.trove.list.TFloatList;
import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TFloatArrayList;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;


/**
 * @author Markus Muller
 * @version 0.0
 */
public class FitDecoyScoreStatisticsTest {

    private class TargetDensityDistribution
    {
        double trueMean;
        double trueStd;
        double rdMean;
        double rdStd;
        double randomClassProb;
        private NormalDistribution trueDist = null;
        private NormalDistribution rdDist = null;

        TargetDensityDistribution(double trueMean, double trueStd, double rdMean, double rdStd, double randomClassProb)
        {
            this.trueMean = trueMean;
            this.trueStd = trueStd;
            this.rdMean = rdMean;
            this.rdStd= rdStd;
            this.randomClassProb = randomClassProb;
            trueDist = new NormalDistribution(trueMean,trueStd);
            rdDist = new NormalDistribution(rdMean,rdStd);
        }


        public double qValue(double score) {
            double pValue = 1.0-rdDist.cumulativeProbability(score);
            double sensitivity = 1.0-trueDist.cumulativeProbability(score);

            if (Math.abs(sensitivity)<0.000001 && Math.abs(pValue)<0.000001) return 1.0;

            return pValue*randomClassProb/((1.0-randomClassProb)*sensitivity+pValue*randomClassProb);
        }
    }

    TargetDensityDistribution targetDistribution;

    int nrTargetScores = 2000;
    int nrDecoyScores = 4000;
    double randomClassProb = 0.8;
    double trueMean = 5.0;
    double trueStd = 1.0;
    double rdMean = 0.0;
    double rdStd = 1.0;

    MersenneTwister rd = new MersenneTwister();
    Normal trueHitGen = new Normal(trueMean,trueStd,rd);
    Normal decoyHitGen = new Normal(rdMean,rdStd,rd);
    Uniform classProbGen = new Uniform(0.0,1.0,rd);

    @Test
    public void test_constructor_float() {
        float[] targetScores = new float[nrTargetScores];
        float[] decoyScores = new float[nrDecoyScores];

        for (int i=0;i<nrTargetScores;i++) {
            targetScores[i] = (float)trueHitGen.nextDouble();
        }

        for (int i=0;i<nrDecoyScores;i++) {
            decoyScores[i] = (float)decoyHitGen.nextDouble();
        }


        FitDecoyScoreStatistics statCalculator1 = new FitDecoyScoreStatistics(targetScores,decoyScores);
        FitDecoyScoreStatistics statCalculator2 = new FitDecoyScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.SORT_IF_UNSORTED);
        FitDecoyScoreStatistics statCalculator3 = new FitDecoyScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DO_SORT);

        Arrays.sort(targetScores);
        Arrays.sort(decoyScores);
        FitDecoyScoreStatistics statCalculator4 = new FitDecoyScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DONT_SORT);


        float score1 = statCalculator1.getScoreForQValue(0.1f);
        float score2 = statCalculator2.getScoreForQValue(0.1f);
        float score3 = statCalculator3.getScoreForQValue(0.1f);
        float score4 = statCalculator4.getScoreForQValue(0.1f);

        Assert.assertEquals(score1,score2,0.1f);
        Assert.assertEquals(score1, score3, 0.1f);
        Assert.assertEquals(score1,score4,0.1f);
    }

    @Test
    public void test_constructor_double() {
        double[] targetScores = new double[nrTargetScores];
        double[] decoyScores = new double[nrDecoyScores];

        for (int i=0;i<nrTargetScores;i++) {
            targetScores[i] = trueHitGen.nextDouble();
        }

        for (int i=0;i<nrDecoyScores;i++) {
            decoyScores[i] = decoyHitGen.nextDouble();
        }


        FitDecoyScoreStatistics statCalculator1 = new FitDecoyScoreStatistics(targetScores,decoyScores);
        FitDecoyScoreStatistics statCalculator2 = new FitDecoyScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.SORT_IF_UNSORTED);
        FitDecoyScoreStatistics statCalculator3 = new FitDecoyScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DO_SORT);

        Arrays.sort(targetScores);
        Arrays.sort(decoyScores);
        FitDecoyScoreStatistics statCalculator4 = new FitDecoyScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DONT_SORT);


        float score1 = statCalculator1.getScoreForQValue(0.1f);
        float score2 = statCalculator2.getScoreForQValue(0.1f);
        float score3 = statCalculator3.getScoreForQValue(0.1f);
        float score4 = statCalculator4.getScoreForQValue(0.1f);

        Assert.assertEquals(score1,score2,0.1f);
        Assert.assertEquals(score1, score3, 0.1f);
        Assert.assertEquals(score1,score4,0.1f);
    }

    @Test
    public void test_constructor_TFloatList() {
        TFloatList targetScores = new TFloatArrayList();
        TFloatList decoyScores = new TFloatArrayList();

        for (int i=0;i<nrTargetScores;i++) {
            targetScores.add((float) trueHitGen.nextDouble());
        }

        for (int i=0;i<nrDecoyScores;i++) {
            decoyScores.add((float)decoyHitGen.nextDouble());
        }


        FitDecoyScoreStatistics statCalculator1 = new FitDecoyScoreStatistics(targetScores,decoyScores);
        FitDecoyScoreStatistics statCalculator2 = new FitDecoyScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.SORT_IF_UNSORTED);
        FitDecoyScoreStatistics statCalculator3 = new FitDecoyScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DO_SORT);

        targetScores.sort();
        decoyScores.sort();

        FitDecoyScoreStatistics statCalculator4 = new FitDecoyScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DONT_SORT);


        float score1 = statCalculator1.getScoreForQValue(0.1f);
        float score2 = statCalculator2.getScoreForQValue(0.1f);
        float score3 = statCalculator3.getScoreForQValue(0.1f);
        float score4 = statCalculator4.getScoreForQValue(0.1f);

        Assert.assertEquals(score1,score2,0.1f);
        Assert.assertEquals(score1, score3, 0.1f);
        Assert.assertEquals(score1,score4,0.1f);
    }

    @Test
    public void test_constructor_TDoubleList() {
        TDoubleList targetScores = new TDoubleArrayList();
        TDoubleList decoyScores = new TDoubleArrayList();

        for (int i=0;i<nrTargetScores;i++) {
            targetScores.add(trueHitGen.nextDouble());
        }

        for (int i=0;i<nrDecoyScores;i++) {
            decoyScores.add(decoyHitGen.nextDouble());
        }


        FitDecoyScoreStatistics statCalculator1 = new FitDecoyScoreStatistics(targetScores,decoyScores);
        FitDecoyScoreStatistics statCalculator2 = new FitDecoyScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.SORT_IF_UNSORTED);
        FitDecoyScoreStatistics statCalculator3 = new FitDecoyScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DO_SORT);

        targetScores.sort();
        decoyScores.sort();

        FitDecoyScoreStatistics statCalculator4 = new FitDecoyScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DONT_SORT);


        float score1 = statCalculator1.getScoreForQValue(0.1f);
        float score2 = statCalculator2.getScoreForQValue(0.1f);
        float score3 = statCalculator3.getScoreForQValue(0.1f);
        float score4 = statCalculator4.getScoreForQValue(0.1f);

        Assert.assertEquals(score1,score2,0.1f);
        Assert.assertEquals(score1, score3, 0.1f);
        Assert.assertEquals(score1,score4,0.1f);
    }

    @Test
    public void test_target_equals_decoy() {
        float[] targetScores = new float[nrTargetScores];
        float[] decoyScores = new float[nrDecoyScores];

        for (int i=0;i<nrTargetScores;i++) {
            targetScores[i] = (float)trueHitGen.nextDouble();
        }

        for (int i=0;i<nrDecoyScores;i++) {
            decoyScores[i] = (float)trueHitGen.nextDouble();
        }

        FitDecoyScoreStatistics statCalculator = new FitDecoyScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DO_SORT);
        Assert.assertEquals(0.5,statCalculator.getTargetDecoyRatio(),0.1);

        Assert.assertEquals(1f,statCalculator.getQValue(1f),0.1);
        Assert.assertEquals(1f,statCalculator.getQValue(2f),0.1);
        Assert.assertEquals(1f,statCalculator.getQValue(3f),0.1);
        Assert.assertEquals(1f,statCalculator.getQValue(4f),0.1);
        Assert.assertEquals(1f,statCalculator.getQValue(5f),0.1);
        Assert.assertEquals(1f,statCalculator.getQValue(6f),0.1);

    }


    @Test
    public void testQValue() throws Exception
    {
        int nrTargetScores = 2000;
        int nrDecoyScores = 3200;

        float[] targetScores = new float[nrTargetScores];
        float[] decoyScores = new float[nrDecoyScores];

        for (int i=0;i<nrTargetScores;i++) {
            double classProb = classProbGen.nextDouble();
            if (classProb<randomClassProb) {
                targetScores[i] = (float)decoyHitGen.nextDouble();
            } else {
                targetScores[i] = (float)trueHitGen.nextDouble();
            }
        }

        for (int i=0;i<nrDecoyScores;i++) {
            decoyScores[i] = (float)decoyHitGen.nextDouble();
        }

        FitDecoyScoreStatistics statCalculator = new FitDecoyScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DO_SORT);

        Assert.assertEquals(statCalculator.getTargetDecoyRatio(),0.5,0.1);

        targetDistribution = new TargetDensityDistribution(trueMean,trueStd,rdMean,rdStd,randomClassProb);

        for (int i=0;i<targetScores.length;i++){
            double trueQValue = targetDistribution.qValue(targetScores[i]);
            double estQValue = statCalculator.getQValue(targetScores[i]);

            Assert.assertEquals(trueQValue,estQValue,0.1);
        }

        float start = 0f;
        for (int i=0;i<20;i++) {
            float score = start + i*0.4f;

            float estQValue = statCalculator.getQValue(score);

            double estScore = statCalculator.getScoreForQValue(estQValue);
            if (estQValue>0)
                Assert.assertEquals(score,estScore,0.2);

        }
    }

}
