package org.expasy.mzjava.stats;

import org.apache.commons.math3.stat.StatUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;

public class HistogramImplTest {
	
	HistogramImpl histo;

	@Before
	public void setUp() throws Exception {
		
		double[] values =
		    new double[] {197.725, 198.053, 199.141, 711.524, 715.513, 738.915,
		        739.374, 743.954, 744.386, 751.116, 768.535, 771.675, 772.725,
		        780};
		double[] weights =
		    new double[] {96.0, 38.0, 34.0, 29.0, 48.0, 186.0, 81.0, 37.0,
		        88.0, 90.0, 462.0, 169.0, 70.0, 2.9};
		
		histo = new HistogramImpl(10., 700., 750.);
		
		histo.addData(values, weights);
	}

	@Test
	public void testEmptyHist() {

		histo = new HistogramImpl();
		Assert.assertTrue(histo.isEmpty());
	}

	@Test
	public void testEmptyHist2() {

		histo = histo.shallowCopy();
		Assert.assertTrue(histo.isEmpty());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBadInstanciationMinGtMax() {

		new HistogramImpl(10., 700., 355.);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBadInstanciationBinNumber0() {

		new HistogramImpl(0, 700., 750.);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBadInstanciationBinWidthNeg() {

		new HistogramImpl(-10., 700., 750.);
	}

	@Test
	public void testBinNumber() {

		Assert.assertEquals(5, histo.size());
	}

	@Test
	public void testNonEmptyBinNumber() {

		Assert.assertEquals(3, histo.getNonEmptyBinIndices().size());
	}

	@Test
	public void testGetMids() {

		double[] mids = histo.getMids(null);

		Assert.assertArrayEquals(new double[]{705.0, 715.0, 725.0, 735.0, 745.0}, mids, 0.1);
	}

	@Test
	public void testGetBreaks() {

		double[] breaks = histo.getBreaks(null);

		Assert.assertArrayEquals(new double[]{700.0, 710.0, 720.0, 730.0, 740.0, 750.0}, breaks, 0.1);
	}

    @Test
	public void testEmpty() {
        histo.clear();

        Assert.assertEquals(0, histo.getCumulFreqs(), 0.1);
	}

	@Test
	public void testCumulFreqs() {
		
		Assert.assertEquals(StatUtils.sum(new double[]{29.0, 48.0, 186.0,
                81.0, 37.0, 88.0}), histo.getCumulFreqs(), 0.01);
	}

	@Test
	public void testCumulFreqsAtInterval() {

		// 0.0, 77.0, 0.0, 267.0, 125.0
		Assert.assertEquals(344, histo.getCumulFreqs(1, 4), 0.01);
	}

	@Test
	public void testRelativeFreqAtInterval() {

		// 0.0, 77.0, 0.0, 267.0, 125.0
		Assert.assertEquals(344/469., histo.getRelativeBinFreq(1, 4), 0.01);
	}

    @Test
	public void testAbsoluteFreqValueAt() {
		
		Assert.assertEquals(77.0, histo.getAbsoluteBinFreq(1), 0.1);
	}

    @Test(expected = ArrayIndexOutOfBoundsException.class)
	public void testAbsoluteFreqValueAtOutOfBoundIndex() {

		Assert.assertEquals(77.0, histo.getAbsoluteBinFreq(100000), 0.1);
	}

	@Test
	public void testFreqs() {

		Set<Integer> indices = histo.getNonEmptyBinIndices();
		double[] bins = new double[] {0.0, 77.0, 0.0, 267.0, 125.0};

		for (int i : indices) {

			Assert.assertEquals(bins[i], histo.getAbsoluteBinFreq(i), 0.1);
		}
	}

	@Test
	public void testFreqs2() {

		Assert.assertArrayEquals(new double[] {0.0, 77.0, 0.0, 267.0, 125.0},
		    histo.getAbsoluteBinFreqs(null), 0.1);
	}

	@Test
	public void testRelativeFreqAt() {
		
		Assert.assertEquals(77.0 / 469, histo.getRelativeBinFreq(1), 0.1);
	}
	
	@Test
	public void testAltConstr1() {
		
		histo = HistogramImpl.valueOfBinWidthMinMax(5, 700, 750);
		
		Assert.assertEquals(10, histo.size());
		Assert.assertEquals(5.0, histo.getBinWidth(), 0.1);
	}
	
	@Test
	public void testAltConstr11() {
		
		histo = HistogramImpl.valueOfBinWidthMinMax(6, 700, 740);
		
		Assert.assertEquals(7, histo.size());
		Assert.assertEquals(6.0, histo.getBinWidth(), 0.1);
	}
	
	@Test
	public void testAltConstr2() {
		
		histo = HistogramImpl.valueOfBinNumWidthMin(5, 10, 700);
		
		Assert.assertEquals(5, histo.size());
		Assert.assertEquals(10.0, histo.getBinWidth(), 0.1);
	}

	@Test
	public void testBinMaxNormalisation() {

		Histogram hist = histo.normalize(new HistogramImpl.Normalization(Histogram.Normalization.NormType.BIN_MAX, 10000));
		Assert.assertEquals(10000, hist.getAbsoluteBinFreq(3), 0.1);
	}
	
	@Test
	public void testCumulNormalisation() {

        Histogram hist = histo.normalize(new HistogramImpl.Normalization(Histogram.Normalization.NormType.BINS_CUMUL, 10000));
		Assert.assertEquals(10000, hist.getCumulFreqs(), 0.1);
	}
	
	@Test
	public void testGetIndices() {
		
		int index;
		
		double[] vals =
		    new double[] {Double.MIN_VALUE, -23, 0, 699, 703, 709.99999999,
		        718, 723, 745, 756.3434, Double.MAX_VALUE};
		
		int[] goodIndices = new int[] {-1, -1, -1, -1, 0, 0, 1, 2, 4, -1, -1};
		
		for (int i = 0; i < vals.length; i++) {
			index = histo.getBinIndex(vals[i]);
			Assert.assertEquals(goodIndices[i], index);
		}
	}
	
	@Test
	public void testGetAbsoluteFreqs() {
		
		int index;
		
		double[] vals =
		    new double[] {Double.MIN_VALUE, -23, 0, 699, 703, 709.99999999,
		        718, 723, 745, 756.3434, Double.MAX_VALUE};
		
		double[] goodValues =
		    new double[] {0, 0, 0, 0, 0, 0, 77.0, 0, 125, 0, 0};
		
		for (int i = 0; i < vals.length; i++) {
			index = histo.getBinIndex(vals[i]);
			
			double value = histo.getAbsoluteBinFreq(index);
			
			Assert.assertEquals(goodValues[i], value, 0.01);
		}
	}

    @Test
    public void testAddBins() {

        Assert.assertArrayEquals(new double[]{0.0, 77.0, 0.0, 267.0, 125.0}, histo.getAbsoluteBinFreqs(null), 0.1);
        histo.addBins(histo.getAbsoluteBinFreqs(null));
        Assert.assertArrayEquals(new double[]{0.0, 154.0, 0.0, 534.0, 250.0}, histo.getAbsoluteBinFreqs(null), 0.1);
    }

    @Test (expected = NullPointerException.class)
    public void testAddNullBins() {

        histo.addBins(null);
    }

    @Test
    public void testCopy() {

	    Histogram copy = histo.copy();

        Assert.assertEquals(copy, histo);
    }

	@Test
	public void testRange() {

		histo = new HistogramImpl(10., 700., 755.);

		Assert.assertEquals(700., histo.getRange().lowerEndpoint(), 0.1);
		Assert.assertEquals(760., histo.getRange().upperEndpoint(), 0.1);
	}

	@Test
	public void testAddValuesAtBounds() {

		histo = new HistogramImpl(10., 700., 760.);

		boolean valueAdded = histo.addData(700.);

		Assert.assertTrue(valueAdded);

		valueAdded = histo.addData(760.);

		Assert.assertFalse(valueAdded);
	}
}
