package org.expasy.mzjava.stats.statscores;

import cern.jet.random.Normal;
import cern.jet.random.Uniform;
import cern.jet.random.engine.MersenneTwister;
import gnu.trove.list.TDoubleList;
import gnu.trove.list.TFloatList;
import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TFloatArrayList;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class FirstRankScoreStatisticsTest {

    private class TargetDensityDistribution
    {
        double trueMean;
        double trueStd;
        double rdMean;
        double rdStd;
        double randomClassProb;
        private NormalDistribution trueDist = null;
        private NormalDistribution rdDist = null;

        TargetDensityDistribution(double trueMean, double trueStd, double rdMean, double rdStd, double randomClassProb)
        {
            this.trueMean = trueMean;
            this.trueStd = trueStd;
            this.rdMean = rdMean;
            this.rdStd= rdStd;
            this.randomClassProb = randomClassProb;
            trueDist = new NormalDistribution(trueMean,trueStd);
            rdDist = new NormalDistribution(rdMean,rdStd);
        }


        public double qValue(double score) {
            double rdProb = rdDist.cumulativeProbability(score);
            double trueProb = 1.0-trueDist.cumulativeProbability(score);
            double pValue = 1.0-rdProb;
            double sensitivity = randomClassProb*pValue+(1.0-randomClassProb)*trueProb;

            if (Math.abs(sensitivity)<0.000001 && Math.abs(pValue)<0.000001) return 1.0;

            return 2*randomClassProb*pValue/(pValue+sensitivity);
        }
    }



    int nrTargetScores = 2003;
    int nrDecoyScores = 2000;
    double randomClassProb = 0.8;
    double trueMean = 5.0;
    double trueStd = 1.0;
    double rdMean = 0.0;
    double rdStd = 1.0;

    MersenneTwister rd = new MersenneTwister();
    Normal trueHitGen = new Normal(trueMean,trueStd,rd);
    Normal decoyHitGen = new Normal(rdMean,rdStd,rd);
    Uniform classProbGen = new Uniform(0.0,1.0,rd);

    TargetDensityDistribution targetDistribution = new TargetDensityDistribution(trueMean,trueStd,rdMean,rdStd,randomClassProb);

    float[] targetScores;
    float[] decoyScores;
    float minScore = 100;
    float maxScore= 0;
    float maxDecoyScore = 0;
    float minTrueScore = 100;


    @Test
    public void test_constructor_float() {
        float[] targetScores = new float[nrTargetScores];
        float[] decoyScores = new float[nrDecoyScores];

        for (int i=0;i<nrTargetScores;i++) {
            targetScores[i] = (float)trueHitGen.nextDouble();
        }

        for (int i=0;i<nrDecoyScores;i++) {
            decoyScores[i] = (float)decoyHitGen.nextDouble();
        }


        FirstRankScoreStatistics statCalculator1 = new FirstRankScoreStatistics(targetScores,decoyScores);
        FirstRankScoreStatistics statCalculator2 = new FirstRankScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.SORT_IF_UNSORTED);
        FirstRankScoreStatistics statCalculator3 = new FirstRankScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DO_SORT);

        Arrays.sort(targetScores);
        Arrays.sort(decoyScores);
        FirstRankScoreStatistics statCalculator4 = new FirstRankScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DONT_SORT);


        float score1 = statCalculator1.getScoreForQValue(0.1f);
        float score2 = statCalculator2.getScoreForQValue(0.1f);
        float score3 = statCalculator3.getScoreForQValue(0.1f);
        float score4 = statCalculator4.getScoreForQValue(0.1f);

        Assert.assertEquals(score1, score2, 0.1f);
        Assert.assertEquals(score1, score3, 0.1f);
        Assert.assertEquals(score1, score4, 0.1f);
    }

    @Test
    public void test_constructor_double() {
        double[] targetScores = new double[nrTargetScores];
        double[] decoyScores = new double[nrDecoyScores];

        for (int i=0;i<nrTargetScores;i++) {
            targetScores[i] = trueHitGen.nextDouble();
        }

        for (int i=0;i<nrDecoyScores;i++) {
            decoyScores[i] = decoyHitGen.nextDouble();
        }


        FirstRankScoreStatistics statCalculator1 = new FirstRankScoreStatistics(targetScores,decoyScores);
        FirstRankScoreStatistics statCalculator2 = new FirstRankScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.SORT_IF_UNSORTED);
        FirstRankScoreStatistics statCalculator3 = new FirstRankScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DO_SORT);

        Arrays.sort(targetScores);
        Arrays.sort(decoyScores);
        FirstRankScoreStatistics statCalculator4 = new FirstRankScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DONT_SORT);


        float score1 = statCalculator1.getScoreForQValue(0.1f);
        float score2 = statCalculator2.getScoreForQValue(0.1f);
        float score3 = statCalculator3.getScoreForQValue(0.1f);
        float score4 = statCalculator4.getScoreForQValue(0.1f);

        Assert.assertEquals(score1,score2, 0.1f);
        Assert.assertEquals(score1,score3, 0.1f);
        Assert.assertEquals(score1,score4, 0.1f);
    }

    @Test
    public void test_constructor_TFloatList() {
        TFloatList targetScores = new TFloatArrayList();
        TFloatList decoyScores = new TFloatArrayList();

        for (int i=0;i<nrTargetScores;i++) {
            targetScores.add((float) trueHitGen.nextDouble());
        }

        for (int i=0;i<nrDecoyScores;i++) {
            decoyScores.add((float)decoyHitGen.nextDouble());
        }


        FirstRankScoreStatistics statCalculator1 = new FirstRankScoreStatistics(targetScores,decoyScores);
        FirstRankScoreStatistics statCalculator2 = new FirstRankScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.SORT_IF_UNSORTED);
        FirstRankScoreStatistics statCalculator3 = new FirstRankScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DO_SORT);

        targetScores.sort();
        decoyScores.sort();

        FirstRankScoreStatistics statCalculator4 = new FirstRankScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DONT_SORT);


        float score1 = statCalculator1.getScoreForQValue(0.1f);
        float score2 = statCalculator2.getScoreForQValue(0.1f);
        float score3 = statCalculator3.getScoreForQValue(0.1f);
        float score4 = statCalculator4.getScoreForQValue(0.1f);

        Assert.assertEquals(score1, score2, 0.1f);
        Assert.assertEquals(score1, score3, 0.1f);
        Assert.assertEquals(score1, score4, 0.1f);
    }

    @Test
    public void test_constructor_TDoubleList() {
        TDoubleList targetScores = new TDoubleArrayList();
        TDoubleList decoyScores = new TDoubleArrayList();

        for (int i=0;i<nrTargetScores;i++) {
            targetScores.add(trueHitGen.nextDouble());
        }

        for (int i=0;i<nrDecoyScores;i++) {
            decoyScores.add(decoyHitGen.nextDouble());
        }


        FirstRankScoreStatistics statCalculator1 = new FirstRankScoreStatistics(targetScores,decoyScores);
        FirstRankScoreStatistics statCalculator2 = new FirstRankScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.SORT_IF_UNSORTED);
        FirstRankScoreStatistics statCalculator3 = new FirstRankScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DO_SORT);

        targetScores.sort();
        decoyScores.sort();

        FirstRankScoreStatistics statCalculator4 = new FirstRankScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DONT_SORT);


        float score1 = statCalculator1.getScoreForQValue(0.1f);
        float score2 = statCalculator2.getScoreForQValue(0.1f);
        float score3 = statCalculator3.getScoreForQValue(0.1f);
        float score4 = statCalculator4.getScoreForQValue(0.1f);

        Assert.assertEquals(score1, score2, 0.1f);
        Assert.assertEquals(score1, score3, 0.1f);
        Assert.assertEquals(score1, score4, 0.1f);
    }

    @Test
    public void test_target_equals_decoy() {
        float[] targetScores = new float[nrTargetScores];
        float[] decoyScores = new float[nrDecoyScores];

        for (int i=0;i<nrTargetScores;i++) {
            targetScores[i] = (float)trueHitGen.nextDouble();
        }

        for (int i=0;i<nrDecoyScores;i++) {
            decoyScores[i] = (float)trueHitGen.nextDouble();
        }

        FirstRankScoreStatistics statCalculator = new FirstRankScoreStatistics(targetScores,decoyScores, ScoreStatistics.SortAction.DO_SORT);

        Assert.assertEquals(1f,statCalculator.getQValue(1f), 0.1);
        Assert.assertEquals(1f,statCalculator.getQValue(2f),0.1);
        Assert.assertEquals(1f,statCalculator.getQValue(3f),0.1);
        Assert.assertEquals(1f,statCalculator.getQValue(4f), 0.1);
        Assert.assertEquals(1f,statCalculator.getQValue(5f),0.1);
        Assert.assertEquals(1f,statCalculator.getQValue(6f),0.1);

    }

    private void setValues() {
        // this simulates db search where only highest scoring hits are considered
        int nrDecoyHits = 0;
        int nrTargetHits = 0;

        targetScores = new float[10000];
        decoyScores = new float[10000];

        minScore = 100;
        maxScore= 0;
        maxDecoyScore = 0;
        minTrueScore = 100;
        for (int i = 0; i < 10000; i++) {
            float targetScore;
            float decoyScore;
            double classProb = classProbGen.nextDouble();

            if (classProb <= randomClassProb) {
                targetScore = (float)decoyHitGen.nextDouble();
            } else {
                targetScore = (float)trueHitGen.nextDouble();
            }

            decoyScore = (float)decoyHitGen.nextDouble();

            if (decoyScore > targetScore) {
                decoyScores[nrDecoyHits] = decoyScore;
                if (decoyScore<minScore) minScore = decoyScore;
                if (decoyScore>maxScore) maxScore = decoyScore;
                if (decoyScore>maxDecoyScore) maxDecoyScore = decoyScore;
                nrDecoyHits++;
            } else {
                targetScores[nrTargetHits] = targetScore;
                nrTargetHits++;
                if (targetScore<minScore) minScore = targetScore;
                if (targetScore>maxScore) maxScore = targetScore;
                if (targetScore< minTrueScore) minTrueScore = targetScore;
            }
        }


        targetScores = Arrays.copyOf(targetScores, nrTargetHits);
        decoyScores = Arrays.copyOf(decoyScores, nrDecoyHits);

    }

    @Test
    public void test_getScoreForQValue() {

        setValues();

        FirstRankScoreStatistics statCalculator = new FirstRankScoreStatistics(targetScores, decoyScores, ScoreStatistics.SortAction.DO_SORT);


        float qValue = statCalculator.getQValue(minScore-1);
        Assert.assertEquals(0.8,qValue,0.1f);

        qValue = statCalculator.getQValue(maxScore+1);
        Assert.assertEquals(0,qValue,0.1f);

        qValue = statCalculator.getQValue(minTrueScore -1);
        Assert.assertEquals(0.8f,qValue,0.1f);

        qValue = statCalculator.getQValue(maxDecoyScore+0.01f);
        Assert.assertEquals(0,qValue,0.1f);

        float step = 0.0065437f;
        float prev = statCalculator.getQValue(0);
        for(int i=0;i<1000;i++) {
            qValue = statCalculator.getQValue(i*step);
            Assert.assertTrue(prev>=qValue);
            prev = qValue;
        }
    }

    @Test
    public void test_getScoreWithQValue()  {
        setValues();

        FirstRankScoreStatistics statCalculator = new FirstRankScoreStatistics(targetScores, decoyScores, ScoreStatistics.SortAction.DO_SORT);

        float score = statCalculator.getScoreForQValue(0.1f);
        float qValue = statCalculator.getQValue(score);

        Assert.assertEquals(0.1,qValue,0.005);
    }

    @Test
    public void test_getNrTargetScores() {
        setValues();

        FirstRankScoreStatistics statCalculator = new FirstRankScoreStatistics(targetScores, decoyScores, ScoreStatistics.SortAction.DO_SORT);

        int cnt = statCalculator.getNrTargetScores(minScore - 1);
        Assert.assertEquals(targetScores.length,cnt);

        cnt = statCalculator.getNrDecoyScores(maxScore + 1);
        Assert.assertEquals(0,cnt);

        cnt = statCalculator.getNrDecoyScores(minScore-1);
        Assert.assertEquals(decoyScores.length,cnt);

        cnt = statCalculator.getNrDecoyScores(maxScore + 1);
        Assert.assertEquals(0,cnt);

        cnt = statCalculator.getNrDecoyScores(maxDecoyScore + 0.01f);
        Assert.assertEquals(0,cnt);

    }

    @Test
    public void test_qValues() {
        setValues();

        FirstRankScoreStatistics statCalculator = new FirstRankScoreStatistics(targetScores, decoyScores, ScoreStatistics.SortAction.DO_SORT);

        double trueQValue = targetDistribution.qValue(-10.0);
        double estQValue = statCalculator.getQValue(-10);

        Assert.assertEquals(0.8,trueQValue,0.1);
        Assert.assertEquals(0.8,estQValue,0.1);

        for (int i=0;i<targetScores.length;i++){
            trueQValue = targetDistribution.qValue(targetScores[i]);
            estQValue = statCalculator.getQValue(targetScores[i]);

            Assert.assertEquals(trueQValue,estQValue,0.1);
        }

    }
}